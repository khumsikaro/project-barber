var express = require('express');
var router = express.Router();
var Deletepoint = require('../../../Services/Setting/setting_point/Deletepoint');

router.delete('/:id', function (req, res, next) {
    var data = req.params.id
    Deletepoint.Deletepoint(data ,(error,rows)=>{
        if(error){
            res.send({result:'error'})
        }
        else{
            res.send({result:'success'})
        }
    })

})
module.exports = router;