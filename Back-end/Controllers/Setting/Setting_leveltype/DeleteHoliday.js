var express = require('express');
var router = express.Router();
var DeleteHoliday = require('../../../Services/Setting/setting_leveltype/DeleteHoliday');

router.delete('/:leave_type_id', function (req, res, next) {
    var data = req.params.leave_type_id
    DeleteHoliday.DeleteHoliday(data ,(error,rows)=>{
        if(error){
            res.send({result:'error'})
        }
        else{
            res.send({result:'success'})
        }
    })

})
module.exports = router;