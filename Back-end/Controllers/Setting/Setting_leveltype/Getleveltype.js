var express = require('express');
var router = express.Router();
var Controller = require('../../../Services/Setting/setting_leveltype/Getleveltype');
router.get('/', function (req, res, next) {
    var data = req.params
    Controller.Getleveltype(data, function (err, row) {

        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;