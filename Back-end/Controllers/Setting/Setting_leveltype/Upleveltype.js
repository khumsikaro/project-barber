var express = require('express');
var router = express.Router();
var Upleveltype = require('../../../Services/Setting/setting_leveltype/Upleveltype');

router.put('/', function (req, res, next) {
    var data = req.body
    Upleveltype.Upleveltype(data, (error, rows) => {
        if (error) {
            res.send({ result: 'error' })
        }
        else {
            res.send({ result: 'success' })
        }
    })

})
module.exports = router;