var express = require('express');
var router = express.Router();
var uploadfile = require('../../Services/Information/uploadfile');

router.patch('/:news_id', function (req, res, next) {
    var body = req.body
    var params = req.params
    uploadfile.uploadfile(body,params, (error, rows) => {
        if (error) {
            res.send({ result: 'error' })
        }
        else {
            res.send({ result: 'success' })
        }
    })

})
module.exports = router;