var express = require('express');
var router = express.Router();
var UpdateInformation = require('../../Services/Information/Updatenews');

router.put('/', function (req, res, next) {
    var data = req.body
    UpdateInformation.UpdateInformation(data, (error, rows) => {
        if (error) {
            res.send({ result: 'error' })
        }
        else {
            res.send({ result: 'success' })
        }
    })

})
module.exports = router;