var express = require('express');
var router = express.Router();
var Controller = require('../../../Services/Information/News/News');
router.get('/', function (req, res, next) {
    var data = req.params
    Controller.News(data, function (err, row) {

        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;