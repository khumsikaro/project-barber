var express = require('express');
var router = express.Router();
var Controller = require('../../Services/Information/search');
router.get('/', function (req, res, next) {
    let data = req.query;
    Controller.SearchInformation(data, function (err, row) {

        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;