var express = require('express');
var router = express.Router();
var DeleteInformation = require('../../Services/Information/Deletenew');

router.delete('/:news_id', function (req, res, next) {
    var data = req.params.news_id
    DeleteInformation.DeleteInformation(data ,(error,rows)=>{
        if(error){
            res.send({result:'error'})
        }
        else{
            res.send({result:'success'})
        }
    })

})
module.exports = router;