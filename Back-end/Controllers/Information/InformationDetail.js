var express = require('express');
var router = express.Router();
var Controller = require('../../Services/Information/InfotmationDetail');

router.get('/:news_id', (req, res, next) => {
    var news_id = req.params.news_id
    Controller.InformationDetail(news_id, (err, row) => {
        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;