var express = require('express');
var router = express.Router();
var Controller = require('../../Services/Information/getlastid');
router.get('/', function (req, res, next) {
 var params = req.param
    Controller.getlastid(params, function (err, row) {

        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;