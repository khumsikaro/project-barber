var express = require('express');
var router = express.Router();
var UpdateBranch = require('../../Services/Branch/UpdateBranch');

router.put('/', function (req, res, next) {
    var data = req.body
    UpdateBranch.UpdateBranch(data, (error, rows) => {
        if (error) {
            res.send({ result: 'error' })
        }
        else {
            res.send({ result: 'success' })
        }
    })

})
module.exports = router;