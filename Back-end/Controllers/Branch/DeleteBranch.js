var express = require('express');
var router = express.Router();
var DeleteBranch = require('../../Services/Branch/DeleteBranch');

router.delete('/:branch_id', function (req, res, next) {
    var data = req.params.branch_id
    DeleteBranch.DeleteBranch(data ,(error,rows)=>{
        if(error){
            res.send({result:'error'})
        }
        else{
            res.send({result:'success'})
        }
    })

})
module.exports = router;