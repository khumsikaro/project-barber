var express = require('express');
var router = express.Router();
var Controller = require('../../Services/Branch/Getprovince');
router.get('/', function (req, res, next) {
    var data = req.params
    Controller.Getprovince (data, function (err, row) {

        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;