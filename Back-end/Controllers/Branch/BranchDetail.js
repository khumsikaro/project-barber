var express = require('express');
var router = express.Router();
var Controller = require('../../Services/Branch/BranchDetail');

router.get('/:branch_id', (req, res, next) => {
    var branch_id = req.params.branch_id
    Controller.BranchDetail(branch_id, (err, row) => {
        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;