var express = require('express');
var router = express.Router();
var UpfileBranch = require('../../Services/Branch/Upfile');

router.patch('/:branch_id', function (req, res, next) {
    var body = req.body
    var params = req.params
    UpfileBranch.UpfileBranch(body,params, (error, rows) => {
        if (error) {
            res.send({ result: 'error' })
        }
        else {
            res.send({ result: 'success' })
        }
    })

})
module.exports = router;