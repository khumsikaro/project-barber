var express = require('express');
var router = express.Router();
var Controller = require('../../Services/Branch/GetZipcode');
router.get('/:district_id', function (req, res, next) {
    var data = req.params.district_id
    Controller.GetZipcode(data, function (err, row) {

        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;