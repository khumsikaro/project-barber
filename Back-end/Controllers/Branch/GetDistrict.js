var express = require('express');
var router = express.Router();
var Controller = require('../../Services/Branch/GetDistrict');
router.get('/:amphur_id', function (req, res, next) {
    var data = req.params.amphur_id
    Controller.GetDistrict(data, function (err, row) {

        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;