var express = require('express');
var router = express.Router();
var Controller = require('../../Services/Branch/GetAmphur');
router.get('/:province_id', function (req, res, next) {
    var data = req.params.province_id
    Controller.GetAmphur(data, function (err, row) {

        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;