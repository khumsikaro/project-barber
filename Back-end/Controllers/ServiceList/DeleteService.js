var express = require('express');
var router = express.Router();
var DeleteService = require('../../Services/ServiceList/DeleteService');

router.delete('/:name', function (req, res, next) {
    var data = req.params.name
    DeleteService.DeleteService(data ,(error,rows)=>{
        if(error){
            res.send({result:'error'})
        }
        else{
            res.send({result:'success'})
        }
    })

})
module.exports = router;