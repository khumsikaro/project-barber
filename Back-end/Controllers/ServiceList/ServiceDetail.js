var express = require('express');
var router = express.Router();
var Controller = require('../../Services/ServiceList/ServiceDetail');

router.get('/:name', (req, res, next) => {
    var name = req.params.name
    Controller.ServiceDetail(name, (err, row) => {
        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;