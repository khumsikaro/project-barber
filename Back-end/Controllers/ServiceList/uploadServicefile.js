var express = require('express');
var router = express.Router();
var uploadServicefile = require('../../Services/ServiceList/uploadServicefile');

router.patch('/:name', function (req, res, next) {
    var body = req.body
    var params = req.params
    uploadServicefile.uploadServicefile(body,params, (error, rows) => {
        if (error) {
            res.send({ result: 'error' })
        }
        else {
            res.send({ result: 'success' })
        }
    })

})
module.exports = router;