var express = require('express');
var router = express.Router();
var Controller = require('../../Services/ServiceList/GetService');
router.get('/', function (req, res, next) {
    var data = req.params
    Controller.GetService(data, function (err, row) {

        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;