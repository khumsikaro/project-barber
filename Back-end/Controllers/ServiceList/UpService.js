var express = require('express');
var router = express.Router();
var UpdateService = require('../../Services/ServiceList/UpService');

router.put('/', function (req, res, next) {
    var data = req.body
    UpdateService.UpdateService(data, (error, rows) => {
        if (error) {
            res.send({ result: 'error' })
        }
        else {
            res.send({ result: 'success' })
        }
    })

})
module.exports = router;