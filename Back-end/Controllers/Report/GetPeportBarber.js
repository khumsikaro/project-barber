var express = require('express');
var router = express.Router();
var Controllers = require('../../Services/Report/GetPeportBarber');
router.get('/:date/:branch_id/:username', function (req, res, next) {
    const data = {
        date : req.params.date,
        branch_id : req.params.branch_id,
        username : req.params.username
    }
    Controllers.GetPortBarber(data, function (err, row) {
        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;