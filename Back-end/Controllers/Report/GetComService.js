var express = require('express');
var router = express.Router();
var GetComService = require('../../Services/Report/GetComService');
router.get('/:date/:username', function (req, res, next) {
    const data = {
        date : req.params.date,
        username : req.params.username
    }
    GetComService.GetComService(data, function (err, row) {

        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;