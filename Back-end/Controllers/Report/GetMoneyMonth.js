var express = require('express');
var router = express.Router();
var Controllers = require('../../Services/Report/GetMoneyMonth');
router.get('/:date', function (req, res, next) {
    let date = req.params.date;
    Controllers.MoneyMonth(date, function (err, row) {

        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;