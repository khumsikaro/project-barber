var express = require('express');
var router = express.Router();
var feedback = require('../../../../Services/Report/feedback/Branch/branch');
router.get('/:date1/:date2', function (req, res, next) {
    let date1 = req.params.date1;
    let date2 = req.params.date2;
    feedback.feedback(date1,date2, function (err, row) {
        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;