var express = require('express');
var router = express.Router();
var feedback = require('../../../../Services/Report/feedback/Barber/BarberDetail');
router.get('/:date1/:date2/:barber_id/:branch_id', function (req, res, next) {
    let date1 = req.params.date1;
    let date2 = req.params.date2;
    let barber_id = req.params.barber_id;
    let branch_id = req.params.branch_id;
    feedback.feedbackDetailBarber(date1,date2,barber_id,branch_id, function (err, row) {
        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;