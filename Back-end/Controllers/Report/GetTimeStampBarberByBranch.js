var express = require('express');
var router = express.Router();
var GetTimeStamp = require('../../Services/Report/GetTimeStampBarberByBranch');
router.get('/:date/:branch_id', function (req, res, next) {
    const data = {
        date : req.params.date,
        branch_id : req.params.branch_id
    }
    GetTimeStamp.GetTimeStamp(data, function (err, row) {
        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;