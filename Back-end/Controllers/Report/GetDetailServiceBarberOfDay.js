var express = require('express');
var router = express.Router();
var report = require('../../Services/Report/GetDetailServiceBarberOfDay');
router.get('/:date/:username', function (req, res, next) {
    let date = req.params.date;
    let username = req.params.username;
    report.report(date,username, function (err, row) {

        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;