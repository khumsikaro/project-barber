var express = require('express');
var router = express.Router();
var UpdateStatusLogin = require('../../Services/login/UPStatusLogin');

router.put('/', function (req, res, next) {
    var data = req.body
    UpdateStatusLogin.UpdateStatusLogin(data, (error, rows) => {
        if (error) {
            res.send({ result: 'error' })
            
        }
        else {
            res.send({ result: 'success' })
        }
    })

})
module.exports = router;