var express = require('express');
var router = express.Router();
var Login = require('../../Services/login/login');

router.post('/', function (req, res) {
    var data = req.body
    Login.Login(data, function (err, row) {
        if (err) {
            res.json(err);
        }
        else if (row.length <= 0) {
            res.send({result:'รหัสผิด'})
        }
        else {
            res.json(row[0])
        }
    })
});
module.exports = router;