var express = require('express');
var router = express.Router();
var HistoryList = require('../../../Services/Users/HistroryListUser/HistoryListUser');
router.get('/:customer_id', function (req, res, next) {
    let customer_id = req.params.customer_id;

    HistoryList.HistoryList(customer_id, function (err, row) {
        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;