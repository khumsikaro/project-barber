var express = require('express');
var router = express.Router();
var HistroryListDetail = require('../../../Services/Users/HistroryListUser/HistroryListUserDetail');
router.get('/:queue_id', function (req, res, next) {
    let queue_id = req.params.queue_id;
    HistroryListDetail.HistroryListDetail(queue_id, function (err, row) {

        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;