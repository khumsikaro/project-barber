var express = require('express');
var router = express.Router();
var GetPassword = require('../../Services/Users/Getpass');

router.get('/:username', (req, res, next) => {
    var username = req.params.username
    GetPassword.GetPassword(username, (err, row) => {
        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;