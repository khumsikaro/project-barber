var express = require('express');
var router = express.Router();
var Detailskills = require('../../Services/Users/ShowSkills');
router.get('/:username', (req, res, next) => {
    var username = req.params.username
    Detailskills.Detailskills(username, (err, row) => {
        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;