var express = require('express');
var router = express.Router();
var Controller = require('../../../Services/Users/StatusQueueUser/Search');
router.get('/', function (req, res, next) {
    let data = req.query;
    
    Controller.Search(data, function (err, row) {

        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;