var express = require('express');
var router = express.Router();
var EditCancelQueue = require('../../../Services/Users/StatusQueueUser/CancelQueue');

router.put('/', function (req, res, next) {
    var data = req.body
    EditCancelQueue.EditCancelQueue(data, (error, rows) => {
        if (error) {
            res.send({ result: 'error' })
            
        }
        else {
            res.send({ result: 'success' })
        }
    })

})
module.exports = router;