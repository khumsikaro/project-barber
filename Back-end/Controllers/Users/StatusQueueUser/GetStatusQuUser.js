var express = require('express');
var router = express.Router();
var Controller = require('../../../Services/Users/StatusQueueUser/GetStatusQuUser');

router.get('/:username/:date', (req, res, next) => {
    var username = req.params.username
    var date =  req.params.date
    
    
    Controller.Getstatus(username,date, (err, row) => {
        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;