var express = require('express');
var router = express.Router();
var PointDetailUser = require('../../Services/Users/GetPointDetailUser');
router.get('/:username', function  (req, res, next)  {
    var username = req.params.username
    PointDetailUser.PointDetailUser(username, (err, row) => {

        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;