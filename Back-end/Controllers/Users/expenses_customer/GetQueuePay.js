var express = require('express');
var router = express.Router();
var GetQueuePay = require('../../../Services/Users/expenses_customer/GetQueuePay');

router.get('/:branchid', (req, res, next) => {
    var branchid = req.params.branchid
    GetQueuePay.GetQueuePay(branchid, (err, row) => {
        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;