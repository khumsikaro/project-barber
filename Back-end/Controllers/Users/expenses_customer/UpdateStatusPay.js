var express = require('express');
var router = express.Router();
var Controllers = require('../../../Services/Users/expenses_customer/UpdateStatusPay');

router.put('/', function (req, res, next) {
    var data = req.body
    Controllers.UpdateStatusPay(data, (error, rows) => {
        if (error) {
            res.send({ result: 'error' })
            
        }
        else {
            res.send({ result: 'success' })
        }
    })

})
module.exports = router;