var express = require('express');
var router = express.Router();
var Controller = require('../../../Services/Users/expenses_customer/GetServiCharge');

router.get('/:service_name', (req, res, next) => {
    var service_name = req.params.service_name
    Controller.getServiceCharge(service_name, (err, row) => {
        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;