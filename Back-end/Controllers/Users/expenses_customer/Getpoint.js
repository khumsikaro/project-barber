var express = require('express');
var router = express.Router();
var GetQueuePay = require('../../../Services/Users/expenses_customer/Getpoint');

router.get('/:customer_id', (req, res, next) => {
    var customer_id = req.params.customer_id
    GetQueuePay.GetPoint(customer_id, (err, row) => {
        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;