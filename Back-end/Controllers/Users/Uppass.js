var express = require('express');
var router = express.Router();
var Editpassword = require('../../Services/Users/Uppass');

router.put('/', function (req, res, next) {
    var data = req.body
    Editpassword.Editpassword(data, (error, rows) => {
        if (error) {
            res.send({ result: 'error' })
            
        }
        else {
            res.send({ result: 'success' })
        }
    })

})
module.exports = router;