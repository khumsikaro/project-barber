var express = require('express');
var router = express.Router();
var Controller = require('../../Services/Users/SearchUsers');
router.get('/', function (req, res, next) {
    let data = req.query;
    Controller.SearchUsers(data, function (err, row) {

        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;