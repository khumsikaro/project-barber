var express = require('express');
var router = express.Router();
var UsersDetail = require('../../Services/Users/UsersDetail');
router.get('/:username', function  (req, res, next)  {
    var username = req.params.username
    UsersDetail.UsersDetail(username, (err, row) => {

        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;