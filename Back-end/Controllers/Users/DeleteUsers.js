var express = require('express');
var router = express.Router();
var DeleteUser = require('../../Services/Users/DeleteUsers');

router.delete('/:username', function (req, res, next) {
    var data = req.params.username
    DeleteUser.DeleteUser(data ,(error,rows)=>{
        if(error){
            res.send({result:'error'})
        }
        else{
            res.send({result:'success'})
        }
    })

})
module.exports = router;