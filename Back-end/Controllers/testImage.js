var express = require('express');
var router = express.Router();
var multer = require('multer');
var db = require('../Database/connectdb');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null,'Controllers/profileImage')
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname + '-' + Date.now() + '.jpg')

        var sql = `INSERT INTO test_image(image1)
                            VALUES('${file.originalname + '-' + Date.now() + '.jpg'}');`
        return db.query(sql);
    }
    
});


// var upload = multer({ storage: storage, }).single('profileImage');
router.post('/', function (req, res) {
    var upload = multer({ storage: storage }).array('profileImage');

    upload(req, res, function (err) {
        if (err) {
            // An error occurred when uploading
        }
        res.json({
            success: true,
            message: 'Image uploaded!'
        });

        // Everything went fine
    })

});
module.exports = router;