var express = require('express');
var router = express.Router();
var DeleteLeave = require('../../../Services/Barber/Bye_Barber/Delete');

router.delete('/:leave_id', function (req, res, next) {
    var data = req.params.leave_id
    DeleteLeave.Delete(data ,(error,rows)=>{
        if(error){
            res.send({result:'error'})
        }
        else{
            res.send({result:'success'})
        }
    })

})
module.exports = router;