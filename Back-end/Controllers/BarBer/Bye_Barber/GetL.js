var express = require('express');
var router = express.Router();
var Controller = require('../../../Services/Barber/Bye_Barber/Get_leare');
router.get('/:Branch', function (req, res, next) {
    var Branch = req.params.Branch
    Controller.Get(Branch, (err, row) => {
        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;