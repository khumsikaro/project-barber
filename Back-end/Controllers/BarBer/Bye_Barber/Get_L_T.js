var express = require('express');
var router = express.Router();
var Controller = require('../../../Services/Barber/Bye_Barber/Get_L _t');
router.get('/', function (req, res, next) {
 var params = req.param
    Controller.Get(params, function (err, row) {

        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;