
var express = require('express');
var router = express.Router();
var Controller = require('../../../../Services/Barber/Bye_Barber/HolidayPrivate/SearchHistoryCustomer');
router.get('/:username/:date', (req, res, next) => {
    var username = req.params.username
    var date =  req.params.date 
    Controller.Search(username,date, (err, row) => {
        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;