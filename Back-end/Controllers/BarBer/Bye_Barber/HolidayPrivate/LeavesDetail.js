var express = require('express');
var router = express.Router();
var Controller = require('../../../../Services/Barber/Bye_Barber/HolidayPrivate/LeavesDetail');
router.get('/:date/:username', function (req, res, next) {
    var date = req.params.date
    var username = req.params.username
    Controller.GetLeavesDetail(date,username, (err, row) => {
        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;