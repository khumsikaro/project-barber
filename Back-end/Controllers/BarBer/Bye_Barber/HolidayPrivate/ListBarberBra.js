var express = require('express');
var router = express.Router();
var GetCheck = require('../../../../Services/Barber/Bye_Barber/HolidayPrivate/ListBarberBra');
router.get('/:Branch', (req, res, next) => {
    var Branch = req.params.Branch
    GetCheck.Get(Branch, (err, row) => {
        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;