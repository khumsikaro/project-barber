var express = require('express');
var router = express.Router();
var GetHolidayBarber = require('../../../../Services/Barber/Bye_Barber/leavebarber/Get');
router.get('/',  (req, res, next) => {
    var data = req.query;
    GetHolidayBarber.GetHolidayBarber(data, (err, row) => {
        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;