var express = require('express');
var router = express.Router();
var Controller = require('../../../Services/Barber/expenses_bar/search');
router.get('/', function (req, res, next) {
    let data = req.query;
    Controller.SearchExpenses(data, function (err, row) {

        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;