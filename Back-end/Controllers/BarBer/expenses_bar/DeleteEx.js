var express = require('express');
var router = express.Router();
var DeleteExpenses = require('../../../Services/Barber/expenses_bar/DeleteEx');

router.delete('/:ex_bar_id', function (req, res, next) {
    var data = req.params.ex_bar_id
    DeleteExpenses.DeleteExpenses(data ,(error,rows)=>{
        if(error){
            res.send({result:'error'})
        }
        else{
            res.send({result:'success'})
        }
    })

})
module.exports = router;