var express = require('express');
var router = express.Router();
var UpDataExpenses = require('../../../Services/Barber/expenses_bar/UpDate');

router.put('/', function (req, res, next) {
    var data = req.body
    UpDataExpenses.UpDataExpenses(data, (error, rows) => {
        if (error) {
            res.send({ result: 'error' })
        }
        else {
            res.send({ result: 'success' })
        }
    })

})
module.exports = router;