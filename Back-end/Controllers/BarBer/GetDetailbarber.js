var express = require('express');
var router = express.Router();
var Controller = require('../../Services/Barber/GetDetailbarber');
router.get('/:username', function (req, res, next) {
    var username = req.params.username
    
    Controller.DetailBarber(username, function (err, row) {

        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;