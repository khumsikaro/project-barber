var express = require('express');
var router = express.Router();
var Controller = require('../../../Services/Barber/CheckQuePriBarber/Get');
router.get('/:username/:date', function (req, res, next) {
    var username = req.params.username
    var date = req.params.date
    
    Controller.CheckQuBar(username,date, function (err, row) {

        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;