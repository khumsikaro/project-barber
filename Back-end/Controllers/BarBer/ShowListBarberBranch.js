var express = require('express');
var router = express.Router();
var Controller = require('../../Services/Barber/ShowListBarberBranch');
router.get('/:branch', function (req, res, next) {
   var branch = req.params.branch
    Controller.ShowListBarberbranch(branch, function (err, row) {

        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;