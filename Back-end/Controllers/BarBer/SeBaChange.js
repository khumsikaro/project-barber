var express = require('express');
var router = express.Router();
var Controller = require('../../Services/Barber/SeBaChange');
router.get('/:serviceid/:branchid/:date/:timestart/:timestop', function (req, res, next) {
    var serviceid = req.params.serviceid
    var branchid = req.params.branchid
    var date = req.params.date
    var timestart = req.params.timestart
    var timestop = req.params.timestop
    Controller.SeBaChange(serviceid,branchid,date,timestart,timestop, function (err, row) {

        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;