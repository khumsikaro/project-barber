var express = require('express');
var router = express.Router();
var SelectServiceBefor = require('../../../../Services/Barber/ChangeBarber/GetBarberAsService/SelectServiceBefor');

router.get('/:branch/:serviceid', (req, res, next) => {
    var branch = req.params.branch
    var serviceid = req.params.serviceid
    SelectServiceBefor.SelectServiceBefor(branch,serviceid, (err, row) => {
        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;