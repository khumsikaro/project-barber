var express = require('express');
var router = express.Router();
var SelectBarberBefor = require('../../../../Services/Barber/ChangeBarber/GetBarberAsService/SelectBarberBefor');

router.get('/:barberid', (req, res, next) => {
    var barberid = req.params.barberid
    SelectBarberBefor.SelectBarberBefor(barberid, (err, row) => {
        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;