var express = require('express');
var router = express.Router();
var GetcountBarber = require('../../../Services/Barber/ChangeBarber/GetcountBarber');
router.get('/:branch_id/:service_name/:date', function (req, res, next) {
    // const data = {
    //     branch_id : req.params.branch_id,
    //     service_name : req.params.service_name
    // }
    let branch_id = req.params.branch_id;
    let service_name = req.params.service_name;
    let date = req.params.date;
    
    GetcountBarber.GetcountBarber(branch_id,service_name,date, function (err, row) {

        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;