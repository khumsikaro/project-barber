var express = require('express');
var router = express.Router();
var controllers = require('../../../Services/Barber/ChangeBarber/AddBarberInQueue');

router.put('/', function (req, res, next) {
    var data = req.body
    controllers.AddBarberInQueue(data, (error, rows) => {
        if (error) {
            res.send({ result: 'error' })
        }
        else {
            res.send({ result: 'success' })
        }
    })

})
module.exports = router;