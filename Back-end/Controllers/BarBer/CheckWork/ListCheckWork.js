var express = require('express');
var router = express.Router();
var ListCheck = require('../../../Services/Barber/CheckWork/ListCheckWork');
router.get('/:username', (req, res, next) => {
    var username = req.params.username
    ListCheck.ListCheck(username, (err, row) => {
        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;