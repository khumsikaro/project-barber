var express = require('express');
var router = express.Router();
var Update = require('../../../Services/Barber/CheckWork/update');

router.put('/', function (req, res, next) {
    var data = req.body
    Update.UpdateCheckWork(data, (error, rows) => {
        if (error) {
            res.send({ result: 'error' })
        }
        else {
            res.send({ result: 'success' })
        }
    })

})
module.exports = router;