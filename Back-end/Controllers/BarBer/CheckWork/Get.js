var express = require('express');
var router = express.Router();
var GetCheck = require('../../../Services/Barber/CheckWork/Get');
router.get('/:branch_id/:date', (req, res, next) => {
    var branch_id = req.params.branch_id
    var date = req.params.date
    GetCheck.GetCheck(branch_id,date, (err, row) => {
        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;