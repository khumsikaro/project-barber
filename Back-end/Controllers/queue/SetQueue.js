var express = require('express');
var router = express.Router();
var CheckSkill = require('../../Services/queue/SetQueue');
router.get('/:CheckSkills', function (req, res, next) {

    var Skills = req.params.CheckSkills 
    
    CheckSkill.CheckSkills(Skills, function (err, row) {
        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;