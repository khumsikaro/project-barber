var express = require('express');
var router = express.Router();
var service = require('../../Services/queue/ChangeBarber');
router.get('/', function (req, res, next) {
    service.Get(req.query, function (err, row) {

        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;