var express = require('express');
var router = express.Router();
var GetQueue = require('../../Services/queue/Getqueue');
router.get('/:branch_id/:date', function (req, res, next) {
    const data = {
        branch_id : req.params.branch_id,
        date : req.params.date
    }
    
    GetQueue.GetQueue(data, function (err, row) {

        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;