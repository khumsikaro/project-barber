var express = require('express');
var router = express.Router();
var Controller = require('../../../Services/queue/status_come/GetQueueDetail');

router.get('/:queue_id', (req, res, next) => {
    var queue_id = req.params.queue_id
    Controller.GetQueueDetail(queue_id, (err, row) => {
        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;