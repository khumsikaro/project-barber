var express = require('express');
var router = express.Router();
var Controller = require('../../../Services/queue/status_come/Get');
router.get('/:branch_id', function (req, res, next) {
    var branch_id = req.params.branch_id
    Controller.GetQueueCome(branch_id, function (err, row) {

        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;