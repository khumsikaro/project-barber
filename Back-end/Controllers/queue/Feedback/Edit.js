var express = require('express');
var router = express.Router();
var SendFeedback = require('../../../Services/queue/Feedback/Edit');
router.put('/', function (req, res, next) {
    var data = req.body
    SendFeedback.SendFeedback(data, (error, rows) => {
        if (error) {
            res.send({ result: 'error' })    
        }
        else {
            res.send({ result: 'success' })
        }
    })
})
module.exports = router;