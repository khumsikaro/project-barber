var express = require('express');
var router = express.Router();
var controllers = require('../../../Services/queue/status_Conferm/DeleteQueueDetail');

router.delete('/:QueueDetailId', function (req, res, next) {
    var data = req.params.QueueDetailId
    controllers.Delete(data, (error, rows) => {
        if (error) {
            res.send({ result: 'error' })
        }
        else {
            res.send({ result: 'success' })
        }
    })

})
module.exports = router;