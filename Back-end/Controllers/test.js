var express = require('express');
var router = express.Router();
var Controller = require('../Services/test');
router.get('/:id', function (req, res, next) {
 var id = req.param.id
    Controller.Get(id, function (err, row) {

        if (err) {
            res.json(err);
        }
        else {
            res.json(row);
        }
    })
});
module.exports = router;