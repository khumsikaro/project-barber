var express = require('express');
var router = express.Router();
var UpdateAbout = require('../../Services/about_us/UpdateAbout');

router.put('/', function (req, res, next) {
    var data = req.body
    UpdateAbout.UpdateAbout(data, (error, rows) => {
        if (error) {
            res.send({ result: 'error' })
        }
        else {
            res.send({ result: 'success' })
        }
    })

})
module.exports = router;