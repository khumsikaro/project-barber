var express = require('express');
var router = express.Router();
var DeleteAbout = require('../../Services/about_us/DeleteAbout');

router.delete('/:about_id', function (req, res, next) {
    var data = req.params.about_id
    DeleteAbout.DeleteAbout(data ,(error,rows)=>{
        if(error){
            res.send({result:'error'})
        }
        else{
            res.send({result:'success'})
        }
    })

})
module.exports = router;