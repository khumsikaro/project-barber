var connectDatabase = require('../../Database/connectdb');
var Get = {
    Get: (data, callback) => {
        let SQL;
        if (data.service_id == "" || data.service_id == null || data.service_id == undefined){
            SQL = `SELECT user.username,user.nickname FROM user
            LEFT JOIN skills ON user.username = skills.barber_name
            LEFT JOIN leaves ON user.username = leaves.barber_id
            WHERE user.username NOT IN((SELECT leaves.barber_id FROM leaves WHERE leaves.date_start <= '${data.date}' AND leaves.date_stop >= '${data.date}'))
            AND user.username NOT IN((SELECT check_work.barber_id FROM check_work WHERE check_work.date = '${data.date}' AND check_work.status_awol = 1))
            AND user.branch_id = '${data.branch_id}'
            AND user.username NOT IN(${data.barber_id})
            AND user.level_barberid IN('2','3')
            GROUP BY user.username
            ORDER BY user.nickname`;
        }
        else{
            SQL = `SELECT user.username,user.nickname FROM user
            LEFT JOIN skills ON user.username = skills.barber_name
            LEFT JOIN leaves ON user.username = leaves.barber_id
            WHERE user.username NOT IN((SELECT leaves.barber_id FROM leaves WHERE leaves.date_start <= '${data.date}' AND leaves.date_stop >= '${data.date}'))
            AND user.username NOT IN((SELECT check_work.barber_id FROM check_work WHERE check_work.date = '${data.date}' AND check_work.status_awol = 1))
            AND user.branch_id = '${data.branch_id}'
            AND user.level_barberid IN('2','3')
            AND skills.service_name = '${data.service_id}'
            GROUP BY user.username
            ORDER BY user.nickname`;
        }
        return connectDatabase.query(SQL, callback);
    }
}
module.exports = Get;