var connectDatabase = require('../../Database/connectdb');
var Get = {
    Get: (data, callback) => {
        let SQL;
        if (data.barber_id == "" || data.barber_id == null || data.barber_id == "null" || data.barber_id == undefined || data.barber_id == "undefined")
            SQL = ` SELECT skills.service_name AS name,service.type AS type,service.service_charge,service.take_time FROM user
                LEFT JOIN skills ON user.username = skills.barber_name
                INNER JOIN service ON skills.service_name = service.name
                LEFT JOIN leaves ON user.username = leaves.barber_id
                WHERE user.username NOT IN(SELECT leaves.barber_id FROM leaves WHERE leaves.date_start <= '${data.date}' AND leaves.date_stop >= '${data.date}')
                AND user.branch_id = '${data.branch_id}'
                AND service.name NOT IN(${data.service_id})
                GROUP BY skills.service_name
                ORDER BY skills.service_name`;
        else {
            if (data.barber_id == "x")
                SQL = `SELECT skills.service_name AS name,service.type AS type,service.service_charge,service.take_time FROM user
                LEFT JOIN skills ON user.username = skills.barber_name
                INNER JOIN service ON skills.service_name = service.name
                LEFT JOIN leaves ON user.username = leaves.barber_id
                WHERE user.username NOT IN(SELECT leaves.barber_id FROM leaves WHERE leaves.date_start <= '${data.date}' AND leaves.date_stop >= '${data.date}')
                AND user.branch_id = '${data.branch_id}'
                GROUP BY skills.service_name
                ORDER BY skills.service_name`;
            else
                SQL = `SELECT skills.service_name AS name,service.type AS type,service.service_charge,service.take_time FROM user
                LEFT JOIN skills ON user.username = skills.barber_name
                INNER JOIN service ON skills.service_name = service.name
                LEFT JOIN leaves ON user.username = leaves.barber_id
                WHERE user.username NOT IN(SELECT leaves.barber_id FROM leaves WHERE leaves.date_start <= '${data.date}' AND leaves.date_stop >= '${data.date}')
                AND user.branch_id = '${data.branch_id}'
                AND skills.barber_name = '${data.barber_id}'
                GROUP BY skills.service_name
                ORDER BY skills.service_name`;
        }
        return connectDatabase.query(SQL, callback);
    }
}
module.exports = Get;