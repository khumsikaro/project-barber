var db = require('../../Database/connectdb'); //reference of connectdb.js
var AddQueue = {
    AddQueue: async function (data, callback) {

        var branch_id = data.branch_id;
        var customer_id = data.customer_id;
        var total_time = data.total_time;
        var total_money = data.total_money;
        var queue_dt = data.queue_dt;
        var status_confirm = data.status_confirm;
        var status_come = data.status_come;
        var status_pay = data.status_pay;
        // var feedback_branch = data.	feedback_branch;
        var queue_detail = data.queue_detail;

        var InsertQueue = `INSERT INTO queue(
                                    branch_id,
                                    customer_id,
                                    total_time,
                                    total_money,
                                    queue_dt,
                                    status_confirm,
                                    status_come,
                                    status_pay) 
                          VALUES   ('${branch_id}',
                                    '${customer_id}',
                                    '${total_time}',
                                    '${total_money}',
                                    '${queue_dt}',
                                    '${status_confirm}',
                                    '${status_come}',
                                    '${status_pay}'
                                    );`;

        await db.query(InsertQueue, callback);
        var InsertQueueDetail

        for (i = 0; i < queue_detail.length; i++) {
            InsertQueueDetail = `INSERT INTO queue_detail (queue_id,service_id,barber_id,start_dt,finish_dt)
                                                                VALUES ((SELECT MAX(queue_id) FROM queue),
                                                                '${queue_detail[i].service_id}',
                                                                '${queue_detail[i].barber_id}',
                                                                '${queue_detail[i].start_dt}',
                                                                '${queue_detail[i].finish_dt}');`;

            await db.query(InsertQueueDetail);

        }

    },
}; module.exports = AddQueue;
