var connectDatabase = require('../../Database/connectdb');
var GetQueue = {
    GetQueue: function (data, callback) {
        let branch_id = data.branch_id
        let date = data.date
        let SQL = `CAll GetTableQueueOfBranch('${branch_id}','${date}');`;
        return connectDatabase.query(SQL,callback);
    }
}
module.exports = GetQueue;