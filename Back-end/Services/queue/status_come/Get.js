var connectDatabase = require('../../../Database/connectdb');
var GetQueueCome = {
    GetQueueCome: function (branch_id, callback) {
        let SQL = `SELECT 
        queue.queue_id,
        queue.branch_id,
        queue.customer_id,
        queue.total_time,
        queue.total_money,
        queue.queue_dt,
        queue.status_confirm,
        queue.status_come,
        queue.cancel_queue,
        queue.dateBranch,
        queue.stempfeedback,
        queue.status_pay,
        queue.feedback_branch,
        user.Email,
        user.firstname,
        user.lastname,
        user.phone_number
        FROM  queue
        LEFT JOIN user ON user.username = queue.customer_id
        WHERE queue.status_confirm = 1 AND status_come = 0 
        AND queue.status_pay = 0
        AND queue.branch_id = '${branch_id}'
        ORDER by queue_dt DESC`;

        return connectDatabase.query(SQL,callback);
    }
}
module.exports = GetQueueCome;