var connectDatabase = require('../../../Database/connectdb');
var GetQueueDetail = {
    GetQueueDetail: (queue_id, callback) => {
        
        let SQL =   `SELECT QL.*,queue.queue_dt,service.take_time,service.service_charge,
        (SELECT user.nickname FROM user WHERE user.username = QL.barber_id) AS Nickname1,
        (SELECT user.nickname FROM user WHERE user.username = QL.change_ber) AS Nickname2,    
        (SELECT point_customer FROM point WHERE point.service_id = QL.service_id AND point.customer_id = queue.customer_id) AS SUMPoint,
        (SELECT Max_point FROM setting_point WHERE QL.service_id = setting_point.service_name) AS Maxpoint
        FROM queue_detail QL
        LEFT JOIN service ON QL.service_id = service.name
        INNER JOIN queue ON QL.queue_id = queue.queue_id
        WHERE QL.queue_id = '${queue_id}'`;
        return connectDatabase.query(SQL,callback);
    }
}
module.exports = GetQueueDetail;