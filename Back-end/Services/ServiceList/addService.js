var db = require('../../Database/connectdb'); //reference of connectdb.js
var AddService = {
    AddService: async function (data, callback) {
        
        var name = data.name;
        var type = data.type;
        var take_time = data.take_time;
        var detail = data.detail;
        var service_charge = data.service_charge;
        var point = data.point;
        var level_barberid = data.level_barberid;
        var active = data.active;
        var img1 = data.img1;
        var caption1 = data.caption1;
        var img2 = data.img2;
        var caption2 = data.caption2;
        var img3 = data.img3;
        var caption3 = data.caption3;
        var img4 = data.img4;
        var caption4 = data.caption4;
        var img5 = data.img5;
        var caption5 = data.caption5;
        var img6 = data.img6;
        var caption6 = data.caption6;
        var img7 = data.img7;
        var caption7 = data.caption7;
        var img8 = data.img8;
        var caption8 = data.caption8;
        var img9 = data.img9;
        var caption9 = data.caption9;
        var img10 = data.img10;
        var caption10 = data.caption10;
        
        var InsertService = `INSERT INTO service(
                                    name,
                                    type,
                                    take_time,
                                    detail,
                                    service_charge,
                                    point,
                                    active,
                                    level_barberid,
                                    img1,
                                    caption1,
                                    img2,
                                    caption2,
                                    img3,
                                    caption3,
                                    img4,
                                    caption4,
                                    img5,
                                    caption5,
                                    img6,
                                    caption6,
                                    img7,
                                    caption7,
                                    img8,
                                    caption8,
                                    img9,
                                    caption9,
                                    img10,
                                    caption10
                                    ) 
                          VALUES   ('${name}',
                                    '${type}',
                                    '${take_time}',
                                    '${detail}',
                                    '${service_charge}',
                                    '${point}',
                                    '${active}',
                                    '${level_barberid}',
                                    '${img1}',
                                    '${caption1}',
                                    '${img2}',
                                    '${caption2}',
                                    '${img3}',
                                    '${caption3}',
                                    '${img4}',
                                    '${caption4}',
                                    '${img5}',
                                    '${caption5}',
                                    '${img6}',
                                    '${caption6}',
                                    '${img7}',
                                    '${caption7}',
                                    '${img8}',
                                    '${caption8}',
                                    '${img9}',
                                    '${caption9}',
                                    '${img10}',
                                    '${caption10}');`;
        await db.query(InsertService,callback);
    },
}; module.exports = AddService;
