var connectDatabase = require('../../Database/connectdb');
var SearchBranch = {
    SearchBranch: function (data, callback) {
        let name = data.name
        let status = data.status
        let SQL = `SELECT 
                    branch.branch_id,
                    branch.name,
                    user.firstname,
                    user.lastname,
                    user.nickname, 
                    branch.detail, 
                    branch.address, 
                    branch.active, 
                    branch.status, 
                    branch.manager_name,
                    branch.map_img,
                    branch.location,
                    branch.province_id,
                    province.province_name,
                    branch.amphur_id,
                    amphur.amphur_name,
                    branch.district_id, 
                    district.district_name,
                    branch.zipcode_id,
                    zipcode.zipcode, 
                    branch.img1,
                    branch.img2,
                    branch.img3,
                    branch.img4,
                    branch.img5,
                    branch.img6,
                    branch.img7,
                    branch.img8,
                    branch.img9,
                    branch.img10
                    from branch 
                    LEFT JOIN province on branch.province_id = province.province_id 
                    LEFT JOIN amphur on branch.amphur_id = amphur.amphur_id 
                    LEFT JOIN district on branch.district_id = district.district_id 
                    LEFT JOIN zipcode on branch.zipcode_id = zipcode.zipcode_id 
                    LEFT JOIN user on branch.manager_name = user.username
                    WHERE branch.name LIKE '%${name}%'  order by branch_id DESC`;
        return connectDatabase.query(SQL, callback);
    }
}
module.exports = SearchBranch;