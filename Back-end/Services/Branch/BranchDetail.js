var connectDatabase = require('../../Database/connectdb');
var BranchDetail = {
    BranchDetail: (branch_id, callback) => {
        let SQL = `SELECT
                branch.branch_id,
                branch.name,
                user.firstname,
                user.lastname,
                branch.detail,
                branch.address,
                branch.active,
                branch.status,
                branch.manager_name,
                province.province_name,
                amphur.amphur_name,
                district.district_name,
                zipcode.zipcode,
                branch.map_img,
                branch.location,
                branch.img1,
                branch.caption1,
                branch.img2,
                branch.caption2,
                branch.img3,
                branch.caption3,
                branch.img4,
                branch.caption4,
                branch.img5,
                branch.caption5,
                branch.img6,
                branch.caption6,
                branch.img7,
                branch.caption7,
                branch.img8,
                branch.caption8,
                branch.img9,
                branch.caption9,
                branch.img10,
                branch.caption10
                from branch 
                LEFT JOIN province on branch.province_id = province.province_id 
                LEFT JOIN amphur on branch.amphur_id = amphur.amphur_id 
                LEFT JOIN district on branch.district_id = district.district_id 
                LEFT JOIN zipcode on branch.zipcode_id = zipcode.zipcode_id
                LEFT JOIN user ON branch.manager_name = user.username
                WHERE branch.branch_id = '${branch_id}'`;
        return connectDatabase.query(SQL,callback);
    }
}
module.exports = BranchDetail;