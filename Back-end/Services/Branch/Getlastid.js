var connectDatabase = require('../../Database/connectdb');
var Getlastbranchid = {
    Getlastbranchid: function (data, callback) {
        let SQL = "SELECT MAX(branch_id) as id FROM branch";
        return connectDatabase.query(SQL,callback);
    }
}
module.exports = Getlastbranchid;