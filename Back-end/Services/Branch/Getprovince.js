var connectDatabase = require('../../Database/connectdb');
var Getprovince = {
    Getprovince: function (data, callback) {
        let SQL = `SELECT province_id,province_name FROM province ORDER BY province_name ASC`;
        return connectDatabase.query(SQL,callback);
    }
}
module.exports = Getprovince;