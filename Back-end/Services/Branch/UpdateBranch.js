var db = require('../../Database/connectdb'); //reference of connectdb.js
var UpdateBranch = {
UpdateBranch: function (data, callback) { 
    
    var name = data.name;
    var manager_name = data.manager_name;
    var status = data.status;
    var detail = data.detail;
    var address = data.address;
    var province_id = data.province_id;
    var amphur_id = data.amphur_id;
    var district_id = data.district_id;
    var zipcode_id = data.zipcode_id;
    var active = data.active;
    var map_img = data.map_img;
    var location = data.location;
    var img1 = data.img1;
    var caption1 = data.caption1;
    var img2 = data.img2;
    var caption2 = data.caption2;
    var img3 = data.img3;
    var caption3 = data.caption3;
    var img4 = data.img4;
    var caption4 = data.caption4;
    var img5 = data.img5;
    var caption5 = data.caption5;
    var img6 = data.img6;
    var caption6 = data.caption6;
    var img7 = data.img7;
    var caption7 = data.caption7;
    var img8 = data.img8;
    var caption8 = data.caption8;
    var img9 = data.img9;
    var caption9 = data.caption9;
    var img10 = data.img10;
    var caption10 = data.caption10;
    var branch_id = data.branch_id;

    var UpdateBranch = `UPDATE branch SET 
                        name ='${name}',
                        manager_name ='${manager_name}',
                        status ='${status}',
                        detail ='${detail}',
                        address ='${address}',
                        province_id ='${province_id}',
                        amphur_id ='${amphur_id}',
                        district_id ='${district_id}',
                        zipcode_id ='${zipcode_id}',
                        active ='${active}',
                        map_img ='${map_img}',
                        location ='${location}',
                        img1='${img1}',
                        caption1='${caption1}',
                        img2='${img2}',
                        caption2='${caption2}',
                        img3='${img3}',
                        caption3='${caption3}',
                        img4='${img4}',
                        caption4='${caption4}',
                        img5='${img5}',
                        caption5='${caption5}',
                        img6='${img6}',
                        caption6='${caption6}',
                        img7='${img7}',
                        caption7='${caption7}',
                        img8='${img8}',
                        caption8='${caption8}',
                        img9='${img9}',
                        caption9='${caption9}',
                        img10='${img10}',
                        caption10='${caption10}' WHERE branch_id='${branch_id}'`;
    return db.query(UpdateBranch,callback);
},
}; module.exports = UpdateBranch;