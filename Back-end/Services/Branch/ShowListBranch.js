var connectDatabase = require('../../Database/connectdb');
var ShowListBranch = {
    ShowListBranch: function (data, callback) { 
        let SQL = `SELECT 
                    branch.branch_id,
                    branch.name, 
                    branch.detail, 
                    branch.address, 
                    branch.active, 
                    branch.status, 
                    branch.manager_name,
                    user.firstname,
                    user.lastname,
                    user.nickname,
                    branch.map_img,
                    branch.location,
                    branch.province_id,
                    province.province_name,
                    branch.amphur_id,
                    amphur.amphur_name,
                    branch.district_id, 
                    district.district_name,
                    branch.zipcode_id,
                    branch.img1,
                    branch.img2,
                    branch.img3,
                    branch.img4,
                    branch.img5,
                    branch.img6,
                    branch.img7,
                    branch.img8,
                    branch.img9,
                    branch.img10,
                    zipcode.zipcode 
                    from branch 
                    LEFT JOIN province on branch.province_id = province.province_id 
                    LEFT JOIN amphur on branch.amphur_id = amphur.amphur_id 
                    LEFT JOIN district on branch.district_id = district.district_id 
                    LEFT JOIN zipcode on branch.zipcode_id = zipcode.zipcode_id 
                    LEFT JOIN user on branch.manager_name = user.username
                    ORDER BY CONVERT (branch.name USING tis620) ASC`;
        return connectDatabase.query(SQL,callback);
    }
}
module.exports = ShowListBranch;