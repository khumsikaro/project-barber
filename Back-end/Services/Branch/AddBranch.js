var db = require('../../Database/connectdb'); //reference of connectdb.js
var AddBranch = {
    AddBranch: async function (data, callback) {
        
        var name = data.name;
        var manager_name = data.manager_name;
        var status = data.status;
        var detail = data.detail;
        var address = data.address;
        var province_id = data.province_id;
        var amphur_id = data.amphur_id;
        var district_id = data.district_id;
        var zipcode_id = data.zipcode_id;
        var active = data.active;
        var map_img = data.map_img;
        var location = data.location;
        var img1 = data.img1;
        var caption1 = data.caption1;
        var img2 = data.img2;
        var caption2 = data.caption2;
        var img3 = data.img3;
        var caption3 = data.caption3;
        var img4 = data.img4;
        var caption4 = data.caption4;
        var img5 = data.img5;
        var caption5 = data.caption5;
        var img6 = data.img6;
        var caption6 = data.caption6;
        var img7 = data.img7;
        var caption7 = data.caption7;
        var img8 = data.img8;
        var caption8 = data.caption8;
        var img9 = data.img9;
        var caption9 = data.caption9;
        var img10 = data.img10;
        var caption10 = data.caption10;

        var Insertbranch = `INSERT INTO branch(
                                        name,
                                        manager_name,
                                        status,
                                        detail,
                                        address,
                                        province_id,
                                        amphur_id,
                                        district_id,
                                        zipcode_id,
                                        active,
                                        map_img,
                                        location,
                                        img1,
                                        caption1,
                                        img2,
                                        caption2,
                                        img3,
                                        caption3,
                                        img4,
                                        caption4,
                                        img5,
                                        caption5,
                                        img6,
                                        caption6,
                                        img7,
                                        caption7,
                                        img8,
                                        caption8,
                                        img9,
                                        caption9,
                                        img10,
                                        caption10) 
                          VALUES ('${name}',
                                  '${manager_name}',
                                  '${status}',
                                  '${detail}',
                                  '${address}',
                                  '${province_id}',
                                  '${amphur_id}',
                                  '${district_id}',
                                  '${zipcode_id}',
                                  '${active}',
                                  '${map_img}',
                                  '${location}',
                                  '${img1}',
                                  '${caption1}',
                                  '${img2}',
                                  '${caption2}',
                                  '${img3}',
                                  '${caption3}',
                                  '${img4}',
                                  '${caption4}',
                                  '${img5}',
                                  '${caption5}',
                                  '${img6}',
                                  '${caption6}',
                                  '${img7}',
                                  '${caption7}',
                                  '${img8}',
                                  '${caption8}',
                                  '${img9}',
                                  '${caption9}',
                                  '${img10}',
                                  '${caption10}');`;
        await db.query(Insertbranch,callback);
    },
}; module.exports = AddBranch;
