var connectDatabase = require('../../../../Database/connectdb');
var CheckHolidayPrivateBarber = {
    GetHoliday: (username, date, callback) => {
        let month = date.length == 7 ? date.substring(0, 2) : date.substring(0, 1);
        let year = date.length == 7 ? date.substring(3, 7) : date.substring(2, 6);
        let SQL = `SELECT 
                        leave_type.vac_type,
                        leaves.barber_id,
                        SUM((CASE WHEN MONTH(leaves.date_start) = MONTH(leaves.date_stop) THEN leaves.total_day ELSE DATEDIFF(LAST_DAY(leaves.date_start),leaves.date_start) + 1 END)) AS MONTH,
                        leave_type.MonthMax,
                        (SELECT SUM(total_day)FROM leaves WHERE YEAR(leaves.date_start) = '${year}'
                                            AND leaves.type_id = leave_type.leave_type_id
                                            AND leaves.barber_id = '${username}')AS YEAR,
                        leave_type.yearMax
                    FROM leaves
                    INNER JOIN leave_type on leaves.type_id = leave_type.leave_type_id
                    WHERE leaves.barber_id ='${username}' 
                    AND MONTH(leaves.date_start) = '${month}' GROUP BY type_id`;
        return connectDatabase.query(SQL, callback);
    }
}
module.exports = CheckHolidayPrivateBarber;