var connectDatabase = require('../../../../Database/connectdb');
var GetLeavesDetail = {
    GetLeavesDetail: function (date,username, callback) {
        let inputdate = new Date(date)
        let day = inputdate.getDate().toString();
        let month =(inputdate.getMonth()+1).toString();
        let year = inputdate.getFullYear().toString();
        let SQL = `SELECT 
                    user.firstname,
                    user.lastname,
                    leave_type.vac_type,
                    leaves.total_day,
                    leaves.date_start,
                    leaves.date_stop
                    FROM leaves
                    LEFT JOIN user ON user.username = leaves.barber_id
                    LEFT JOIN leave_type ON leaves.type_id = leave_type.leave_type_id
                    WHERE MONTH(leaves.date_start) = '${month}'
                    AND YEAR(leaves.date_start) = '${year}'
                    AND MONTH(leaves.date_stop) = '${month}'
                    AND YEAR(leaves.date_stop) = '${year}'
                    AND leaves.barber_id = '${username}'`;
        return connectDatabase.query(SQL, callback);
    }
}
module.exports = GetLeavesDetail;