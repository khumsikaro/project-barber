var connectDatabase = require('../../../Database/connectdb');
var ShowLeaveList = {
    Get: function (Branch, callback) {

        let SQL = `SELECT leaves.leave_id,
                          leaves.type_id,
                          leaves.detail,
                          leaves.barber_id,
                          leaves.date_start,
                          leaves.date_stop,
                          leaves.time_start,
                          leaves.time_end,
                          leaves.total_day,
                          leave_type.leave_type_id,
                          leave_type.vac_type,
                          user.username,
                          user.firstname,
                          user.lastname
                    FROM leaves
                    LEFT JOIN leave_type on leaves.type_id = leave_type.leave_type_id 
                    LEFT JOIN user ON  leaves.barber_id = user.username
                    WHERE level_barberid in('2','3') AND branch_id = '${Branch}'`;

        return connectDatabase.query(SQL,callback);
    }
}
module.exports = ShowLeaveList;