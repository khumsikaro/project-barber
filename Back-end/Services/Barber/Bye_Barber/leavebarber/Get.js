var connectDatabase = require('../../../../Database/connectdb');
var GetHolidayBarber = {
    GetHolidayBarber:  function (data, callback) {
        let branch_id = data.branch_id
        let date = data.date
        
        let inputdate = new Date(date)
        let day = inputdate.getDate().toString();
        let month =(inputdate.getMonth()+1).toString();
        let year = inputdate.getFullYear().toString();
        let d1 = year + "-" + month + "-" + day
        
        let SQL = `CALL ShowUserLeaves('${branch_id}','${d1}')`;

        return connectDatabase.query(SQL, callback);
    }
}
module.exports = GetHolidayBarber;