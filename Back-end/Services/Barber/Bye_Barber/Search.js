var connectDatabase = require('../../../Database/connectdb');
var SearchLeaveList = {
    Search: function (data, callback) {  
        let type = data.type
        let Branch = data.Branch 
        let name = data.name
        let SQL = `SELECT leaves.leave_id,
                          leaves.type_id,
                          leaves.detail,
                          leaves.barber_id,
                          leaves.date_start,
                          leaves.date_stop,
                          leaves.total_day,
                          leaves.time_start,
                          leaves.time_end,
                          leave_type.leave_type_id,
                          leave_type.vac_type,
                          user.username,
                          user.firstname,
                          user.lastname
                  FROM leaves
                  LEFT JOIN leave_type on leaves.type_id = leave_type.leave_type_id 
                  LEFT JOIN user ON  leaves.barber_id = user.username
                  WHERE leaves.type_id LIKE '%${type}%' 
                  AND leaves.barber_id LIKE '%${name}%'
                  AND user.branch_id = '${Branch}'`;
        return connectDatabase.query(SQL,callback);
    },
};
module.exports = SearchLeaveList;