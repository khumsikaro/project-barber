var db = require('../../../Database/connectdb'); //reference of connectdb.js
var UpdateLeave = {
    UpData: async function (data, callback) {
        var type_id = data.type_id;
        var detail = data.detail;
        var barber_id = data.barber_id;
        // var date_start = data.date_start;
        // var date_stop = data.date_stop;
        var total_day = data.total_day;
        var leave_id = data.leave_id;

        let inputdate1 = new Date(data.date_start)
        let day1 = inputdate1.getDate().toString();
        let month1 = (inputdate1.getMonth() + 1).toString();
        let year1 = inputdate1.getFullYear().toString();
        let date_start = year1 + "-" + month1 + "-" + day1

        let inputdate2 = new Date(data.date_stop)
        let day2 = inputdate2.getDate().toString();
        let month2 = (inputdate2.getMonth() + 1).toString();
        let year2 = inputdate2.getFullYear().toString();
        let date_stop = year2 + "-" + month2 + "-" + day2
        console.log(date_start,date_stop,total_day,leave_id,barber_id,detail,type_id);
        

        var UpdateLeave = `CALL EditLeave('${type_id}','${detail}','${barber_id}','${date_start}','${date_stop}','${total_day}','${leave_id}');`;
        
        await db.query(UpdateLeave, callback);

    },
}; module.exports = UpdateLeave;