var connectDatabase = require('../../../../Database/connectdb');
var GetLate = {
    Get: function (date,username, callback) {
        let inputdate = new Date(date)
        let day = inputdate.getDate().toString();
        let month =(inputdate.getMonth()+1).toString();
        let year = inputdate.getFullYear().toString();
        let SQL = `SELECT check_work.*,
                    user.username,
                    user.firstname,
                    user.lastname
                    FROM check_work 
                    LEFT JOIN user ON  check_work.barber_id = user.username
                    WHERE MONTH(check_work.date) = '${month}' 
                    AND  YEAR(check_work.date) = '${year}'
                    AND check_work.barber_id = '${username}' 
                    AND (SELECT TIME(check_work.start_work) > (SELECT chack_in FROM setting_bar))`;

        return connectDatabase.query(SQL, callback);
    }
}
module.exports = GetLate;