var connectDatabase = require('../../../Database/connectdb');
var GetcountBarber = {
    GetcountBarber: function (branch_id,service_name,date , callback) {
        //ต้อง substring เก็บเดือน และปี
        let month = date.length == 7 ? date.substring(0,2) : date.substring(0,1);
        let year = date.length == 7 ? date.substring(3,7) : date.substring(2,6);

        let SQL = `SELECT user.nickname,
        (SELECT COUNT(queue_detail.barber_id) 
		FROM queue_detail
		LEFT JOIN queue ON queue_detail.queue_id = queue.queue_id
		WHERE queue_detail.barber_id = skills.barber_name
		AND queue.status_confirm = 1 AND queue.status_come = 1 AND queue.status_pay =1)AS CountQ
        FROM skills
        INNER JOIN user ON skills.barber_name = user.username
        INNER JOIN queue_detail ON user.username = queue_detail.barber_id
        INNER JOIN queue ON queue.queue_id = queue_detail.queue_id
        WHERE skills.service_name = '${service_name}' AND user.branch_id = '${branch_id}'
        AND MONTH(queue.queue_dt) = '${month}'
        AND YEAR(queue.queue_dt) = '${year}'
        GROUP BY skills.barber_name`;
        return connectDatabase.query(SQL,callback);
    }
}
module.exports = GetcountBarber;