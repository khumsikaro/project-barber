var connectDatabase = require('../../../../Database/connectdb');
var SelectServiceBefor = {
    SelectServiceBefor: (branch,serviceid, callback) => {
        let SQL = `SELECT 
                    skills.barber_name AS username,
                    user.level_barberid,
                    user.firstname,
                    user.lastname,
                    user.nickname
                    FROM skills 
                    LEFT JOIN user ON skills.barber_name = user.username
                    WHERE service_name = '${serviceid}' AND user.branch_id = '${branch}'`;
        return connectDatabase.query(SQL, callback);
    }
}
module.exports = SelectServiceBefor;