var connectDatabase = require('../../Database/connectdb');
var ShowListBarberChackwork = {
    ShowListBarberChackwork: function (data, callback) {
        let SQL = `SELECT
		user.username,
        check_work.status_awol,
        check_work.start_work,
        level_barber.level_barbername,
        user.status,
        user.img_profile,
        user.firstname,
        user.lastname,
        user.nickname,
        user.date_of_birth,
        user.phone_number,
        user.gender,
        user.Email,
        user.address,
        province.province_name,
        amphur.amphur_name,
        district.district_name,
        zipcode.zipcode,
        user.active,
        user.branch_id,
        branch.name as brachname,
        user.cr_date,
        user.cr_by,
        user.up_date,
        user.up_by
        FROM user
        LEFT JOIN level_barber on user.level_barberid = level_barber.level_barberid	
        LEFT JOIN province ON user.province_id = province.province_id
        LEFT JOIN amphur on user.amphur_id = amphur.amphur_id
        LEFT JOIN district ON user.district_id = district.district_id
        LEFT JOIN branch on user.branch_id = branch.branch_id
        LEFT JOIN zipcode ON user.zipcode_id = zipcode.zipcode_id
        LEFT JOIN check_work ON user.username = check_work.barber_id
        WHERE user.level_barberid IN ('2','3') AND check_work.date = CURRENT_DATE 
        ORDER BY check_work.status_awol`;
        return connectDatabase.query(SQL,callback);
    }
}
module.exports = ShowListBarberChackwork;