var connectDatabase = require('../../../Database/connectdb');
var GetCheck = {
    GetCheck: async function (branch_id, date, callback) {
        let inputdate = new Date(date)
        let day = inputdate.getDate().toString();
        let month = (inputdate.getMonth() + 1).toString();
        let year = inputdate.getFullYear().toString();
        let d1 = year + "-" + month + "-" + day
        let SQL = `CALL GetCheckWork('${branch_id}','${d1}');`;
        await connectDatabase.query(SQL, callback);
    }
}
module.exports = GetCheck;