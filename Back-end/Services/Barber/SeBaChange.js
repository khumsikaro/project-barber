var connectDatabase = require('../../Database/connectdb');
var ServiceBarberChange = {
    SeBaChange: function (serviceid,branchid,date,timeStart,timeStop, callback) {

        let SQL = ` SELECT   user.username,user.firstname,user.lastname,user.nickname 
                    FROM user 
                    LEFT JOIN skills ON user.username = skills.barber_name
                    LEFT JOIN leaves ON user.username = leaves.barber_id
                    LEFT JOIN queue_detail ON user.username = queue_detail.barber_id
                    LEFT JOIN queue ON queue_detail.queue_id = queue.queue_id
                    WHERE skills.service_name ='${serviceid}' AND user.branch_id = '${branchid}'
                    AND user.username NOT IN(SELECT leaves.barber_id FROM leaves WHERE  leaves.date_start <= '${date}' AND leaves.date_stop >= '${date}')
                    AND user.username NOT IN(SELECT queue_detail.barber_id FROM queue LEFT JOIN queue_detail ON queue_detail.queue_id = queue.queue_id WHERE queue.queue_dt = '${date}' AND queue_detail.start_dt = '${timeStart}' AND queue_detail.finish_dt = '${timeStop}' GROUP BY queue_detail.barber_id)
                    GROUP BY user.username,user.firstname,user.lastname,user.nickname 
                    ORDER BY CONVERT (firstname USING tis620) ASC`;
        
        return connectDatabase.query(SQL,callback);
    }
}
module.exports = ServiceBarberChange;