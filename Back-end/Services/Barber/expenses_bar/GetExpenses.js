var connectDatabase = require('../../../Database/connectdb');
var GetExpensesBar = {
    GetExpensesBar: function (data, callback) {
        
        let SQL = `SELECT user.username,
                          user.firstname,
                          user.lastname,
                          expenses_bar.ex_bar_id,
                          expenses_bar.type,
                          expenses_bar.money,
                          expenses_bar.time_stamp,
                          expenses_bar.Detail
                          FROM expenses_bar
                          LEFT JOIN user on expenses_bar.barbername = user.username`;
        
        
        return connectDatabase.query(SQL,callback);
    }
}
module.exports = GetExpensesBar;