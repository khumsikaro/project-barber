var connectDatabase = require('../../../Database/connectdb');
var SearchExpenses = {
    SearchExpenses: function (data, callback) {
        
        let username = data.username
        let type = data.type 
        let SQL =  `SELECT user.username,
                         user.firstname,
                        user.lastname,
                        expenses_bar.type,
                        expenses_bar.money,
                        expenses_bar.time_stamp,
                        expenses_bar.Detail
                        FROM expenses_bar
                        LEFT JOIN user on expenses_bar.barbername = user.username
                        WHERE user.username LIKE '%${username}%' AND expenses_bar.type LIKE '%${type}%'`;
        
        
        return connectDatabase.query(SQL,callback);
    }
}
module.exports = SearchExpenses;