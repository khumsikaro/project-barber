var connectDatabase = require('../../../Database/connectdb');
var searchQueuePrivateBarber = {
    Search: function (data, callback) {
        let username = data.username

        let inputdate1 = new Date(data.date)
        
        let day1 = inputdate1.getDate();
        let month1 = (inputdate1.getMonth() + 1);
        let year1 = inputdate1.getFullYear();
        let d1 = year1 + "-" + month1 + "-" + day1
        let SQL = `SELECT 
                    queue.status_come,
                    queue.queue_dt,
                    queue_detail.*
                    FROM queue
                    LEFT JOIN queue_detail ON queue.queue_id = queue_detail.queue_id
                    WHERE queue_detail.barber_id = '${username}' 
                    AND queue.queue_dt = '${d1}'
                    AND queue.status_confirm = 1
                    AND queue.status_pay = 0`;
        return connectDatabase.query(SQL, callback);
    }
}
module.exports = searchQueuePrivateBarber;