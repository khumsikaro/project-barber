var connectDatabase = require('../../Database/connectdb');
var DetailBarber = {
    DetailBarber: function (username, callback) {
        let SQL = `SELECT 
        user.username,
        level_barber.level_barbername,
        user.status,
        user.img_profile,
        user.firstname,
        user.lastname,
        user.nickname,
        user.date_of_birth,
        user.phone_number,
        user.gender,
        user.Email,
        user.address,
        province.province_name,
        amphur.amphur_name,
        district.district_name,
        zipcode.zipcode,
        user.active,
        user.branch_id,
        branch.name,
        user.cr_date,
        user.cr_by
   FROM user
   LEFT JOIN level_barber on level_barber.level_barberid = user.level_barberid
   LEFT JOIN province on 	province.province_id = user.province_id
   LEFT JOIN amphur ON amphur.amphur_id =	user.amphur_id
   LEFT JOIN district on district.district_id = user.district_id
   LEFT JOIN zipcode ON zipcode.zipcode_id = user.zipcode_id
   LEFT JOIN branch ON branch.branch_id = user.branch_id
   WHERE user.username = '${username}'`;
        
        return connectDatabase.query(SQL,callback);
    }
}
module.exports = DetailBarber;