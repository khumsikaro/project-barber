var db = require('../../Database/connectdb'); //reference of connectdb.js
var EditProfile = {
    upservices:  function (data, callback) {
        
        var img_profile     = data.img_profile;
        var firstname       = data.firstname;
        var lastname        = data.lastname;
        var nickname        = data.nickname;
        var date_of_birth   = data.date_of_birth;
        var phone_number    = data.phone_number;
        var Email           = data.Email;
        var gender          = data.gender;
        var province_id     = data.province_id;
        var amphur_id       = data.amphur_id;
        var district_id     = data.district_id;
        var zipcode_id      = data.zipcode_id;
        var address         = data.address;
        var up_by           = data.up_by;
        var username        = data.username;
        
        var UpdateUser = `UPDATE user SET   
                                            img_profile  ='${img_profile}',
                                            firstname  ='${firstname}',
                                            lastname  ='${lastname}',
                                            nickname  ='${nickname}',
                                            date_of_birth  ='${date_of_birth}',
                                            phone_number  ='${phone_number}',
                                            Email  ='${Email}',
                                            gender  ='${gender}',
                                            province_id  ='${province_id}',
                                            amphur_id  ='${amphur_id}',
                                            district_id  ='${district_id}',
                                            zipcode_id  ='${zipcode_id}',
                                            address  ='${address}',
                                            up_date = CURRENT_TIMESTAMP(),
                                            up_by  ='${up_by}'
                                            WHERE username ='${username}'`;

        return db.query(UpdateUser,callback);
    },
}; module.exports = EditProfile;