var connectDatabase = require('../../../Database/connectdb');
var GetQueuePay = {
    GetQueuePay: (branchid, callback) => {

        let SQL = `SELECT  queue.queue_id,
                    queue.branch_id,
                    queue.customer_id,
                    queue.total_time,
                    queue.total_money,
                    queue.queue_dt,
                    queue.status_confirm,
                    queue.status_come,
                    queue.cancel_queue,
                    queue.dateBranch,
                    queue.stempfeedback,
                    queue.status_pay,
                    queue.feedback_branch,
                    user.img_profile,
                    user.firstname,
                    user.lastname,
                    user.phone_number,
                    user.nickname,
                    user.Email
                    FROM queue
                    LEFT JOIN user ON queue.customer_id = user.username
                    WHERE queue.status_confirm = 1 
                    AND queue.status_come = 1
                    AND queue.status_pay = 0 
                    AND queue.branch_id = '${branchid}'`;
        return connectDatabase.query(SQL, callback);
    }
}
module.exports = GetQueuePay;