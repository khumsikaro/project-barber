var connectDatabase = require('../../../Database/connectdb');
var GetPointCustomer = {
    GetPoint: (Customer_id, callback) => {

        let SQL = `SELECT 
        point.service_id,
        (SELECT SUM(point_customer) FROM point WHERE point.service_id =queue_detail.service_id) AS SUMPoint,
        (SELECT Max_point FROM setting_point WHERE point.service_id=setting_point.service_name) AS Maxpoint,
        (SELECT service_charge FROM service WHERE point.service_id = service.name)AS service_charge,
        (SELECT take_time FROM service WHERE point.service_id = service.name)AS take_time
        FROM point
        INNER JOIN queue_detail ON point.service_id = queue_detail.service_id
        WHERE customer_id = '${Customer_id}'GROUP BY point.service_id`;
        return connectDatabase.query(SQL, callback);
    }
}
module.exports = GetPointCustomer;