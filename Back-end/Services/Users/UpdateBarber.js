var db = require('../../Database/connectdb'); //reference of connectdb.js
var UpdateUser = {
    Updatabarber: async function (data, callback) {
        
        var level_barberid = data.level_barberid;
        var status = data.status;
        var img_profile = data.img_profile;
        var firstname = data.firstname;
        var lastname = data.lastname;
        var nickname = data.nickname;
        var date_of_birth = data.date_of_birth;
        var phone_number = data.phone_number;
        var gender = data.gender;
        var Email = data.Email;
        var address = data.address;
        var province_id = data.province_id;
        var amphur_id = data.amphur_id;
        var district_id = data.district_id;
        var zipcode_id = data.zipcode_id;
        var active = data.active;
        var skill = data.skill;
        var up_by = data.up_by;
        var status_soc = data.status_soc;
        var branch_id = data.branch_id;
        var username = data.username;

        var Updatabarber = `UPDATE user SET   level_barberid ='${level_barberid}',
                                                status ='${status}',
                                            img_profile  ='${img_profile}',
                                            firstname  ='${firstname}',
                                            lastname  ='${lastname}',
                                            nickname  ='${nickname}',
                                            date_of_birth  ='${date_of_birth}',
                                            phone_number  ='${phone_number}',
                                            gender  ='${gender}',
                                            Email  ='${Email}',
                                            address  ='${address}',
                                            province_id  ='${province_id}',
                                            amphur_id  ='${amphur_id}',
                                            district_id  ='${district_id}',
                                            zipcode_id  ='${zipcode_id}',
                                            active ='${active}',
                                            up_date = CURRENT_TIMESTAMP(),
                                            up_by  ='${up_by}',
                                            status_soc = '${status_soc}',
                                            branch_id = '${branch_id}'
                                            WHERE username ='${username}'`;

        var deleteskills = `DELETE FROM skills WHERE  barber_name = '${username}'`;
        await db.query(deleteskills);

        var updataskills
        for (i = 0; i < skill.length; i++) {
            updataskills = `INSERT INTO skills (barber_name, service_name) VALUES ('${username}','${skill[i]}')`;
            await db.query(updataskills);
        }
        await db.query(Updatabarber, callback);
    },
}; module.exports = UpdateUser;
