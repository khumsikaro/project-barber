var connectDatabase = require('../../Database/connectdb');
var ShowUsers = {
    ShowUsers: function (data, callback) {
        let SQL = `SELECT
        user.username,
        user.password,
        user.level_barberid,
        level_barber.level_barbername,
        user.status,
        user.img_profile,
        user.firstname,
        user.lastname,
        user.nickname,
        user.date_of_birth,
        user.phone_number,
        user.gender,
        user.Email,
        user.address,
        province.province_name,
        user.province_id,
        amphur.amphur_name,
        user.amphur_id,
        district.district_name,
        user.district_id,
        zipcode.zipcode,
        user.zipcode_id,
        user.active,
        user.branch_id,
        branch.name,
        user.cr_date,
        user.cr_by,
        user.up_date,
        user.up_by,
        user.status_soc
        FROM user
        LEFT JOIN level_barber on user.level_barberid = level_barber.level_barberid	
        LEFT JOIN province ON user.province_id = province.province_id
        LEFT JOIN amphur on user.amphur_id = amphur.amphur_id
        LEFT JOIN district ON user.district_id = district.district_id
        LEFT JOIN branch on user.branch_id = branch.branch_id
        LEFT JOIN zipcode ON user.zipcode_id = zipcode.zipcode_id
        ORDER BY CONVERT (user.firstname USING tis620) ASC`;
        return connectDatabase.query(SQL,callback);
    }
}
module.exports = ShowUsers;