var connectDatabase = require('../../Database/connectdb');
var SearchUsers = {
    SearchUsers: function (data, callback) {
        let firstname = data.firstname
        let level_barberid = data.level_barberid 
        let SQL = `SELECT
		user.username,
        user.level_barberid,
        level_barber.level_barbername,
        user.status,
        user.password,
        user.img_profile,
        user.firstname,
        user.lastname,
        user.nickname,
        user.date_of_birth,
        user.phone_number,
        user.gender,
        user.Email,
        user.address,
        province.province_name,
        user.province_id,
        amphur.amphur_name,
        user.amphur_id,
        district.district_name,
        user.district_id,
        zipcode.zipcode,
        user.zipcode_id,
        user.active,
        user.branch_id,
        branch.name,
        user.cr_date,
        user.cr_by,
        user.up_date,
        user.up_by
        FROM user
        LEFT JOIN level_barber on user.level_barberid = level_barber.level_barberid
        LEFT JOIN province ON user.province_id = province.province_id
        LEFT JOIN amphur ON user.amphur_id = amphur.amphur_id
        LEFT JOIN district on user.district_id = district.district_id
        LEFT JOIN zipcode on user.zipcode_id = zipcode.zipcode_id
        LEFT JOIN branch on user.branch_id = branch.branch_id
        WHERE user.firstname LIKE '%${firstname}%' AND user.level_barberid LIKE '%${level_barberid}%'`;
        return connectDatabase.query(SQL,callback);
    }
}
module.exports = SearchUsers;