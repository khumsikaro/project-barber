var connectDatabase = require('../../../Database/connectdb');
var HistroryListDetail = {
    HistroryListDetail: function (queue_id, callback) {

        let SQL = `SELECT 
        user.nickname,
        user.firstname,
        user.lastname,
        service.take_time,
        service.service_charge,
        queue_detail.service_id,
        queue_detail.add_money,
        queue_detail.add_time,
        queue_detail.detail,
        queue_detail.start_dt,
        queue_detail.finish_dt,
        queue_detail.status_point,
        queue_detail.feedback_service,
        queue_detail.feedback_barber,
        queue_detail.change_ber,
        queue_detail.detail_change_ber,
        queue_detail.seq
        FROM queue_detail
        LEFT JOIN user ON queue_detail.barber_id = user.username
        LEFT JOIN service ON queue_detail.service_id = service.name
        WHERE queue_detail.queue_id = '${queue_id}'`;
        return connectDatabase.query(SQL, callback);
    }
}
module.exports = HistroryListDetail;