var connectDatabase = require('../../../Database/connectdb');
var searchStatusQueueUser = {
    Search: function (data, callback) {
        let username = data.username
        let date = data.date 
        let inputdate = new Date(date)
        let day = inputdate.getDate().toString();
        let month =(inputdate.getMonth()+1).toString();
        let year = inputdate.getFullYear().toString();
        // let day = date.substring(1,2)==="-"? date.substring(0,1):date.substring(0,2);
        // let month =date.length==8 ? date.substring(2,3):date.length==9 && date.substring(1,2)==="-" ? date.substring(2,4) : date.length==9 && date.substring(2,3)==="-" ? date.substring(3,4):date.substring(3,5);
        // let year = date.length==8 ? date.substring(4,8) :date.length==9?  date.substring(5,9) : date.substring(6,10) ;
        let SQL = `SELECT * 
                    FROM queue
                    WHERE cancel_queue = 0
                    AND status_come = 0
                    AND status_pay = 0
                    AND DAY(queue_dt) LIKE '%${day}%'
                    AND MONTH(queue_dt) LIKE '%${month}%'
                    AND YEAR(queue_dt) LIKE '%${year}%'
                    AND customer_id LIKE '%${username}%'`;
        return connectDatabase.query(SQL,callback);
    }
}
module.exports = searchStatusQueueUser;