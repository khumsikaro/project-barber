var connectDatabase = require('../../../Database/connectdb');
var GetStatusQueueUser = {
    Getstatus: (username,date, callback) => {
        let inputdate = new Date(date)
        let day = inputdate.getDate().toString();
        let month =(inputdate.getMonth()+1).toString();
        let year = inputdate.getFullYear().toString();
        let SQL = `SELECT * 
                    FROM queue
                    WHERE cancel_queue = 0
                    AND status_come = 0
                    AND status_pay = 0
                    AND MONTH(queue_dt) = '${month}'
                    AND YEAR(queue_dt) = '${year}'
                    AND DAY(queue_dt) = '${day}'
                    AND customer_id = '${username}' 
                    ORDER BY queue.queue_id DESC`;

        return connectDatabase.query(SQL,callback);
    }
}
module.exports = GetStatusQueueUser;