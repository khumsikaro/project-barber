var db = require('../../Database/connectdb'); //reference of connectdb.js
var AddUser = {
    AddUser: async function (data, callback) {

        var username = data.username;
        var password = data.password;
        var level_barberid = data.level_barberid;
        var status = data.status;
        var img_profile = data.img_profile;
        var firstname = data.firstname;
        var lastname = data.lastname;
        var nickname = data.nickname;
        var date_of_birth = data.date_of_birth;
        var phone_number = data.phone_number;
        var gender = data.gender;
        var Email = data.Email;
        var address = data.address;
        var province_id = data.province_id;
        var amphur_id = data.amphur_id;
        var district_id = data.district_id;
        var zipcode_id = data.zipcode_id;
        var active = data.active;
        var branch_id = data.branch_id;
        var cr_by = data.cr_by;
        var status_soc = data.status_soc;
        var skills = data.skill;

        var InsertUser = `INSERT INTO user
                            (username,password,level_barberid,status,
                            img_profile,firstname,lastname,nickname,
                            date_of_birth,phone_number,gender,Email,
                            address,province_id,amphur_id,district_id,
                            zipcode_id,active,branch_id,cr_date,cr_by,status_soc) 
                         VALUES('${username}',
                                '${password}',
                                '${level_barberid}',
                                '${status}',
                                '${img_profile}',
                                '${firstname}',
                                '${lastname}',
                                '${nickname}',
                                '${date_of_birth}',
                                '${phone_number}',
                                '${gender}',
                                '${Email}',
                                '${address}',
                                '${province_id}',
                                '${amphur_id}',
                                '${district_id}',
                                '${zipcode_id}',
                                '${active}',
                                '${branch_id}',
                                CURRENT_TIMESTAMP(),
                                '${cr_by}',
                                '${status_soc}');`;

        var Insertskills
        if (level_barberid == '2' || level_barberid == '3') {
            for (i = 0; i < skills.length; i++) {

                Insertskills = `INSERT INTO skills (barber_name,service_name) VALUES ('${username}','${skills[i]}');`;
                await db.query(Insertskills);

            }
        }
        await db.query(InsertUser, callback);


    },
}; module.exports = AddUser;
