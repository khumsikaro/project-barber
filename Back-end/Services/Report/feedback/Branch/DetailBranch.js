var connectDatabase = require('../../../../Database/connectdb');
var BranchFeedbackReportDetail = {
    feedbackDetailBranch: function (date1,date2,branch_id, callback) {
        let SQL = `SELECT queue.queue_dt,queue.dateBranch,
                    (SELECT user.firstname FROM user WHERE user.username = queue.customer_id) AS firstname,
                    queue.feedback_branch AS feedback FROM queue
                    LEFT JOIN branch ON queue.branch_id = branch.branch_id
                    WHERE queue.queue_dt BETWEEN '${date1}' AND '${date2}' AND branch.branch_id = '${branch_id}'
                    AND NOT (queue.stempfeedback IS NULL OR queue.stempfeedback = 0)
                    ORDER BY queue.queue_dt ASC`;
        return connectDatabase.query(SQL, callback);
    }
}
module.exports = BranchFeedbackReportDetail;