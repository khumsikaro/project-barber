var connectDatabase = require('../../../../Database/connectdb');
var feedbackbranch = {
    feedback: function (date1, date2, callback) {
        let inputdate1 = new Date(date1)
        let day1 = inputdate1.getDate().toString();
        let month1 = (inputdate1.getMonth() + 1).toString();
        let year1 = inputdate1.getFullYear().toString();
        let d1 = year1 + "-" + month1 + "-" + day1
        let inputdate2 = new Date(date2)
        let day2 = inputdate2.getDate().toString();
        let month2 = (inputdate2.getMonth() + 1).toString();
        let year2 = inputdate2.getFullYear().toString();
        let d2 = year2 + "-" + month2 + "-" + day2
        
        let SQL = `SELECT queue.branch_id,
                branch.name AS branch_name,
                
            (SELECT IFNULL(AVG(queue.feedback_branch),0)
                FROM queue
                WHERE branch.branch_id = queue.branch_id
                AND NOT(queue.stempfeedback IS NULL OR queue.stempfeedback = 0)) AS feedbackbranch,
                
            (SELECT IFNULL(AVG(queue_detail.feedback_barber),0)
                FROM queue_detail
                LEFT JOIN queue ON queue_detail.queue_id = queue.queue_id
                WHERE queue.branch_id = branch.branch_id
                AND NOT(queue.stempfeedback IS NULL OR queue.stempfeedback = 0)) AS feedbackBarber,
                
            (SELECT IFNULL(AVG(queue_detail.feedback_service),0)
                FROM queue_detail
                LEFT JOIN queue ON queue_detail.queue_id = queue.queue_id
                WHERE queue.branch_id = branch.branch_id
                AND NOT(queue.stempfeedback IS NULL OR queue.stempfeedback = 0)) AS feedbackService
                
                FROM queue
                LEFT JOIN branch ON queue.branch_id = branch.branch_id
                WHERE queue.queue_dt BETWEEN '${d1}' AND '${d2}'
                GROUP BY branch.name ORDER BY CONVERT (branch_name USING tis620)`;

        // let SQL = `SELECT branch.branch_id,
        //             branch.name AS branch_name,
                    
        //         (SELECT IFNULL(AVG(queue.feedback_branch),0)
        //             FROM queue
        //             WHERE branch.branch_id = queue.branch_id
        //             AND queue.queue_dt BETWEEN '${d1}' AND '${d2}'
        //             AND NOT (queue.stempfeedback IS NULL OR queue.stempfeedback = 0)) AS feedbackbranch,
                    
        //         (SELECT IFNULL(AVG(queue_detail.feedback_barber),0)
        //             FROM queue_detail
        //             LEFT JOIN queue ON queue_detail.queue_id = queue.queue_id
        //             WHERE queue.queue_dt BETWEEN '${d1}' AND '${d2}'
        //             AND queue.branch_id = branch.branch_id
        //             AND NOT(queue.stempfeedback IS NULL OR queue.stempfeedback = 0)) AS feedbackBarber,
                    
        //         (SELECT IFNULL(AVG(queue_detail.feedback_service),0)
        //             FROM queue_detail
        //             LEFT JOIN queue ON queue_detail.queue_id = queue.queue_id
        //             WHERE queue.queue_dt BETWEEN '${d1}' AND '${d2}'
        //             AND queue.branch_id = branch.branch_id
        //             AND NOT(queue.stempfeedback IS NULL OR queue.stempfeedback = 0)) AS feedbackService
                    
        //     FROM branch
        //     LEFT JOIN queue ON branch.branch_id = queue.branch_id
        //     GROUP BY branch.name
        //     ORDER BY CONVERT (branch_name USING tis620)`;
        return connectDatabase.query(SQL, callback);
    }
}
module.exports = feedbackbranch;