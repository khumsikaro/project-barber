var connectDatabase = require('../../../../Database/connectdb');
var feedbackbranchAsbarber = {
    feedbackbranchAsbarber: function ( date1, date2, callback) {
        let inputdate1 = new Date(date1)
        let day1 = inputdate1.getDate().toString();
        let month1 = (inputdate1.getMonth() + 1).toString();
        let year1 = inputdate1.getFullYear().toString();
        let d1 = year1 + "-" + month1 + "-" + day1
        let inputdate2 = new Date(date2)
        let day2 = inputdate2.getDate().toString();
        let month2 = (inputdate2.getMonth() + 1).toString();
        let year2 = inputdate2.getFullYear().toString();
        let d2 = year2 + "-" + month2 + "-" + day2

        let SQL = `SELECT branch.name AS Branch,
                    (SELECT IFNULL(AVG(queue_detail.feedback_barber),0)
                     FROM queue_detail
                     LEFT JOIN queue ON queue_detail.queue_id = queue.queue_id
                     WHERE queue.queue_dt BETWEEN '${d1}' AND '${d2}'
                      AND queue.branch_id = branch.branch_id
                      AND NOT(queue.stempfeedback IS NULL OR queue.stempfeedback = 0)) AS feedbackBarber                                              
                    FROM branch
                    LEFT JOIN queue ON queue.branch_id = branch.branch_id
                    GROUP BY branch.name
                    ORDER BY CONVERT (Branch USING tis620)`;
        return connectDatabase.query(SQL, callback);
    }
}
module.exports = feedbackbranchAsbarber;