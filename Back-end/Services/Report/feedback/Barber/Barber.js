var connectDatabase = require('../../../../Database/connectdb');
var BarberFeedbackReport = {
    feedbackBarber: function (date1,date2,branch_id, callback) {
        let inputdate1 = new Date(date1)
        let day1 = inputdate1.getDate().toString();
        let month1 = (inputdate1.getMonth() + 1).toString();
        let year1 = inputdate1.getFullYear().toString();
        let d1 = year1 + "-" + month1 + "-" + day1
        
        let inputdate2 = new Date(date2)
        let day2 = inputdate2.getDate().toString();
        let month2 = (inputdate2.getMonth() + 1).toString();
        let year2 = inputdate2.getFullYear().toString();
        let d2 = year2 + "-" + month2 + "-" + day2

        // let SQL =`SELECT user.nickname,user.username,

        // (SELECT IFNULL(AVG(queue_detail.feedback_barber),0)FROM queue_detail
        // LEFT JOIN queue ON queue_detail.queue_id = queue.queue_id
        // WHERE queue_detail.barber_id = user.username
        // AND queue.queue_dt BETWEEN '${d1}' AND '${d2}'
        // AND queue.branch_id = '${branch_id}'
        // AND NOT(queue.stempfeedback IS NULL OR queue.stempfeedback = 0))  AS FeedBarber, 
           
        // (SELECT IFNULL(AVG(queue_detail.feedback_service),0)FROM queue_detail
        // LEFT JOIN queue ON queue_detail.queue_id = queue.queue_id
        // WHERE queue_detail.barber_id = user.username
        // AND queue.queue_dt BETWEEN '${d1}' AND '${d2}'
        // AND queue.branch_id = '${branch_id}'
        // AND NOT(queue.stempfeedback IS NULL OR queue.stempfeedback = 0)) AS feedService
           
        // FROM user
        // LEFT JOIN queue ON user.branch_id = queue.branch_id
        // LEFT JOIN queue_detail ON user.username = queue_detail.barber_id
        // WHERE queue.branch_id = '${branch_id}'AND user.level_barberid in(2,3)
        // GROUP BY user.nickname ORDER BY CONVERT (user.nickname USING tis620)`;
        let SQL = `SELECT user.nickname,user.username,
        AVG(queue_detail.feedback_barber)  AS FeedBarber, 
       AVG(queue_detail.feedback_service) AS feedService
      FROM queue
      LEFT JOIN queue_detail ON queue.queue_id = queue_detail.queue_id
      LEFT JOIN user ON queue_detail.barber_id = user.username
      LEFT JOIN branch ON queue.branch_id = branch.branch_id
      WHERE queue.queue_dt BETWEEN '${d1}' AND '${d2}'  
      AND branch.branch_id = '${branch_id}'
      AND NOT (queue.stempfeedback IS NULL OR queue.stempfeedback = 0)
      AND user.username IS NOT NULL
      GROUP BY user.nickname ORDER BY CONVERT (user.nickname USING tis620)`;
        return connectDatabase.query(SQL, callback);
    }
}
module.exports = BarberFeedbackReport;