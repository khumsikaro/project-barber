var connectDatabase = require('../../../../Database/connectdb');
var BarberFeedbackReportDetail = {
    feedbackDetailBarber: function (date1,date2,barber_id,branch_id, callback) {
        let inputdate1 = new Date(date1)
        let day1 = inputdate1.getDate().toString();
        let month1 = (inputdate1.getMonth() + 1).toString();
        let year1 = inputdate1.getFullYear().toString();
        let d1 = year1 + "-" + month1 + "-" + day1
        
        let inputdate2 = new Date(date2)
        let day2 = inputdate2.getDate().toString();
        let month2 = (inputdate2.getMonth() + 1).toString();
        let year2 = inputdate2.getFullYear().toString();
        let d2 = year2 + "-" + month2 + "-" + day2
        
        let SQL = `SELECT
        queue.queue_dt,
        queue.dateBranch,
        (SELECT user.firstname FROM user WHERE user.username = queue.customer_id) AS firstname,
        service.name,
        queue_detail.feedback_service AS feedBarber 
        FROM queue
        LEFT JOIN queue_detail ON queue.queue_id = queue_detail.queue_id
        LEFT JOIN service ON queue_detail.service_id = service.name
        LEFT JOIN user ON queue_detail.barber_id = user.username
        LEFT JOIN branch ON queue.branch_id = branch.branch_id
        WHERE queue.queue_dt BETWEEN '${d1}' AND '${d2}' AND queue_detail.barber_id = '${barber_id}'
        AND queue.branch_id = '${branch_id}'
        AND NOT (queue.stempfeedback IS NULL OR queue.stempfeedback = 0)
        ORDER BY queue.queue_dt ASC`;
        return connectDatabase.query(SQL, callback);
    }
}
module.exports = BarberFeedbackReportDetail;