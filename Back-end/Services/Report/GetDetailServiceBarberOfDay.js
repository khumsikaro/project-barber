var connectDatabase = require('../../Database/connectdb');
var TimeAndJobBarber = {
    report: function (date,username, callback) {
        let inputdate = new Date(date)
        let day = inputdate.getDate().toString();
        let month = (inputdate.getMonth() + 1).toString();
        let year = inputdate.getFullYear().toString();
        let d1 = year + "-" + month + "-" + day
        
        let SQL = `SELECT 
        queue_detail.start_dt,
        queue_detail.finish_dt,
        queue_detail.service_id,
        ((SELECT service.service_charge FROM service WHERE service.name = queue_detail.service_id) + (queue_detail.add_money)) AS money
        FROM queue
        LEFT JOIN queue_detail ON queue.queue_id = queue_detail.queue_id
        LEFT JOIN user ON queue.customer_id = user.username
        WHERE queue.queue_dt = '${d1}' AND queue_detail.barber_id = '${username}' AND queue.status_pay = 1`;
        return connectDatabase.query(SQL, callback);
    }
}
module.exports = TimeAndJobBarber;