var connectDatabase = require('../../Database/connectdb');
var GetChangeBarberReport = {
    GetChangeBarber: function (data, callback) {
        let date = data.date
        let branch_id = data.branch_id
        let month = date.length == 6 ? date.substring(0,1) : date.substring(0,2);
        let year = date.length == 6 ? date.substring(2,6) : date.substring(3,7);
        let SQL = `SELECT 
                    queue.queue_dt,
                    QL.service_id,
                    user.nickname AS NickNews,
                    (SELECT user.nickname FROM user WHERE user.username = QL.change_ber) AS nickOld,
                    QL.detail_change_ber
                    FROM queue_detail QL
                    LEFT JOIN user ON QL.barber_id = user.username
                    LEFT JOIN queue ON QL.queue_id = queue.queue_id
                    WHERE queue.branch_id = '${branch_id}'
                    AND YEAR(queue.queue_dt) = '${year}'
                    AND MONTH(queue.queue_dt) = '${month}'
                    AND QL.change_ber IS NOT NULL`;
        return connectDatabase.query(SQL, callback);
    }
}
module.exports = GetChangeBarberReport;