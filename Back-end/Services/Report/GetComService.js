var connectDatabase = require('../../Database/connectdb');
var CancelServiceDetail = {
    GetComService: function (data, callback) {
        let date = data.date
        let username = data.username
        let year = date.substring(0, 4);

        let SQL = `SELECT *
                    FROM queue
                    WHERE cancel_queue = 1 
                    AND customer_id = '${username}' 
                    AND YEAR(queue_dt) = '${year}'`;
        return connectDatabase.query(SQL, callback);
    },
};
module.exports = CancelServiceDetail;