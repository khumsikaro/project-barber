var connectDatabase = require('../../Database/connectdb');
var GetSalaryBarber = {
    GetPort: function (data, callback) {
        let date = data.date
        let branch_id = data.branch_id
        let month = date.length == 6 ? date.substring(0,1) : date.substring(0,2);
        let year = date.length == 6 ? date.substring(2,6) : date.substring(3,7);
        let SQL = `CAll REPORT_SUMMARY_MONEY_EACH_BARBER('${month}','${year}','${branch_id}')`;
        return connectDatabase.query(SQL,callback);
    }
}
module.exports = GetSalaryBarber;