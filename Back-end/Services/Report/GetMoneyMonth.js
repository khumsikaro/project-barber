var connectDatabase = require('../../Database/connectdb');
var GetMoneyToMonth = {
    MoneyMonth: function (date, callback) {
        let month = date.length == 6 ? date.substring(0,1) : date.substring(0,2);
        let year = date.length == 6 ? date.substring(2,6) : date.substring(3,7);
        
        let SQL = `SELECT	branch.name AS Branch,

		(SELECT IFNULL(SUM(queue.total_money),0)
         FROM queue 
         WHERE queue.branch_id = branch.branch_id
        	AND MONTH(queue.queue_dt) = '${month}'
        	AND YEAR(queue.queue_dt) = '${year}'
        	AND queue.status_confirm = 1
        	AND queue.status_come = 1
        	AND queue.status_pay = 1)AS MoneytoMonth
        
        FROM branch
        LEFT JOIN queue ON branch.branch_id = queue.branch_id
        GROUP BY branch.name ORDER BY branch.branch_id ASC`;
        // let SQL = `SELECT
        // branch.name AS Branch,SUM(queue.total_money)AS MoneytoMonth
        // FROM queue
        // LEFT JOIN branch ON queue.branch_id = branch.branch_id
        // WHERE 
        //    MONTH(queue.queue_dt) = '${month}'
        // AND YEAR(queue.queue_dt) = '${year}'
        // AND queue.status_pay = 1
        // AND queue.status_confirm = 1
        // AND queue.status_come = 1
        // GROUP BY queue.branch_id`;
        return connectDatabase.query(SQL, callback);
    }

}
module.exports = GetMoneyToMonth;