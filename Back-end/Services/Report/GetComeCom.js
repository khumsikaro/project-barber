var connectDatabase = require('../../Database/connectdb');
var CancelService = {
    GetComeCom: function (data, callback) {
        let date = data.date
        let branch_id = data.branch_id
        let year = date.substring(0, 4);
        
        let SQL = `SELECT
                    user.username, 
                    user.firstname,
                    user.lastname,
                    (SELECT COUNT(cancel_queue)FROM queue WHERE queue.cancel_queue = 1 AND queue.customer_id = user.username)AS count
                    FROM queue
                    LEFT JOIN user ON queue.customer_id = user.username
                    WHERE YEAR(queue.queue_dt) = '${year}' 
                    AND queue.branch_id = '${branch_id}'
                    AND  NOT queue.cancel_queue = 0
                    AND user.level_barberid = 4
                    GROUP BY queue.customer_id
                    ORDER BY count DESC`;
        return connectDatabase.query(SQL, callback);
    }
}
module.exports = CancelService;