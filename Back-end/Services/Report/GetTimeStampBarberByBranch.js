var connectDatabase = require('../../Database/connectdb');
var GetTimeStampBarberByBranchReport = {
    GetTimeStamp: function (data, callback) {
        let date = data.date
        let branch_id = data.branch_id
        let month = date.length == 6 ? date.substring(0,1) : date.substring(0,2);
        let year = date.length == 6 ? date.substring(2,6) : date.substring(3,7);
        // let SQL = `SELECT
        //             user.username, 
        //             USER.nickname,
        //             COUNT(barber_id) AS count,

        //             (SELECT SUM(leaves.total_day) FROM leaves WHERE 
        //             MONTH(leaves.date_start) = '${month}'
        //             AND YEAR(leaves.date_start) = '${year}'
        //             AND MONTH(leaves.date_stop) = '${month}'
        //             AND YEAR(leaves.date_stop) = '${year}'
        //             AND leaves.barber_id = user.username)AS leavess
        //             FROM check_work 
        //             LEFT JOIN user ON  check_work.barber_id = user.username
        //             WHERE MONTH(check_work.date) = '${month}' 
        //             AND YEAR(check_work.date) = '${year}'
        //             AND user.branch_id = '${branch_id}' 
        //             AND (SELECT TIME(start_work) > (SELECT chack_in FROM setting_bar)) GROUP BY barber_id`;
        let SQL = `CALL GetTimeStampBarBer('${month}', '${year}', '${branch_id}')`;
        console.log(SQL);
        
        return connectDatabase.query(SQL, callback);
    }
}
module.exports = GetTimeStampBarberByBranchReport;