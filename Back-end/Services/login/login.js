var connectDatabase = require('../../Database/connectdb');
var Login = {
    Login: function (data, callback) {
        var username = data.username;
        var password = data.password;
        var selectSQL = `SELECT
        user.username,
        user.img_profile,
        user.firstname,
        user.lastname,
        user.Email,
        user.status,
        user.level_barberid AS levelUsers,
        user.branch_id,
        user.status_login,
        branch.name AS branchname
        FROM user
        LEFT JOIN branch on user.branch_id = branch.branch_id
        WHERE user.username=? AND user.password=?`;

        return connectDatabase.query(selectSQL, [username,password], callback);

    },
}; module.exports = Login;