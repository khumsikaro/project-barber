var connectDatabase = require('../../Database/connectdb');
var GetAbout = {
    GetAbout: function (data, callback) {
        let SQL = "SELECT * FROM about_us ORDER BY about_id ASC";
        return connectDatabase.query(SQL,callback);
    }
}
module.exports = GetAbout;