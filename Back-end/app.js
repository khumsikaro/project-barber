var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors=require('cors');
var bodyParser = require('body-parser');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
// Login
var Login = require('./Controllers/login/login');

//select
var News = require('./Controllers/Information/News/News');
var Promotion = require('./Controllers/Information/Promotion/Promotion');
var GetInformation = require('./Controllers/Information/GetInformation');
var InformationDetail = require('./Controllers/Information/InformationDetail');
var ShowListService = require('./Controllers/ServiceList/GetService');
var ServiceDetail = require('./Controllers/ServiceList/ServiceDetail');
var ShowListBranch = require('./Controllers/Branch/ShowListBranch');
var Getprovince = require('./Controllers/Branch/Getprovince');
var GetAmphur = require('./Controllers/Branch/GetAmphur');
var GetDistrict = require('./Controllers/Branch/GetDistrict');
var GetZipcode = require('./Controllers/Branch/GetZipcode');
var BranchDetail = require('./Controllers/Branch/BranchDetail');
var ShowUsers = require('./Controllers/Users/ShowUsers');
var UsersDetail = require('./Controllers/Users/UsersDetail');
var getlastServiceid = require('./Controllers/ServiceList/getlastServiceid');
var uploadServicefile = require('./Controllers/ServiceList/uploadServicefile');
var GetSettingSalary = require('./Controllers/Setting/GetSettingSalary');
var GetSettingbarber = require('./Controllers/Setting/getbaeber');
var Getleveltype = require('./Controllers/Setting/Setting_leveltype/Getleveltype');
var GetAbout = require('./Controllers/about_us/GetAbout');
var ShowListBarber = require('./Controllers/BarBer/ShowListBarber');
var ShowListBarberbranch = require('./Controllers/BarBer/ShowListBarberBranch');
var DetailBarber = require('./Controllers/BarBer/GetDetailbarber');
var Detailskills = require('./Controllers/Users/ShowSkills');
var GetExpensesBar = require('./Controllers/BarBer/expenses_bar/GetExpenses');
var GetHolidayList = require('./Controllers/BarBer/Bye_Barber/Get_L_T');
var ShowLeaveList = require('./Controllers/BarBer/Bye_Barber/GetL');
var GetCheck = require('./Controllers/BarBer/CheckWork/Get');
var GetQueue = require('./Controllers/queue/Getqueue');
var GetHolidayBarber = require('./Controllers/BarBer/Bye_Barber/leavebarber/Get');
var CheckSkill = require('./Controllers/queue/SetQueue');
var GetQuestion = require('./Controllers/Information/Query/Get');
var email = require('./Controllers/Information/Query/GetEmail');
var GetPassword = require('./Controllers/Users/Getpass');
var GetQueueConFirm = require('./Controllers/queue/status_confirm/Get_QConfrem');
var GetQueueCome = require('./Controllers/queue/status_come/Get');
var ServiceBarberChange = require('./Controllers/BarBer/SeBaChange');
var GetcountBarber = require('./Controllers/BarBer/ChangeBarber/GetcountBarber');
var GetQueueDetail = require('./Controllers/queue/status_come/GetQueueDetail');
var testza = require('./Controllers/test');
var SelectServiceBefor = require('./Controllers/BarBer/ChangeBarber/GetBarberAsService/SelectServiceBefor');
var SelectBarberBefor = require('./Controllers/BarBer/ChangeBarber/GetBarberAsService/SelectBarberBefor');
var GetQueuePay = require('./Controllers/Users/expenses_customer/GetQueuePay');
var UpdateQueueDetail = require('./Controllers/Users/expenses_customer/UpdaQueueDetail');
var UpdateStatusPay = require('./Controllers/Users/expenses_customer/UpdateStatusPay');
// var GetPointCustomer = require('./Controllers/Users/expenses_customer/Getpoint');
// var GetServiceCharge = require('./Controllers/Users/expenses_customer/GetServiCharge');
var HistroryList = require('./Controllers/Users/HistroryListUser/HistoryListUser');
var HistroryListDetail = require('./Controllers/Users/HistroryListUser/HistroryListUserDetail');
var GetStatusQueueUser = require('./Controllers/Users/StatusQueueUser/GetStatusQuUser');
var CheckQueuePrivateBarber = require('./Controllers/BarBer/CheckQuePriBarber/Get');
var CheckHolidayPrivateBarber = require('./Controllers/BarBer/Bye_Barber/HolidayPrivate/HolidayPrivateBarber');
var GetSalaryBarber = require('./Controllers/Report/GetReportBig');
var ListBarberBranch = require('./Controllers/BarBer/Bye_Barber/HolidayPrivate/ListBarberBra');
var GetListCheckWork = require('./Controllers/BarBer/CheckWork/ListCheckWork');
var PriatveSalaryBarber = require('./Controllers/Report/GetPeportBarber');
var CancelService = require('./Controllers/Report/GetComeCom');
var CancelServiceDetail = require('./Controllers/Report/GetComService');
var TimeAndJobBarber = require('./Controllers/Report/GetDetailServiceBarberOfDay');
var GetMoneyToMonth = require('./Controllers/Report/GetMoneyMonth');
var GetChangeBarberReport = require('./Controllers/Report/GetChangeBarber');
var GetTimeStampBarberByBranchReport = require('./Controllers/Report/GetTimeStampBarberByBranch');
var BranchFeedbackReport = require('./Controllers/Report/feedback/branch/branch');
var BranchFeedbackReportDetail = require('./Controllers/Report/feedback/branch/DetailBranch');
var ServiceFeedbackReport = require('./Controllers/Report/feedback/Servicess/Service');
var ServiceFeedbackReportDetail = require('./Controllers/Report/feedback/Servicess/ServiceDetail');
var BarberFeedbackReport = require('./Controllers/Report/feedback/Barber/Barber');
var BarberFeedbackReportDetail = require('./Controllers/Report/feedback/Barber/BarberDetail');
var PointDetailUser = require('./Controllers/Users/GetPointDetailUser');
var ShowListBarberChackwork = require('./Controllers/BarBer/ShowListBarberChackW');
var GetLate = require('./Controllers/BarBer/Bye_Barber/Late/GetLate');
var GetLeavesDetail = require('./Controllers/BarBer/Bye_Barber/HolidayPrivate/LeavesDetail');
// var feedbackbranchAsbarber = require('./Controllers/Report/feedback/branch/branchAsbarber');
// var feedbackbranchAsService = require('./Controllers/Report/feedback/branch/branchAsService');

//#region จองคิว
var ChageService = require('./Controllers/queue/ChageService');
var ChangeBarberQ = require('./Controllers/queue/ChangeBarber');
var ChangeTimes = require('./Controllers/queue/ChangeTimes');
var DeleteQueueDetail = require('./Controllers/queue/status_confirm/DeleteQueueDetail');
//#endregion
// Search
var SearchInformation = require('./Controllers/Information/search');
var SearchBranch = require('./Controllers/Branch/SearchBranch');
var SearchService = require('./Controllers/ServiceList/SearchService');
var SearchUsers = require('./Controllers/Users/SerachUsers');
var getlastid = require('./Controllers/Information/getlastid');
var Getlastbranchid = require('./Controllers/Branch/Getlastid');
var Getpoint = require('./Controllers/Setting/settingpoint/Getpoint');
var SearchExpenses = require('./Controllers/BarBer/expenses_bar/search');
var SearchLeaveList = require('./Controllers/BarBer/Bye_Barber/Search');
var searchStatusQueueUser = require('./Controllers/Users/StatusQueueUser/Search');
var searchQueuePrivateBarber = require('./Controllers/BarBer/CheckQuePriBarber/search');
var SearchHistoryCustomer = require('./Controllers/BarBer/Bye_Barber/HolidayPrivate/SearchHistoryCustomer');

// add
var addInformation = require('./Controllers/Information/addnewspro');
var AddService = require('./Controllers/ServiceList/addService');
var AddBranch = require('./Controllers/Branch/AddBranch');
var AddUser = require('./Controllers/Users/AddUsers');
var Addpoint = require('./Controllers/Setting/settingpoint/Addpoint');
var AddHoliday = require('./Controllers/Setting/Setting_leveltype/AddHoliday');
var AddAbout = require('./Controllers/about_us/AddAbout');
var AddExpenses = require('./Controllers/BarBer/expenses_bar/Add');
var AddLeave = require('./Controllers/BarBer/Bye_Barber/AddL');
var AddQueue = require('./Controllers/queue/Add');
var SendInformation = require('./Controllers/Information/Query/AddQuery');
var AddBarberInQueue = require('./Controllers/BarBer/ChangeBarber/AddBarberInQueue');

// Update
var UpdateInformation = require('./Controllers/Information/Updatenews');
var UpdateService = require('./Controllers/ServiceList/UpService');
var UpdateBranch = require('./Controllers/Branch/UpdateBranch');
var EditProfile = require('./Controllers/Users/UpdateUsers');
var uploadfile = require('./Controllers/Information/uploadfile');
var UpfileBranch = require('./Controllers/Branch/Upfile');
var UpdateSettingSaraly = require('./Controllers/Setting/UpdateSettingSaraly');
var UpdateSettingbarber = require('./Controllers/Setting/Upsettingber');
var Upleveltype = require('./Controllers/Setting/Setting_leveltype/Upleveltype');
var Uppoint = require('./Controllers/Setting/settingpoint/Uppoint');
var UpdateAbout = require('./Controllers/about_us/UpdateAbout');
var UpDataExpenses = require('./Controllers/BarBer/expenses_bar/UpDate');
var UpdateLeave = require('./Controllers/BarBer/Bye_Barber/UpDateL');
var UpdateCheckWork = require('./Controllers/BarBer/CheckWork/Update');
var EditInformation  = require('./Controllers/Information/Query/updata');
var Editpassword = require('./Controllers/Users/Uppass');
var EditConfirmQueue = require('./Controllers/queue/status_confirm/EditConfirm');
var ComeQueue = require('./Controllers/queue/status_come/Edit');
var ChangeBarber = require('./Controllers/BarBer/ChangeBarber/EditChangeBarber');
var UpdateUser = require('./Controllers/Users/Updatebarber');
var SendFeedback = require('./Controllers/queue/Feedback/Edit');
var CancelQueue = require('./Controllers/Users/StatusQueueUser/CancelQueue');
var SendAWOL = require('./Controllers/BarBer/expenses_bar/SendAwol');
var UpdateStatusLogin = require('./Controllers/login/UPStatusLogin');

// Delete
var DeleteInformation = require('./Controllers/Information/Deletenews');
var DeleteService = require('./Controllers/ServiceList/DeleteService');
var DeleteBranch = require('./Controllers/Branch/DeleteBranch');
var DeleteUser = require('./Controllers/Users/DeleteUsers');
var DeleteHoliday = require('./Controllers/Setting/Setting_leveltype/DeleteHoliday');
var Deletepoint = require('./Controllers/Setting/settingpoint/Deletepoint');
var DeleteAbout = require('./Controllers/about_us/DeleteAbout');
var DeleteExpenses = require('./Controllers/BarBer/expenses_bar/DeleteEx');
var DeleteLeave = require('./Controllers/BarBer/Bye_Barber/Delete');

//Uploage
var testUpload = require('./Controllers/testImage');

var app = express();

// view engine setup 555
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/', indexRouter);
app.use('/users', usersRouter);
// Login
app.use('/Login',Login);
//Upload
app.use('/testUpload',testUpload);

// select
app.use('/GetNews',News);
app.use('/GetPromotion',Promotion);
app.use('/GetInformation',GetInformation);
app.use('/GetInformationDetail',InformationDetail);
app.use('/ShowListService',ShowListService);
app.use('/ServiceDetail',ServiceDetail);
app.use('/ShowListBranch',ShowListBranch );
app.use('/GetProvince',Getprovince);
app.use('/GetAmphur',GetAmphur);
app.use('/GetDistrict',GetDistrict);
app.use('/GetZipcode',GetZipcode);
app.use('/BranchDetail',BranchDetail);
app.use('/ShowUsers',ShowUsers);
app.use('/UsersDetail',UsersDetail);
app.use('/getlastid',getlastid);
app.use('/Getlastbranchid',Getlastbranchid);
app.use('/getlastServiceid',getlastServiceid);
app.use('/GetSettingbarber',GetSettingbarber);
app.use('/GetSettingSalary',GetSettingSalary);
app.use('/Getleveltype',Getleveltype);
app.use('/Getpoint',Getpoint);
app.use('/GetAbout',GetAbout);
app.use('/ShowListBarber',ShowListBarber);
app.use('/ShowListBarberbranch',ShowListBarberbranch);
app.use('/DetailBarber',DetailBarber);
app.use('/Detailskills',Detailskills);
app.use('/GetExpensesBar',GetExpensesBar);
app.use('/GetHolidayList',GetHolidayList);
app.use('/ShowLeaveList',ShowLeaveList);
app.use('/GetCheck',GetCheck);
app.use('/GetQueue',GetQueue);
app.use('/GetHolidayBarber',GetHolidayBarber);
app.use('/CheckSkill',CheckSkill);
app.use('/GetQuestion',GetQuestion);
app.use('/Email',email);
app.use('/GetPassword',GetPassword);
app.use('/GetQueueConFirm',GetQueueConFirm);
app.use('/GetQueueCome',GetQueueCome);
app.use('/ServiceBarberChange',ServiceBarberChange);
app.use('/GetcountBarber',GetcountBarber);
app.use('/GetQueueDetail',GetQueueDetail);
app.use('/testza',testza);
app.use('/SelectServiceBefor',SelectServiceBefor);
app.use('/SelectBarberBefor',SelectBarberBefor);
app.use('/GetQueuePay',GetQueuePay);
// app.use('/GetPointCustomer',GetPointCustomer);
// app.use('/GetServiceCharge',GetServiceCharge);
app.use('/HistroryList',HistroryList);
app.use('/HistroryListDetail',HistroryListDetail);
app.use('/GetStatusQueueUser',GetStatusQueueUser);
app.use('/CheckQueuePrivateBarber',CheckQueuePrivateBarber);
app.use('/CheckHolidayPrivateBarber',CheckHolidayPrivateBarber);
app.use('/GetSalaryBarber',GetSalaryBarber);
app.use('/ListBarberBranch',ListBarberBranch);
app.use('/GetListCheckWork',GetListCheckWork);
app.use('/PriatveSalaryBarber',PriatveSalaryBarber);
app.use('/CancelService',CancelService);
app.use('/CancelServiceDetail',CancelServiceDetail);
app.use('/TimeAndJobBarber',TimeAndJobBarber);
app.use('/GetMoneyToMonth',GetMoneyToMonth);
app.use('/GetChangeBarberReport',GetChangeBarberReport);
app.use('/GetTimeStampBarberByBranchReport',GetTimeStampBarberByBranchReport);
app.use('/BranchFeedbackReport',BranchFeedbackReport);
app.use('/BranchFeedbackReportDetail',BranchFeedbackReportDetail);
app.use('/ServiceFeedbackReport',ServiceFeedbackReport);
app.use('/ServiceFeedbackReportDetail',ServiceFeedbackReportDetail);
app.use('/BarberFeedbackReport',BarberFeedbackReport);
app.use('/BarberFeedbackReportDetail',BarberFeedbackReportDetail);
app.use('/PointDetailUser',PointDetailUser);
app.use('/ShowListBarberChackwork',ShowListBarberChackwork);
app.use('/GetLate',GetLate);
app.use('/GetLeavesDetail',GetLeavesDetail);
// app.use('/feedbackbranchAsbarber',feedbackbranchAsbarber);
// app.use('/feedbackbranchAsService',feedbackbranchAsService);

// search
app.use('/SearchInformation',SearchInformation);
app.use('/SearchBranch',SearchBranch);
app.use('/SearchService',SearchService);
app.use('/SearchUsers',SearchUsers);
app.use('/SearchExpenses',SearchExpenses);
app.use('/SearchLeaveList',SearchLeaveList);
app.use('/searchStatusQueueUser',searchStatusQueueUser);
app.use('/searchQueuePrivateBarber',searchQueuePrivateBarber);
app.use('/SearchHistoryCustomer',SearchHistoryCustomer);

// add
app.use('/AddInformation',addInformation);
app.use('/AddService',AddService);
app.use('/AddBranch',AddBranch);
app.use('/AddUser',AddUser);
app.use('/Addpoint',Addpoint);
app.use('/AddHoliday',AddHoliday);
app.use('/AddAbout',AddAbout);
app.use('/AddExpenses',AddExpenses);
app.use('/AddLeave',AddLeave);
app.use('/AddQueue',AddQueue);
app.use('/SendInformation',SendInformation);
app.use('/AddBarberInQueue',AddBarberInQueue);

// UPdate
app.use('/UpdateInformation',UpdateInformation);
app.use('/UpdateService',UpdateService);
app.use('/UpdateBranch',UpdateBranch);
app.use('/EditProfile',EditProfile);
app.use('/uploadfile',uploadfile);
app.use('/UpfileBranch',UpfileBranch);
app.use('/uploadServicefile',uploadServicefile);
app.use('/UpdateSettingbarber',UpdateSettingbarber);
app.use('/UpdateSettingSaraly',UpdateSettingSaraly);
app.use('/Upleveltype',Upleveltype);
app.use('/Uppoint',Uppoint);
app.use('/UpdateAbout',UpdateAbout);
app.use('/UpDataExpenses',UpDataExpenses);
app.use('/UpdateLeave',UpdateLeave);
app.use('/UpdateCheckWork',UpdateCheckWork);
app.use('/EditInformation',EditInformation);
app.use('/Editpassword',Editpassword);
app.use('/EditConfirmQueue',EditConfirmQueue);
app.use('/ComeQueue',ComeQueue);
app.use('/ChangeBarber',ChangeBarber);
app.use('/UpdateQueueDetail',UpdateQueueDetail);
app.use('/UpdateStatusPay',UpdateStatusPay);
app.use('/UpdateUser',UpdateUser);
app.use('/SendFeedback',SendFeedback);
app.use('/CancelQueue',CancelQueue);
app.use('/SendAWOL',SendAWOL);
app.use('/UpdateStatusLogin',UpdateStatusLogin);

// Delete
app.use('/DeleteInformation',DeleteInformation);
app.use('/DeleteService',DeleteService);
app.use('/DeleteBranch',DeleteBranch);
app.use('/DeleteUser',DeleteUser);
app.use('/DeleteHoliday',DeleteHoliday);
app.use('/Deletepoint',Deletepoint);
app.use('/DeleteAbout',DeleteAbout);
app.use('/DeleteExpenses',DeleteExpenses);
app.use('/DeleteLeave',DeleteLeave);
app.use('/DeleteQueueDetail',DeleteQueueDetail);

//#region จองคิว
app.use('/ChageService',ChageService);
app.use('/ChangeBarber',ChangeBarberQ);
app.use('/ChangeTimes',ChangeTimes);
//#endregion
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
