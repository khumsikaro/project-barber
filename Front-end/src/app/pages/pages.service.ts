import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { urlServer } from "../URL"

@Injectable({
  providedIn: 'root'
})
export class PagesService {

  constructor(private http: HttpClient) { }

  public SendInformation(data) {
    return this.http.post(urlServer.ipServer + `SendInformation`, data)
  }

  public checkPassword(username) {
    return this.http.get(urlServer.ipServer + `Getpassword/${username}`)
  }
  public ChangePassword(data){
    return this.http.put(urlServer.ipServer+`Editpassword`,data)
  }
}
