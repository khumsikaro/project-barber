import { Component, OnInit } from '@angular/core';
import { NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { LoginService } from '../login/login.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PagesService } from '../pages.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  USER;
  ChangePasswordForm: FormGroup;
  constructor(private modalService: NzModalService,
    private session: LoginService,
    private service: PagesService,
    private notification: NzNotificationService,
    private fb: FormBuilder, ) { }

  ngOnInit() {
    this.ChangePasswordForm = this.fb.group({
      OldPassword: [null, [Validators.required]],
      NewPassword: [null, [Validators.required]],
      ConfirmNewPassword: [null, [Validators.required]]
    });
    this.USER = this.session.getActiveUser();
  }

  ConfirmChangePassword() {
    for (const i in this.ChangePasswordForm.controls) {
      this.ChangePasswordForm.controls[i].markAsDirty();
      this.ChangePasswordForm.controls[i].updateValueAndValidity();
    }
    if(this.ChangePasswordForm.value.OldPassword != null && this.ChangePasswordForm.value.NewPassword!= null && this.ChangePasswordForm.value.ConfirmNewPassword!= null){
    this.service.checkPassword(this.USER.username).subscribe(
      (res) => {
        if (this.ChangePasswordForm.value.OldPassword === atob(res[0].password)) {
          if (this.ChangePasswordForm.value.NewPassword === this.ChangePasswordForm.value.ConfirmNewPassword) {
            let value = { username: this.USER.username, password: btoa(this.ChangePasswordForm.value.NewPassword) }
            this.service.ChangePassword(value).subscribe(
              (res) => {
                Swal.fire({
                  // type: 'success',
                  // title: 'OK',
                  html: '<p>เปลี่ยนรหัสผ่านเรียบร้อย</p>'
                }).then((result) => {
                  if (result.value) {
                    this.modalService.closeAll();
                  }
                })
              }
            )
          }
          else{
            Swal.fire({
              html: '<p>ใส่รหัสผ่านใหม่ไม่เหมือนกัน</p>',
            })
          }
        }
        else{
          Swal.fire({
            html: '<p>รหัสผ่านเก่าไม่ถูกต้องหรือใส่รหัสผ่านใหม่ไม่เหมือนกัน</p>',
          }).then((result) => {
            if (result.value) {
              this.modalService.closeAll();
            }
          })
        }
      }
    )
  }
  }
}
