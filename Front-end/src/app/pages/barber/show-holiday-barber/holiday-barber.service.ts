import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { urlServer } from '../../../URL'

@Injectable({
  providedIn: 'root'
})
export class HolidayBarberService {

  constructor(private http: HttpClient) { }

  public HolidayBarber(Branch_id,date) {
    return this.http.get(urlServer.ipServer + `GetHolidayBarber/?branch_id=${Branch_id}&date=${date}`);
  }

  public GetBranch() {
    return this.http.get(urlServer.ipServer + `ShowListBranch`)
  }
}
