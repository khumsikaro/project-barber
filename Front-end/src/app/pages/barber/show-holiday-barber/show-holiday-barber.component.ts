import { Component, OnInit } from '@angular/core';
import * as moment from 'moment'
// import swal from 'sweetalert';
import { HolidayBarberService } from './holiday-barber.service';
import { LoginService } from '../../login/login.service';
import {} from 'date-fns/difference_in_calendar_days';
import { DisabledTimeFn, DisabledTimePartial } from 'ng-zorro-antd/date-picker/standard-types';
import Swal from 'sweetalert2';
import {differenceInCalendarDays} from 'date-fns'
@Component({
  selector: 'app-show-holiday-barber',
  templateUrl: './show-holiday-barber.component.html',
  styleUrls: ['./show-holiday-barber.component.css']
})
export class ShowHolidayBarberComponent implements OnInit {
  criteriaDate=new Date();
  holidaydata:any;
  Branch_id;
  BranchList:any[];
  check=0;
  USER;
  disabledDate = (current: Date): boolean => {
    let today =new Date
    // Can not select days before today and today
    return differenceInCalendarDays(current, today) < 0;
  };
  constructor(private service:HolidayBarberService,private session: LoginService,) { }

  ngOnInit() {
    this.USER = this.session.getActiveUser();
    this.GetNameBranch()
    document.getElementById("content").classList.remove("p-0")
  }

  Listholidaybarber() {
    if(this.Branch_id ==undefined){
      Swal.fire({
        html: '<p>กรุณาเลือกสาขาก่อนกดปุ่มค้นหา!</p>',
      })
    }
    else{
      this.service.HolidayBarber(this.Branch_id,this.criteriaDate).subscribe(
        (res:any[]) => {
          if(res[0].length==0){
            Swal.fire({
              html: '<p>ไม่มีข้อมูลที่คุณต้องการค้นหา!</p>',
            })
            this.holidaydata=[];
          }
          else{
            this.holidaydata = res[0] as any[]
            this.check =1;
          }
          this.check = this.holidaydata.length
        }
      )
    }
  }
  GetNameBranch() {
    this.service.GetBranch().subscribe(
      (res) => {
        this.BranchList = res as any[];
        this.Branch_id =res[0].branch_id
        this. Listholidaybarber()
      }
    )
  }
}
