import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BarberComponent } from './barber.component';
import { ShowListBarberComponent } from './show-list-barber/show-list-barber.component';
import { ShowHolidayBarberComponent } from './show-holiday-barber/show-holiday-barber.component';
import { BarberTimeStampComponent } from './barber-time-stamp/barber-time-stamp.component';
import { BarberLeaveComponent } from './barber-leave/barber-leave.component';
import { BarberExpensesComponent } from './barber-expenses/barber-expenses.component';
import { GuardService } from '../login/guard.service';
import { ManagerGuardsService } from './manager-guards.service';
import { MasterGuardsService } from '../master/master-guards.service';


const routes: Routes = [
{
  path:'Barber',
  component:BarberComponent,
  children:[
    {
      path:'ShowListBarber',
      component:ShowListBarberComponent,
    },

    {
      path:'ShowHoliday',
      component:ShowHolidayBarberComponent,
    },
    {
      path:'BarberTimeStamp',
      component:BarberTimeStampComponent,
      canActivate:[ManagerGuardsService],
    },
    {
      path:'BarberLeave',
      component:BarberLeaveComponent,
      canActivate:[ManagerGuardsService],
    },
    {
      path:'BarberExpenses',
      component:BarberExpensesComponent,
      canActivate:[MasterGuardsService],
    },
  ]
},


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BarberRoutingModule { }
