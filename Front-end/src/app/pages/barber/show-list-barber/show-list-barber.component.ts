import { Component, OnInit } from '@angular/core';
import { BarberService } from '../barber.service';
import {urlServer} from '../../../URL'
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-show-list-barber',
  templateUrl: './show-list-barber.component.html',
  styleUrls: ['./show-list-barber.component.css']
})
export class ShowListBarberComponent implements OnInit {
  selectedValue ="";
  gridStyle = {
    width: '25%',
    textAlign: 'center'
  };
  ShowListBarberCome: any[];
  ShowListBarberNoCome:any[];
  BranchList: any[];
  constructor(private service: BarberService,private http:HttpClient) { }

  ShowBarberList() {
    this.http.get(urlServer.ipServer+`ShowListBarberChackwork`).subscribe(
      (res:any[]) => {
        
        let barberActive = res.filter(x=>x.active==="Y")
        let  barberstart_workNotnull =barberActive.filter(x=>x.start_work !=null)
        this.ShowListBarberCome =barberstart_workNotnull.filter(x=>x.status_awol==0)
        let barberstart_workNull = barberActive.filter(x=>x.start_work ==null)
        this.ShowListBarberNoCome = barberstart_workNull.filter(x=>x.status_awol==0)
      }
    )
  }
  GetNameBranch() {
    this.service.GetBranch().subscribe(
      (res) => {
        this.BranchList = res as any[];
      }
    )
  }
  Filter(value) {
    this.http.get(urlServer.ipServer+`ShowListBarberChackwork`).subscribe(
      (res: any[]) => {
        if (value ===""){
          let barberActive = res.filter(x=>x.active==="Y")
          let  barberstart_workNotnull =barberActive.filter(x=>x.start_work !=null)
          this.ShowListBarberCome =barberstart_workNotnull.filter(x=>x.status_awol==0)
          let barberstart_workNull = barberActive.filter(x=>x.start_work ==null)
          this.ShowListBarberNoCome = barberstart_workNull.filter(x=>x.status_awol==0)
        }
        else {
          let barberActive = res.filter(x=>x.active==="Y")
          let barberBranch = barberActive.filter(x => x.branch_id == value)
          let  barberstart_workNotnull =barberBranch.filter(x=>x.start_work !=null)
          this.ShowListBarberCome =barberstart_workNotnull.filter(x=>x.status_awol==0)
          let barberstart_workNull = barberBranch.filter(x=>x.start_work ==null)
          this.ShowListBarberNoCome = barberstart_workNull.filter(x=>x.status_awol==0)
        }
      }
    )
  }
  ngOnInit() {
    this.ShowBarberList()
    this.GetNameBranch()
    document.getElementById("content").classList.remove("p-0")
  }

}
