import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {urlServer} from '../../URL'

@Injectable({
  providedIn: 'root'
})
export class BarberService {

  constructor( private http:HttpClient) { }
  ///////////////////////////////////ShowListBarber/////////////////////////////
  public GetListBarber(){
    return this.http.get(urlServer.ipServer+`ShowListBarber`)
  }
  public GetBranch(){
    return this.http.get(urlServer.ipServer + `ShowListBranch`)
  }
    /////////////////////////////////////////////////////////////////////////////
}
