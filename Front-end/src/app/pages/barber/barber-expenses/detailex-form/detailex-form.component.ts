import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-detailex-form',
  templateUrl: './detailex-form.component.html',
  styleUrls: ['./detailex-form.component.css']
})
export class DetailexFormComponent implements OnInit {
  @Input() Detail
  @Input() firstname
  @Input() lastname
  @Input() money
  @Input() type
  fullname
  Type

  constructor() { }

  ngOnInit() {
this.fullname = this.firstname+" "+ this.lastname
this.Type =this.type==0 ? "เคมี" : this.type==1 ? "อุปกรณ์" : this.type==2 ? "ค่าห้อง":  this.type==3 ? "ประกันร้าน":"ขาดงาน"
  }

}
