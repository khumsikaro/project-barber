import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { urlServer } from '../../../URL';

@Injectable({
  providedIn: 'root'
})
export class BarberExpensesService {

  constructor(private http: HttpClient) { }
  public GetShowlistExpenses() {
    return this.http.get(urlServer.ipServer + `GetExpensesBar`)
  }
  public DeleteUserExpenses(id: number) {
    return this.http.delete(urlServer.ipServer + `DeleteExpenses/${id}`)
  }
  public SearchUserExpenses(username: string, type: number) {
    return this.http.get(urlServer.ipServer + `SearchExpenses/?username=${username}&type=${type}`)
  }
  public AddUserExpenses(data) {
    return this.http.post(urlServer.ipServer + `AddExpenses`, data);
  }
  public EditUserExpenses(data) {
    return this.http.put(urlServer.ipServer + `UpDataExpenses`, data);
  }
  public GetUserAll(){
    return this.http.get(urlServer.ipServer + `ShowListBarber`)
  }
}
