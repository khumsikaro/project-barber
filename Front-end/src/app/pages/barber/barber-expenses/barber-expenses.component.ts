import { Component, OnInit, Input } from '@angular/core';
import { BarberExpensesService } from './barber-expenses.service';
import { NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { FromBarberExpensesComponent } from './from-barber-expenses/from-barber-expenses.component';
import { isNullOrUndefined } from 'util';
import { from } from 'rxjs';
import { TypeCheckCompiler } from '@angular/compiler/src/view_compiler/type_check_compiler';
import { DetailexFormComponent } from './detailex-form/detailex-form.component';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-barber-expenses',
  templateUrl: './barber-expenses.component.html',
  styleUrls: ['./barber-expenses.component.css']
})
export class BarberExpensesComponent implements OnInit {
  ShowListExpensesData: any
  selectedtypeExpenses: any = "";
  isLoading: boolean;
  username: string = ""
  GetShowlistExpenses: any;

  Today = new Date;
  constructor(private service: BarberExpensesService,
    private modulService: NzModalService,
    private notificationService: NzNotificationService, ) { }

  ngOnInit() {
    this.ShowListExpenses()
    this.GetBarber()
    document.getElementById("content").classList.remove("p-0")


  }
  ShowListExpenses() {
    this.service.GetShowlistExpenses().subscribe(
      (res: any[]) => {
        this.ShowListExpensesData = res as any[]
      }
    )
  }
  openModal(typeComponent: string, data?: any) {
    const modal = this.modulService.create({
      nzTitle: typeComponent === 'new' ? 'เพิ่มบริการเเละเเต้ม' : 'เเก้ไขบริการเเละเเต้ม',
      nzWidth:700,
      nzContent: FromBarberExpensesComponent,
      nzComponentParams: {
        typeComponent: typeComponent,
        ex_bar_id: isNullOrUndefined(data) ? null : data.ex_bar_id,
        username: isNullOrUndefined(data) ? null : data.username,
        money: isNullOrUndefined(data) ? null : data.money,
        type: isNullOrUndefined(data) ? null : data.type,
        Detail: isNullOrUndefined(data) ? null : data.Detail,
      },
      nzClosable: true,
      nzFooter: [
        {
          label: 'ยกเลิก',
          shape: 'default',
          onClick: () => modal.destroy()
        },
        {
          label: 'บันทึก',
          type: 'primary',
          onClick: () => new Promise(resolve => setTimeout(resolve, 0)).then(
            () => {
              const instance = modal.getContentComponent();
              return instance
            }
          ).then((instance) => {
            if(isNullOrUndefined(instance.username))
            this.notificationService.create("error","<span>กรุณาเลือกช่าง</span>", null)
            if(isNullOrUndefined(instance.money))
            this.notificationService.create("error","<span>กรุณาใส่จำนวนเงิน</span>", null)              
            if(isNullOrUndefined(instance.type))
            this.notificationService.create("error","<span>กรุณาเลือกประเภทรายจ่าย</span>", null)
          else if(!isNullOrUndefined(instance.username)&&!isNullOrUndefined(instance.money)&&!isNullOrUndefined(instance.type)){
            this.AddOrEdit(instance).subscribe(
              (res) => {
                if (JSON.stringify(res)[11] === 's') {
                  this.notificationService.create("success", typeComponent === "new" ? "<span>เพิ่มข้อมูลสำเร็จ</span>" : "<span>แก้ไขข้อมูลสำเร็จ</span>", null)
                  modal.destroy();
                  this.ShowListExpenses();
                }
              }
            )
          }
          })
        }
      ]
    })
  }
  AddOrEdit(data) {
    if (data.typeComponent === "new") {
      const value = {
        date: this.Today,
        Detail: data.Detail==null? "ไม่ได้ระบุรายละเอียด":data.Detail,
        barbername: data.username,
        money: data.money,
        type: data.type,
      }
      return this.service.AddUserExpenses(value)
    }
    else {
      const value = {
        date: this.Today,
        ex_bar_id: data.ex_bar_id,
        Detail: data.Detail==null? "ไม่ได้ระบุรายละเอียด":data.Detail,
        barbername: data.username,
        money: data.money,
        type: data.type,
      }
      return this.service.EditUserExpenses(value)
    }
  }
  DeleteListExpenses(data) {
    this.modulService.confirm({
      nzTitle: '<h5 class = "model-title">ยืนยันการลบ<h5>',
      nzContent: 'คุณต้องการลบรายจ่ายของคุณ ' + data.firstname + ' ใช่หรือไม่',
      nzClosable: false,
      nzCancelText: "ยกเลิก",
      nzOkText: "ยืนยัน",
      nzOnOk: () => new Promise(resolev => setTimeout(resolev, 100)).then(() => {
        this.service.DeleteUserExpenses(data.ex_bar_id).subscribe(
          (res) => {
            this.notificationService.create("success", "ลบข้อมูลสำเร็จ", null)
            this.ShowListExpenses()
          }
        )
      }
      )
    })
  }
  public search() {
    this.service.SearchUserExpenses(this.username, this.selectedtypeExpenses).subscribe(
      (res) => {
        this.ShowListExpensesData = res as any[]
        if( this.ShowListExpensesData.length==0){
          Swal.fire({
            html: '<p>ไม่มีข้อมูลที่คุณต้องการค้นหา!</p>',
          })
        }
      }
    )
  }
  GetBarber() {
    this.service.GetUserAll().subscribe(
      (res: any[]) => {
        this.GetShowlistExpenses = res as any[]
      })
  }
  Detail(data) {
    const modal = this.modulService.create({
      nzTitle: 'รายละเอียดรายจ่าย',
      nzContent: DetailexFormComponent,
      nzWidth:700,
      nzComponentParams: {
        Detail: data.Detail,
        firstname: data.firstname,
        lastname: data.lastname,
        money: data.money,
        type: data.type
      },
      nzClosable: true,
      nzFooter: null,
    })
  }
}
