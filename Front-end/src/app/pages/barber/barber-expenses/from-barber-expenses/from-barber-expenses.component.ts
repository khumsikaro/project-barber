import { Component, OnInit, Input } from '@angular/core';
import { BarberExpensesService } from '../barber-expenses.service';

@Component({
  selector: 'app-from-barber-expenses',
  templateUrl: './from-barber-expenses.component.html',
  styleUrls: ['./from-barber-expenses.component.css']
})
export class FromBarberExpensesComponent implements OnInit {
  GetShowlistExpenses: any[];
  @Input() typeComponent: string;
  @Input() ex_bar_id: string;
  @Input() money: number;
  @Input() username: string;
  @Input() type: number;
  @Input() Detail: string;
  ExpensesType: string;
  ShowListEspensrs: any[];
  @Input() baebername: string[];
  constructor(private service: BarberExpensesService) {

  }

  ShowlistExpenses() {
    this.service.GetShowlistExpenses().subscribe(
      (res: any[]) => {
        this.GetShowlistExpenses = res as any[]
      }
    )
  }
  ShowUserExpemses() {
    this.service.GetUserAll().subscribe(
      (res) => {
        this.ShowListEspensrs = res as any[];
      }
    )
  }

  ngOnInit() {
    this.ShowlistExpenses();
    this.ShowUserExpemses();
    this.ex_bar_id = this.typeComponent === "new" ? null : this.ex_bar_id
    this.money = this.typeComponent === "new" ? null : this.money
    this.username = this.typeComponent === "new" ? null : this.username
    this.type = this.typeComponent === "new" ? null : this.type
    this.baebername = this.typeComponent === "new" ? null : this.baebername
    this.Detail = this.typeComponent === "new" ? null : this.Detail
  }

}
