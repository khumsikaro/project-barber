import { Component, OnInit, Input } from '@angular/core';
import { Time } from '@angular/common';

@Component({
  selector: 'app-barber-time-stamp-from',
  templateUrl: './barber-time-stamp-from.component.html',
  styleUrls: ['./barber-time-stamp-from.component.css']
})
export class BarberTimeStampFromComponent implements OnInit {

  @Input() start_work:Date;
  @Input() check_work_id: number;
  Start_work:Date
  constructor() {
  }
  
  ngOnInit() {
    this.Start_work = new Date(this.start_work)
    this.check_work_id == null ? null : this.check_work_id
  }
}
