import { Component, OnInit } from '@angular/core';
import { BarberTimeStampService } from './barber-time-stamp.service';
import { prototype } from 'moment';
import { Time } from '@angular/common';
import { NzNotificationService, NzModalService } from 'ng-zorro-antd';
import { BarberTimeStampFromComponent } from './barber-time-stamp-from/barber-time-stamp-from.component';
import { isNullOrUndefined } from 'util';
import { LoginService } from '../../login/login.service';

@Component({
  selector: 'app-barber-time-stamp',
  templateUrl: './barber-time-stamp.component.html',
  styleUrls: ['./barber-time-stamp.component.css']
})
export class BarberTimeStampComponent implements OnInit {
  gridStyle = {
    width: '33.3%',
    textAlign: 'center'
  };
  ShowListBarber: any[];
  USER;
  today = new Date();
  date = this.today.getDate() + '-' + (this.today.getMonth() + 1) + '-' + this.today.getFullYear();
  nzDisabled: boolean;
  constructor(private service: BarberTimeStampService, private modalService: NzModalService,
    private notificationService: NzNotificationService, private session: LoginService, ) { }

  ShowBarberList() {
    const datetaday = this.today
    this.service.GetListBarber(this.USER.branch_id, datetaday).subscribe(
      (res) => {
        if (res[0][0].vac_type != undefined) {
        }
        else {
          this.ShowListBarber = res[0] as any[];

        }
      }
    )
  }
  StempRealTimes(id) {
    let date = new Date
    let time = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    const datetaday = this.today.getFullYear() + '-' + (this.today.getMonth() + 1) + '-' + this.today.getDate() + " " + time;
    const value = { start_work: datetaday, check_work_id: id }
    this.ShowBarberList()
    this.service.TimeStamp(value).subscribe(
      (res) => {
        this.ShowBarberList()
      }
    )
  }
  EditStempModal(data: any) {
    const modal = this.modalService.create({
      nzTitle: '<h5 class="modal-title">แก้ไขเวลาเข้างาน</h5>',
      nzContent: BarberTimeStampFromComponent,
      nzWidth: 250,
      nzComponentParams: {
        start_work: isNullOrUndefined(data) ? null : data.start_work,
        check_work_id: isNullOrUndefined(data) ? null : data.check_work_id,
      },
      nzClosable: true,
      nzFooter: [
        {
          label: 'ยกเลิก',
          shape: 'default',
          onClick: () => modal.destroy()
        },
        {
          label: 'บันทึก',
          type: 'primary',
          onClick: () => new Promise(resolve => setTimeout(resolve, 0)).then(
            () => {
              const instance = modal.getContentComponent();
              return instance

            }
          ).then((instance) => {
            if (isNullOrUndefined(instance.Start_work) || instance.Start_work === '0000-00-00 00:00:00')
              this.notificationService.create("error", "กรุณาใส่เวลาให้ถูกต้อง", null)
            else if (!isNullOrUndefined(instance.Start_work) && instance.Start_work !== '0000-00-00 00:00:00') {
              this.Edit(instance).subscribe(
                (res) => {
                  if (JSON.stringify(res)[11] === 's') {
                    this.notificationService.create("success", "แก้ไขข้อมูลสำเร็จ", null)
                    modal.destroy();
                    this.ShowBarberList()
                  }
                }
              )
            }
          })
        }
      ]
    })
  }
  AWOL(data) {
    this.service.GetSettingbarber().subscribe((res) => {
      var time = res[0].chack_in
      const datetaday = this.today.getFullYear() + '-' + (this.today.getMonth() + 1) + '-' + this.today.getDate() + " " + time;
      let FromInsertExpenses = {
        type: 4,
        barbername: data.barber_id,
        money: data.Awol,
        time_stamp: this.today,
        Detail: `คุณ${data.firstname} ${data.lastname} ได้ขาดงานโดยไม่ทำการลา ทางร้านจำเป็นต้องหักเงินตามที่ทางร้านกำหนดไว้`
      }
      let value =
      {
        check_work_id: data.check_work_id,
        status_awol: 1,
        start_work: datetaday,
        From: FromInsertExpenses
      }

      this.service.AWOL(value).subscribe((res) => {
        this.ShowBarberList()
      })
    })

  }
  Edit(data) {
    const value = {
      start_work: this.substringDate(data.Start_work.toString()),
      check_work_id: data.check_work_id,

    }
    return this.service.TimeStamp(value)
  }
  ngOnInit() {
    this.USER = this.session.getActiveUser();
    this.ShowBarberList()
    document.getElementById("content").classList.remove("p-0")
  }
  substringDate(date: string) {
    let mount: any;
    switch (date.substring(4, 7)) {
      case "Jan": mount = "01";
        break;
      case "Feb": mount = "02";
        break;
      case "Mar": mount = "03";
        break;
      case "Apr": mount = "04";
        break;
      case "May": mount = "05";
        break;
      case "Jun": mount = "06";
        break;
      case "Jul": mount = "07";
        break;
      case "Aug": mount = "08";
        break;
      case "Sep": mount = "09";
        break;
      case "Oct": mount = "10";
        break;
      case "Nov": mount = "11";
        break;
      case "Dec": mount = "12";
        break;
    }
    let strDate = date.substring(11, 15) + "-" + mount + "-" + date.substring(8, 10) + " " + date.substring(16, 24);
    return strDate;
  }

}
