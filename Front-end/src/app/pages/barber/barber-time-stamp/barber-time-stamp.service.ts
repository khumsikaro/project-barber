import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { urlServer } from '../../../URL'

@Injectable({
  providedIn: 'root'
})

export class BarberTimeStampService {

  constructor(private http: HttpClient) { }

  public GetListBarber(Branch_id, Date) {
    return this.http.get(urlServer.ipServer + `GetCheck/${Branch_id}/${Date}`)
  }

  public TimeStamp(data) {
    return this.http.put(urlServer.ipServer + `UpdateCheckWork`, data)
  }
  public AWOL(data){
    return this.http.put(urlServer.ipServer + `SendAWOL`, data)
  }
  public GetSettingbarber(){
    return this.http.get(urlServer.ipServer +`GetSettingbarber`);
  }
}
