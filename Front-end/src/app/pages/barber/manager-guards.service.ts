import { Injectable } from '@angular/core';
import {
  CanActivate, Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivateChild
} from '@angular/router';
import { LoginService } from '../login/login.service';
@Injectable()
export class ManagerGuardsService implements CanActivate {
  constructor(private _router: Router, private AuthenService: LoginService) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> | boolean {
    let ManagerUser = this.AuthenService.getActiveUser();
    return new Promise<boolean>(resolve => {
      if(ManagerUser!=null){
        if (ManagerUser.levelUsers== 1) {
          resolve(true);
        } else {
          resolve(false);
          this._router.navigate(['']);
        }
      }
      else{
        resolve(false);
        this._router.navigate(['']);
      }
    });
  }
}
