import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { urlServer } from '../../../URL'

@Injectable({
  providedIn: 'root'
})
export class BarberLeaveService {

  constructor(private http: HttpClient) { }

  public GetLeaveList(Branch) {
    return this.http.get(urlServer.ipServer + `ShowLeaveList/${Branch}`)
  }
  public UpdateLeave(data) {
    return this.http.put(urlServer.ipServer + `UpdateLeave`, data)
  }
  public AddLeave(data) {
    return this.http.post(urlServer.ipServer + `AddLeave`, data)
  }
  public DeleteLeave(id:number){
    return this.http.delete(urlServer.ipServer+`DeleteLeave/${id}`)
  }
  public Search(name: string, type: any,Branch) {

    return this.http.get(urlServer.ipServer + `SearchLeaveList/?name=${name}&type=${type}&Branch=${Branch}`)
  }
  public ListBarber(Branch){
    return this.http.get(urlServer.ipServer+`ListBarberBranch/${Branch}`)
  }
  public GetListBarber(){
    return this.http.get(urlServer.ipServer+`ShowListBarber`)
  }
  
  public GetHolidayList(){
    return this.http.get(urlServer.ipServer+`GetHolidayList`)
  }
}
