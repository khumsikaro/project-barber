import { Component, OnInit, Input } from '@angular/core';
import { BarberLeaveService } from '../barber-leave.service';
import * as moment from 'moment'
import { LoginService } from 'src/app/pages/login/login.service';
import { differenceInCalendarDays } from 'date-fns';
import { urlServer } from '../../../../URL'
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-barber-leave-from',
  templateUrl: './barber-leave-from.component.html',
  styleUrls: ['./barber-leave-from.component.css']
})
export class BarberLeaveFromComponent implements OnInit {
  @Input() typeComponent: string;
  @Input() leave_id: number;
  @Input() type_id: number;
  @Input() detail: string;
  @Input() date_start: Date;
  @Input() date_stop: Date;
  @Input() total_day: number;
  @Input() barber_id: string;
  @Input() firstname: string;
  @Input() lastname: string;
  @Input() vac_type: string;
  @Input() username: string;
  @Input() leave_type_id: number;
  dateFormat = 'dd-MM-yyyy';
  HolidayList: any[];
  LeaveType;
  ShowListBarber: any[];
  Barber_ID: string;
  Date;
  AllDate: any[];
  USER;
  /////////////////////////////////////////////////
  today = new Date();
  time = this.today.getHours() + ":" + this.today.getMinutes() + ":" + this.today.getSeconds();
  date = this.today.getDate() + '-' + (this.today.getMonth() + 1) + '-' + this.today.getFullYear();
  CheckHolidayPrivateBarberdata:any[];
  ////////////////////////////////////////////////////////////////
  disabledDate = (current: Date): boolean => {
    // Can not select days before today and today
    return differenceInCalendarDays(current, this.today) < 0;
  };
  constructor(private service: BarberLeaveService, private session: LoginService, private http: HttpClient) { }
  GetHolidayList() {
    this.service.GetHolidayList().subscribe(
      (res) => {
        this.HolidayList = res as any[];
      }
    )
  }
  ShowBarberList() {
    this.service.ListBarber(this.USER.branch_id).subscribe(
      (res) => {
        this.ShowListBarber = res as any[];

      }
    )
  }
  ngOnInit() {
    this.USER = this.session.getActiveUser();
    this.GetHolidayList();
    this.ShowBarberList();
    this.Barber_ID = this.typeComponent === "new" ? null : this.username
    this.detail = this.typeComponent === "new" ? null : this.detail
    this.LeaveType = this.typeComponent === "new" ? null : this.type_id
    this.total_day = this.typeComponent === "new" ? null : this.total_day
    this.AllDate = [this.date_start, this.date_stop]
    this.Date = this.typeComponent === "new" ? [] : this.AllDate
    if(this.typeComponent==='edit'){
      this.CheckHolidayPrivateBarber(this.Barber_ID)
    }


  }
  CheckHolidayPrivateBarber(username) {
    
    let dates = (this.today.getMonth() + 1) + '-' + this.today.getFullYear()
    this.http.get(urlServer.ipServer + `CheckHolidayPrivateBarber/${username}/${dates}`).subscribe(
      (res) => {
        this.CheckHolidayPrivateBarberdata = res as any;
      }
    )
  }
}
