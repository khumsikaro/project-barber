import { Component, OnInit } from '@angular/core';
import { BarberLeaveService } from './barber-leave.service';
import { NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { BarberLeaveFromComponent } from './barber-leave-from/barber-leave-from.component';
import { isNullOrUndefined } from 'util';
import * as moment from 'moment'
import { LoginService } from '../../login/login.service';
import { DetailFormComponent } from './detail-form/detail-form.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-barber-leave',
  templateUrl: './barber-leave.component.html',
  styleUrls: ['./barber-leave.component.css']
})
export class BarberLeaveComponent implements OnInit {
  LeaveListData: any[];
  HolidayList: any[];
  NameBarber: string = '';
  HolidayType: string = "";
  ShowListBarber: any[];
  constructor(private service: BarberLeaveService, private modalService: NzModalService,
    private notificationService: NzNotificationService, private session: LoginService, ) { }

  GetLeaveList() {
    this.service.GetLeaveList(this.USER.branch_id).subscribe(
      (res) => {
        this.LeaveListData = res as any[];
      }
    )
  }
  GetHolidayList() {
    this.service.GetHolidayList().subscribe(
      (res) => {
        this.HolidayList = res as any[];
      }
    )
  }
  ShowBarberList() {
    this.service.ListBarber(this.USER.branch_id).subscribe(
      (res) => {
        this.ShowListBarber = res as any[];
      }
    )
  }
  search() {
    this.service.Search(this.NameBarber, this.HolidayType, this.USER.branch_id).subscribe(
      (res) => {
        this.LeaveListData = res as any;
        if(this.LeaveListData.length==0){
          Swal.fire({
            html: '<p>ไม่มีข้อมูลที่คุณต้องการค้นหา!</p>',
          })
        }
      }
    )
  }
  deleteLeave(data) {
    this.modalService.confirm({
      nzTitle: '<h5 class = "model-title">ยืนยันการลบ' + data.vac_type + '<h5>',
      nzContent: 'คุณต้องการลบ ' + data.firstname + data.lastname + ' ใช่หรือไม่ ?',
      nzClosable: false,
      nzCancelText: "ยกเลิก",
      nzOkText: "ยืนยัน",
      nzOnOk: () => new Promise(resolev => setTimeout(resolev, 100))
        .then(() => {
          this.service.DeleteLeave(data.leave_id).subscribe(
            (res) => {
              this.notificationService.create("success", "ลบข้อมูลสำเร็จ", null)
              this.GetLeaveList()
            }
          )
        }
        )
    })
  }
  openModal(typeComponent: string, data?: any) {
    const modal = this.modalService.create({
      nzTitle: typeComponent === 'new' ? '<h5 class="modal-title">เพิ่มการลา</h5>' : '<h5 class="modal-title">เเก้ไขการลา<h5>',
      nzContent: BarberLeaveFromComponent,
      nzComponentParams: {
        typeComponent: typeComponent,
        leave_id: isNullOrUndefined(data) ? null : data.leave_id,
        type_id: isNullOrUndefined(data) ? null : data.type_id,
        detail: isNullOrUndefined(data) ? null : data.detail,
        date_start: isNullOrUndefined(data) ? null : data.date_start,
        date_stop: isNullOrUndefined(data) ? null : data.date_stop,
        total_day: isNullOrUndefined(data) ? null : data.total_day,
        barber_id: isNullOrUndefined(data) ? null : data.barber_id,
        firstname: isNullOrUndefined(data) ? null : data.firstname,
        lastname: isNullOrUndefined(data) ? null : data.lastname,
        vac_type: isNullOrUndefined(data) ? null : data.vac_type,
        username: isNullOrUndefined(data) ? null : data.username,
        leave_type_id: isNullOrUndefined(data) ? null : data.leave_type_id,

      },
      nzClosable: true,
      nzWidth:700,
      nzFooter: [
        {
          label: 'ยกเลิก',
          shape: 'default',
          onClick: () => modal.destroy()
        },
        {
          label: 'บันทึก',
          type: 'primary',
          onClick: () => new Promise(resolve => setTimeout(resolve, 0)).then(
            () => {
              const instance = modal.getContentComponent();
              return instance
            }
          ).then((instance) => {
            if (instance.Date.length < 2)
              this.notificationService.create("error", "<p>กรุณาเลือกขอบเขตวันหยุดให้ครบถ้วน<p>", null)
            if (isNullOrUndefined(instance.LeaveType))
              this.notificationService.create("error", "<p>กรุณาเลือกประเภทการลา<p>", null)
            if (isNullOrUndefined(instance.Barber_ID))
              this.notificationService.create("error", "<p>กรุณาเลือกช่างที่จะลา<p>", null)
            if (isNullOrUndefined(instance.detail) || instance.detail.trim() === "" || instance.detail === "")
              this.notificationService.create("error", "<p>กรุณาใส่รายละเอียดการลา<p>", null)
            else if (instance.Date.length == 2 && !isNullOrUndefined(instance.LeaveType) && !isNullOrUndefined(instance.Barber_ID) && !isNullOrUndefined(instance.detail) && instance.detail.trim() !== "" && instance.detail !== "") {
              this.AddOrEdit(instance).subscribe(
                (res) => {
                  if (Object.keys(res[0][0])[0] === 'success') {
                    this.notificationService.create("success", typeComponent === "new" ? "<p>เพิ่มข้อมูลสำเร็จ</p>" : "<p>แก้ไขข้อมูลสำเร็จ</p>", null)
                    modal.destroy();
                    this.GetLeaveList()
                  }
                  else if (Object.keys(res[0][0])[0] === 'error') {
                    this.notificationService.create("error", "<p>ไม่สามารถลาในคาบวันที่เลือกได้<p>", null)
                  }
                }
              )
            }
          })
        }
      ]
    })
  }
  AddOrEdit(data) {
    if (data.typeComponent === "new") {
      const a = moment(data.Date[0].getFullYear() + '/' + (data.Date[0].getMonth() + 1) + '/' + data.Date[0].getDate());
      const b = moment(data.Date[1].getFullYear() + '/' + (data.Date[1].getMonth() + 1) + '/' + data.Date[1].getDate());
      const Sumday = b.diff(a, 'days');
      const totalday = Sumday + 1
      const value = {
        type_id: data.LeaveType,
        detail: data.detail,
        barber_id: data.Barber_ID,
        date_start: data.Date[0],
        date_stop: data.Date[1],
        total_day: totalday,
      }
      return this.service.AddLeave(value)

    }
    else {
      let Date0 = new Date(data.Date[0])
      let Date1 = new Date(data.Date[1])
      const a = moment(Date0.getFullYear() + '/' + (Date0.getMonth() + 1) + '/' + Date0.getDate());
      const b = moment(Date1.getFullYear() + '/' + (Date1.getMonth() + 1) + '/' + Date1.getDate());
      const Sumday = b.diff(a, 'days');
      const totalday = Sumday + 1
      const value = {
        leave_id: data.leave_id,
        type_id: data.LeaveType,
        detail: data.detail,
        barber_id: data.Barber_ID,
        date_start: data.Date[0],
        date_stop: data.Date[1],
        total_day: totalday,
      }
      return this.service.UpdateLeave(value)
    }

  }
  USER;
  ngOnInit() {
    this.USER = this.session.getActiveUser();
    this.GetLeaveList()
    this.GetHolidayList()
    this.ShowBarberList()
    document.getElementById("content").classList.remove("p-0")
  }
  DetailFrom(data) {
    const modal = this.modalService.create({
      nzTitle: '<h5 class="modal-title">รายละเอียดการลา</h5>',
      nzContent: DetailFormComponent,
      nzComponentParams: {
        detail: isNullOrUndefined(data) ? null : data.detail,
        date_start: isNullOrUndefined(data) ? null : data.date_start,
        date_stop: isNullOrUndefined(data) ? null : data.date_stop,
        total_day: isNullOrUndefined(data) ? null : data.total_day,
        barber_id: isNullOrUndefined(data) ? null : data.barber_id,
        firstname: isNullOrUndefined(data) ? null : data.firstname,
        lastname: isNullOrUndefined(data) ? null : data.lastname,
        vac_type: isNullOrUndefined(data) ? null : data.vac_type,
        username: isNullOrUndefined(data) ? null : data.username,
      },
      nzClosable: true,
      nzWidth:700,
      nzFooter: null,
    })
  }

}
