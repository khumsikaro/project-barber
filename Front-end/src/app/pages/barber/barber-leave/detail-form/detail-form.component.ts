import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-detail-form',
  templateUrl: './detail-form.component.html',
  styleUrls: ['./detail-form.component.css']
})
export class DetailFormComponent implements OnInit {

  @Input() detail
  @Input() date_start
  @Input() date_stop
  @Input() total_day
  @Input() barber_id
  @Input() firstname
  @Input() lastname
  @Input() vac_type
  @Input() username
  AllDate
  Date;
  Type
  Barbername
  constructor() { }

  ngOnInit() {
    this.AllDate = [this.date_start,this.date_stop]
    this.Date =this.AllDate
    this.Barbername= this.firstname+" "+ this.lastname
    this.Type =this.vac_type
  }

}
