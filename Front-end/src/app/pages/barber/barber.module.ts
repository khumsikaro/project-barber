import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BarberRoutingModule } from './barber-routing.module';
import { ShowListBarberComponent } from './show-list-barber/show-list-barber.component';
import { ZorroModule } from 'src/app/lib.ngZorro';
import { ShowHolidayBarberComponent } from './show-holiday-barber/show-holiday-barber.component';
import { BarberComponent } from './barber.component';
import { BarberTimeStampComponent } from './barber-time-stamp/barber-time-stamp.component';
import { BarberLeaveComponent } from './barber-leave/barber-leave.component';
import { BarberExpensesComponent } from './barber-expenses/barber-expenses.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BarberLeaveFromComponent } from './barber-leave/barber-leave-from/barber-leave-from.component';
import { FromBarberExpensesComponent } from './barber-expenses/from-barber-expenses/from-barber-expenses.component';
import { BarberTimeStampFromComponent } from './barber-time-stamp/barber-time-stamp-from/barber-time-stamp-from.component';
import { ManagerGuardsService } from './manager-guards.service';
import { MasterGuardsService } from '../master/master-guards.service';
import { DetailFormComponent } from './barber-leave/detail-form/detail-form.component';
import { DetailexFormComponent } from './barber-expenses/detailex-form/detailex-form.component';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';


@NgModule({
  declarations: [ShowListBarberComponent, ShowHolidayBarberComponent, BarberComponent, BarberTimeStampComponent, BarberLeaveComponent, BarberExpensesComponent, BarberLeaveFromComponent,FromBarberExpensesComponent, BarberTimeStampFromComponent, DetailFormComponent, DetailexFormComponent],
  imports: [
    CommonModule,
    BarberRoutingModule,
    ZorroModule,
    FormsModule,
    ReactiveFormsModule,
    NzDatePickerModule,
  ],
  entryComponents: [
    BarberLeaveFromComponent,
    FromBarberExpensesComponent,
    BarberTimeStampFromComponent,
    DetailFormComponent,
    DetailexFormComponent,
  ],
  providers: [
    ManagerGuardsService,
    MasterGuardsService
  ]
})
export class BarberModule { }
