import { Component, OnInit } from '@angular/core';
import { NzModalService, NzNotificationService, NzDrawerModule, NzDrawerService } from 'ng-zorro-antd';
import { LoginComponent } from './login/login.component';
import { LoginService } from './login/login.service';
import { PagesService } from './pages.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { AskComponent } from './ask/ask.component';
import { AddCustomerComponent } from './add-customer/add-customer.component';
import { UserService } from './master/user/user.service';
import { isNullOrUndefined } from 'util';
import { HttpClient } from '@angular/common/http';
import{urlServer} from '../URL'
@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.css']
})
export class PagesComponent implements OnInit {
  validateForm: FormGroup;
  username: string = "USER_NAME"
  isCollapsed = false;
  visible = false;
  isVisible = false;
  selectedValue = null;
  USER: any;
  COUNT:number;
  public modal;
  public drawerRef;
  constructor(
    private modalService: NzModalService,
    private session: LoginService,
    private service: PagesService,
    private notification: NzNotificationService,
    private drawerService: NzDrawerService,
    private userservice:UserService,
    private http:HttpClient
  ) { }
  ngOnInit() {
    this.USER = this.session.getActiveUser();
    if(this.USER.levelUsers==4){
      let date= new Date();
      this.http.get(urlServer.ipServer+`GetStatusQueueUser/${this.USER.username}/${date}`).subscribe((res:any[])=>{ 
        this.COUNT=res.filter(x=>x.status_confirm==0).length;
      })
    }else if(this.USER.levelUsers==1){
      this.http.get(urlServer.ipServer + `GetQueueConFirm/${this.USER.branch_id}`).subscribe((res:any[])=>{
        this.COUNT=res.length;
      })
    }
    else if(this.USER.levelUsers==2 || this.USER.levelUsers==3){
      let date= new Date();
      this.http.get(urlServer.ipServer+`CheckQueuePrivateBarber/${this.USER.username}/${date}`).subscribe((res:any)=>{ 
        this.COUNT=res.length;
      })
    }
  }
  onLogin() {
    this.modal = this.modalService.create({
      nzTitle: '<h5>ยินดีต้อนรับเข้าสู่ระบบ</h5>',
      nzContent: LoginComponent,
      nzFooter: null,
      nzClosable: false,
    })
  }
  onLogout() {
    let Upstatuslogin={username:this.USER.username,status_login:0,}
    this.http.put(urlServer.ipServer + `UpdateStatusLogin`, Upstatuslogin).subscribe((res)=>{
      this.session.clearActiveUser();
      window.history.go(0)
    })
  }
  ChangePassword() {
    this.modal = this.modalService.create({
      nzTitle: '<h5>เปลี่ยนรหัสผ่าน</h5>',
      nzContent: ChangePasswordComponent,
      nzFooter: null,
      nzClosable: false,
    })
  }
  Ask() {
    this.drawerRef = this.drawerService.create({
      nzTitle: '<h5>ฟอร์มสอบถาม</h5>',
      nzContent: AskComponent,
      nzWidth: 800,
      nzClosable: false,
    })
  }
  phonenumber(inputtxt) {
    debugger
    var phoneno = /^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/;
    if (inputtxt.match(phoneno)) {
      return true;
    }
    else {
      return false;
    }
  }
  AddCustomer() {
    const modal = this.modalService.create({
      nzTitle: 'สมัครสมาชิก',
      nzContent: AddCustomerComponent,
      nzWidth:1000,
      nzComponentParams: {},
      nzClosable: true,
      nzFooter: [
        {
          label: 'ยกเลิก',
          shape: 'default',
          onClick: () => modal.destroy()
        },
        {
          label: 'ยืนยัน',
          type: 'primary',
          onClick: () => new Promise(resolve => setTimeout(resolve, 0)).then(
            () => {
              const instance = modal.getContentComponent();
              return instance
            }
          ).then((instance) => {
            var CheckphoneNo=this.phonenumber(instance.phone_number)
            if(isNullOrUndefined(instance.username)|| instance.username.trim()==="")
            this.notification.create("error", "<p>กรุณาใส่ไอดีผู้ใช้งาน</p>", null)
            if(isNullOrUndefined(instance.Password)|| instance.Password.trim()==="")
            this.notification.create("error", "<p>กรุณาใส่รหัสผ่าน</p>", null)
            if(isNullOrUndefined(instance.firstname)|| instance.firstname.trim()==="")
            this.notification.create("error", "<p>กรุณาใส่ชื่อจริง</p>", null)
            if(isNullOrUndefined(instance.lastname)|| instance.lastname.trim()==="")
            this.notification.create("error", "<p>กรุณาใส่นามสกุล</p>", null)
            if(isNullOrUndefined(instance.Amphur_id))
            this.notification.create("error", "<p>กรุณาเลือกอำเภอ</p>", null)
            if(isNullOrUndefined(instance.District_id))
            this.notification.create("error", "<p>กรุณาเลือกตำบล</p>", null)
            if(isNullOrUndefined(instance.Email)|| instance.Email.trim()==="" || instance.Email.search("@")==-1)
            this.notification.create("error", "<p>กรุณาใส่อีเมลให้ถูกต้อง</p>", null)
            if(isNullOrUndefined(instance.Province_id))
            this.notification.create("error", "<p>กรุณาเลือกจังหวัด</p>", null)
            if(isNullOrUndefined(instance.address)|| instance.address.trim()==="")
            this.notification.create("error", "<p>กรุณาใส่ที่อยู่</p>", null)
            if(isNullOrUndefined(instance.date_of_birth))
            this.notification.create("error", "<p>กรุณาใส่วันเกิด</p>", null)
            if(isNullOrUndefined(instance.nickname)|| instance.nickname.trim()==="")
            this.notification.create("error", "<p>กรุณาใส่ชื่อเล่น</p>", null)
            if(isNullOrUndefined(instance.phone_number)|| instance.phone_number.trim()==="" || CheckphoneNo==false)
            this.notification.create("error", "<p>กรุณาใส่เบอร์โทรให้ถูกต้อง</p>", null)
            else if(!isNullOrUndefined(instance.username)&&!isNullOrUndefined(instance.Password)&&!isNullOrUndefined(instance.firstname)&&!isNullOrUndefined(instance.lastname)&&!isNullOrUndefined(instance.Amphur_id)
            &&!isNullOrUndefined(instance.District_id)&&!isNullOrUndefined(instance.Email)&&!isNullOrUndefined(instance.Province_id)&&!isNullOrUndefined(instance.address)&&!isNullOrUndefined(instance.date_of_birth)
            &&!isNullOrUndefined(instance.nickname)&&!isNullOrUndefined(instance.phone_number) &&instance.username.trim()!=="" &&instance.Password.trim()!==""&& instance.firstname.trim()!=="" &&instance.lastname.trim()!==""
            &&instance.Email.trim()!==""&&instance.Email.search("@")!=-1&&instance.address.trim()!==""&&instance.nickname.trim()!=="" && instance.phone_number.trim()!=="" && CheckphoneNo==true){
              this.FormAddCustomer(instance).subscribe(
                (res:any[]) => {
                  console.log(res["result"]);
                  
                  if (JSON.stringify(res)[11] === 's') {
                    this.notification.create("success", "<p>เพิ่มข้อมูลลูกค้าสำเร็จ</p>", null)
                    modal.destroy();
                  }
                 else if (JSON.stringify(res)[11] === 'e') {
                    this.notification.create("error", "<p>ไอดีนี้มีผู้ใช้งานแล้ว</p>", null)
                  }
                }
              )
            }
          })
        }
      ]
    })
  }
  FormAddCustomer(data) {
   var value={
    username: data.username,
    password:btoa(data.Password),
    status:"Y",
    active:"Y",
    status_soc:"N",
    level_barberid:4,
    img_profile:data.fileList.length==0 ? data.IMG : data.fileList[0].thumbUrl,
    firstname:data.firstname,
    lastname:data.lastname,
    nickname: data.nickname,
    date_of_birth:data.date_of_birth,
    phone_number:data.phone_number,
    gender:data.gender,
    Email:data.Email,
    address:data.address,
    province_id: data.Province_id,
    amphur_id: data.Amphur_id,
    district_id: data.District_id,
    zipcode_id: data.Zipcode_id,
    cr_by:this.USER.username,
   }
    return this.userservice.AddUser(value)
  }
}
