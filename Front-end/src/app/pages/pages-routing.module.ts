import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { GuardService } from './login/guard.service';

const routes: Routes = [
  {
    path:'',
    loadChildren:() => import('./home/home.module').then(x => x.HomeModule)
  },
  {
    path:'',
    loadChildren:() => import('./informations/informations.module').then(x => x.InformationsModule)
  },
  {
    path:'',
    loadChildren:() => import('./master/master.module').then(x => x.MasterModule)
  },
  {
    path:'',
    loadChildren:() => import('./report/report.module').then(x => x.ReportModule)
  },
  {
    path:'',
    loadChildren:() => import('./barber/barber.module').then(x => x.BarberModule)
  },
  {
    path:'',
    loadChildren:() => import('./service/service.module').then(x=>x.ServiceModule)
  },
  {
    path:'',
    loadChildren:() => import('./branch/branch.module').then(x=>x.BranchModule)
  },
  {
    path:'',
    loadChildren:() => import('./queue/queue.module').then(x=>x.QueueModule)
  },
  {
    path:'',
    loadChildren:() => import('./profile/profile.module').then(x=>x.ProfileModule)
  },
  {
    path:'',
    loadChildren:() => import('./Shared/informationdetail/informationdetail.module').then(x=>x.InformationdetailModule)
  },
  {
    path:'',
    loadChildren:() => import('./Shared/servicedetail/servicedetail.module').then(x=>x.ServicedetailModule)
  },
  {
    path:'',
    loadChildren:()=> import('./Shared/branchdetail/branchdetail.module').then(x=>x.BranchdetailModule)
  },
  {
    path:'',
    loadChildren:()=> import('./Shared/barberdetail/barberdetail.module').then(x=>x.BarberdetailModule)
  },
  {
    path:'',
    redirectTo:'',
    pathMatch:'full'
  },
  {
    path:'**',
    component:PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
