import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { ZorroModule } from '../lib.ngZorro';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { NzButtonModule, NzSelectModule, NzDropDownModule } from 'ng-zorro-antd';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { NgxWebstorageModule } from 'ngx-webstorage';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { AskComponent } from './ask/ask.component';
import { AddCustomerComponent } from './add-customer/add-customer.component';
import { GuardService } from './login/guard.service';
import { ChartsModule } from 'ng2-charts';
import { NgbPaginationModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { ApproveQueueComponent } from './queue/approve-queue/approve-queue.component';

@NgModule({
  declarations: [
    PagesComponent,
    PageNotFoundComponent,
    LoginComponent,
    ChangePasswordComponent,
    AskComponent,
    AddCustomerComponent,
  ],
  imports: [
    CommonModule,
    PagesRoutingModule,
    ZorroModule,
    NzButtonModule,
    NzSelectModule,
    FormsModule,
    NzDropDownModule,
    FormsModule,
    ReactiveFormsModule,
    NgxWebstorageModule.forRoot(),
    ChartsModule,
    NgbPaginationModule, 
    NgbAlertModule,
  ],
  exports: [
    PagesComponent,
  ],
  entryComponents: [
    LoginComponent,
    ChangePasswordComponent,
    AskComponent,
    AddCustomerComponent,
  ],
  providers: [
    GuardService,
  ]
})
export class PagesModule { }
