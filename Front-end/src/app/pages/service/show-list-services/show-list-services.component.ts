import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-show-list-services',
  templateUrl: './show-list-services.component.html',
  styleUrls: ['./show-list-services.component.css']
})
export class ShowListServicesComponent implements OnInit {
  selectedValue ="0";

  constructor(private service: ServiceService) { }
  ShowListServiceData: any;
  isLoading: boolean;
  listhairdata;
  listnaildata;
  listfasedata;
  AllService;
  ngOnInit() {
    this.ShowListService()
    document.getElementById("content").classList.remove("p-0")
  }
  ShowListService() {
    this.isLoading = true;
    this.service.GetShowListService().subscribe(
      (res: any[]) => {
        let Resactive = res.filter(y=>y.active==='Y')
        this.ShowListServiceData = Resactive.filter(y=>y.type==0)
        this.isLoading = false;
      }
    )
  }
  Filter(value) {
    this.isLoading = true;
    this.service.GetShowListService().subscribe(
      (res:any[]) => {
        let Resactive = res.filter(y=>y.active==='Y')
        this.ShowListServiceData = Resactive.filter(x=>x.type== value)
        this.isLoading = false;
      }
    )
  }
}
