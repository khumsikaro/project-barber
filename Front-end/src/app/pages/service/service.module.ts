import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServiceRoutingModule } from './service-routing.module';
import { ShowListServicesComponent } from './show-list-services/show-list-services.component';
import { ZorroModule } from 'src/app/lib.ngZorro';
import { ServiceComponent } from './service.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzTableModule } from 'ng-zorro-antd';
import { GuardService } from '../login/guard.service';


@NgModule({
  declarations: [ShowListServicesComponent, ServiceComponent],
  imports: [
    CommonModule,
    ServiceRoutingModule,
    ZorroModule,
    FormsModule,
    ReactiveFormsModule,
    NzTableModule,
  ],
  providers: [
    GuardService
  ]
})
export class ServiceModule { }
