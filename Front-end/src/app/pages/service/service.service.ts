import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {urlServer} from '../../URL'

@Injectable({
  providedIn: 'root'
})
export class ServiceService {


  constructor(private http:HttpClient) { }
  
  public GetShowListService(){
    return this.http.get(urlServer.ipServer+'ShowListService')
  }
}
