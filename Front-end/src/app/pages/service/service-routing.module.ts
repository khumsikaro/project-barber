import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ServiceComponent } from './service.component';
import { ShowListServicesComponent } from './show-list-services/show-list-services.component';
import { GuardService } from '../login/guard.service';


const routes: Routes = [
  {
    path:'Service',
    component:ServiceComponent,
    children:[
      {
        path:'ShowListServices',
        component:ShowListServicesComponent,
      },
      
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServiceRoutingModule { }
