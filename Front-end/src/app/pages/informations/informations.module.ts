import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InformationsRoutingModule } from './informations-routing.module';
import { NewsComponent } from './news/news.component';
import { PromotionComponent } from './promotion/promotion.component';
import { ShowListNewsComponent } from './news/show-list-news/show-list-news.component';
import { ShowListPromotionComponent } from './promotion/show-list-promotion/show-list-promotion.component';
import { ZorroModule } from 'src/app/lib.ngZorro';
import { GuardService } from '../login/guard.service';


@NgModule({
  declarations: [
    NewsComponent,
    PromotionComponent,
    ShowListNewsComponent,
    ShowListPromotionComponent,
  ],
  imports: [
    CommonModule,
    InformationsRoutingModule,
    ZorroModule,
  ],
  providers: [
    GuardService
  ]
})
export class InformationsModule { }
