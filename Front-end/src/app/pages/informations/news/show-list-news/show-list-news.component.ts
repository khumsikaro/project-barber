import { Component, OnInit } from '@angular/core';
import { NewsService } from '../news.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-show-list-news',
  templateUrl: './show-list-news.component.html',
  styleUrls: ['./show-list-news.component.css']
})
export class ShowListNewsComponent implements OnInit {
  isLoading:boolean;
  NewsData:any;
  constructor(private service: NewsService,
              private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.Showlistnew()
    document.getElementById("content").classList.remove("p-0")
  }

  Showlistnew() {
    this.isLoading = true;
    this.service.getNews().subscribe(
      (result:any[]) => {
        this.NewsData = result.filter(x => x.active === "Y")
        this.isLoading = false;
      }
    )
  };
  sanitize(url: string) {
    // this.isSpinningImg = false;
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }
}
