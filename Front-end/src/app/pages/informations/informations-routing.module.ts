import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewsComponent } from './news/news.component';
import { PromotionComponent } from './promotion/promotion.component';
import { ShowListNewsComponent } from './news/show-list-news/show-list-news.component';
import { ShowListPromotionComponent } from './promotion/show-list-promotion/show-list-promotion.component';
import { GuardService } from '../login/guard.service';

const routes: Routes = [
  {
    path: '',
    component: NewsComponent,
    children: [
      {
        path: 'News',
        component: ShowListNewsComponent,
      }
    ]
  },
  {
    path: '',
    component: PromotionComponent,
    children: [
      {
      path: 'Promotion',
      component: ShowListPromotionComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InformationsRoutingModule { }
