import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { urlServer } from '../../../URL'
@Injectable({
  providedIn: 'root'
})
export class PromotionService {
  constructor(private http: HttpClient) { }


  public getPromotion() {
    return this.http.get(urlServer.ipServer + 'getPromotion');
  }
  public getdetailpromotions(NewsId: number) {
    return this.http.get(urlServer.ipServer + `GetDetailPromotion/${NewsId}`); //รูปแบบservice ที่มีการส่งค่าพารามิเตอร์ คือ NewId ไปให้ ฝั่ง back-end
  }
}