import { Component, OnInit } from '@angular/core';
import { PromotionService } from '../promotion.service';

@Component({
  selector: 'app-show-list-promotion',
  templateUrl: './show-list-promotion.component.html',
  styleUrls: ['./show-list-promotion.component.css']
})
export class ShowListPromotionComponent implements OnInit {
  isLoading:boolean;
  PromotionsData:any;
  constructor(private service: PromotionService) { }

  ngOnInit() {
    this.ShowlistPromotion()
    document.getElementById("content").classList.remove("p-0")
  }

  ShowlistPromotion() {
    this.isLoading = true;
    this.service.getPromotion().subscribe(
      (result:any[]) => {
        this.PromotionsData = result.filter(x => x.active === "Y")
        this.isLoading = false;
      }
    )
  };

}
