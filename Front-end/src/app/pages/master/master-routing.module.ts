import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user/user.component';
import { BranchComponent } from './branch/branch.component';
import { MasterComponent } from './master.component';
import { InformationComponent } from './information/information.component';
import { HolidayComponent } from './holiday/holiday.component';
import { HomeComponent } from './home/home.component';
import { NumberOfRightsGrantedComponent } from './number-of-rights-granted/number-of-rights-granted.component';
import { ServiceComponent } from './service/service.component';
import { SettingbarberComponent } from './settingbarber/settingbarber.component';
import { MasterGuardsService } from './master-guards.service';


const routes: Routes = [
  {
    path: 'Master',
    component: MasterComponent,
    children: [
      {
        path: 'Branch',
        component: BranchComponent,
        canActivate:[MasterGuardsService],
      },
      {
        path: 'Holiday',
        component: HolidayComponent,
        canActivate:[MasterGuardsService],
      },
      {
        path: 'Home',
        component: HomeComponent,
        canActivate:[MasterGuardsService],
      },
      {
        path:'Information',
        component:InformationComponent,
        canActivate:[MasterGuardsService],
      },
      {
        path:'Service',
        component:ServiceComponent,
        canActivate:[MasterGuardsService],
      },
      {
        path:'SettingBar',
        component:SettingbarberComponent,
        canActivate:[MasterGuardsService],
      },
      {
        path:'User',
        component:UserComponent,
        canActivate:[MasterGuardsService],
      },
      {
        path:'NumberOfRightsGranted',
        component:NumberOfRightsGrantedComponent,
        canActivate:[MasterGuardsService],
      },
    ]
  },
  {
    path:'',
    redirectTo:'Master',
    pathMatch:'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MasterRoutingModule { }
