import { Component, OnInit } from '@angular/core';
import { ServiceService } from './service.service';
import { NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { Router } from '@angular/router';
import { ServiceFormComponent } from './service-form/service-form.component';
import { isNullOrUndefined } from 'util';
import { DomSanitizer } from '@angular/platform-browser';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.css']
})
export class ServiceComponent implements OnInit {
  ShowListServiceData: any;
  criterianameservice: string = "";
  selectedtype: string = "";
  criteriaDate;
  selectedValue = '';
  isLoading: boolean;
  //#region variable imgage
  IMGDefault = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAgAAAQABAAD/7QCEUGhvdG9zaG9wIDMuMAA4QklNBAQAAAAAAGccAigAYkZCTUQwMTAwMGFhMDAzMDAwMGZiMGEwMDAwNjExNzAwMDA4MzE3MDAwMGQ0MTcwMDAwZjkyOTAwMDAxNjNjMDAwMDlhM2UwMDAwYmMzZTAwMDBmNDNlMDAwMGZlNWQwMDAwAP/iAhxJQ0NfUFJPRklMRQABAQAAAgxsY21zAhAAAG1udHJSR0IgWFlaIAfcAAEAGQADACkAOWFjc3BBUFBMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD21gABAAAAANMtbGNtcwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACmRlc2MAAAD8AAAAXmNwcnQAAAFcAAAAC3d0cHQAAAFoAAAAFGJrcHQAAAF8AAAAFHJYWVoAAAGQAAAAFGdYWVoAAAGkAAAAFGJYWVoAAAG4AAAAFHJUUkMAAAHMAAAAQGdUUkMAAAHMAAAAQGJUUkMAAAHMAAAAQGRlc2MAAAAAAAAAA2MyAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHRleHQAAAAARkIAAFhZWiAAAAAAAAD21gABAAAAANMtWFlaIAAAAAAAAAMWAAADMwAAAqRYWVogAAAAAAAAb6IAADj1AAADkFhZWiAAAAAAAABimQAAt4UAABjaWFlaIAAAAAAAACSgAAAPhAAAts9jdXJ2AAAAAAAAABoAAADLAckDYwWSCGsL9hA/FVEbNCHxKZAyGDuSRgVRd13ta3B6BYmxmnysab9908PpMP///9sAQwAJBgcIBwYJCAgICgoJCw4XDw4NDQ4cFBURFyIeIyMhHiAgJSo1LSUnMiggIC4/LzI3OTw8PCQtQkZBOkY1Ozw5/9sAQwEKCgoODA4bDw8bOSYgJjk5OTk5OTk5OTk5OTk5OTk5OTk5OTk5OTk5OTk5OTk5OTk5OTk5OTk5OTk5OTk5OTk5/8IAEQgB2AGpAwAiAAERAQIRAf/EABsAAQACAwEBAAAAAAAAAAAAAAAGBwMEBQIB/8QAFQEBAQAAAAAAAAAAAAAAAAAAAAH/xAAVAQEBAAAAAAAAAAAAAAAAAAAAAf/aAAwDAAABEQIRAAABnAAAAAAAAAAIrAdrIWhtAAAAAAAAAAAAAAAAAAAAAAAAAx5MJTcuiMvSdhQAAAAAAAAAAAAAAAAAAAAAAAHz6KV6GLVsuj7E5NLmAAAAAAAAAAAAAAAAAAAAAAAAi23WoPNn35m9G9Lq79S3Qrywj6AAAAAAAAAAAAAAAAAAAABgzxMheu2LOlPtrNKBy6/tTAU9OoeLefPoAAAAAAAAAAAAAAAAAAAAryw63I/K4pNiZAAAhEMnsDLZ6HH7AAAIySHLTXTLSam2AAAAAAAAAAAAAAAILOuWVXKormsuJq7UoA0SIRTLIyc5wAeNeuzo+upKCp+fLYkm7Z9R9VbTefQAAAAAAAAAAAAAABCIbdMTIlOq7wlyY6eFgwrU+1ltqFz2AHG5kZMc+3NwA51UXRByHvn2ycy+oLel+gAAAAAAAAAAAAAAAwRaYCqebdFanB+fVlhSaLdqXbgHNkxy7A9gAB59Cv4tdNakftqp7fNkA1jZaG+VtKY3Gy6EbkgAAAAAAAAAAAAhsy4xWD32a3OHnsaNLuAAAAx69bm/wtqyTT7YHiDE80Nz0VHbNTSckVWXPDyFWrU3dLMAPJ68w6IFoeKq+lw7FK5i5FT2wAAAAAAMOvXBk2+tMDFlAAABxubAzP2OpNDDmDm78K5JZ1YWbxiJ2PStknivrlr8m+zAp6VbyJlDks3t1jZy/K470BPmXt2EV9sWAKs5N0xsru2an65aDz6AAADViRNYvEt458+39wAAAGibUC5eE1rE3ukNbZ8nH7VQzE6NY3VWp35XUlskE41oVMW/jj0kKjmXEjhuahYk/GsiWvuf57dWJmIAAhcKtmpSwZTXthAACHc/rnBl3aHz6AADR3tcjGaF6hOYXhkZq2Rp6JIopK+CeZBTVsHBglyVaWX9ic0KjnOzXhbsSkUPI1vcj7Z8+g289ky/N4KY72hq2XEw5pQAOfUs7gx6kcaWWh2KW70tlxD3xjsywAHnUrs7WOLzEmXyqeyT4HOq244aQm5qXtox1TdNTE671VWuVL1pbWBc3JydIqC3K32Swql6cdPosHs8STtSqXFlACHQe5qxNyw6Y65aSJ5iTc2Kxc9YXyz68jakcZ70vXlkDnBmMZkjnDipm6HUwHQiHiWHM7PK4hPJVGpKNPcFKymN9GrR5HMlsUvZ3L4RZcRl3wrexqi9nbjJYAJgcGxN/wBygAAMOYQCL3PgKdWZgK63rG6hDe51xq4OiIzy50KosOHy460FnWAqWW44ebvnq7RtRTVkZy5PoRcmk2hM2Hj3wCtuxxrEIPbdVS0lVV2pWp1NeKj59LB9PnRkUyl5fXAAAAAAAAAAACD9/V1iWRmTeCm5h2K7Ojzeh3z5HtHrnMsLqbYAq+TwAWLEOcWBX0oiR2eN9WAPmxOZYvP+iAAAAAAAAAAAAAMNbWfACfuB3zncCYCIwW3NY4My+gByOdAD38x96sfGEAHSObJ5R2ZdfYAAAAAAAAAAAAAAYzJHZFXZoWHEOWWmiknMgAAOHypiKd2rAq82NftcWgR0OeLY6FOWHLIAAAAAAAAAAAAAEMlBkgFgU0XXG9zrFOW3V8wNSL27HSHbebpEf5nU2ywvQAIXNPJTvQ++TmM2GwACYzWmO5LZrW2QAAAAAAAAB496p92aqtMrX5IoAXVW02zkAsun7LNeC2vWZZevCdQj2zg+2ZepxuvLYe9WllgAHOqq5occLi5d852ORSMrtv8APs+g27Gq71LcyKyoAAAAAAARSVwkl2auLJKanfL4ha9YWjpEUnFRWycKHWfTxbdf8ABYOscjvzDtSwjek1WlqtbZAHn0KrWDVZt23Wk2OpGJSKi1LliJCGbDYlMWFy+6usmXYAAAABEtGQ1eXVz9LulK2vD9omFTXFEze71WWOQXxg451uSWHzeNHozGSyx+RAA4HfFc2NW0rO8ABDJn5KfsKG7BZQANeGzoUz5tuEEb3tFZbO/UFly9QAAAHmoLhhBy7LpezToVbbsGJp4rrRNLz6WHzpHN35pJJYxJ/QAAAA0q0tiIEryQecAwmaM8SLmXZ+Zyzs8VlQAAByoPZopbPYsHLA6tO2gdIAADX2BT2OwK7JFp8r5Z9ZZIRXtzvpy8DvgAAAAAA8+hVdh6leFj13qzAhkq99ohujIeeZ7Kpi4jKAAAB8+iNRS0PJqbvI64AAAhkzED7chGPIAAAAAAAAACMyYcHvB5rWzIucPRsCEEfsWupIWKAAAAAAAAAAAAAAAAAAAAAAABw+5HTYjMojhEM+PzZc3vhd2UAAAAAAAAAAAAAAAAAAAAAABDJfDSWxnpbRFoncNTkhn1Q28AAAAAAAAAAAAAAAAAAAAAAAanvY55wJfq7Qj0hFK2vFtkmYAAAAAAAAAAAAAAAAAAAAAAAAAPMTl3POgAAAAAAD//xAAvEAACAgIABQMEAAcAAwAAAAADBAIFAAEQERMUIAYSMCExQFAVIiMkNDVBJTIz/9oACAEAAAEFAvxLiwkvhJSni2txW/ZtF6zaIe4c/Zz37YR+3pyGtl/Zl/8AlH7enJci/tNfTEWO2b/aNR1FzKmwH2sCDJ+yt7Ht9ceXLYLJsGJ24T7/AF1q92Yvrvec8iIsskE0c58KuzkDf6wpIiGwaTJ8rq6TmLprr8WkF2seRKlLKFvZB/q/UJvaviwdsMCHEQ/AwoGG0CSrChu3a/V+oJc3M9Oj5m8vUY/rv7IT6iXnKcIfo7//AGGem/8A08vUf+Nv7VOuVd5WVrBbCSkaaViZTazA2RfoPUY/589PE9p/L1CXmbluWwD6QfDe9R1ZW+55W1HuyxH0X8UaImVY8GRfn2S3dKYEsgGXNBgXg2yNUJSSMWiV6hvBg41xuPHsCVlVFfh6iDyLwrXNpH1vUtfn3Nfv3Ym4VOa1sqbNEhLJnDDGboENMsFaIIcjlXFEAeL9gJOP91aMoICThwfX7pXlvW+FA17x/oH6eJdlGQE85Zy4+ng+4vGytohxJIz5FwDWH4XiPLfBU21mdfXX6AohmizRjlh69tfw9P65IZOUYRsraRsramR8jGMI+P3yzq9i3m/tX75o+TzR17Svsxt/mXg/ZYcPT0uaTTQlBuumenW1Oh/FbyUkfUdzkuPog4nLoAVGhNwy/D7HP+1Nh3MfyvUY/wCnwWJupD/cPs19cNPXwTlGEbK1kxgAkYJX1w09eD0femizJQ8JxJCwW7tbetx3CchTSZi2v+Tbj6leKEyz5jrsTUO+VVUSg/gZZEsN94jskkyuTVVEoPhvXPSlyQcoy1OOOA7Zr0+1wu0PfrKdrtmvHe9a1N9SGasU95Awifgl9nSK0MMK2qkxkIRHD4H7AacWWCMkrqybWDHEUMZdCrOMtS1lwDoPU73bky5S7gIiyERcsThy1W7VveVTXdKcN75aduuWzFKffLOWcsiQsMULOT3ytNCVG46Z4lbUah8VlbaDm9ynOtqOXh6kj9ax+SZNb56tFO7Wymb7hbLpHpSoW/ZPPUnLhUMdu7n2y1sdtTwYyGkOmbln8CYw9c2Dh9dYmxppf4t71rTlyIWa0zYsV9eNOPwTlGEbG2kfAimUldWQU4ME6IVLNdreXC+2Evvqha6gcvEunOtZ7VvJwiSDYJKMrXAu1bYm2fh/GyZfNdIOVdd3eCFAUONjVjZ1LW4SrHdpm1vnrzOcS8Wb3JSefxemZJJZYSw/gaaEqN14rsk1CuESTEoPh98cX7ZqmsNm4W6XamSP2rX3wkIkg2vJZikb66+XjAjm4pKycP2CuWpetYBHsxhDiIfjfq/TKBjZF/KwudDxetaektWKr/A2zBQW74OQvFt4a9FqJikOSvr5u7CGAB56glKAauw03DL1XqBHOQ5rl0cJhQOJ1WShqRnrq5fre8Cx5rmcsGG/BVYjZVFhqhye/cSij7rHyfhoiWvt6e/zPG1spMTrKmIPiMKJhNLTUNwrKzbWOl7NJC1E1LLsXVQEWYSKHiyD75ZKdmz6eP8Ay5aqd0qkzJU8GBSBZ2sTD8EUiuzWWGqPhv6bope2w8rCfTS19oylGS10ceLWSrHG+c2OFLX9GHhvfLVhcZU2J5NeDqkGwlHMJeftlrlys4dRDWVDndA3rUtPLbUZomekxlkr3aqh9rMxlqccfjGD/wDzwr6qZ8hCI4cbAfReCXYDCnEo/H1Cx7RceWVpHsLdhHqvFKysPBlkSo3rArmxCmaYQL1EHHjOS9O7J1+PqBf+Tf2Qn1EseX2q1XMds5linFwEtTHNBnTS2Xa/RdrLPSwm7c5/GEZEnXVMQ+V+t7oZUWHbb1vnri64NMZizOXiuLrsDhqwaq2VTG1rl4WFoNbDFIeaKBXNmcXrYEnIs0K4rmGsAIwq7ObReDYuutr7AbkqpGWpRtEtOAlret1LHcJZeJdSFK327G96jq5dEz5JImckmmJSHlLWpRskZJFxOxOpg70G9bu1MPeTlhJzLPOec9ZzwJeiac5JnTsEATESJR5OcRxsLeReClZGEHrSRda1ve1q4S0HrMjOQjKcqqu7TwJ9C7/09Az7w5dIe+NM327OffGhwGydo7EfDXOW0KbnkY6jH4CDiSDlMSG5a3CXDni6jDOAotYOtTFnbgyaKs8LSKyzSDiO7IMAFrP9fnqORPeuEjBNQVqIttmbmqsVue9qU8WWCskTUK5ORlamCFuQjPCctQjz924wlKmTP2rWt63rLVbSzlbb6jB+42WPimkZySSAU9fIQQy6nUpSzVIngq9QW/P1Fr+8qv8AX4UYywashLxlve5I1kz6bsoChveJVe5xbtNajlVVz0ThdH6KX/KMWv4bYK9ozRN9QOXZoGc8Y6lOSNNkY6jr8j1LHKjfOuy/nOCUI7nIKS9eN6wK5sQ5lmNZarg68ZyS4SMEr6wSnhbNd01y3LaTMYN3gOqmEkhEas2WdeKVadvFEwqR/Kvh+9D06X3K5KOp6ckGsWKUh5ooGc2ZtatgScizQrSt4uAa4+N270RZWR114mJo71x1Q+IQlYmjTjD+aWGijrC7SseDysXALUcIzvTzAGOue6+n5Z9vCwegmMk5FJCEiEelEI/JKnIXAhGCH53qBX2kqHO6X4sLiZGqgurvwu3CrQ3vcpYLmtXeKaJnNpVwFNfoGHV5HnE9U6m2NsXwWaHfaJRQ6ZhEARE+gGYDtY/gm6VOajYmx/nRlGes9QD9rldODwShaqzp3QiZCcZx+CySi4Det63D+9U14iLMBK+zG3+aaXTDRudE2Xq/VTETYia6bIG6PnkxNJShaOxz+NOYS0dJmuqc0ft4XqWCJIJHhx8664z7/ku2hVbBc42RFj1BZVs90pvXPT621GfT5/etlxs4QpkVdKakWjHaQp69PezvPHetb1ZKdmwkWGsMKQC+VfZEUwBhnH+JLeoxAYZx5dw9tlUNds1l0r0GqVjoOZbqd0tWs9s3jENEBr7FMU+CJIBW/wCkZNiLQPF5aLa84ShL/OUzUJbj4rMlVIi+JyP4bX+NWObTNret6vltkDv66rz9wm0CDIWQEWNWs92rlyt0G0bfoLtWx2I8UJxnlaxKvc8r1P3RCWQSvChlET2PN1AD40mwpvwjuUJVttE34L1mRJ0BoHHvXPRR7EWha6gN656s09pn9Om+uXKncLVbnaMEMMcLWw05vxTqTsZepf06Rzrh8d656slOzYRLHOU0nvvm9c9OUwi4wsZWXHeV1tIWRlqWvm9Riymb7djL9b2FQP2zmNrxaAGc0nISjOEt61Evt62+e/FSuYaxOtArwlHU4mgSsfXNFgPi+rFteUdxl/np1RushwlGM4uUkZYYRF58a+wIlsBoHH8XqD3wFWWs9Ex4Hcq71vK8/cpsCicJwyAWnY7hPL8Xsdr7SSg3bAzniqkw3idSBfxt0+6Xo3OibyvU+egGkA1Z7db8CiGaDlLuOTjKE+CbZUyKNCbF8NwLq1/31Ts9wnl6r0j+nT8p5fK+8NY32jJmQgjaO6dL4KqHb2pTADn287pPt2Khzul/Hf11ZqdmzUO9qbyZVCzFunMHP+4ucixUXBuC+Detb0wLa7FKfoO4yGLAByIi4OcSQP7Ol/3l4KqMNbUpgizWta18DQIsgXISvdhLU4eFjawXyc5lmkDuDhJEwvNxADeN1bC3AJZgKg7BwXweoFvrlY7psOeoF+W0LOaYmmzt74qostYpTADmtctfHep++FA5xISAoWFvM3BRfuSMMx2L0+b3KfC5VgZxtBhTAGmAqLcHA+ZhRMI4ZLmhOY5wum46ZsWWo8VatpjFalYHz7+uWAJIuos6bXffEnptszcsTppkxTe1LI4ugekN0n/jcpwGz2N1R1WINB87lLuBa4iGQ0l6Qs8WRXW/Ct1O6WXZOtkpSJOuqMu0Pbune7oPqFf2Gsf6moy2OY56IP4961vQ0ogP8D1Nshh0Jd4CnUHkYxhr8SwqYMyQrRKcN656eDOsel3txof9Wp/5RG6iX7H1CP3JV3LsQx9ptfahN7HP2N3/AKyn/wBb9rXX2ETYTR3qUf2F9L211Tr21y8NsWe9bhLeUpuqh+w9Qz92DjoC9BrcsvlPbPPTxfaf9fOWoQrYSeeuT9FGuB2yZhRMJkElTqF6Df69kOmACHAAof8Ak7Hhbpd0H76rTddL9e6vtoC4YLh43SHsl6cLzh+039cgp2Fl83//xAAUEQEAAAAAAAAAAAAAAAAAAACQ/9oACAECEQE/AUq//8QAFBEBAAAAAAAAAAAAAAAAAAAAkP/aAAgBAREBPwFKv//EAEMQAAIBAQUEBwUGBQIFBQAAAAECAwAREiExQQQQIlETICMyUmFxMDNCUKEUQGKBsdFDcpGiwYKSBURTZPAkNGOT4f/aAAgBAAAGPwL7oIIT2hxJ5Vedix5k1EDmEHzSWTm2HpUceltp9PmjHkN08moAHzR/TdOmpAPzZJPhyb0+azqMg53COaRVdMMeVcDq3obfmXQwntTmfD1LRWEt8cnxq5J2T+eXy+xfetkP80STaTmd/DE5/wBNcUMg/wBO8QzG2LQ+H5a0j91RaaaZ82+m6+xKwjXnXZxKDz138cYveIZ1jxRnJtx2dzxJ3fT5YkI/iHH0G5IR8RpUQWKuA6rRuLVanhbTI8xUcvI4+nyxF5Julk8Is68MuvdO6BvwD2HEyr6n5GP5Bun9R14v590Hp1zHFxy/RaLyMWY61Zbfi8JoSRG1fkMMund3SxeIWjrxQj4RaaCrm2AqOPwqB1SSbAKMWzGxdX5+lCbaRhon71Mmlto3dJHl8S86EsZ4T9PkDRjvZr6150kyZrSyxnA9UyP+Q500r95q+0sOFMF9eqZJWsFdEgNwnBBrQlm4pfou6OcZHhO+3+E3eFAg2g/IDtUIt8a/53Wx4qc1OtYt0bcm/esHU/nXHKi+rVZDbK39BV+VrToNBSxJ3mNLEmS9THikOS14j/atYcUhzbe8WunrRBwIwO87OxxTu+nyEybPYj+HQ1clQo3n1pJyO7gOoYtnsaTVtFouxNz4pDQjiWwdU7VEP5x/nfHKNDj6VaPkN2RAw86t2eQofCcRXFEWHNcepbzc7izGxRqaMcBux6tq1CWcFYtF1agqgADQdczbOLY9U5b4P5B15mjcriMNDhVxuCblz9Pvl7xrbvZfC9X5T6DU0Fxu/DGtCXaRa+i8vZf+nXj+NhlQRRazYCo4/CtnUeVhgotq/E3qNRuEukg+tWjAiuil98v933uGXkbu9jJjPLZZFy9a1eQ/Sr3elObexLMbFGpoxQWrFqdWoRxLa1W96XVurOv4DSyjLJhzFB1NqnEU0fxZr60VYWMMCKWRDYy4illX8xyP3qX8IvUEjW8x0qxbsu1+L4Y6JtNlvFIauRD1PP2N+VrB+tY8MQyWrEwQZtVyIep1O8jKuj2sWgYXhnQZTaDkd0kOgy9KOysfNNx2qIcQ745jdcb3cmB9etaTYKx2iP8ArX/uErgkRvQ/cW6QgJZjbRh2LhX4pT3moSTWrFy1agqCxRkPY48UpyWukma0/pQkltSH6tQRFCqNBuRJTdv61eU2g67mPwycQroZD2T5fhO7pEHap9RSSp3lNtJKmTDcQvcfiXcCx7ReFt9pyopsoDHxmrZpGf16nDK6+jVs/SSM3GMz7a/IfQc6sxu/DGKEu0i19E0HsjFs9jSanRatJLO31oS7UMdI/wB+ps7eoq6xtgbPyq0UQPeLiu66x7SPA7vtMQ4G745GjsznBsU9d0Hix3KCeCThO8xRGyEf3brsSM58hXFcT1Ne9jq0x3l5pjuBGYpZRrn5H2dpNgorB2r89BWsj89Fq3vS6t7EsxAUamjFs5Kx6tqaEcS3mNX345ufL03PJdvXRbZV0Ndfwtua73k4huMDHijy9N32mMcLd/150rnuNg24owtU4Gmjt7uKtV6c2Srp4qMr/kOQ392hAh4pM/Td0klohH91XY0CryHULxAJN+tFWFjDAirG90/e8vOrRl7C9K4UVZs8f+pqxEsg8hw12gESa421ciWwfr7G/K3oNTXFwx6JVyMcOraCrsYx1bn1Hi5HD0r7PMbZB3T4t3SIOxc/0NJLpk3puKMLVOBp4W0yPMV0THtI8PUbkERvXM2HUEa934m5V7lalOi8IpIlzc2UsaCxVFg6w2pRiMH3NC2cWXp1zHs1jN49K6adioOrZ1hHebm2PsOkkDFfIVhDIa4kkX8qPQxuz+eVdJK15qvHhhGbc6Eca3VG6GRDYQ+dXXwmXPz3dOg448/SlkQ8S4iklXJhTRyC1WoxNiPhPMV0bHjjw/LcNoHejz9KWaPMfWrCbkfhHU6OP8zyoRxj1PPczeI21b4VJ686nwHdJy6P/PW+zbNbcyJHxUJZwGl5aL7Jo3HC1GJ/yPMbxLNhDy8VNJEq8Flg0oRsOjk5aHc9macVLLGbGWllTI/TcVHu2xSn2c6cS7jYO0XFaWZfRhzFCcOOj5mmggFoOb9WxeGMZvXRxCwfrvI5VZ4lI687fgO68jFWGoqyZRKvPI1wyXW8LYbxs6HFxxelDaJR2jd38I6tpyox7IfV/wBqEEzXw+Xl1Sj5/CeVNFIOJaViLQDlQsyqZfw27rr+9TPzog5GmiPdzU+VdAx4JMvXcU+MYr60kvhOIoMptBxG6dU7t6rLcM7OqJJ7Vi0GrUEQWKMgOpMv4rRSSrmppZFNqtiOsuzjN8T6dXsZLIVzMh4RQCjpW1K4Cm2iUcANp/wOrflawfrV3uReEa+tCOJbzV020PenOQH+K4zdj0QVIoJ6K7j69RdpGa8LboW/ANzxfDmvpSP8J4W9N13J1xU1dNquppJdcm9d18Dhlx/PWjFNaUHds/SisXZJ9eqEjUsx0FCTaLHk5aL1l2lRimDem7oZT2RyPhq0dS85x0XU00snebqRxZXjZTQ29HskGSLRii2RY7BaDzrDqFI+ObloKMkrXmq0cMWrUYNjUNJq1GSRrzHU1ePBD4udfZ9iRWIzOn/7RilRQ1loI3yR+Jd2xyjugsjj86DKbQcRVg96vdNFWFjDMUhPeXhbd9pjHGve8xXRsezk+hq0mwUsUXFcNt/rcIux6uasjGOranrlSLQatGMLZHl5bro44/Ca445FP9a/iH/TVkEV3zai8jFm5nqpKMbhtr7bsvHBLn5eRouNmaNjqDbSyIbVbLcWchVGpox7Naqavqa86+0bcbiDG4f810OzdnDlhrQVRax0FfaNvIwySuji7OH6mgiKWY6CjJIbZW+nUkH4jQ8p/wDFNs7d5MvTcdpiHGO8OYq6x7OXD898scZvIDhQWWQlRp1Qqi1joKEm1f8A1/vV1RYBoPYlHW1TmKL7Nxr4TnV11KtyPU7KI2eI5VbPLb5LWECn+bGvcx/7ax2eP+llcF+P0NtEw3Z4j3k5/lS3FKB0D3D8PlUH8u6Jceiz/OujhW0/pV6Q9LtOgq9KcNF0FXIl9W0FWL2u0n/z8qvzNbyGgqyMcOrHIUY4R0u0amlinVbHwBG8scgLaLc6YqLQs1p/pUc2gz9KtGW5lXuNxChFtRyyejHswKrq+vW4BYmrnKuEWvq5z9rZIisPMV7q76Ma+P8A3VakC2+ePsEP/wAf+TUH8u67IoZfOjDsCqPxgYUWc3mOprpZ+zhz8zX2fYBdUfH+1Wk4mum2o9HCMbOddBsQ6OMYXq86XaNowsxVN7D4pOEbuIWiQk/4ox/AcV9K+zueOPL03WIbQgst6wVAWY6Cg+1Y/gFWKLANB95gf1FQ+m6xMmaxqCIt5jkBQ2jbSGfRas7kXhoRxLeY0Jtqa/P8K1x8MeiCujhW0/pV9uObxcvTqcJ7NMFoKuZwFNsOFkagKeZ1ouO9FjSyxmxlq7b0acl6wb3cXiOvpVkS46scz97LeA208eqN+u4qwBB0NGWKFAxwGFdJK15qw4YtXowbIoaXVqMkjXmOpoM3BDz51ciW6Op0CHtJM/Ibm2h/dwC8fXSvtAPaXr1dHApF4cRPWuQpeNB5rJJP7R99aM5MLKuPgCbjb+iY2ag1enk6QD4RhUUMXAr52fpQVRaToKEm1C06R/v1bTjIe6tNI5tZs6WNBazGwUNhiNoU2yt4m61mtB9otRPDrVyNAq+X38bSowbBvWrGPapg379S5Kt4VbGnFzPVjSI2F9aLMxZuZ3faIFtke1Xf/p9bgFiauatAvSeM/IZNi2lboOF7Q1aPyOjCr6H1HL2Kcdxk8q7KVuk/FkaMcq3WFWP7mThkHlTRNpkeY6tqcSapV+M+o1H3+1SCPLcr6OtHYdoxKi2NqvA2cnGRoLOOjbnpV5GDDmPY2fxF7hoqwsYYEV0f/MQDg/EvLrCSJrrCrjcE3Ln6ffXfwqTXQuezkOHkd18Diix/KklXNTbQNgZHFthotszWfgasRJF5ivfW+oFfw/6V726PwilAZmkY4Y0Ler9qjGXf/elljPEuNLtUI7GXTwty63nQi2o+kn7/AHpo7A0QstFCWM2qadPELKs1FKx74wb1qw5U0Xw5r6UYTnGfpuG0QuRdwYaEV0W07PGjt3WThq907oPxWU/2bahM6C27dstp7e9d4etYcqsHum7tNs83uJf7Tzpon7y9cK3HDy5elCSNryn7qWOQxrpImvKdzHxAGgpPZyYHcZAOzlx/Orh7kuH57uH3iYr+1LIe4eFt0iNkV3DpZGezK2llj7y1Ht2zYJJxD8LaillXXMcj1mjOeYPI00bixhga/wC52cf7k3M4UlVzPLrX4j6jQ1w8L6r90l/kNAn3Td4VaMjQnUcUefpujk1ssPrTRPkfpRifvLkaV/iGDeu68BwS4j1ro5lZ7vcIoooEaHln1H2OU9nN3T4WpoZsEJsbyPPr/aoxxL3/AEpZk7y0u0w+5l08LcqKaSLRaPsn8sq7ROHxDLqh0N1hkRQinsWXQ6N9xuMgaIi3zoSRm1TVhp4jmpsowMeKPL0qw5Vh7p+75eVSwH+Ybi6+8jxFWn3b4NXSPIqpztoJGvZqbbTmesGfsk886XaFtJXB/PzroXPaR/Udaw5VYPdtitNs03uZv7Tzpb+cTf1G6w0WhPRNy0qyZLPPTqiLaTamj6irVNoOo9vDN/pNCJj2ch/odw2lcmwb1pJPhyb03NE+uvKgxHFG1hFBlNoOIoljYPOpAmKXjd9KFpJsy6oIFyPxNQIF+TxNuKsLQcDXD8JtXzFLKmTdYxnvZqeRoo4sZcDX/c7OP9y1E2oF0/lvKsAQdDRbZjcPhOVXJkKnqXe/DqvKhJG15T7OKRHZbGswNLDtLXlOAfluki1Iw9asOYqOT4sm9aeJ+61NC+a0tp44+E7g4/iLjXRsnSJ8OOVXTwR+EdXs0sXxHKrz9q/M9W1R2iYr+1dC57OQ4eR6/wBqjGI79LMma05i9xLxr5HUdW5IgZfOi2ym0eA0UdSrDQ770fd+JedX4z6jl7KXmvFuF48acJ3dOvckz9ak2c68Q3DaF70efpQJ92+DVelkVaW6tiJlbr1eyTDxHKr0vav55ew6VfdyfQ1xHtUwb9+tYasX3b4r5V0T+6kP9D17sqW+dFoO1TlrVmu4SRHHUc6vJgw7y8vYkHI1JCfhNXCeGXD89NzxNk1AkccTY0rqbVOIp+lICWcVtEDEdXsk4fEcqDTdq30qwZexaJtfpXEMVNjjmKDKbVItHVMcXHL9FovI15jrWOEacTtyFLIndbH2HaLY3iGdXh2kfMbhLEbGFXlwcd5eXsU2lf5W3C09qveG5NpX+VqaO7fX4ccqtlbDRRl1OzSxPE2VXpe1fzyqwZe0+0oOJe96V9lc+aby7sFUamjHs9qR+LU7mF+4iLeY+VfZ9mUpBqTm/rTRaxn6H2Razo5PEtWut5PEtCWI2MPrV9cD8S8vYNG/dYWU0L5rV+NircxWIjb1FFHKhDoB1LSvRpzagSOkfm37fcODAd5DSyDP4hyNY8TnJBV6VsNFGQ3B9pNxfCM6CyaNcb0qSI/CaC6SCz2l6Lsn8sqEl3DmO61CWM4H6ew6WMdqn1HUuxRs58hVs73ByGJrs4+LxHP7lwjtExWj0L3Cc6vMS7tQl2oekf719piHD8YGnnVxz2qZ+fnS7QMnwPrWz7V/1Usb1FLIuam0UrjJhb7Swi0V0mzm4D3k0PsTJs7Kt7NWrtZ1H8otq1lMh/EasVQo5D7qZI26OTXkavd+XxHdYaEkJsU4r+1W2KkSf0tqVdYZA35HdcOcZs+ZB9UaoLvgFf8AEodLj/Q7jHpIPr8ym/L9RUP5/rW28ujf9NySjNDbQYYg/MWHiYCoPS2v+IKNUZf8UUYWMuB3JzThPzGHZ1xZjbZSrbhGtlbROfjb/wA/WhtSDA4PukhPxC0fMCzGxRiafbpBwKeAU9nefhFRxnvZn1po3FqtgaaF9MjzqKXQNj8waIsQG5UEQXUWuk/5bZ8vxHffQdqmXn5bon1ssPzAxdIUtzIpYkyHUO0xDhPfH+alh5G8PmthpJY/cScJ/D7f/8QAKhABAAEDAgQGAgMBAAAAAAAAAREAITFBURBhcYEgkaHB0fAwsUBQ4fH/2gAIAQAAAT8h/iWwD5H80sp+oWsY8vWP7S9cjex6VZv9Sz/ac9R4DMw3vPx/aGQ3f64B6ErtPz/aJIjrQuHI1N7l3FCJIyP9pEjBnnSSVivTOJ0UbJu5f2SUtPofmjgg0XAiYSkiYeU+aODLuv7/ANeIRue05qVTKlMvBJVzP3E0PIu6qA0g5pHa2XP+KESRt/WO5CqMsJY0GhwbkJCM8hQ5GNZPm4hMRcvz0YzUH+mkmpQwynXb2/rEcXl+jeOCKw3nY1aCICDwgTJCVdIl+gaQvHznpRf+r2nm814A/onv/wA8ZgzUnLJ78KeVVkeZn8H6zD+jEJv73CHP9p8aINfhwGE731fHYHhfc58qux2VReXUbHR0qQKeY7P9CgXIU+p78DUcfY/74z1hd7H6oL0sBzaA1m89DwmWBKtgqdbBqex1ppGZrl+tKgECyBADf34TQlWTA+61P6yGq2f6DQ58p9iiSzILJWdjmN9yp6B+Ts+F28WButin3lZanPVZ/jwnhPzeRREKBndVWGZw+l+CFrM/PJ7+XGWJbPvFGWBImv8AQZ6Vy+u9COKm8clvregQ7fnso6TNwNDyNyClwaG0FckYMHKjumEfNYdODnz8ECsllv32r6UGrC6jLvTY4jYpkvYYpCauB04ZpdTvm/x/QsGm79UNRhzTV0qBqPOo8Twxx6ufvPwSgw8nyND1p1KuxvUdDzuu74UkWcX13ovwlKi031Ugkkbj/QsXfQTUp9iRz+6vHTO8E5u36PbgLckqQFSPY8HQ2K1ihg+Aq2DYCA8SAhuNLja5Z6eVCOOEj3Mfp40NFzkGSrRnlfpp/Mnhgu/HtxxRKnoU5g7PoUvAT1kvPdqMzNp9e7+KMFF3P/aklAA3q+sxN3g8D0GcDNXaAyWTnwSOt78lv1FXAmQJkq/gFnZv1/lyl113ufrhyLrgKQ24FpNdmcfT/ic+xRFW1zTkfhFuSVICpMYOH4Cpy1nYN2gueXdOR4XEyweVSItSRCjJyo7aC46UWsmRkpqSpKsALbzg/lQ8LwO1/wBUy/HFM5cFy6G7V1J1X/rRGDuZXP8ACz2garYq7aiTfVqNcy/g/wBojB3MnPjEipIk0q8ZJFZ1NaKOaQuNN7JSoZJ59Fciv9j38+GJD9w7UM3qMTDyND4nYgyrBSETOU/1T8HcYr05F/gsXQBqCKleW3pHYqAmWYfAUWUsBp+GKWC3+3YpLtA05QrlqHscudWpQgW4ITgUhIRvQcApAyPC2X3H1pTvLL6RwYpw2jyqYCBCtVCf5wE/BQbblCSr5HVZ0e/EGQBdWiJBZxdipfDSVjoVGoVGjIO2QpqD191/Myh7GVyotEJ1N/dqFLNk6m/4kkYeX5GmDuy3VVgrkmD608GA2velEHIs/coCQRuJUafX3bvTcRsmlQcvqaaP3bhP3FYO93/dBrpj5jgpn/jbhbQ800fPgoJWA1pIC7p92rFcoEzUPPRyPpSBabv8UavL6DNCNSksSRpryyzzD8aIAZWixmH5WtZ6nKwexQzOynobfhut0UgK0olh6OxSJNEe9BECdv214IKp8LExRRzYsL04AInvUZ9KKGnbXt3/AD8cNYtANPk+5qSj6XvwiFGB1KlqFaJjRpyxCkutyhHibaexw5kiYa5akhjvz/fzRYpXfSAMr4osTaEeDIEEmOv5pZz5GlTMlgG6gEgq4n4OrXm70KMmb9cU5MjxZe1BDWBC7BRPeDqt38NvDY9ArPB4G3V3qwEM+H7pVxRZsrigIbjSJzEj9FFWYutxt14Ns1R5VPLsXuM0IJLjho3QYKkSfhDWwp/Qfam11oPCCYzOCo4nQhvtOHpKSvSj/ZpdrGW1WpAHiLOjrGjWSlYlITux7+NqObOQ6b/c11K730KhEOtvii1g8amYIzNTrZuoVnb6Eoq0AmP2mlC01dKnos5nIUVPADgmm3jRhq0olmzc4OVod/8An5qFMglY+GxtyqMKISty0pI3UfnoeEFTBzL4aRjfNBqNSnrj6utBGOJUevgVbSZ1Fu8Fbyq70JPTP17+MiJuHUucJse5Pk8SJEnWHscv3QWYBn8j+KbcEJRU847bnDFNATxg/wCaYYREe4KlBOE+Q8FUJQ8ufSamCKRrD83Jy1KQENxyUwrVOW3ap17+U1+8+AFku+s7d6usHuCgw2TNAdaWdZsRbkUW8EIZF7ByOdFdyOq3eIkMqGjl6Z1z7eMFGLB1SDhAsAEhqB+wHaoEBOa+OMaaYzTZ3oLA1j9JfCDIAurTSJtD9d6TROk5WeGeNroX1VUaB4aDCAKsNaUstFKVn9F/argSRNad0wnK0aIsQhHWg4ZdVSX/AEF/v44GEGR+taDY4OVhKK4MQ1Hg0AhY0dTzq+dfyJ8Chmo3Kvh+Ao8JYBY8Cr2FdJvWL5Mb7lQggJ4gX3o+THr+vAhoBP8A0mftSpDLC/ym9SD4VpPhOVCGvQU86FP+lNXXQ96jELH1POmnNBbvvUpASGhIj38E4v6Bw/d6vpziB/VIJCWrVY7qxUnsP3HD9aJu3SnA9zCUTE0hpq4SUAvs9e9GnfXclPJhZ96CPBbwMUQyyTP5HxOjPwZ8/wB1mtT31HxQEgjcTwJTLxNynYlJ6cvBM7A0m1MdbMTFvW/29P8ABoYoN9f3QCAByPBZV9i05NN8HIqPiWX23rBUbMw83V5U8POoUJt4X6KHZCOrm6qz7CGHlx33QOunrwWKXc6XeZQYByGpWAN32qlWFEyNSCnzo/yOGiXtPO7fqgsq3fsNOTBdVgKj4CVo6HiwaHYdt6y683jBRChHWoIKPnKzUWp5H0dK5mlECjFhtihyTm5fL/tOnepwhXOqNMdEMN6l2SoOE52KtVPYEPvKoMClRHC4VBSAqUfQHQ2oIYJV61GHbEj65ZqJkWViPYpydoBK1GfMnE893lRCDjgsHP4qySAuU8WFEGDt4CAYAPOhPMfX/NMx1fvs/vhjg7R3upRW5AnbQ8EBCSOlFRu4v2oJOgaHm7vPwhIWgErSdxA/t7UGDbAQH4S8FhDNOh5M6b00BsjDUnKrcqgVHSek81W63twebXnHD30RRa6aHjrQr0rM06D1p9M6mPNUJvMypp6XwumEPI0HJ1nTmaPkYsZOmxz/AOVgR46MdWp6lKbWu6nw9auKOwcirMwrBYEIZx1dOlS6BcSHTih8OTyqaWUrTHLCaH/ZTgZoJqs0CSVcTXgAUWXadPOk4iccybNJk1ksum1BFvDgnf4W9TmbwPRt+XlgompVmnUlAZR8mnXQr/rWPHFGsg4iKM/QZKdLagh03edKluFJWjSRmVh9jnUJtsg+u/8A2ri5uK3mt5aDCOexW0c0henzmsOqvNokl84nd+OMTv0n0rFBCBOHb/in50v6VJcwdf8APAWKTjfxWSoFxahy2RrHVoOWyAgP5OF5/pHvU/2R6vAZIEQ2vamlWFwtaYM9weRq+h61N1jBc9adrjBRhGuF4eR71cSBnAOu9Dk67pzNAHMlj6a+CTud+3aCErgbzRRsYaB9dqj1ej01+8ql4KRpZCtnX6uaCLeBQzUwEnUHJrWe1uP5ZD2T9vvXOkdv+HhYoshI0otyOErvytTl6aunSpGYm4fresDJteHm69KeHnUMyvO9PzR4o+bzfBoyrvvmggiiAXfO0KBg1HOgdrFp5yHzQR4ZgNWMHVroRUehr/Nym996zDDbGbPnxniYwdGgWgAQetZOQK2AizzpFC2Li0UWz6B7ulACAgNPAnQJ1ebypYypVNjjCrHoh5HQ8WUEqsBmpQ5UM+u1ARLQfz19Ot0NQuI7jt4HvCp0p1eNeXw3UXnWBHzT4hlJXg7gaTudANJ3o+vhwS2MDtvQLdw37bf0MGzATkJ7UYRtpdHQa+127/CMOQYbrOaj4AM/QUhDRb8ylE6e1lr2q/BlpaD4ZsS5mz02azJmX1P5989JKk4WzdTzPpSAu6MNu36oi3R5RfiguRe7+KDIuEk/CkgF7m7dKf8AJUaNc7lbtfspST4WgbLWiCEi+n6afzXCz5AUob4Prfgl7J83z2rD3hUhE8CZGgQflOzUkd3kHuWqNCPZ3rE0RYfOfzRiQbQD1zRLvBKWetBCUoXd/DLr7CalIajQabbMvmvE1EsLiVB0b6PX/tCCS44f5J0CySHEyPevXFgdmr4xLPqVCCrehUsp8na0DBKslQmZ3d6Sc03V/s8JwZl3Ryef7rfYAhdqUAPKkH6qeYDnQMw60mnXvXxIwFWR1p1Jyv8AZ2p1+ivk0H+E8+fizpSMvzf20oEeoP4qqWZUKDqjgyWneUe1NCTh7OjwS+xdtqKjFi5/T478JZGfnb91SVjyVovcaj4lD5Vf0a00ozEmKfaFk58qGbr9g19awJ4oSz2IamZGDnV4M2O/zFDJNQcgiJuxNCPhtbzlw860mS/k6bn8TQ59mnoWz71A0EJE1qd1L1/54CemX2KzQs7LqtypFC4GpolCjtfALQu1aj7vV+hZqDZq39xyRtNY4kCGQ7LT0WrcaHjaYCAa7u366U2sLJz5UCHr+qpOOURzL/NRx55u6lZxab6qZ8CsbrIKkbhYfgf4MVmSFh3oPuK0TBZIayCtdqZydU5/z8UDAKsjU4OsqIWvvD7cAJW3mGpUQuMdtntVnoSKx6VBkjCuex4VCoMucRf0KghUHcjRVmMFpfrHiBgFWRpXJzvt2oqzSny6QSQVfpqUIJMNAgCNkagm2WfbT7ap5Robrv4AOamcx5OpuUHLfCSP58Li/wC496XGG/Q8IGenaHy/VT03PeUVggwhlo06rqkYaFcOQ1KHIJK2AUCrlG8rVgssCzB4FDNXl7SiTka1ZlNKXttwK4NB1KuDqTr/AGKSSSnpy8USxQj7EQOlJz+7+c+5qSn1BZxzg2CRrzJ3XR0rqwvD0fBMUpqX5igxag/G29spjP8AyrMIqyufzwO03DdBigSEWmclTgyjsM0OEhD81m9onc0ajU3+C+XAA1geot+opM4L4Hk6VbS7mz1fAsUxPaekMEOjY6HhmZ3XHemElw+ufG+KsE1N6bW9MbmpQ2zeOU3p4XSloK35Se/Z+ayIiCHjIaVlwaIda5XP8SEMEe2fSeCt6d92fLhDFcjl/wB1IT6x9uEUtLn/AM1Mt8j2e1RYiSS3ehSMwkWUvBMVDV32j3qI7ct7fNACCweO0MLP7aseJ5m3d4gIAjZGjkjATcbUuQJ83z44GtuCdGpwvkfz9tUwlILI8I8g6A2anVqDK+6/hJCQhKlBlw5mj5VGQPtPuvDGnROzo1gLAbmvmVG2MnKssFiUENQCEKw7lR4zFXV7rQ71LDktj21oEACwH4cf5s7tGp0h9VO5UOwk3PDYXhfc50w27VUIdiQ5pcpCH4Ap2G3SZGLTudShHFenpDya9bAF8fhYC+tmr5GEuNQ9BR7/AAMVn/Frcjywl8VgL3sB24qFQiqcr/VQX6J7fNAQALAfk09Nprv7VBO8n9T38+OYECWqQeA49LY9aAKkEIPugzBUc6ZyLf4VNGz8z9z+KLJF4MvM1qU7hjvtXoqRyNHLbbUf4CWlSrN5Z3N6JBeEhqGM7ofpqIu0xp9+M6a1CTLlvYzVpe552w/OAIkjpV0WXluXaoaxW8wrqvhfvsVgK7CiSKPRa5+7tUY0IbhVv9ppVfO2lTC3/fk/Gk2akYeWv7VOr0NwNq07Amq2fwOckf8AKlJPCSu6AKKjjffND1o+Q7nzfwle937cpRpYGB9Gmqqy3VrXJzsfWlK/Ll5FaGeOVRBtiXkx6fqnFZmO2+1YZP1Cn3tB0fyOxJkblS687n7P4R5ymQA8orEDs37RUKC62vKjZjgIP4oFk2XahXz9DocAYBGyNdg1fmqSMhBhe5rBPZBFZpHpf9uT47f2QRlgzybfFSuANLeL+tHdYsHkcCa3TnJc9J/s3nPT/ZUHNh4IBbGG9KCAkd/7H7ADPtTif+ytQACWujNJZAQdGrioM/HY9I/sY2kW8j3ph2WXIK5bv9rT9WCOjwlJbvR/30/sB2BU2CoYUBPTy/dQRaN1z6TULIh3V2oJAgrJJpMNGtKhS5Nn0/sI12C55oLgbVaRVIen1jtG/HnK+VrRokZnzwt/YAUasJk2owYPzd/A+w56NJW0u5Z/X9qAoCNkaWY3yzJwdJi/5//aAAwDAAABEQIRAAAQ8888888888Y08888888888888888888888888X8888888888888888888888888s9c8888888888888888888888888d94w8888888888888888888884dk8YU8888888888888888888888o888Q8884k08888888888888884h8840c8483Y0888888888888888YwAMM88888t48888888888888888Ms5c4M8884o880g0888888888888okUc88808c80ooc88gI08888888sE8888kAcc88M0n0AoMMc888888U8884sosQYcMM89YYc88UU84Q88888g4co84Mowt9k8x088cN0k880g48Y8gMIso0d5E88c48451I08wwkQcsUsQ4cd99k8888sQMccMUs88ssUoEg4c99k888888888888woMY0M8wsk99w8888888888888888css84oV95cc88888888888888oA0888MMw99488888888888884s48YUIc8oY19t888888888888wgowok9I088og099c88888888oUgw8Is9xEw088oAsU1NY88888gkYo08958888088sk88cwl88888Y0scd5ks8888s88wcc888MQQ8888QtxsM888888800Ecc88888I08888M8888888888888wQ888888888888888888888888oIl088888888888888888888888og4c88888888888888888888888M8sE88888888888888888888888888w8888888//xAAUEQEAAAAAAAAAAAAAAAAAAACQ/9oACAECEQE/EEq//8QAFBEBAAAAAAAAAAAAAAAAAAAAkP/aAAgBAREBPxBKv//EACoQAQABBAIBBAEEAwEBAAAAAAERACExQVFhcRCBkaEgMFCxwUDR8OHx/9oACAEAAAE/EP8AEgSxbV1gNSjeC+US8CJUnytSQ3DzAj9n7o7d4nRj6invxFfdHUhB21j9zYDIPsT6X0AKNP8AH90c7Iz5egtfoph/j+6FiQR8UyskCdjXF5XsL7WfJRIAJEuJ+6MsowwE4PbFEo7qXsZTjF4trFvanJvkYfD+5DLloXE35mDRfiRBdlbq5Xb6ZwqKW0oRHka3YVdupw9krGKhfXrV6Q8r+3rCel4HYcGAcvQ05bzKot1cqvpkmiRCwI+Yio9jdADyxWCaBgU7Zh9eEnfXWrWokBRIlxP2ybri3Aa5dBtp9NRZHYOg+c5fRvaMKc5TXKQdsx3kFP339hj1Y9DITiYZ8IlOztxQTw6YPDpzBBHDTDB1L2D5EDpDX7Y4obA5gUfcvaggirHQS0i/ALRqUhMB/Ltdt/xmNkX0nCNx03pgG+YiS/wZ4ZKc6AHdWPyY7pAEZG4/tb6pjzK/g9DalQuly+8R/OCou8EB9/OjKpAFxsoAvkf0I26QEUvloRJLj+xI5hh8/wCnp8k/Fj+/zArK351k8VmBfYLH0/nYZZTK77dGNxheZMrK+OCLAWNVFN7S8PmRxjk2BltUwaJDSTj3JEf2EfPDpGBex+HpGWhTtePMT9n82ylgaMLAdIJ8JT1TRyoAHlaiICeUBP1+KQKLgAlVcHdZKOe3VwfJ1G5+lE8cLwN7O4wiFeywQAFgCFuPQTYJLxnhNC54kYfFhLEyGkn+yRH9ghIMCts0HUiy7q4NFQQjseKvcHKsC4m4RR81ccd3uDSNn/X42lfAnt2d6L0BlWPBwHAAB1T34GXDl2OA/KRj8cDB9roMr0eW1XxXFVGR5tpjB7TUESQO66jntg1y0qiWDAFk7R9UC6QO8Gu4X3JOELoR6QJInJ+wOV4RvYuDeLC++YJmipSi583TGB7yWqff5y1pjIcSi8V1WZn80rIMv9hWJPkJV8rd9iHmrFxIKFXga8t3a1CbbqNroBXooXoKzlbXaqvb+BpbpHLhWnbd0N4U0+zMn9HyvbUIih8cdjW9r6riErdpTomz0tJHJYlDcTkT0QCOGpmxTrdW57k9kNfsM1hF2XqM3w9Xae6wFgbWE7FKR3JqDEPFX5btAGD0lRAO4SKOEAPH4YG5yFsNBxgcyiVOhDM2Ut19G9CfG+22k2vPsWg/FaggHxBrxaPly0wSYfRxoKLZY+4sd3o4AgGE1+w6SDcDycPZekLgElR0GA7aSgPPHl1QuHaFDJw4RyefRw0O6b8x6B0ehADa0bdreg4GU+UzFygMlnINLtOcpiCGga3CQcAY/JyYBESRKntwCvaMvTJ4wbKrNV9aQ9gf1+Y1SC3AAksybyTZqPAjNw5U55lcOQX/ADCJQ64gL/g+/o4aFBsFuFB+V+KiimSIrwN5L4NtM5MZSRswvqLW0EsxYWMludOjB24/RuoApBuAwyupB5cJ0volTYPerfmU4QC/N/wYBmDkGibTSORAPJYBrFkkdNvQkWA9ERSKnULIk2ZyI1myDAOMGmw8mw/yopHlKH4Cfz6LKAqAEq6AozBV2Iw+zKy9jugR+VWx/oJ/+reMm2a1txcGpy7tY/QdHoQA2tSEa7IHH9GUzxSj7lY2kwBz7F4KWoWLcLi4O8u7WPwBuY93JPsrXelhMnnZ2cUTwMOFEj8V02LgJi/CKPTSOHCQg3E6SjOvDpPp4hyWqO7I0oB/IJyI/wCVesJ0TF9GrOKi37VwG5WDdWW07csh07UgxyIiuydc3Q3cwPEwVGFWbrnLfjBo/RB8divqBt+jKhelhgYSeXb9GtrhZw7zwcwwHvBeo6phvuUt+MGj1ZxELCkyd0ylGWFGGKwOQHy0fizIBwjsoBZA2R3Reo128t1O2GHuaUvl8vEqfvh6LmM4tB9wX5PFyAMNJGLym3+6YemdfkCSJGA7dUqEGQH/ACqFO9x+UpQH3A78D/gzbMiAxGXxRE2so61E8ZA+LyfZuL/mc5THJGNwMA/Rl8SC79L/AOjozEgAsdi1oH25Vb1yDoLf0uSzrkCq1kAz87XK3fS9Yg8oFxcmcxHKUDnAoDsTJ6KNG/6lx83McJU3uKlkbJwmyab8zUjckC+R7pkPJutjdBDDh6SybKWqTHaO12Ij2elirgwVYPCMHCVNlYiDI3Au+F55nj1DSqhAG1dFTUgBUv3eW3SXqM2qhXm0B0BQOq6647eKi6GBPwNACpYlRGXl/Wj77gpMTA35wbSl+ak+SwoXvZjwF6g913H8GusHev0QnyckYtoOMDmblXSQ5FH5VbUKtyhXs0X4c8AAQEBj1e4LlaG8P5+KG1EWWa/3XDJ3FBJYQkTTOyop5VLSBeegWyXBcUEmqRVkdiU1sPIzDvu5BFmZS59LHOqCNh0n28gIzDLFiH2hJ2cvpC8QUm5WfZ6NaZnoT/qToX0cmBKrAVBlohFjl88Hu3gABBYKxHqUQ7XAdtqDXDhnxT7qWL4MSmfkzQDLOAjKkd0bZ9qmwGNkRsnuUdokG4LfJjkh3+mYIJUAHK0ASTCs3L/Sw804jKTE346AvoVq3A8Yrp0+3eg/QAGsIByripCDNyDjb7EzFyl1zbQbVgDa2Kimy6J5gOONnoU9FsjMgBLDGYFqza8IjhYXoZ69J2NBCWBgbumxlikdLUJYCpEsHd1uhHpJ4wazNugrPfdBFVimCeF4YfAm6ESS40TlvBIXKTpqZUnJOm0MYR4pPAjflAloYvgHpKkoQEZFMSc5VdqtseiNkSkMiXH029BshsM/K3YCgRFgpumPALIOhhTwXlB3DBCeXl5W7v8AAzmEFD8DT0vzOlryFhRZGpwMoHADqbhk7Cg0qISJpHZ+hwmPUaC69A0txXGexb8ip4qZwQFCasAXtv3UlyTCHhGfKf1QJju995Nv0YALfoooFkhKmtmc4NpV4UZbenI7SDQXm4BSI+42uhd8Sg+CyP5y6OAseZWSQm7j0cmAREkSop0p5VddsW8zV9xm3EupyQmcpduK0rugwWyPEMp1JqoTEVN2rNwXDkKMuAEGRP8AVL/Wksifz3qoARyhElw9sxhk1QlMFJb4e6Il7LmkFgBdXVP6/wAmYQO4hZLXpCylz1bAoYt/HLEBz0Nf/AaaEGHILU+z3qFhYCYLdjgJWoWBcACL8vLtv+VvgkRkt5htO5OKQj00jM1C7MjuEHRB+SgS2ClwlFwv+fttQzCRhS6lwcTBGBKCHZ3nINj2A0AkALAa/MqgLzTMTKAWiVzROsVfwL/NW7kD8kM/VYZSLPtiTxBPJXUMsBwGjoKIwOQXD5XStjttQaPssu1cq7W76XuXrCkX8NTODp2OK+JNPT6TvIQC7svyV8NPKY8B3yaTdQ3xGUqcp5ER7KDBOLPSOkbjpq504MGp6dJp6hboAkjf/REe079CldjhdQfZPhWoBKXcn4CfDcuFXRfagPm8G3VAIEHrZSkInvLzwZWoDAuDt9lsaLejjShdpX+ayiebIfxf5xtSE8CnyDTkUUrcg1JD9L+KgS2CskEYqYtXvtb6ZsOsq511yuBxiX9GTJ+90nCMI6SkMIWUJVh3qNNqg4pgLgKRLEzqjh1zS7g5JfIjEDEgiCHVMjt5pf5uk8L6QUjAzEpeyVROG1E2JsRROKl0LpkhZOxPcvunJgEQkT/VICvsliV58pjOIXNZe29mTB8DD7vSIM0RcC58CI5h1RWJkG0iQ82E7CsPEejESwjZG82oAz28ZCjXviWNwYaIIPXGaQogCOixuDj5iopbdmYl0228GAD1XOh5BvQdYQ/Ij+D/ADivBLsfYhRgUmzpfPwl6DGFooB5LvcF210ykS9Tc+F9ZmjZhWAfKejuszx3ShmNA+QtZU/EJKqEAbvoqXBErmTsDnyPGmjUkBkQZSZEEh6iLzQgkZHCeo5XASYdk5NJs7hLBrPSaTkREeKKDekCJInCEUkl6OCIt7UJ8zgNrDS5EEgsiYZqdHK3Nz3WEeybSU7FhkgSEeqUQaRvWL8mHs4ilippngceIEefRkHGaBieBI+Z1QeTrSv5ULnDDWME8QBE6hn0VEkaICHhJ7UrlhwTbgCxiYImgCx6gyqtJZirT2c5TGRq0LwAOj8CK6AYfxMVfKWi0D6EUaHkGGxJLaenD+VgjgabLyJPOggj1zxUIa4CiIYnDVr76L0VTk6+0pw9ny1DYdN19kAJtcL5/F2a8HduMi/RtCjVJN8kTIrbcYOJvWLDuENqwHKsUQaUWYtcGelwRa03ZEJlYIY7O32DFX+6VVoI4UPIL4PwO9YQmWueG3s4q9rY75AAvspkBRCNxKRAjLO05bSE8jT+IQZCV8MPt6MbZCuC6eUA+zqoXHkw91wlkqVgnDYQjRcTpPQpKYcQYB3KKsuqD5U3V8SyOr9REiWyuTbX19gTlohB7v4L0mGlXnrtbG6YM0ZSydBzg1Nn8hcAEc3B0k/0KEE5GjNF+958+5zw320ElhCRNM7PwCoCeMRwcuDzA3jgAwdCZgCx+BOOXIJu8TAxRCJRaJkL2pKyhIXWiSECmguSDcYnyrohQB+B4jWQync30X5irBSJVmVg0XsFE1NA7PR2+jbpQ5bMAb7psgGcYpbcSiX2NGgLGqvUa9smi54nB2kUiNZcvOgjYuiBYG0BNkjwQkoVveZGOufQFwZXwRlewGlMHJmhEeJs4MGIGR9sLRHG3IQkTqGk4Ex9r7bwh7MPSkIhkAYRNIlWuzklhAXtke19BXSSuP2Rv+gVfJI1QD8AzD5FxRY/gwOVbBTi6LklCMuTa4YIkvUc/goEtitYoEw5Bt0Y2lDzRhdr26OAt73/ADWFI6QJCPJFOH8Ylne5yaXJfMggIbjQRbcqgy7vsTeKMWXYRvAyL8FI3UgD9tTu1gtbkNh8o6rJX2UxwaAmwWNVMUnl9ILugmEWwAbk6ktSdjoFwmFdDcfa95NF7KsRQlIJCxSMkM1Iem56FBGEA5WisHrouNz3l62QGrAF1Ni2VmotLJERxKZHg3NnhbpPYB2ADVaC6ZsxTJJFidAF1oTFZRIyLM0YSczcDey20VoRYI0tytMnOGUvg+ZoqWneWoodrBLjRy+v0vMCihc3QeAWk4Fb7dTj3I8A16bJ1fAMI0HueAckwDBt50So+ZcejkwIVcSnAhHQDttJidxUu8SwgWD7EtABBY/ByMC1GgC605an9Kg/h7uSi1UCQaALB0fo3N3BA/686b1KjVZo61ge4+c1Y3CiXkSSpd+ic5pdMdHnusxwS0U21jDxmT2KIhK0oSnvj2ihwDwIf4ppds/LEH7oddDlDyTX5oT0RFm7CnhFeosqYNFZ2ZN7JF7zTn0gDYFCQLedKCRxLS+XfAHJgP8A4XqcseBc4+YndvG6Wn1cEejbysvtBV1Apeh8rngLvFGQQ6RcJvnJMEq0yXEzTmGytYA+3atXefAkC0k7YbBfwXqBOJErqw3G/PNR4fPR8EVkWDUTPrLAIsAJX4KfsC0qv80OkL6MJTianhXwMVm4GTsKN+UQkDhOT0gM8NgJnwAh1FQRtyCWBCZ1MX3e7OImwjCHTuZ6KIwx+CgS2Kj0ygCCMll3R7xmrfEgJXJoOj3X9UPrYaeJLU+CKX4ATB8VAR+B9E1bxLkwPItj2oAAEBYD802ZHZJ9JSlep9vo6K3FBN3w90OwuaXI+5J50+m5ah2uWnbyjGCZJtDeWsGyFf351ddeXdyaokwZSUtq7ZonF3ZZKc3OXUWaInFCFNhbhuVzezm0BK4yj9rNL5UX7XDGTKcxEPoMLPAZhz8XHuUFjgoFAh5FXCcJ8Wrs0195S3PIkfe6O2N2YyOZW8R36GYU5IzVB3Ex5mkHNY/Bk9Qyjoo0s/oIWXoY7cUCzIQDgCwf5NknJ+Q+qFPXV9i/r0WzO3zFLpQn43ScpgpH/XomJv0oNHy/QUdMqnXWy9vWDRN2x5rTNq4DlWCgp5BwDmyjvAahzM7oJ4FZUbfYKysVgCYkwH/F6hAQsLmwOOJXehj8By28Ck+dIHYFB5eGUgA7VpUjb0go8rLMd6KcZGZbQ+Iu05d/YeRNiKJxUCO3AS+44gQdlEIY/ACVFKqXCb/wF2DvVWGDD5CdHRB1/lqQHF5YWf1P2oV3CHVJ9+idIoSDhHJQSJowZSReAmJvEWmaI3/QLsAwXwEUKdE2p5Dt4sbTamtdwBssi9kAzjC9uJZL7GA0BY1V1LystTg/UrHcRWTICXXkyvn2t+CLGSBlLL03B1LZigIaqWM1ahQXlbnio+62sKVZOGUScVG8HJaIHtZjpg2EIMfjvYysHnAHlpNY7juOll226G/+b9VgoR/mpvlwWgkxAC/C+qsMTZTEmNkKR3SsVSjAwqZTojzFqxsbVrhBYHKNEYmlR2CKHQF1p4yRYPnYXS3M4DJgQCwH4Mwleuvo7bwXpPb2cuCNEWAwWoVDA7Xng3LYLtq+1y1H0YdhNyXFR+AwlQAlLYttanyVkcesLemXUGavRs3HlcrbLL/noAsxGIixyEeTuiloBLZ+yTPY8n4OxIyMovKyPjw2po7atM4HXtE7/GQJnEOEE4Vy1FqvdvfZ2rLSoKZoRGtZSIAuijwsZSgBmVurKu/xlp4gTybLo90qIIliZbhg+Lxlf2FjARaABVLxbLIJMlISTlESi72gTTjTUVIwCdRPazh/RjDkDCwQSS9hPjckW0Olo2BInYscNbRsMC9zCWslSUtkMpC02mRLxIZp7oqDl3FwicNmTX43ziLQe3obDyJajkiAeC8D2sln/PmBoCSCiSbETz6OtFjhOiHs/KltE0MXDtyC4yMBU4jYFT4HfK4kmzWlEwK+Rz4MndYErm3uWf0YhkUWjkeUQ8Z1RpEEhBRE5EoMuFmu/YHU4sGWgIfjir44DYmEYuNE7cCxDlTnmVzsF/zTOkCdo/1W3KuFkGeIB5h59EdkuMzIPiIXVU9jjphuPSWeqcFsiABBOf4anql5V+6nQj5pTMsCy4Lz5oeD9lfNx80WK7lN/SoZghW+0EezU4gLpNm6QMzovUP3DkRd/Fi2kZawA7Cz1DpqK1Wk8ibEUTiruRU9GfjCmJJggPyuNLRCEdI9UyzeHboh8Q990MuAEGRP9f5IGZxAULImzMlsGaiZJEbGyGk/9JEa+tDkh/dBwXIjZQ/TNfK+ZA+wjxKmqKaVRcRMNMZLsvIsC7S49lZXnHc6nn+CPSEFhw5gWYUQTExlahx8g0DyBiUxNltEpNxbo5eYgeWhCUwoukowEzUCN1sWuW9xGNT+Rr1QBAlxNlLiSvvBy8p9yHNRERH6sfEMD1mxFWORqYGhORET8kBDIq91rynmem5WemWsJrsg7EyPI3/xYyTU4AV+qm8QEwjsRuPT6BtZ1yA/nWS6IYd9uGy4hZweiRJAGM7eW55eGgiciOAzP5X0JQ2wS9v0AjsNTR5xDGGL+yDa9qQSQNxN1eOfOCVf2zVpeFSCyJhmmUIAqCMxiWLuXdRGZzQNraJInDUlLaBYJ8CZtYSQsTTOQCJJQye+OSHf5T7jwwf5EelpQL6cgY8J2Zoh8MaX8q+5ytEYqDn5sXJcBhu0BIz+OpEvkGh7sJc00VVDK/cXJs9w/wAQKWSDzOnwMcU0TkmeyThDXoQkCSM7p10EAumXm4nwrQJzyVm2Rm8kj5SfDU8cLOBhOR+cNmpH0J4JpVxb2bZKewi5AS7GhITzGvSNQkAg1Xuj0Q1U0cxAnQ7iNJNrRa5QXTTjFOCHoKAEGPWOE0CYsRqbDzYkJpwEzlkx4O8Izo/OHsUBjx3cH+lIXQ8QwrpFHppyIliiLk+iLpYtixNTzzFhCC+wPerylfJvhPJHN6VSSRK4Uul4QeKAJGfwcnMlCUoloyFrycYXESH+CMK6CJkUlDcWEviTNAbKQyOxNI2Spbjg5EvV8VeiJkkg6gnxUCg5yKTBe9xHAIoJKoEibtspwpZuEWVPJrk7mizpBa17ufz6CDUIL/f2JDkjbTu1pO/2l9l3RdEgYCJJbUwErqsSw2Qkk3AtstljBH4WzK4DNIPO5+jDHljkmsQzqSAKMskLGzAVPQUnI6ntkD1HL+QSVQJE3bZV641zYm7dqYziHdYU8020+i4DiSJsRQMEoJAEubj6GiDSAibKDCyBInCbpdJyoS/+3ZYpNRmPjPCem5soRuX9RYFMWHqkPG/Zk7LAPIBANIlk7P10IFirzY+q24kYtlg6JsPUOvR7c5CYC55Eee1W7eoWhXww+SkIIyOGjug5vEcHY/JJuhrGi7BHsWH3o0r8EhIntSIPLGwVcHmoeSWgFL4ikQDZGNQE2Oiseokqr2IJYE3mswluyi8ikgPJx7b8r6X4TRCEI+Sj6ilhpFnmSV2MVHiUDlbXYyflt9S0YfDh6eYphLMshvTJOAcQ/t+ztsggc9lkue0B9/UkvQWThGyVFLkmVe4+DJ4KaXGiNktOBOxaz6IJDcoSLM5Q5Vw8mHpuD7u2QdiaTY/ppLCsDkWOP51IgMBEQA9k7XMqmKXkNYLhdEkPU1Ic1AQg3EyMlL5/EwKDEpMcNS5j5hyDsQTso9ocBjUdJD/7WOBTZQPugTtH0i49kOr6VVsSBKzeSIyv0zQya+FMxs5nxAbib0AEGD1ISsUKuFGc+Ycr0D3TNgSX38Puy8R+KXQcRZ/sLnYctBuhTaMD2gHmNT+d1A2SOAjyYeocDUIs0mNx0in8XqcoMP8ATIRsMRMTE/jnbXMh5HI9kJWyWwQ4xviHlaTBkPh7Pyet5ekv/wAiaS52SMoqILTjD+HDr9Iy5N0zmhBB1RLIZ3ID7IX2j6Wr7JxGr7AnyNWKgh9kD2pL2fQuMwyzvyZ8LRH4wNCcPa/MTGaMQeEo9F32GgvOdDIlSYCxB7uYPVAlYoYpOJPMuz2AvVIWpeIG63+R0UZMAABAH5pcY22M4OBye5qoBjceTH8C/Y6j8hwEgSI67pAhPfJELM2WBcm5mjDQBeeA4Rg6Q6h/KJvDmfRc8YdjTApvKA/Bb23owCqgQjsjSPptZ4vvBsfkyQ3oJAYOvv5GGNumQ/QEogjCJejMk05W/uIferLTGavu8yoenhxZdK4diDUAh48XADwjDwzQAiw7RIxqkTAyAEjL4YpjrA5ECwxqSiSW7y39UCVihQqUbOv27AXqpxzvFK/4zZ4ox6gAAcBo/RPWLZLjcPD8lsNS4GLiYmOdJzDijXBDhAR+H8WYvlMtvsODG4wuSyVlfGg0BY1TPcvWuknlBD5wNOLHVsw6TTqP0FVihDHiXZqEeorezsf+U8knKULdUmH407DY7P7vRSA4O3GzlRZ+b/o2gZEev/si+KcHqAMI6R1R8AsJXAThzbDbiacTEYozDN3EkvAUmJU3h25N5WYN+Wj6VIOX1kvar365xpFRL4rk37BpBqUMGE639yPBQSVAIA1bR+o5zYwLlty/w8VMtLI9z/OP/HqKHJiB/wC9ZanaF1If+XLq5RsHzSE8CMCkF1JxQeUZBPhOLSYfQTRuQcSD9JXu0hA5MV15svNNwOsTQdM5bI4Wpuzxo2GxjH8MNYeWpng9jkdnYh+cHMDaThOxhHSUBuGaah0iP/tZ12v2eTkbNQa7dPgD6qckY8QRLs5A5qIt6JEF1YC6vEUjxGFT5XJMDzXJnhAfgdTKc1ix+sLYSKJE2JunbEptJn3Ijshc1FcEXREP7OkoeOFJA+17EwroYazMszvCNvKy9xRFTDYD+KIBdmlaysasXdMNCgrVjK1nJc7BQWBPW0yu8IieatBsBbRueZEPP6YBARsjhq44Xzrw6+SOYaQbFmlHKdLFhBEkKmgXG6chpPshLJ+giRZAJRleamT3MtCQ9EMtYMvIgla7gO1imbDliU2T9h7KJQ7N792PBB/hQAIbN7LPkD3Ci87JKpuBh7zRkYSy9AHLoD2rBuTY4PL/AI7KipoLBMIINQA2s32wAygSN8Z2dPd9lRujEMFc9uHnQ40dpYeP+QUrEMXCCT7lTFlvoE/n9QEkoMQdJspgpYiylw1TZJDEQv6KTWzwLqg2W8IRpiARkNsL72PhpNBJFMvCBOkax1oMvAW/xV7PM58xDD2TOyb0VHQsrczV5y8gx6BJdAkTZGymQTlZQTHOCfhLzQPQN7ANhuxMcE6m71b6lwV/IVr7CouFDLKu54hR+5LofTFx9tHGBqCCH+xfdOCHMZmriqxuk9xF1h8x+5zMl4qWT29kIX+axVLkC8AjHhLUOkxsBJE9n9x/+J3REKFfsE+mlXwTSAL1ZpQGZECiPhKEpU9FRm8H7P3E1+vtePtQeKv7Bvhk/BNGnuYUvJkeR8U16MIwJd6Qh7DbWat4nBd9zykv3Aw4Y4BK/BTvEbuSEA8UVi6kvNW/B3FC4fZzFTiBfkMsD4mPBUgZ9iHZwmR03q7C82AuDqO7MmSp9YJ/1Fz+4AkdMEAKGrxDbDUaSYWwF1XnKru9Bv8AIjImTkUScJHqe8CYC+fybO7bac+kycNJZ8usrfL2pPv+4AJ3BrYjh6/iSoVLAudyjKt38BXLyDM4dO+G+FgFHI7wPgEfP7qNlkCRNkbKNxPBKv07ECxh0v63/9k='
  //#endregion 
  ///////////////////////////////////////////////////////////

  ///////////////////////////////////////////////////////////
  constructor(private service: ServiceService,
    private modalService: NzModalService,
    private notificationService: NzNotificationService,
    private route: Router,
    private sanitizer: DomSanitizer
  ) { }
  ngOnInit() {
    this.ShowListService();
    document.getElementById("content").classList.remove("p-0")

  }
  ShowListService() {
    this.isLoading = true;
    this.service.GetShowListService().subscribe(
      (res: any[]) => {
        this.ShowListServiceData = res as any[]
        this.isLoading = false;
      }
    )
  }
  DeleteService(data) {
    let type = data.type == 0 ? "บริการทำผม" : data.type == 1 ? "บริการทำเล็บ" : "บริการทำหน้า"
    this.modalService.confirm({
      nzTitle: '<h5 class = "modal-title">ยืนยันการลบบริการ ' + type + '</h5>',
      nzContent: 'คุณต้องการลบ ' + data.name + ' ใช่หรือไม่ ?',
      nzClosable: false,
      nzCancelText: "ยกเลิก",
      nzOkText: "ยืนยัน",
      nzOnOk: () => new Promise(resolev => setTimeout(resolev, 100)).then(() => {
        this.service.DeleteService(data.name).subscribe(
          (res) => {
            this.notificationService.create("success", "ลบข้อมูลสำเร็จ", null)
            this.ShowListService()
          }
        )
      })
    })
  }
  openModal(typeComponent: string, data?: any) {
    this.ShowListService()
    const modal = this.modalService.create({
      nzTitle: typeComponent === 'new' ? 'เพิ่มบริการ' : 'แก้ไขบริการ',
      nzContent: ServiceFormComponent,
      nzComponentParams: {
        typeComponent: typeComponent,
        name: isNullOrUndefined(data) ? null : data.name,
        type: isNullOrUndefined(data) ? null : data.type,
        take_time: isNullOrUndefined(data) ? null : data.take_time,
        detail: isNullOrUndefined(data) ? null : data.detail,
        service_charge: isNullOrUndefined(data) ? null : data.service_charge,
        point: isNullOrUndefined(data) ? null : data.point,
        level_barberid: isNullOrUndefined(data) ? null : data.level_barberid,
        active: isNullOrUndefined(data) ? null : data.active,
        img1: isNullOrUndefined(data) ? null : data.img1,
        img2: isNullOrUndefined(data) ? null : data.img2,
        img3: isNullOrUndefined(data) ? null : data.img3,
        img4: isNullOrUndefined(data) ? null : data.img4,
        img5: isNullOrUndefined(data) ? null : data.img5,
        img6: isNullOrUndefined(data) ? null : data.img6,
        img7: isNullOrUndefined(data) ? null : data.img7,
        img8: isNullOrUndefined(data) ? null : data.img8,
        img9: isNullOrUndefined(data) ? null : data.img9,
        img10: isNullOrUndefined(data) ? null : data.img10,
      },
      nzClosable: true,
      nzWidth:700,
      nzFooter: [
        {
          label: 'ยกเลิก',
          shape: 'default',
          onClick: () => modal.destroy()
        },
        {
          label: 'บันทึก',
          type: 'primary',
          onClick: () => new Promise(resolve => setTimeout(resolve, 0)).then(
            () => {
              const instance = modal.getContentComponent();
              return instance
            }
          ).then((instance) => {
            if (isNullOrUndefined(instance.name) || instance.name === "" || instance.name.trim() === "")
            this.notificationService.create("error", "<p>กรุณาใส่ชื่อบริการ</p>", null)
            if (isNullOrUndefined(instance.take_time) || instance.take_time <= 0 )
            this.notificationService.create("error", "<p>กรุณาใส่เวลาที่ใช้ในการทำ</p>", null)
            if (isNullOrUndefined(instance.detail) || instance.detail === "" || instance.detail.trim() === "")
            this.notificationService.create("error", "<p>กรุณาใส่รายละเอียด</p>", null)
            if (isNullOrUndefined(instance.service_charge) || instance.service_charge <=0 )
            this.notificationService.create("error", "<p>กรุณาใส่ราคา</p>", null)
            if (isNullOrUndefined(instance.point) || instance.point <0)
            this.notificationService.create("error", "<p>กรุณาใส่แต้ม</p>", null)
            else if(!isNullOrUndefined(instance.name)&&instance.name !== ""&&instance.name.trim() !== "" &&!isNullOrUndefined(instance.take_time)&& instance.take_time > 0 &&!isNullOrUndefined(instance.detail)&& instance.detail !== "" &&instance.detail.trim() !== ""&&!isNullOrUndefined(instance.service_charge)&&instance.service_charge >0 &&!isNullOrUndefined(instance.point)&&instance.point >=0){
              this.AddOrEdit(instance).subscribe(
                async (res) => {
                  let id: any
                  let idup: any
                  if (instance.typeComponent === "new") {
                    await this.service.getlastServiceid().subscribe(
                      async (res) => {
                        id = instance.name
                        if (instance.fileList.length == 0) {
                          this.service.uploadServicefile(id, { img1: this.IMGDefault }).subscribe((res) => { });
                        }
                        else {
                          for (let i = 0; i < instance.fileList.length; i++) {
                            switch (i) {
                              case 0: await this.service.uploadServicefile(id, { img1: instance.fileList[0].thumbUrl }).subscribe((res) => {  }); break;
                              case 1: await this.service.uploadServicefile(id, { img2: instance.fileList[1].thumbUrl }).subscribe((res) => {  }); break;
                              case 2: await this.service.uploadServicefile(id, { img3: instance.fileList[2].thumbUrl }).subscribe((res) => {  }); break;
                              case 3: await this.service.uploadServicefile(id, { img4: instance.fileList[3].thumbUrl }).subscribe((res) => {  }); break;
                              case 4: await this.service.uploadServicefile(id, { img5: instance.fileList[4].thumbUrl }).subscribe((res) => {  }); break;
                              case 5: await this.service.uploadServicefile(id, { img6: instance.fileList[5].thumbUrl }).subscribe((res) => {  }); break;
                              case 6: await this.service.uploadServicefile(id, { img7: instance.fileList[6].thumbUrl }).subscribe((res) => {  }); break;
                              case 7: await this.service.uploadServicefile(id, { img8: instance.fileList[7].thumbUrl }).subscribe((res) => {  }); break;
                              case 8: await this.service.uploadServicefile(id, { img9: instance.fileList[8].thumbUrl }).subscribe((res) => {  }); break;
                              case 9: await this.service.uploadServicefile(id, { img10: instance.fileList[9].thumbUrl }).subscribe((res) => {  }); break;
                            }
  
                          }
                        }
  
                      }
                    )
                  }
                  if (instance.typeComponent === "edit") {
                    idup = instance.name
                    if (instance.fileList.length == 0) {
                      this.service.uploadServicefile(idup, { img1: this.IMGDefault }).subscribe((res) => { });
                    }
                    else {
                      for (let i = 0; i < instance.fileList.length; i++) {
                        switch (i) {
                          case 0: await this.service.uploadServicefile(idup, { img1: instance.fileList[0].thumbUrl }).subscribe((res) => { }); break;
                          case 1: await this.service.uploadServicefile(idup, { img2: instance.fileList[1].thumbUrl }).subscribe((res) => { }); break;
                          case 2: await this.service.uploadServicefile(idup, { img3: instance.fileList[2].thumbUrl }).subscribe((res) => { }); break;
                          case 3: await this.service.uploadServicefile(idup, { img4: instance.fileList[3].thumbUrl }).subscribe((res) => { }); break;
                          case 4: await this.service.uploadServicefile(idup, { img5: instance.fileList[4].thumbUrl }).subscribe((res) => { }); break;
                          case 5: await this.service.uploadServicefile(idup, { img6: instance.fileList[5].thumbUrl }).subscribe((res) => { }); break;
                          case 6: await this.service.uploadServicefile(idup, { img7: instance.fileList[6].thumbUrl }).subscribe((res) => { }); break;
                          case 7: await this.service.uploadServicefile(idup, { img8: instance.fileList[7].thumbUrl }).subscribe((res) => { }); break;
                          case 8: await this.service.uploadServicefile(idup, { img9: instance.fileList[8].thumbUrl }).subscribe((res) => { }); break;
                          case 9: await this.service.uploadServicefile(idup, { img10: instance.fileList[9].thumbUrl }).subscribe((res) => { }); break;
                        }
                      }
                    }
                  }
                  if (JSON.stringify(res)[11] === 's') {
                    this.notificationService.create("success", typeComponent === "new" ? "เพิ่มข้อมูลสำเร็จ" : "แก้ไขข้อมูลสำเร็จ", null)
                    modal.destroy();
                    this.ShowListService()
                  }
                 else if (JSON.stringify(res)[11] === 'e') {
                    this.notificationService.create("error","ชื่อบริการนี้มีอยู่แล้ว", null)
                  }
                }
              )
            }
          })
        },
      ]
    })
  }
  AddOrEdit(data) {
    if (data.typeComponent === "new") {
      const value = {
        name: data.name,
        type: data.Servicetype === "0" ? 0 : data.Servicetype === "1" ? 1 : 2,
        take_time: data.take_time,
        detail: data.detail,
        service_charge: data.service_charge,
        point: data.point,
        level_barberid: data.ServiceBarberLV === "2" ? 2 : 3,
        active: data.ActiveCheckbox ? "Y" : "N",
      }
      return this.service.AddService(value)
    }
    else {
      const value = {
        name: data.name,
        type: data.Servicetype === "0" ? 0 : data.Servicetype === "1" ? 1 : 2,
        take_time: data.take_time,
        detail: data.detail,
        service_charge: data.service_charge,
        point: data.point,
        level_barberid: data.ServiceBarberLV === "2" ? 2 : 3,
        active: data.ActiveCheckbox ? "Y" : "N",
      }
      return this.service.EditService(value)
    }
  }
  search() {
    this.isLoading = true;
    this.service.SearchService(this.criterianameservice, this.selectedtype).subscribe(
      (res) => {
        this.ShowListServiceData = res as any[]
        this.isLoading = false;
        if(this.ShowListServiceData.length==0){
          Swal.fire({
            html: '<p>ไม่มีข้อมูลที่คุณต้องการค้นหา!</p>',
          })
        }
      }
    )
  }
  sanitize(url: string) {
    // this.isSpinningImg = false;
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }
}
