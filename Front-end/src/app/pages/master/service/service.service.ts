import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { urlServer } from '../../../URL'

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(private http: HttpClient) { }
  public GetShowListService() {
    return this.http.get(urlServer.ipServer + `ShowListService`)
  }
  public GetServiceDetail(name: string) {
    return this.http.get(urlServer.ipServer + `ServiceDetail/${name}`)
  }
  public SearchService(name: string, type: any) {
    return this.http.get(urlServer.ipServer + `SearchService/?name=${name}&type=${type}`)
  }
  public DeleteService(Service_id:number) {
    return this.http.delete(urlServer.ipServer + `DeleteService/${Service_id}`)
  }
  public AddService(data) {
    return this.http.post(urlServer.ipServer + `AddService`, data)
  }
  public EditService(data) {
    return this.http.put(urlServer.ipServer + `UpdateService`, data)
  }
  //////////////////////////////////////////////////////////////////////////
  public getlastServiceid(){
    return this.http.get(urlServer.ipServer + `getlastServiceid`)
  }
  public uploadServicefile(id,file){
    return this.http.patch(urlServer.ipServer + `uploadServicefile/${id}`,file)
  }
}
