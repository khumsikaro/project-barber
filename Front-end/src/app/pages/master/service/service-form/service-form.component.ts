import { Component, OnInit, Input } from '@angular/core';
import { UploadFile, NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-service-form',
  templateUrl: './service-form.component.html',
  styleUrls: ['./service-form.component.css']
})
export class ServiceFormComponent implements OnInit {
  @Input() typeComponent: string;
  @Input() serviceid: number;
  @Input() name: string;
  @Input() type?:number;
  @Input() take_time?:number;
  @Input() detail:string;
  @Input() service_charge?:number;
  @Input() point?:number;
  @Input() level_barberid?:number;
  @Input() active?:string;
  @Input() img1: string;
  @Input() img2: string;
  @Input() img3: string;
  @Input() img4: string;
  @Input() img5: string;
  @Input() img6: string;
  @Input() img7: string;
  @Input() img8: string;
  @Input() img9: string;
  @Input() img10: string;
  ActiveCheckbox:boolean;
  Servicetype:string;
  ServiceBarberLV:string;
  /////////////////////////
  showUploadList = {
    showPreviewIcon: true,
    showRemoveIcon: true,
    hidePreviewIconInNonImage: true
  };
  fileList = [];
  previewImage: string | undefined = '';
  previewVisible = false;
  Img = [];
  ImgAll = [];

  handlePreview = (file: UploadFile) => {
    this.previewImage = file.url || file.thumbUrl;
    this.previewVisible = true;
  };
  constructor() { }

  ngOnInit() {
    this.name = this.typeComponent === "new" ? null : this.name
    this.Servicetype = this.typeComponent === "new" ? "0" : this.type.toString()
    this.take_time = this.typeComponent === "new" ? null : this.take_time
    this.detail = this.typeComponent === "new" ? null : this.detail
    this.service_charge = this.typeComponent === "new" ? null : this.service_charge
    this.point = this.typeComponent === "new" ? null : this.point
    this.ServiceBarberLV = this.typeComponent === "new" ? "3" : this.level_barberid.toString()
    this.ActiveCheckbox = this.typeComponent === "new" ? true : this.active === "Y" ?  true : false
    this.img1 = this.typeComponent === "new" ? null : this.img1
    this.img2 = this.typeComponent === "new" ? null : this.img2
    this.img3 = this.typeComponent === "new" ? null : this.img3
    this.img4 = this.typeComponent === "new" ? null : this.img4
    this.img5 = this.typeComponent === "new" ? null : this.img5
    this.img6 = this.typeComponent === "new" ? null : this.img6
    this.img7 = this.typeComponent === "new" ? null : this.img7
    this.img8 = this.typeComponent === "new" ? null : this.img8
    this.img9 = this.typeComponent === "new" ? null : this.img9
    this.img10 = this.typeComponent === "new" ? null : this.img10
    if (this.typeComponent === "edit") {
      const Img = [
        { uid: 1, thumbUrl: this.img1 },
        { uid: 2, thumbUrl: this.img2 },
        { uid: 3, thumbUrl: this.img3 },
        { uid: 4, thumbUrl: this.img4 },
        { uid: 5, thumbUrl: this.img5 },
        { uid: 6, thumbUrl: this.img6 },
        { uid: 7, thumbUrl: this.img7 },
        { uid: 8, thumbUrl: this.img8 },
        { uid: 9, thumbUrl: this.img9 },
        { uid: 10, thumbUrl: this.img10 },

      ]
      for (var item in Img) {
        if (Img[item].thumbUrl !== "undefined" && Img[item].thumbUrl != "") {
          this.ImgAll.push(Img[item])
        }
      }
      this.fileList=this.ImgAll
    }
  }

}
