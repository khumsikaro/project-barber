import { Component, OnInit } from '@angular/core';
import { HomeService } from './home.service';
import { NzModalService, NzNotificationService, isEmpty } from 'ng-zorro-antd';
import { HomeFormComponent } from './home-form/home-form.component';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  HomeListData: any = [];
  isVisible = false;
  constructor(private service: HomeService,
    private modalService: NzModalService,
    private notificationService: NzNotificationService,
  ) { }
  ///////////////////////////////////////////
  data: any;
  ShowImg(img) {
    this.isVisible = true;
    this.data = img
  }
  handleOk(): void {
    this.isVisible = false;
  }

  handleCancel(): void {
    this.isVisible = false;
  }
  //////////////////////////////////////
  ngOnInit() {
    this.GetListHome()
    document.getElementById("content").classList.remove("p-0")
  }
  GetListHome() {
    this.service.GetHomeList().subscribe(
      (res) => {
        console.log(res);
        
        this.HomeListData = res as any;
      }
    )
  }

  DeleteListHome(data) {
    let types = data.type == 0 ? "หัวข้อ" : data.type == 1 ? "ย่อหน้า" : "รูปภาพ"
    this.modalService.confirm({
      nzTitle: '<h5 class = "model-title">ยืนยันการลบ' + types + '<h5>',
      nzContent: 'คุณต้องการลบลำดับที่ ' + data.about_id + ' ใช่หรือไม่ ?',
      nzClosable: false,
      nzCancelText: "ยกเลิก",
      nzOkText: "ยืนยัน",
      nzOnOk: () => new Promise(resolev => setTimeout(resolev, 100))
        .then(() => {
          this.service.DeleteListHome(data.id).subscribe(
            (res) => {
              this.notificationService.create("success", "<p>ลบข้อมูลสำเร็จ<p>", null)
              this.GetListHome()
            }
          )
        }
        )
    })
  }
  openModal(typeComponent: string, data?: any) {
    const modal = this.modalService.create({
      nzTitle: typeComponent === 'new' ? 'เพิ่มข้อมูลประวัติร้าน' : 'เเก้ไขข้อมูลประวัติร้าน',
      nzContent: HomeFormComponent,
      nzComponentParams: {
        typeComponent: typeComponent,
        id:isNullOrUndefined(data) ? null : data.id,
        about_id: isNullOrUndefined(data) ? null : data.about_id,
        type: isNullOrUndefined(data) ? null : data.type,
        detail: isNullOrUndefined(data) ? null : data.detail,
        active: isNullOrUndefined(data) ? null : data.active,
      },
      nzClosable: true,
      nzWidth:700,
      nzFooter: [
        {
          label: 'ยกเลิก',
          shape: 'default',
          onClick: () => modal.destroy()
        },
        {
          label: 'บันทึก',
          type: 'primary',
          onClick: () => new Promise(resolve => setTimeout(resolve, 0)).then(
            () => {
              const instance = modal.getContentComponent();
              return instance
            }
          ).then((instance) => {
            switch (parseInt(instance.HomeType)) {
              case 0: {
                if (isNullOrUndefined(instance.about_id))
                  this.notificationService.create("error", "<p>กรุณาใส่ลำดับ</p>", null)
                if (isNullOrUndefined(instance.Title) || instance.Title === "" || instance.Title.trim() === "")
                  this.notificationService.create("error", instance.HomeType === '0' ? '<p>กรุณาใส่หัวข้อ</p>' : instance.HomeType === '1' ? '<p>กรุณาใสย่อหน้า</p>' : '<p>กรุณาใส่รูปภาพ</p>', null)
                else if (!isNullOrUndefined(instance.Title) && !isNullOrUndefined(instance.about_id) && instance.Title !== "" && instance.Title.trim() !== "") {
                  this.AddOrEdit(instance).subscribe(
                    (res) => {
                      if (JSON.stringify(res)[11] === 's') {
                        this.notificationService.create("success", typeComponent === "new" ? "<p>เพิ่มข้อมูลสำเร็จ<p>" : "<p>แก้ไขข้อมูลสำเร็จ<p>", null)
                        modal.destroy();
                        this.GetListHome()
                      }
                      else if (JSON.stringify(res)[11] === 'e') {
                        this.notificationService.create("error", "<p>มีลำดับนี้อยู่แล้ว<p>", null)
                      }
                    }
                  )
                }
                break;
                  
              }
              case  1 : {
                if (isNullOrUndefined(instance.about_id))
                  this.notificationService.create("error", "<p>กรุณาใส่ลำดับ</p>", null)
                if (isNullOrUndefined(instance.detail) || instance.detail === "" || instance.detail.trim() === "")
                  this.notificationService.create("error", instance.HomeType === '0' ? '<p>กรุณาใส่หัวข้อ</p>' : instance.HomeType === '1' ? '<p>กรุณาใสย่อหน้า</p>' : '<p>กรุณาใส่รูปภาพ</p>', null)
                else if (!isNullOrUndefined(instance.detail) && !isNullOrUndefined(instance.about_id) && instance.detail !== "" && instance.detail.trim() !== "") {
                  this.AddOrEdit(instance).subscribe(
                    (res) => {
                      if (JSON.stringify(res)[11] === 's') {
                        this.notificationService.create("success", typeComponent === "new" ? "<p>เพิ่มข้อมูลสำเร็จ<p>" : "<p>แก้ไขข้อมูลสำเร็จ<p>", null)
                        modal.destroy();
                        this.GetListHome()
                      }
                      else if (JSON.stringify(res)[11] === 'e') {
                        this.notificationService.create("error", "<p>มีลำดับนี้อยู่แล้ว<p>", null)
                      }
                    }
                  )
                }
                break;
              }
              case 2: {
                if (isNullOrUndefined(instance.about_id))
                  this.notificationService.create("error", "<p>กรุณาใส่ลำดับ</p>", null)
                if (instance.fileList.length == 0)
                  this.notificationService.create("error", instance.HomeType === '0' ? '<p>กรุณาใส่หัวข้อ</p>' : instance.HomeType === '1' ? '<p>กรุณาใสย่อหน้า</p>' : '<p>กรุณาใส่รูปภาพ</p>', null)
                else if (!isNullOrUndefined(instance.about_id) && instance.fileList.length != 0) {
                  this.AddOrEdit(instance).subscribe(
                    (res) => {
                      if (JSON.stringify(res)[11] === 's') {
                        this.notificationService.create("success", typeComponent === "new" ? "<p>เพิ่มข้อมูลสำเร็จ<p>" : "<p>แก้ไขข้อมูลสำเร็จ<p>", null)
                        modal.destroy();
                        this.GetListHome()
                      }
                      else if (JSON.stringify(res)[11] === 'e') {
                        this.notificationService.create("error", "<p>มีลำดับนี้อยู่แล้ว<p>", null)
                      }
                    }
                  )
                }
                  break;
              }
            }
          })
        }
      ]
    })
  }
  AddOrEdit(data) {
    if (data.typeComponent === "new") {

      if (data.HomeType === "0") {
        const value = {
          about_id: data.about_id,
          type: data.HomeType,
          detail: data.Title,
          active: data.ActiveCheckbox ? "Y" : "N",
        }
        return this.service.AddListHome(value)
      }
      else if (data.HomeType === "1") {
        const value = {
          about_id: data.about_id,
          type: data.HomeType,
          detail: data.detail,
          active: data.ActiveCheckbox ? "Y" : "N",
        }
        return this.service.AddListHome(value)
      }
      else {
        const value = {
          about_id: data.about_id,
          type: data.HomeType,
          detail: data.fileList[0].thumbUrl,
          active: data.ActiveCheckbox ? "Y" : "N",
        }
        return this.service.AddListHome(value)
      }

    }
    else {

      if (data.HomeType === "0") {
        const value = {
          id:data.id,
          about_id: data.about_id,
          type: data.HomeType,
          detail: data.Title,
          active: data.ActiveCheckbox ? "Y" : "N",
        }
        console.log(value);
        
        return this.service.EditListHome(value)
      }
      else if (data.HomeType === "1") {
        const value = {
          id:data.id,
          about_id: data.about_id,
          type: data.HomeType,
          detail: data.detail,
          active: data.ActiveCheckbox ? "Y" : "N",
        }
        return this.service.EditListHome(value)
      }
      else {
        const value = {
          id:data.id,
          about_id: data.about_id,
          type: data.HomeType,
          detail: data.fileList.isEmpty ? null : data.fileList[0].thumbUrl,
          active: data.ActiveCheckbox ? "Y" : "N",
        }
        return this.service.EditListHome(value)
      }
    }
  }
}
