import { Injectable } from '@angular/core';
import{urlServer} from '../../../URL'
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private http:HttpClient) { }
public GetHomeList(){
  return this.http.get(urlServer.ipServer+`GetAbout`)
}
public DeleteListHome(order:number){
return this.http.delete(urlServer.ipServer+`DeleteAbout/${order}`)
}
public AddListHome(data){
  return this.http.post(urlServer.ipServer+`AddAbout`,data)
}
public EditListHome(data){
  return this.http.put(urlServer.ipServer+`UpdateAbout`,data)
}
}
