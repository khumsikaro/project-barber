import { Component, OnInit, Input } from '@angular/core';
import { UploadFile } from 'ng-zorro-antd';

@Component({
  selector: 'app-home-form',
  templateUrl: './home-form.component.html',
  styleUrls: ['./home-form.component.css']
})
export class HomeFormComponent implements OnInit {
  @Input() typeComponent: string;
  @Input() about_id: number;
  @Input() type: string;
  @Input() detail: string;
  @Input() active?: string;
  @Input() id:number;
  ActiveCheckbox: boolean;
  HomeType: string;
  Title: string;
  /////////////////////////////////
  showUploadList = {
    showPreviewIcon: true,
    showRemoveIcon: true,
    hidePreviewIconInNonImage: true

  };
  fileList = [];
  previewImage: string | undefined = '';
  previewVisible = false;

  handlePreview = (file: UploadFile) => {
    this.previewImage = file.url || file.thumbUrl;
    this.previewVisible = true;
  };
  ////////////////////////////////////////////////
  constructor() { }

  ngOnInit() {

    this.about_id = this.typeComponent === "new" ? null : this.about_id
    this.HomeType = this.typeComponent === "new" ? "0" : this.type
    switch (this.type) {
      case "0": {
        this.Title = this.typeComponent === "new" ? null : this.detail;
        break;
      }
      case "1": {
        this.detail = this.typeComponent === "new" ? null : this.detail;
        break;
      }
      case "2": {
        let IMG = [{ uid: 1, thumbUrl: this.detail }]
        this.fileList = this.typeComponent === "new" ? null : IMG;
        break;
      }
    }
    this.ActiveCheckbox = this.typeComponent === "new" ? true : this.active === "Y" ? true : false
  }
  Reset() {
    this.fileList = [];
    this.Title = "";
    this.detail = "";
  }
}
