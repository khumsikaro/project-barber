import { Injectable } from '@angular/core';
import { urlServer } from '../../../URL';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BranchService {

  constructor(private http: HttpClient) { }

  public GetBranch() {
    return this.http.get(urlServer.ipServer + `ShowListBranch`)
  }
  public GetShowUser() {
    return this.http.get(urlServer.ipServer + `ShowUsers`)
  }
  public GetBranchDetail(Branch_id: number) {
    return this.http.get(urlServer.ipServer + `BranchDetail/${Branch_id}`)
  }
  public SearchBranch(name: string) {
    return this.http.get(urlServer.ipServer + `SearchBranch/?name=${name}`)
  }
  public DeleteBranch(Branch_id: number) {
    return this.http.delete(urlServer.ipServer + `DeleteBranch/${Branch_id}`)
  }
  public AddBranch(data) {
    return this.http.post(urlServer.ipServer + `AddBranch`, data)
  }
  public EditBranch(data) {

    return this.http.put(urlServer.ipServer + `UpdateBranch`, data)
  }
  /////////////////////////////////////////////////////////////////////
  public GetProvince() {
    return this.http.get(urlServer.ipServer + `GetProvince`)
  }
  public GetAmphur(Province_id: number) {
    return this.http.get(urlServer.ipServer + `GetAmphur/${Province_id}`)
  }
  public GetDistrict(Amphur_id: number) {
    return this.http.get(urlServer.ipServer + `GetDistrict/${Amphur_id}`)
  }
  public GetZipcode(District_id: number) {
    return this.http.get(urlServer.ipServer + `GetZipcode/${District_id}`)
  }
  /////////////////////////////////////////////////////////////////////////
  public GetShowListService() {
    return this.http.get(urlServer.ipServer + `ShowListService`)
  }
  //////////////////////////////////////////////////////////////////////////
  public Getlastbranchid() {
    return this.http.get(urlServer.ipServer + `Getlastbranchid`)
  }
  public UpfileBranch(id, file) {
    return this.http.patch(urlServer.ipServer + `UpfileBranch/${id}`, file)
  }
  public Getskill(username: string) {
    return this.http.get(urlServer.ipServer + `Detailskills/${username}`)
  }
}
