import { Component, OnInit, Input, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';
import { BranchService } from '../branch.service';
import { UploadFile, NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-branch-form',
  templateUrl: './branch-form.component.html',
  styleUrls: ['./branch-form.component.css']

})
export class BranchFormComponent implements OnInit {
  ProvinceData: any = [];
  AmphurData: any = [];
  DistrictData: any = [];
  ZipcodeData: any = [];
  /////////////////
  @Input() typeComponent: string;
  @Input() branch_id: number;
  @Input() name: string;
  @Input() manager_name: string;
  @Input() detail: string;
  @Input() address: string;
  @Input() status?: string;
  @Input() active?: string;
  @Input() province_name: string;
  @Input() amphur_name: string;
  @Input() district_name: string;
  @Input() zipcode: string;
  @Input() province_id: number;
  @Input() amphur_id: number;
  @Input() district_id: number;
  @Input() zipcode_id: number;
  @Input() location:string;
  @Input() img1: string;
  @Input() caption1: string;
  @Input() img2: string;
  @Input() caption2: string;
  @Input() img3: string;
  @Input() caption3: string;
  @Input() img4: string;
  @Input() caption4: string;
  @Input() img5: string;
  @Input() caption5: string;
  @Input() img6: string;
  @Input() caption6: string;
  @Input() img7: string;
  @Input() caption7: string;
  @Input() img8: string;
  @Input() caption8: string;
  @Input() img9: string;
  @Input() caption9: string;
  @Input() img10: string;
  @Input() caption10: string;
  @Input() map_img: string;
  //////////////////
  Province: string;
  Amphur: string;
  District: string;
  Zipcode: string;
  Province_id: number;
  Amphur_id: number;
  District_id: number;
  Zipcode_id: number;
  Managerfirstname: string;
  Managerlastname: string;
  Manager_id: string;
  Status: boolean;
  Userlistdata;
  Managerlistdata;
  ActiveCheckbox: boolean;
  DisabledAmphurData = true;
  DisabledDistrictData = true;
  DisabledZipcodeData = true;
  showUploadList = {
    showPreviewIcon: true,
    showRemoveIcon: true,
    hidePreviewIconInNonImage: true
  };
  fileList = [];
  previewImage: string | undefined = '';
  previewVisible = false;
  Img = [];
  ImgAll = [];

  handlePreview = (file: UploadFile) => {
    this.previewImage = file.url || file.thumbUrl ;
    this.previewVisible = true;
  };

   /////////////////////////////////////////
   showUploadListMap = {
    showPreviewIcon: true,
    showRemoveIcon: true,
    hidePreviewIconInNonImage: true
  };
  previewImageMap: string | undefined = '';
  previewVisibleMap = false;
  MAP=[];
  handlePreviewMap = (file: UploadFile) => {
    this.previewImageMap = file.url || file.thumbUrl ;
    this.previewVisibleMap = true;
  };
  ///////////////////////////////////
  constructor(private service: BranchService) {
  }


  ngOnInit() {
    this.GetUser()
    this.ShowProvince()
    
    if (this.typeComponent === "new") {
      this.Amphur_id = null;
      this.District_id = null;
      this.Zipcode_id = null;
    }
    else {
      this.ShowAmphur(this.province_id)
      this.ShowDistrict(this.amphur_id)
      this.ShowZipcode(this.district_id)
      this.Amphur_id = this.amphur_id;
      this.District_id = this.district_id;
      this.Zipcode_id = this.zipcode_id;
    }
    this.manager_name = this.typeComponent === "new" ? null : this.manager_name
    this.name = this.typeComponent === "new" ? null : this.name
    this.detail = this.typeComponent === "new" ? null : this.detail
    this.address = this.typeComponent === "new" ? null : this.address
    this.Status = this.typeComponent === "new" ? true : this.status === "Y" ? true : false
    this.ActiveCheckbox = this.typeComponent === "new" ? true : this.active === "Y" ? true : false
    this.Province = this.typeComponent === "new" ? null : this.province_name
    this.Amphur = this.typeComponent === "new" ? null : this.amphur_name
    this.District = this.typeComponent === "new" ? null : this.district_name
    this.zipcode = this.typeComponent === "new" ? null : this.zipcode
    this.Province_id = this.typeComponent === "new" ? null : this.province_id
    this.location= this.typeComponent === "new" ? null : this.location
    this.img1 = this.typeComponent === "new" ? null : this.img1
    this.img2 = this.typeComponent === "new" ? null : this.img2
    this.img3 = this.typeComponent === "new" ? null : this.img3
    this.img4 = this.typeComponent === "new" ? null : this.img4
    this.img5 = this.typeComponent === "new" ? null : this.img5
    this.img6 = this.typeComponent === "new" ? null : this.img6
    this.img7 = this.typeComponent === "new" ? null : this.img7
    this.img8 = this.typeComponent === "new" ? null : this.img8
    this.img9 = this.typeComponent === "new" ? null : this.img9
    this.img10 = this.typeComponent === "new" ? null : this.img10
    
    if (this.typeComponent === "edit") {
      const Img = [
        { uid: 1, thumbUrl: this.img1 },
        { uid: 2, thumbUrl: this.img2 },
        { uid: 3, thumbUrl: this.img3 },
        { uid: 4, thumbUrl: this.img4 },
        { uid: 5, thumbUrl: this.img5 },
        { uid: 6, thumbUrl: this.img6 },
        { uid: 7, thumbUrl: this.img7 },
        { uid: 8, thumbUrl: this.img8 },
        { uid: 9, thumbUrl: this.img9 },
        { uid: 10, thumbUrl: this.img10 },

      ]
      for (var item in Img) {
        if (Img[item].thumbUrl !== "undefined" && Img[item].thumbUrl != "") {
          this.ImgAll.push(Img[item])
        }
      }
      this.fileList = this.ImgAll
      this.MAP = [{ uid: 1, thumbUrl: this.map_img }]
    }

  }
  GetUser(){
    this.service.GetShowUser().subscribe(
      (res:any[])=>{
        if(this.typeComponent==="edit"){
          this.Userlistdata=res.filter(x=>x.level_barberid==1)
          this.Managerlistdata= this.Userlistdata.filter(y=>y.branch_id==this.branch_id)
        }
        else{
          this.Managerlistdata=res.filter(x=>x.level_barberid==1)
        }
      })}

  ShowProvince() {
    this.service.GetProvince().subscribe(
      (res) => {
        this.ProvinceData = res as any[]

      }
    )
  }
  ShowAmphur(data) {
    this.DisabledAmphurData = false;

    this.service.GetAmphur(data).subscribe(
      (res) => {
        this.AmphurData = res as any[]

      }
    )
  }
  ShowDistrict(data) {
    this.DisabledDistrictData = false;

    this.service.GetDistrict(data).subscribe(
      (res) => {
        this.DistrictData = res as any[]

      }
    )
  }
  ShowZipcode(data) {
    this.DisabledZipcodeData = false;
    this.service.GetZipcode(data).subscribe(
      (res) => {
        this.ZipcodeData = res as any[]

      }
    )
  }

}
