import { Component, OnInit } from '@angular/core';
import { BranchService } from './branch.service';
import { NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { Router } from '@angular/router';
import { BranchFormComponent } from './branch-form/branch-form.component';
import { isNullOrUndefined } from 'util';
import { DomSanitizer } from '@angular/platform-browser';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-branch',
  templateUrl: './branch.component.html',
  styleUrls: ['./branch.component.css']
})
export class BranchComponent implements OnInit {
  BranchData: any = [];
  criteriaNameBranch: string = "";
  selectedValue = '';
  isLoading: boolean;
  //#region IMG
  IMGHOME = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAs8klEQVR42u2dCZxT5bmH38lkMvvKDPsIogJVEVBRAsii1AUpi7IqVqrU5dZWvXWtWPGq1bZ6S2+1VYsKVhSRTXasVJRlUMEdBEFA2WGGycxklsxkkvu+JwtJ5pyT9eQseZ/f75gxCSdfTs73fMv/LGnAMEzKkqZ2ARiGUQ8WAMOkMCwAhklhWAAMk8KwABgmhWEBMEwKwwJgmBSGBcAwKQwLgGFSGBYAw6QwLACGSWFYAClAzYQJWfhwBS7DcemNS3dc8kLeZsflAC67cNmAy/rCRYua1C47oywsAAODFf88fLgHlynQtsKHg4SwAJfZKIIdan8XRhlYAAYEK34pPjyNy3RczHGuzonLXFweRhFUqv3dmMTCAjAYWPlvxIfZuJQmeNVU+e9BCcxX+zsyiYMFYBCw4nfHh3/gcrXCH7UWlztRBAfU/s5M/LAAdA5WfOri343LLIh+nB8rdu/n/RVF4FR7GzCxwwLQMVj5++LDHFwuVqkI23CZgRL4Uu1twcQGC0CHeGO9J8Azwx/vJF+8UA+A5hwe5dhQf7AAdAZWfsrzqdXvrnZZQjgAnt7AerULwkQOC0AneKO9P4Mn2tMyc3G5nyNDfcAC0AEKRntKwZGhTtC8AKqvu64oLS3tRlwm4P/2w6VIwY+jHXeb2+1ehsu84iVLVB3TJjHaUwqODDWOpgWAlX8sVvwX8c+OKnz8IZTADJTAumR/sErRnlJwZKhhNCsArPx3Y+WfrXIxnF4JzEvWB2og2lMKjgw1iCYFcGr8+Kuw8q9VuxxeSALDS5Yu3azkh9iuvz4Lv7NWoj2lECJD3J6PFi1ezJGhBtCcAKrGjaOK8C1oK+b6BgXQR6mVY+Ufht95rsa+s5LspZ4VSuBDtQuS6mhRAJOxMixQuxyh4A47st2yZQnNuLHi0wTnX0D70Z5SzMXtei+KwKZ2QVIVLQrgLfCcv641nkcB/DpRK8PKT6J7HvQT7SlFJUrgLpTA22oXJBXRngDGjv0aH85XuxwibGj37rsj4l1J9XXXdceK/zf8c7TaX0hjrEQR/Lp4yZIDahckldCcACrHjt0P2hwLbyiNQwBY8Wli71dY+Z8E/Ud7SmFHCczExxdQBBwZJgHtCWDMGO0KYPnymARwavz4viaTiY5nGKj2l9AJW90u1x3FS5dyZKgwmhPASQ0LoCxKAWDFp7P2HsNW/z4wbrSnFBS/PouPj5csXcqRoUJoTgAnfvYzzQqg/YoVEQsAKz9Fe3RAz9lqF1znCJEhSoAjQwXQngBGj9auAFauDCuAqnHjUj3aUwohMmy3bBlHhglEcwI4rmEBdAgjAO8xDBztKYcQGaIEODJMENoTwLXXalcAq1aJCqBy7Fgh2kvjaC8puL2RYem77x5Quyx6R3MCOKZhAXQMEQBWfI721MMfGaIIODKMEc0J4OioUZoVQKfVq/0CODlmTF+T51RljvbUZSuK4I7S5cs5MowB7Qngmms0KQBsYjaUr1kz4tjo0VnpJhNHe9rCHxmWLV/OkWEUaE4ARzQmAJfbDY24NLndG9pbLLPS09M52tMue90u14yyFSs4MowQ7Qng6qs1I4BmrPj1LhdNOkGBxdKUnZ6epXaZmIgQIsP2K1ZwZBgGzQng8FVXqS4AFy5U8UkAWWYzFGZkAI731d40THQIkSFKgCNDGTS3Vx9SWQAOb6tPFZ4qflZ6utqbhIkDX2TYYeXKA2qXRYtoTwBXXqmKAFpxsWPFd6IAcrHVz+NW30h4IkO3+4UOq1ZxZBiA5vbwgyoIoBErPk30manVt1jAYjKpvRkYZRAiQ5QAR4ZetCeAn/40aQKg1p5afWr987HFp5Y/jVt9o0OR4TP4+FTHVatSPjLU3N7+48iRiguAJvm80Z7Q2tNYP4Nb/VRjF/UGOq5endKRoeYE8IPCAqCZ/QZchGgPK36OmY/lSXHmoAju77R6dUpGhtoTwBVXKCIAl7fiN+PfNLNfgGP9dO7uMx6OUWSIElisdkGSjeZqwAEFBODwVn5ftJfJ0R4jDkWGd3Zes+aQ2gVJFtoTwOWXJ0wArZTp40K5D3X18znaY8JDkeFD4Ha/1HntWsNHhpqrDftHjIhbADS+b/Iew282maCIoz0meigynIES2KF2QZREcwLYF6cAnN5Wn2b6Odpj4sQfGXZZu9aQkaHmasa+4cNjEgC1+g0uFzjwUYj2sNXnaE8aXy+JyEJBam5H0BZCZNhl3TrDRYaa+92/HzYsagG0gOfkHWzqhdl9jvbkafUeAOUb4NLWykNZcioSFiEy7LpunWEiQ8394tEIIDTaK+RoTxZq72lehA59FiMLJZDDvYFwHMPteBdKwBCRoeZ+671Dh0YkAF+0R+P7osxMPmsvDIGHPfswFRYKj66aGv9ztBWpN2BmkYZDiAzL33tP15Gh5n7lcAJweis+dV9pgi8fW32O9qTx9ZIc3vG+j6zhwyF/6lThb/tbb0Hjhg1Br2fiNqXeAG9bWfyRYfm//63LyFBzv+6eyy4TFUBotFdM0R63+rI4Aq5o5CO9QwcomDEDLOeeG/Te5p07ofbVV6H16FH/c7Rz5OK2zmQJhEOIDFECuosMNffLfjdkSBsBkFppR3bhjkjRHp2rz9GeNL4DoFoCW30a3197LeRddx2k4fYjfNvQ7X2fu6UF6pctg/oVK7DrcHqeIAPfl4sLz6/I4okM3e6nznj/fd1Ehpr7RQMFEBrt0Vifoz1phF6S99oGga1+xtlnQ/4tt0BGt27+57KysiA3N1f4u76+HpqaTu+zzoMHoXbOHGjZu9f/HO0o2SgAmijU3E6jLYTIECWgi8hQc7/l7sGDBQEEnrVXiBU/19tqMeKERntEGm63/IkTIfvKKyHNO1wyYQXOz88HCw6hAmluboa6ujps+D0tv7u1FRrXr4e6BQvA7XD438eRYcQIkWG399/XdGSouV/x20GD9mPF7+6L9qjV551NGn+0FzLJl9mvHxRMnw7pZWX+57Kzs4VWX2r4REMB6g00Njb6n2utqhLmBhxffBH0XuoNZHNkGA7hLMNu69drNjLU3O9XYbXuN6Wldafj97P5gB5ZaIxv9x727MOErXvBzTdDltXqf85MaQk+b45wezqdTqE3QI8+mj7+GGrnzQuKDGkwlocSyGBBy+K9MOmd3dev11xkqKlfbu/w4eaG1tb9+RZLVx7pS0NtPU3yNYe0+tnDhkH+tGlg8o7tqaXPyckRWv5oJ02pN0A9gYaGBv8koQt7B3VvvgmNH3wQ9F6Ld5JQUzuT9rDjVhQiw+7/+Y9mIkPN/Gb7Row4L91korvu8L32ZHAEzI34EIv2MjIyhFY/Pc6otLW1VegNtLS0+J8TIsM5c6D1+HH/c7Qj0XEDHBmGhSLD6SiB3WoXhFD919o/YkQWNk+PYAv1EPC99iRp9Vb8lsAnTSbI9UV73kk9aunz8vKEWf5EQimB3W4/HRk2N4N9yRKoX7UqODIEjwh43kYWigyfxI35xzM/+EDVyFDVXwkr/2DcYanV761mObQMVTdfqx8IRXsFt94qGu2ZFIpKKSEIjQxbfvgBal95JSgyJHy9AdaALBQZzkAJbFarAKr8PvuGDy/Civ80Nld3qPXF9UDgFY18CNHepEmQE0G0pxRikWHDe+9B3cKFbSJDPoAoAtzuF1EED/fYsCHpkWHSfxms/Ndj5Z+Nf3ZN9mfrBV+01xQa7fXvD4W/+EVU0Z5iZRSLDE+ehJrXXgPH558HvTeLI8NIOITb9B6UQFIjw6T9Jt8PG9Y1zWT6G37guGR+Qb3REnBFIx9CtDd9OmQPGuR/LtpoTynEIsPGLVugdu5ccOHz/u8Ant4AR4byoPKX0b0Mz9qwISmRYVJ+Daz8v8IW6kn8sygZn6dHhGiP7kgc8jxFewU//zmYcnKE/6eWnlp8avm1BPUEqEfgjwwbGqD29deh8cPgI2JpkJLLhxOHw0b3Mjzrww9fUPqDFP0dsOL3wh12LnC0J4tUtFd4222QqUC0pxRikaFj506oeflljgxjQ4gMUQSKRYaK/AJ7hw6laO9BXPlM4GhPEurmh561RxN7uaNHQ/711yse7SmFWGRYt3gx1K9cKUwY+vCdZcgHfcnixK0oRIZnf/RRwiPDhAsAKz9He2GQi/aKZsyAjO7d/c8pHe0phWhkeOAA2ELOMiQ4MowIITJECSQ0MkzYNt9z2WUc7UWAVLRXMHky5F51lWrRnlKIRYb169ZB7dtvc2QYC97I8JyNGxMSGSZka2Pl52gvDFLRXtaFF0LhLbeAWQPRnmLfXSQydFJk+Oqr0PTZZ8HbgyPDSBAiQ5RA3JFhXNv5uyFDumJL9TfgaE8WqWiPKn5OEqM9B7a4zbg4sRUWJuqwTBl0JWXsdVBPIxN7IkpKRywybNiyRRABR4YxsQx7Vr/uuWlTzJFhzFsYKz9He2EIvKJRIDnDhkHhzTcn5Kw9OajbXVtbC3W40KScS+Jy4D5o2EHlyC8ogAJclBCR1FmGNfPmQUNIZJhJ24ojw3AIkSFKIKbIMOptixWfo70IaPa2+qHRXvHtt0Pmeef5n1Mi2qMWtrKyEqpPnQpb6aUgERUWFkIZDk0smZkJ3z6ikeGOHWB7+WVwHjt2uhzg6Q1YuDcQDiEyRBFEFRlGvFV3Dx5MGdSDuGNwtCeDEO1hpQs8a48m9vJGjRIm+pSM9qhFPVVVBSdOnGhT8ataM2C/M0d4rHZZwOn96U2oqEKTE8rSHdAlvQm6mB1t1ltaWgpl7dsrkkSIRYa1ixaBfcWK4MgQPAcQ6SsLSTqeswwB/thr8+aIIsOIBICVn6O9MEhGez16QPEdd4AlINqjsTZV/kRWKGr1Dx48CA3YnfbR4DLBZ45C+Ko5Hyt9ZNdUzElrhT6WOrgwsxZK0k9rjHoBXbt2VeQIRLHIsJkiw3/+E5r37AkpH0eGESBEhiiBsJGh7HbEip+HD0/jcpfa30jLUDslRHuBB/Rgy144cSLkXXut4tEeVZwfsML4Jtec7jTY0lQkLPR3LJCaLsisg8uzq3Ac7vKXvwtKgOYHlEAsMrSvXQs1dGHSADmYfZcpV6QUhuJ5XB5GEdil3iC5d2Dl74UPi3A5X+1voVUko71+/aD4l78EM3abfSgV7dFkGlV+X6U51mqBpXXtodKVGMlQj2BM7kk4x9Ig/D+Vv2t5uWISEI0McUhTTZHh9u3B25kjw0j4BpdxKIHvxV4U3Xbeyr8Bl45ql16rSEV7RdOnQ+7Qof7naHKPWv0MBS5rTi1mYMu/05EDS2tL/eP7RDIixwZDcz0XBCUJlJ9xhv++AkpAk4PUG2gNmAeoxx6t7ZVXODKMHppVHY4SaDNB2GarYeWng3k+Ba78oshGezfdBOkBLaPvrD0lsnVq8Q/++KOQ7RM7mrLhHVsJCkm5ijAotw6uzvdIgMTWrVs3MCt4vwZfZFgfMK9BkaFt7lyODKOHJDAAJRB0zEDQ9sLKT7P7FbhcrHZptYhotFdWJkzyZfXp438uGWftVZ48CdXV1cLf+x0WeK2qFGIL/KJjTKENLsn1DAeyc3KEiUGlEYsMm3bsgOq//124CIkPjgzDsg0XK0rAfyRWqAAew4dZapdSa7h899oLfBJbG5rgK1Q42hODWv3Dhw55xssuE8w+1g7srckJyEy4x9zVvgo6WTz7UPsOHQTZJQOxyLDm7bfBLnJh0ly+s7EUs1AAj/v+x7+FsPJTl59uy6WPc06TgNrRnhTHjx/3x33zT+bDlw3J/ck6ZzjhN52rhfE39XYoGUjWeQs09CEJOAJOJKLIsPrFF6Fl376g93JkKArFKWeiBISjrQIF8Bd8uEft0mkF0bP2sGUvoGhv1ChVL8h59MgR4e/DjnT438OFqmyfKWV2GJDvuX5Ru9JSQX7JRDQyXL0aat95JzgyBD7LUITZKIB76Q9hq+waPDgL/6Abw6f8cf1y0R6dq5+MaE8OOry3zjsL/urRXPi6Xp2bppZmtMIj3TzloN5Ph47JnzOWigzpmgNNIfcy5MgwCBvu3Z16b97c5BHAoEFjcSdepnap1EbyrL2bb4acyy7zP0eTe0qdLBMOav2p1bM50+DxfXngcse/zlj5r/IG6JXjiek6duqk2qXKKAalk54CI8OGjRuFE4w4MhQH5Tmu95Yt7wpbArv//8CHlL2Qh2y0R/faS1K0Fw6aBT+JLRzxUXUGvHNM3YuFWIuccEMnz1YrKi4WziRUC9HIEKVQ88YbHBmK8yIOA+70CYAu5N5P7RKpgVS0V3T77UmP9sJBOzed2ku8+KMFvqpT92DYMosLHj/HIwCSYmGR+iNI0cjw66/B9tJLHBkG8wUKoL9vDqAO/0juLI7KSEZ7SThrL1aom9vY4MngH9llgaoW9Xfev5zrgOx0jyBL2rVTuzh+RM8ypMhw9WqODEHo9dp7b96cL3zrbwYNcqfSuKhJItqjVl+taC8SbDabcEUfGvff8ZU6k3+hPNLTCd2y3ULPiNIALSEVGVJvINUjQ5rvOn/LFk+t/xIFkJUCApCK9vInTFA12ouUGhQAdW0rmwEe/EYbUnrgHBf08h4HVBpwXUMtIRUZ1i1alLKRITWCuHdnC990u9XqztNIK6cEbu8Xbgy9117fvkK0F3ivPZrIokWLF+Sk8b9PAPd/rY3yPdjTDb29AtDSECAUGgrQmZMN3iEUQXMCFBk6vvwy6L2pEBnaUYYXVVR49vKtVmtdkcmUZ8QvTOfo28XutUfR3pAh/ueUPGsvUQgX1PQK4L4vVcz/Aniod5pfAMUlJWoXJyxiZxk2bNoEtSKRYR5WD7MGG4J4oT3H5nLZB1ZUeOYAKqzWz/HL9jPSjKhUtJfti/YCjl9XM9qLhnocz/pO/b1pqzPOtSWGJ/ukQ7dcbC2xB6nUNQISjWhkiJWfIsPGFIgMmz2N4hfWior+vh7AP3D0e0eBQYYBzd5JvsBWX4j26F57Gov2oqEJd1oazxL//VkLHG9SvxfwyqUWyMLNl242K3p9ACUQvTApRYZ0L8OAyJBqRY6BIsNabBix//Mi9gA8xwF8arWOxfZkWb7Oj5KSi/byJ03SZLQXDS1Y+X3Xzfu/3S1QcbI1zjXGR3muCf7U37NN6f4CetuePkTvZbhwoSEjQ5r9r8PFDDBuQEWF50jAbVZrFlaao2i6okKddnd8k3yBbaIQ7WGrn6HhaC8aaBbbdxzA+qNOeGlPc5xrjI9Rnc0w/WyPAGi7KnlxEKURiwyFexlibyAwMqS6QROEekzNqG7U4PdEpdnwl+p0cUVFk/9b4DDgL/iGe+iL5ejoy4lFe9i399xr75prNB/tRUuj94YadS1uuL2iHpqTcRUQCf5wYTb0LPBs32yNJifRInovwzVrDBEZNnhPcsMSz8bu/+mzAQkcBnTESkTXYM4r1MEXk4z2zj8fCm+/Pehee7477BphB6UUwDcR+Py3jfCfoy1xrjE2zswzwXOXeA4eJbkqcfMQtfBFhm3uZSgSGfp6A1rfs6ihrPHUFTu2/udg6x98PQACewH34Vv+THbL1/BQQDLamzZNOIHHB03uUXdfy9FetNDO2eztph5ucMFvKmpVOSPwob65cGmZZ7vS9jXpZCI1Gnz3Mgw9y7D29dd1FRnS7lGHPRpqNrCE92Hr/5zvtaASf2K1mls9FwTtl6PBcY5ktDdkiOdeewHRnhL32tMKtGO6vDvlS7saYPWPEd0EJmH8pNgMzwzwRH60fTN0PqySg4RLk4R6jgwDDn3/Ahv3AQMqKsSvCUh8bLVehC3rVvzTXKShGU+paK/w1luFi3X4oNaIWn29RHuxQDul0xtdNTrdcHeFDY42JGcygCK/vw4qgs45nu1L8Z8eJ1SjhXoBNEmot8iQkjGbp/I78RcbeElFRdDNFURLiUOBP+M/uY86ePkq/7hy0V7ehAlg8kZP1BLROF+vUVS00A7p6wXsq3PCA1troKlV+bHAzAsLYGB7T4tPFT9dhYuiqImvN+C/szFFiHQvQ41GhnXe+1RiKZ7Frv/9oa+Llg6HAnm4a32Nf3bPU9FmotFet27CJJ+lRw//cxRBUeVPhZYokFYcCvh2xB3VLfD7T2zQqKAEbjs3D8Z19170gyaKsZdlxCFWOHz3Mgw6y3DfPqihswx/+MH/nNqRofeIP/rzAGq6D3b929wiTLJkOBS4Cn22lt5QmGSTSUV7dDBPaLRH3X29R3uxQpU/8C7AO081w/9ss0FNgrNBSoTu6VsAI7uevjEobftUrPyBUGRIw4I2keHChXTSgf99akSGLu+sP1V/rC1XY9d/ndj7ZEtUYbX+Cx+mJSsVkIr2LOefLxzQk27QaC8eSALugO11vKEVnvvCBl9VJeYgofI8M9zfvxB6Fp2WLG3zVN/uPnwXJg28s7FwluHLL0PzN98EvTdZkWHgrD/yhrWi4iap98qWBYcCpTgU+BboIrD4nzwFJSAV7eXfeKPho714EQQQIs21PzbC3F21UO2IrTeQm5EG43vkweSzc8FiCvjVufKLQpOD1BsIigw//BDq5s9PamRIe4HdO+5HKrHx/gl2/Sul3h+2FNgLmIIPb9HfZm/hEzkckIv2Cn7+85SJ9hKBO0QCzS43bDrSBO8dbICvKh0QyfTAWQUZcHl5NlxzRq4ggUB4u8vjO8sw8JoDVPnpuIHGTZuC3qtEZOjyNqK+oTOue9rAior5cv8mos/farX+E/edGfR3ImMOuWiPLtbhIxWiPaWxt7jgGxwWHKhrEYYJLQE26JxrhvJ8M5xXYoGSLN7G8SIaGX75JdS88opikWFoXcI1zsHK/8tw/y6iT/7YajW7PL2ACb7nqAOeHWNXxukd54ccxOo0l5TMbvfss9NM2dnCXSZSLdpjjIVYZGh74YVtjm3b6MAVf36qQF1ahHKZemnAAT9SRPyJXgnQpOCUwOfpW5DB6EvIzXK2egvZHDq77+ELS3n5jIsWLty+H8H/7+47a4+7nYkh0u0YOoxg4oO2Z+BZhq179kyve/PNb5p2754DIZfiT1BdWoCV/6ZIKj8Rde3aYrX+Fv/RMxBgsMCVUQeSuja0w7m9XRKaFpHYrZrSzOaZmWec8df+8+cLBf7hhx/2Y8XvnqrRHmNMfJFh66FD03sMHjzvq7vvNjfu3Hm3y25/EkRuyBtDXXLi8w8NCjjOPxJial4rrNZe+DAbl6vj2CbL8MPvxXHKgcAna2tr9+MX7h7HehlGkwhnGaIAOp577jzfc5+MHNm9tb6ebsw7Lo5V0/E692Bd2h3tP4yrf40iuAgf7gLP3EAkNxahOGIRLi9aKyq+FHtDXV2dMASIp1wMo1Wabbbp7crL54U+j3WJZr3p9nxUlyK5wQId1Ud16XlryPH90ZCQATYW3ozdj4txZZfi//YGz12GaSKPzjm24bILPCcYbbeGGZuwABgj0+pwTC8qLZ0n9TrVJXyghnUgSNelbUDTchGO8+XQ3AwbC4AxMm6Xa3pBYeG8+NeUGFgADJNE3G739IKCAhaAFDQJCCwAxriwAORgATAGhwUgBwuAMTgsADlYAIyRoTmAQp4ElIYFwBgZFkAYWACMkWEBhKGmpoYFwBgZFoAcLADG4LAA5GABMAaHBSAHC4AxMjQHUFRUxAKQggXAGBkWQBhYAIyRYQGEwWazsQAYI8MCkIMFwBgcFoAcLADG4LAA5GABMAaHBSAHC4AxMjQJWFxczAKQggXAGBkWQBhYAIyRYQGEobq6mgXAGBkWgBwsAMbgsADkYAEwBocFIAcLgDEyNAdQUlLCApCCBcAYGRZAGFgAjJFhAYSBBcAYGRZAGE6dOsUCYIwMC0CORAugtcUBtqO7oaHmGDibG9X+epBb3BmKOvUCS3aB2kVJCE5HA5zY/yk46m1qFyXhpJnSobhzLyjscE4iV8sCkCNRAnDUV8POD16GvR8vBJezWe2vFQTtWJ17D4MLrr4HCsrOVLs4MXPq0A7YOO9X0GSvUrsoinLWpRPh4nG/T9TqWAByJEIAtSf3w4Y5v4TG2uNqfx1ZzJYcuGTCE1De50q1ixI1LpcT1vzvGLBXHVS7KEnhkglPwpkXjY17PTQH0K5dOxaAFPEKgLqka/86Huqrj6j9VSLCZLbAyDvnY1ezt9pFiYoT+z6F//zzFrWLkTQ6nDUQRsz4Z9zrYQGEIV4BfPP+32HH+n+o/TWiouzMi+Hy215TuxhRsX/7Mvj4nZlqFyNp5BZ3gZ89uC7u9bAAwlBVVRWXAJY/PVLzXX8xrr1vFeS1O0PtYkTM/u3vwieL2gogMyMNzi3PgvysdLWLGDVOlxsOVjbj0tLmNZq8Hf1A/AJAWAByxCMAmulf/vRPI/vipnTILOwk+56m6kOiz5vMmWDJLwv7GS31p6C1uSGi8lin/gm69b0mji2XXEgAH4cIwGJOgzEDCqEoV3+VP5CNO+2w+4gj6DkSwM9YAMoTjwDqqw/D8j9eJfuejJwSOPPqR6GszxihIsux8dGuos8XdrfCBbe+E7Y8bpcTbN9vgn2rZ0FD5V7Z91468UnocdG4BG5JZREEEDIEuKB7FlxyTq7aRYsbR4sL3vyoGlpdp58TBJCAIQCwAOSJWwDPSAsgPasA+t22HLLLzo5ofZukBHCmFfrcEl4APlqb6+HrVyeC/fBXku8RBHCxvgVwVf986NouQ+2iJYTln9bCyRqn//8TJQCaAygtLWUBSBGvAN6VEUCPa5+AzgN/EfH6Nsn0APpE0AMIxH50B3zxd+myDdSZAPaJTAJOsBbpvvvvY9X2WjhafXougAQw5sH34l4vCyAM8QjATgJ4WjpTt87cDemZkXdRE9UD8PH5C1dC/bGd4mWb9JS+BLCtrQAmDymC/GyDCGBbDQoguAcw5iEWgOJUVlbG0QM4IikAS2EnGHDfp1Gtb7OEAApiFMCuBbdB1Y7Voq8NnKz/OYBJg0kAJrWLlhCoB3CsOvFDAIQFIEc8ApDrAWQWlcNFv62Ian1bZARwfgwC2Lvkv+HE5wtFX9NdD0BEAJMNJoA2PQAWgPLEJYBTh2GZpAC6ogC2RrW+LTJDgPNiEsC9KADxf2ed/BScpScBGH4IEDoH0AWHACwAxYlXAEsljgPw9ACiFUAX0edpEvC8WxdFXT6PAMR7AIMm/0F3AtiaYj2AsQmaAygrK2MBSBG3AP4gJYCucGGUAqiQGQLE2gM4KdEDEAQwgAWgFVgAKhGvAJYkQwBCDyCxAhjMAtAULACViFcAi58aKf5FTelgKegU1focNulDgTPywh8KHIqzQfrQ4MFT/gBnDxgfx5ZLLiSAioWhAig0kADq2ghg3MMsAMU5efJkfD2ApyI7F0BrkAC4B6AdlOoBICwAOeIVgFQPQOvosQfAAogJFoAc8Qpg0ZP6FMCQqfoTgPgQwCAxoIgAEjEEABaAPCkrAOoBXGIEARilB6DcHED79u1ZAFLEK4B3nrhC7a8QE0OmPg3n6EgA39MQgAUQNSyAMMQrgIX/Iy6ANHMm5JVfGNX66vaLHzpMpxXndDov6vI1Ve6FlrqToq9ddoP+BFDx9iNBz00ZUgR5WUYRQNshwPjf/Tvu9bIAwnDixIn4BCDRA6DjAPr+9uOo1veJxJGAdCBQ71uiPxJw35J7oVLiSMDLdNYD4CFAzLAA5IhbABI9AAsK4IL7ojsQaNtM8QOB8lEAvWI4EGj/4nuhSuJAIF32AEIEMMVAAlgZIoA8FkByiEcAdYIALhd9zVJUDhdEeSTgNokeQH53EkD0PYD9S0gA4j2AoYIArotjyyUXKQEYZwiAArCFDAFYAMoTrwDeflxcADQE6BO1AGR6ADGcC3BAGAKI/zsSQM9LdSaAkDkAYw8BusD43yVmErBDhw4sACniFsAsiR5AcfQC2C4xBMiLcQhwQGYIMPRG/QlgS+gkoIEEsFJkDuC6BE0CsgBkiFcAC2aNEH3NIvQAopsE3C41BEAB9IxhEvCAzBBg2I3PsAA0BAtAJY4fPx7XJOBbEj2AjIJOcMED0V0SLNE9gO/fug1sEpcE8whAZ5OAb4emAAWGEcCq7XaRGDAxcwAsABniEUBdFQrg8RGSr/d9ZCeYswojXt92mUnAnjFMAu782+XQeGK36GvDddkDCJ0ENI4AVooI4DoWgPLELYBZ0gLoOmoWlA2aEfH6PpfpAZwTZQ+g4fBXsPsfoyRfHz5NjwLgIUAMsADkiE8Ah+DNx6QFYLLkQq87V0NWhDcG+XymeA/AI4DIewAuRz18N2c8NB7dIfme4dP+CL0G6kgAn7YVgLFTABTAI4mZA+jYsSMLQIp4BTBfRgCEyZIDnUc+AO36T4L0bPnhwGczpScBIxGAy9kEtXs2wOE1T4Dj1AHZ947QoQA2t+kBGHcIkMcCSA5xTQJWH4U3Zg6N+P00MUhXCpKiWeKKQGkRXhGoufYoWqA1orJc8Yu/wDkXj45jyyUXFkBssADCEI8AWltb4V8zL4PG2hNqf42omfzov6Gk45lqFyNi9n66FDYvSJ05gLySLnA9C0B5jh07FrMAcOPCxsV/gh0fzFH7a0RFhx4DYPSv54LFYlG7KBHDPYCYYQHIEY8AiKqTx2DN8zdC7ckf1P4qEWG2ZMOVd86Dbj37Q1qa5n4OSSIRQFp2KaSln74Fu6upmq6MqnbRI4IFoBLxCqC5uRkO7/saNr7xW7CfOhTrapICVf5BU56GHhdcDjk5OWoXJyr2SgnAezKQKf8MyB+3DtJMZv/rzmMfg33dNLWLHhGCAGzBAkjEEABYAPLEKwCivr4eaqtPws4PXxPuYdfSZFf7awVBE49dfjIM+l31GygsK4eCggJdtf6EIIA2cwD5/h6AueOlkHfV/KDXXfZDULt4uNpFj4iV2+vb9ACun5mYOYBOnTqxAKRIhABoLqCxsVFYXC4n2I7uAWdzo9pfTSDdbIHC9j3AnJkDGRkZkJ+fr7vKT3gE8Lug5wKHAPoXQOgQoAsLIBkkQgA+nE4nOBwOaGlpEaSgBaiym81myMzMFASgV0gAm8QEkBUogDeCXnfZD+tHAJ+1FcAEFoDyHD16NGECYJRDfAhwugeQ3uESEQEcgroll0f8GWoiNgmYiB4AwgKQgwWgD8LNAZAAcn76r6DX3fWHwb5ULwJQZg4AWADysAD0gdgQYCr2AHyXBPMI4PWg190Nx6HhvRvCrtvd6gB3Y6Wq30+pIQCwAORhAegDQQBvSU8CprcfANkhAogGx6dPQMt3b6r2/doMAUpYAEmBBaAP9ogIYGrAEMCEAsi6Ivb93HlgBTRXPKja96MhwJEQAUxM0CRg586dWQBSsAD0gZQA/EMAFEDmFXODXrfb7bBgwYKw66bkpu7bhfCbweoNA1Z+Vt+mB8ACSAIsAH2w5xORGHBQcA8g8/LXgl7H3xbGj4/ssmf3jWyCa893RvReJRAmAW3BcwATH2UBKM6RI0dYADqA5gA2yg0BygZAxohXg1532o/A4QXXhF13doYbCrPVPW5jxXZlegAIC0AOFoA+CDcHkFZ2MWQMDxYAxYAtq8MLQAuwAFSCBaAPxAQQOAQgAZiHhZyW3XAEBTAq0o9QFaXmAIAFIA8LQB/QHMBG2R7ARZA+tK0AnGuuVbvoEbFCiAFPX82JBDApQXMAXbp0YQFIwQLQB3s+WYoCCD4ScOrgvNPXAyhtKwB3/RFwrdOLAOpDBNAZBfB+3OtlAYSBBaAPSAAfhfYAAoYAJADTZS8H/6OGoygAfVz3kIYAgccB5As9ABaA4hw+fJgFoAPCDQFIAGlDXgr+RzgEcL83Ru2iR4TYJGAihgAIC0AOFoA+EBfA6SHAYUdHKPnZO5Cdne1//cddW+GM3XepXfSIaDsEYAEkBRaAPiABfCQmAO+RgN8eS4eHVneAvLw8/+vpjhPw+k02tYseEZ4hwGkB5LMAkgMLQB8IcwBvSg8BmrH3/Nx/sqHSfvoioVf0aoFR5zWrXfSIEBsCTP59YuYAunbtygKQggWgD8QFkGeYy4KLDQFYAEmABaAPvpMSQJYxBBA6BCABTGEBKA8LQB9893Hq9QCmPMYCUJxDhw6xAHSAMAkoKgDN7VIxsWJ7g8gQIDGTgCwAGVgA+sDoQ4AVn4n0ABIwBAAWgDwsAH0gKoBBBhsC2FgASYcFoA94DiA2aA6gvLycBSAFC0AfkAA+bCOAXAMNAdrOAUxNgACAewDysAD0gUcADwc9Z6ghgCCA4AOBpj62Pu71pqWl8ZGAchw8eJAFoANoDuDD+cE9gBsMNgQIPRQ4EUMAs9nMlwWXgwWgD8SGADfQEMAgAli+XZkhQEoIwG63Z+HDYFy6RvJ+unGn7+adTU1Nz+JDqdobhpHn+8/WwJqXfxX03NRBxhFA6BxAu849Yerv18a93vT09DkZGRmb6G+6UazEnaHpjKn1eXl5it/XPuECwMo/Gb/UbPyzo9KFZ9Tjx2+3wFt/CL7Et5EFcMZPBsHU3y1NZhGasFGciRJ4TskPSagAsPLfiJX/jfjXxGidH7/djAK4Lug5jwA0N6qMiRWfNQYJoLz3ILjhkaQKQAAl8CRK4FGl1p+wX6uuri4PK/9B/LMoKVuGURXqASx4OlgAUwblGCYGXEkCsAULYOrvlqhRFCdKoF9+fv4OJVaeSAFcjwJYlLztwqgJCyB5oABmoQAeV2LdCRNAbW3tYyiAWUnbKoyqHNxFArg+6DmjC2DKw4vVKs5cFMAvlFgxC4CJCTEBTLZmG0YAqz5vYgFEAwkAH2Yla4sw6kICePuZCUHPGVsAVpj8kHoCKCgo0LYAampquAeQQrAAkoo+BADcA0gZDu6qgIV/DBbApIEkAGPEgKu/IAG4/P9PApj0oGpz3HMLCwu1LQCbzcY9gBSCBZBU9CEA4B5AykACeOdPE4OeM7IAuvYiAbyjVnHmFhUVaVsA1dXV3ANIIUQFcGkW5BlFAF864BgLIHJYAKkFCyCpaF8Ap06dYgGkEId2kwAmBT1ndAFMfGChWsWZW1xczAJgtAMLIKnoQgB348PspG0SRlVIAIv+PDnoOWMLYCBMuF81AbxYUlJypxIrTqQA+uLDF0nbJIyqsACSyjQUwHwlVpzQX6uqqmoFPoxOyiZhVOXQ7q2w+NnUEsD1972tRlF24dK/Xbt2TUqsPNECoEt5rcHl4iRsGEZFWABJ4RAuI7Hy71bqAxL+a6EEzG63my4WNw744p6GhQSw5LkpQc8ZWQBdepIAFiTr4ytxWZaWlvYCVn6bkh9kjF+LYZiYYAEwTArDAmCYFIYFwDApDAuAYVIYFgDDpDAsAIZJYVgADJPCsAAYJoVhATBMCsMCYJgUhgXAMCnM/wMT84+tX5/nFQAAAABJRU5ErkJggg==`
  IMGMAP = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAMAAAD04JH5AAAAA3NCSVQICAjb4U/gAAAACXBIWXMAACISAAAiEgG/ZH/cAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAdpQTFRF////EIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgEIHgGJJn7gAAAJ10Uk5TAAECAwQFBgcICQoLDA4PEBESExQWGRscHR4iIyYnKissMTIzNDU4OT0/REVGR0lOUlVWV1hZW11fYWJlaGlqa2xvcHV2d3l6e3x9f4CDhIaHiouOj5CRkpOWmpudoaOkpaanqqusr7a3uLm7vL2+v8HDxMbIycrLzM/Q0dXW2Nnb3N3f4OHi4+Tl5ujp6u7v8PHz9PX29/j5+/z9/qb1FyQAAAQqSURBVBgZxcGNXxNlAAfw37lDnJlNoyHhorRZshxYK19KN8OXdGpk0CapCBLaCsnMZUrHi0LzbZk4YrLf/5qf8pMiu+ee5+652/cLd4yO5J7DuQtXph49mrpyIXd4T7LDQGDWdg3c4Qp3BrrWIgBt+y8u0MbCxf1t8NWq3Tfp4ObuVfBN9wQlTHTDH51FSip2Qr9tl6ng8jbotX6EikbWQ6PYJJVNxqBN8iFdeJiEJj1LdGWpBzqsGaRrg2vgWbRID4pReLTxFj25tRGehMbo0VgIXvTRsz54sIsa7IJrWxeowcJWuNQyRy3mWuCKMU5Nxg24sZPa7IQLIYvaWCGo20uN9kJZ8yw1mm2GqgPU6gAUrbtPre6vg5qj1OwolIQeULMHIajYTu22Q0U/pUz/ePbrwfE/KKMfKmbp7Prn7fjP20csOpqFgi10NPORgefMz+7RyRbIO0EnZ5ux3Gvf08EJyJugWK0HK4S+odgEpG2mgy9Qz7cU2wxZGYoNoa6mnyiUgawche6+gvpaFyiSg6xLFPoUdr6iyCXImqLIVAh2IvMUmIIks0qRXtj7jgJVE3LaKfQe7H1CkXbI2UGR+VWw10KRHZCTpogFkb8pkIacfoqMQWSGAv2Qc4YioxD5jQJnIGeAIr9A5B4FBiDnNEXmINC0RIHTkJOjyJNXYa+DIjnI6aPQh7B3hCJ9kHOKQoOwV6TIKcjppdD8G7ATp1Av5Jyk2ADsjFHoJOQco9iTd1BfF8WOQU6KDm63oJ7YnxRLQU4HnfwcxkqvT9JBB+SYi3RyPYqXvTVDB4smJN2go7sfYBnj47/o5AZknaOEH97Fc52/0tk5yDpIKb9/+X5bM8JvdvbfpoyDkJWkvAqlJSGrlb5ohbRp+mAa8o7TB8chr50+aIeCq9TuKlTso3b7oGLDIjVb3AAlo9RsFGq6qVk31DRZ1MpqgqIEtUpA2Qg1GoG6TfPUZn4TXDhEbQ7BjdUWNbFWw5UENUnApfPU4jzcipSoQSkC1xI1elZLwIM8PcvDi7BFj6wwPIlX6Uk1Do+y9CQLr8xr9OCaCc9ij+na4xg0SNO1NHQwCnSpYECLaJmulKPQJEVXUtBmiC4MQZ9IicpKEWiUqFFRLQGt8lSUh15hi0qsMDSLV6mgGod2WSrIQj+zSGlFEz6IVSipEoMv0pSUhj+MAqUUDPgkWqaEchS+SVFCCj4apqNh+ClSooNSBL5K1ChUS8BneQrl4bewRQErDN/Fq7RVjSMAWdrKIghmkTaKJgIRq7CuSgwBybCuDIJiFFhHwUBgomWuUI4iQCmukEKghvmSYQQrUuIypQgClqjxBbUEApfnC/IIXtji/6wwGiBe5TPVOBoiy2eyaAyzyH8VTTRIrMKnKjE0TIZPZdA4RoEsGGigaLkcRUOlUvDmH5Vzm7vVz1dHAAAAAElFTkSuQmCC`
  //#endregion
  constructor(private service: BranchService,
    private modalService: NzModalService,
    private notificationService: NzNotificationService,
    private sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.GetBranch()
    document.getElementById("content").classList.remove("p-0")

  }

  GetBranch() {
    this.service.GetBranch().subscribe(
      (res) => {
        this.BranchData = res as any[]
      }
    )
  }
  deleteBranch(data) {
    this.modalService.confirm({
      nzTitle: '<h5 class = "modal-title">ยืนยันการลบสาขา</h5>',
      nzContent: 'คุณต้องการลบสาขา ' + data.name + ' ใช่หรือไม่ ?',
      nzClosable: false,
      nzCancelText: "ยกเลิก",
      nzOkText: "ยืนยัน",
      nzOnOk: () => new Promise(resolev => setTimeout(resolev, 100))
        .then(() => {

          this.service.DeleteBranch(data.branch_id).subscribe(
            (res) => {
              this.notificationService.create("success", "ลบข้อมูลสำเร็จ", null)
              this.GetBranch()
            }
          )
        })
    })
  }
  openModal(typeComponent: string, data?: any) {
    this.GetBranch();
    const modal = this.modalService.create({
      nzTitle: typeComponent === 'new' ? 'เพิ่มสาขา' : 'แก้ไขสาขา',
      nzContent: BranchFormComponent,
      nzComponentParams: {
        typeComponent: typeComponent,
        branch_id: isNullOrUndefined(data) ? null : data.branch_id,
        name: isNullOrUndefined(data) ? null : data.name,
        detail: isNullOrUndefined(data) ? null : data.detail,
        address: isNullOrUndefined(data) ? null : data.address,
        active: isNullOrUndefined(data) ? null : data.active,
        status: isNullOrUndefined(data) ? null : data.status,
        manager_name: isNullOrUndefined(data) ? null : data.manager_name,
        province_name: isNullOrUndefined(data) ? null : data.province_name,
        amphur_name: isNullOrUndefined(data) ? null : data.amphur_name,
        district_name: isNullOrUndefined(data) ? null : data.district_name,
        zipcode: isNullOrUndefined(data) ? null : data.zipcode,
        province_id: isNullOrUndefined(data) ? null : data.province_id,
        amphur_id: isNullOrUndefined(data) ? null : data.amphur_id,
        district_id: isNullOrUndefined(data) ? null : data.district_id,
        zipcode_id: isNullOrUndefined(data) ? null : data.zipcode_id,
        location: isNullOrUndefined(data) ? null : data.location,
        map_img: isNullOrUndefined(data) ? null : data.map_img,
        img1: isNullOrUndefined(data) ? null : data.img1,
        caption1: isNullOrUndefined(data) ? null : data.caption1,
        img2: isNullOrUndefined(data) ? null : data.img2,
        caption2: isNullOrUndefined(data) ? null : data.caption2,
        img3: isNullOrUndefined(data) ? null : data.img3,
        caption3: isNullOrUndefined(data) ? null : data.caption3,
        img4: isNullOrUndefined(data) ? null : data.img4,
        caption4: isNullOrUndefined(data) ? null : data.caption4,
        img5: isNullOrUndefined(data) ? null : data.img5,
        caption5: isNullOrUndefined(data) ? null : data.caption5,
        img6: isNullOrUndefined(data) ? null : data.img6,
        caption6: isNullOrUndefined(data) ? null : data.caption6,
        img7: isNullOrUndefined(data) ? null : data.img7,
        caption7: isNullOrUndefined(data) ? null : data.caption7,
        img8: isNullOrUndefined(data) ? null : data.img8,
        caption8: isNullOrUndefined(data) ? null : data.caption8,
        img9: isNullOrUndefined(data) ? null : data.img9,
        caption9: isNullOrUndefined(data) ? null : data.caption9,
        img10: isNullOrUndefined(data) ? null : data.img10,
        caption10: isNullOrUndefined(data) ? null : data.caption10,

      },
      nzClosable: true,
      nzWidth:700,
      nzFooter: [
        {
          label: 'ยกเลิก',
          shape: 'default',
          onClick: () => modal.destroy()
        },
        {
          label: 'บันทึก',
          type: 'primary',
          onClick: () => new Promise(resolve => setTimeout(resolve, 0)).then(
            () => {
              const instance = modal.getContentComponent();
              return instance
            }
          ).then((instance) => {
            if (isNullOrUndefined(instance.name) || instance.name==="" || instance.name.trim()==="")
              this.notificationService.create("error", "<p>กรุณาใส่ชื่อสาขา</p>", null)
            if (isNullOrUndefined(instance.detail)|| instance.detail==="" || instance.detail.trim()==="")
              this.notificationService.create("error", "<p>กรุณาใส่รายละเอียดของสาขา</p>", null)
            if (isNullOrUndefined(instance.address) || instance.address===""  || instance.address.trim()==="")
              this.notificationService.create("error", "<p>กรุณาใส่ที่อยู่</p>", null)
            if (isNullOrUndefined(instance.location)|| instance.location==="" || instance.location.trim()==="")
              this.notificationService.create("error", "<p>กรุณาใส่ลิ้งแผนที่</p>", null)
            if (isNullOrUndefined(instance.Province_id))
              this.notificationService.create("error", "<p>กรุณาเลือกจังหวัด</p>", null)
            if (isNullOrUndefined(instance.Amphur_id))
              this.notificationService.create("error", "<p>กรุณาเลือกอำเภอ</p>", null)
            if (isNullOrUndefined(instance.District_id))
              this.notificationService.create("error", "<p>กรุณาเลือกตำบล</p>", null)
            else if (!isNullOrUndefined(instance.name) &&instance.name!=="" &&instance.detail!=="" &&instance.address!=="" &&instance.location!=="" && !isNullOrUndefined(instance.detail) &&instance.name.trim()!=="" &&instance.location.trim()!==""  &&instance.address.trim()!=="" &&instance.detail.trim()!=="" && !isNullOrUndefined(instance.address) && !isNullOrUndefined(instance.location) && !isNullOrUndefined(instance.Province_id) && !isNullOrUndefined(instance.Amphur_id) && !isNullOrUndefined(instance.District_id)) {
              this.AddOrEdit(instance).subscribe(
                async (res) => {
                  let id: any
                  let idup: any
                  if (instance.typeComponent === "new") {
                    await this.service.Getlastbranchid().subscribe(
                      async (res) => {
                        id = res[0].id
                        if (instance.fileList.length == 0) {
                          this.service.UpfileBranch(id, { img1: this.IMGHOME }).subscribe((res) => { });
                        }
                        else {
                          for (let i = 0; i < instance.fileList.length; i++) {
                            switch (i) {
                              case 0: await this.service.UpfileBranch(id, { img1: instance.fileList[0].thumbUrl }).subscribe((res) => { }); break;
                              case 1: await this.service.UpfileBranch(id, { img2: instance.fileList[1].thumbUrl }).subscribe((res) => { }); break;
                              case 2: await this.service.UpfileBranch(id, { img3: instance.fileList[2].thumbUrl }).subscribe((res) => { }); break;
                              case 3: await this.service.UpfileBranch(id, { img4: instance.fileList[3].thumbUrl }).subscribe((res) => { }); break;
                              case 4: await this.service.UpfileBranch(id, { img5: instance.fileList[4].thumbUrl }).subscribe((res) => { }); break;
                              case 5: await this.service.UpfileBranch(id, { img6: instance.fileList[5].thumbUrl }).subscribe((res) => { }); break;
                              case 6: await this.service.UpfileBranch(id, { img7: instance.fileList[6].thumbUrl }).subscribe((res) => { }); break;
                              case 7: await this.service.UpfileBranch(id, { img8: instance.fileList[7].thumbUrl }).subscribe((res) => { }); break;
                              case 8: await this.service.UpfileBranch(id, { img9: instance.fileList[8].thumbUrl }).subscribe((res) => { }); break;
                              case 9: await this.service.UpfileBranch(id, { img10: instance.fileList[9].thumbUrl }).subscribe((res) => { }); break;
                            }
                          }
                        }
                      }
                    )
                  }
                  if (instance.typeComponent === "edit") {
                    idup = instance.branch_id
                    if (instance.fileList.length == 0) {
                      this.service.UpfileBranch(idup, { img1: this.IMGHOME }).subscribe((res) => { });
                    }
                    else {
                      for (let i = 0; i < instance.fileList.length; i++) {
                        switch (i) {
                          case 0: await this.service.UpfileBranch(idup, { img1: instance.fileList[0].thumbUrl }).subscribe((res) => { }); break;
                          case 1: await this.service.UpfileBranch(idup, { img2: instance.fileList[1].thumbUrl }).subscribe((res) => { }); break;
                          case 2: await this.service.UpfileBranch(idup, { img3: instance.fileList[2].thumbUrl }).subscribe((res) => { }); break;
                          case 3: await this.service.UpfileBranch(idup, { img4: instance.fileList[3].thumbUrl }).subscribe((res) => { }); break;
                          case 4: await this.service.UpfileBranch(idup, { img5: instance.fileList[4].thumbUrl }).subscribe((res) => { }); break;
                          case 5: await this.service.UpfileBranch(idup, { img6: instance.fileList[5].thumbUrl }).subscribe((res) => { }); break;
                          case 6: await this.service.UpfileBranch(idup, { img7: instance.fileList[6].thumbUrl }).subscribe((res) => { }); break;
                          case 7: await this.service.UpfileBranch(idup, { img8: instance.fileList[7].thumbUrl }).subscribe((res) => { }); break;
                          case 8: await this.service.UpfileBranch(idup, { img9: instance.fileList[8].thumbUrl }).subscribe((res) => { }); break;
                          case 9: await this.service.UpfileBranch(idup, { img10: instance.fileList[9].thumbUrl }).subscribe((res) => { }); break;
                        }
                      }
                    }
                  }
                  if (JSON.stringify(res)[11] === 's') {
                    this.notificationService.create("success", typeComponent === "new" ? "เพิ่มข้อมูลสำเร็จ" : "แก้ไขข้อมูลสำเร็จ", null)
                    modal.destroy();
                    this.GetBranch();
                  }
                }
              )
            }
          })
        }
      ]
    })
  }
  AddOrEdit(data) {
    if (data.typeComponent === "new") {
      const value = {
        name: data.name,
        detail: data.detail,
        manager_name: data.manager_name,
        address: data.address,
        status: data.Status ? "Y" : "N",
        active: data.ActiveCheckbox ? "Y" : "N",
        map_img: data.MAP.length == 0 ? this.IMGMAP : data.MAP[0].thumbUrl,
        location: data.location,
        province_id: data.Province_id,
        amphur_id: data.Amphur_id,
        district_id: data.District_id,
        zipcode_id: data.Zipcode_id,

      }
      return this.service.AddBranch(value)
    }
    else {
      const value = {
        branch_id: data.branch_id,
        name: data.name,
        detail: data.detail,
        manager_name: data.manager_name,
        address: data.address,
        status: data.Status ? "Y" : "N",
        active: data.ActiveCheckbox ? "Y" : "N",
        map_img: data.MAP.length == 0 ? this.IMGMAP : data.MAP[0].thumbUrl,
        location: data.location,
        province_id: data.Province_id,
        amphur_id: data.Amphur_id,
        district_id: data.District_id,
        zipcode_id: data.Zipcode_id,

      }
      return this.service.EditBranch(value)
    }
  }
  sharch() {
    this.isLoading = true;
    this.service.SearchBranch(this.criteriaNameBranch).subscribe(
      (res) => {
        this.BranchData = res as any[]
        this.isLoading = false;
        if(this.BranchData.length==0){
          Swal.fire({
            html: '<p>ไม่มีข้อมูลที่คุณต้องการค้นหา!</p>',
          })
        }
      }
    )
  }
  sanitize(url: string) {
    // this.isSpinningImg = false;
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }
}
