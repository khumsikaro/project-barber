import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MasterRoutingModule } from './master-routing.module';
import { InformationComponent } from './information/information.component';
import { HomeComponent } from './home/home.component';
import { BranchComponent } from './branch/branch.component';
import { UserComponent } from './user/user.component';
import { HolidayComponent } from './holiday/holiday.component';
import { MasterComponent } from './master.component';
import { ZorroModule } from 'src/app/lib.ngZorro';
import { InformationFromComponent } from './information/information-from/information-from.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NumberOfRightsGrantedComponent } from './number-of-rights-granted/number-of-rights-granted.component';
import { GuardService } from '../login/guard.service';
import { ServiceComponent } from './service/service.component';
import { ServiceFormComponent } from './service/service-form/service-form.component';
import { BranchFormComponent } from './branch/branch-form/branch-form.component';
import { UserFormComponent } from './user/user-form/user-form.component';
import { SettingbarberComponent } from './settingbarber/settingbarber.component';
import { HomeFormComponent } from './home/home-form/home-form.component';
import { HolidayFormComponent } from './holiday/holiday-form/holiday-form.component';
import { FromNumberofrightComponent } from './number-of-rights-granted/from-numberofright/from-numberofright.component';
import { MasterGuardsService } from './master-guards.service';


@NgModule({
  declarations: [
    InformationComponent,
    InformationFromComponent,
    HomeComponent,
    BranchComponent,
    UserComponent,
    HolidayComponent,
    MasterComponent,
    NumberOfRightsGrantedComponent,
    ServiceComponent,
    ServiceFormComponent,
    BranchFormComponent,
    UserFormComponent,
    SettingbarberComponent,
    HomeFormComponent,
    HolidayFormComponent,
    FromNumberofrightComponent,
  ],
  imports: [
    CommonModule,
    MasterRoutingModule,
    ZorroModule,
    FormsModule,
    ReactiveFormsModule
  ],
  entryComponents: [
    // มันการทำให้คอมโพเน้นที่ถูกประกาศตรงนี้ ทำงานพร้อมกับคอมโพเน้นอื่นภายในmasterได้
    InformationFromComponent,
    ServiceFormComponent,
    BranchFormComponent,
    UserFormComponent,
    HomeFormComponent,
    HolidayFormComponent,
    FromNumberofrightComponent,
  ],
  providers: [
    GuardService,
    MasterGuardsService,
    
  ]
})
export class MasterModule { }
