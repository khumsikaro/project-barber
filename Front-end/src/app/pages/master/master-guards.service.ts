import { Injectable } from '@angular/core';
import {
  CanActivate, Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivateChild
} from '@angular/router';
import { LoginService } from '../login/login.service';
@Injectable()
export class MasterGuardsService implements CanActivate {
  constructor(private _router: Router, private AuthenService: LoginService) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> | boolean {
    let MasterUser = this.AuthenService.getActiveUser();
    return new Promise<boolean>(resolve => {
      if(MasterUser!=null){
        if (MasterUser.levelUsers== 0) {
          resolve(true);
        } else {
          resolve(false);
          this._router.navigate(['']);
        }
      }
      else{
        resolve(false);
        this._router.navigate(['']);
      }
    });
  }


}
