import { Component, OnInit, Input } from '@angular/core';
import { UploadFile, NzMessageService } from 'ng-zorro-antd';
import { FormGroup, FormControlName, FormControl } from '@angular/forms';

@Component({
  selector: 'app-information-from',
  templateUrl: './information-from.component.html',
  styleUrls: ['./information-from.component.css']
})
export class InformationFromComponent implements OnInit {
  @Input() typeComponent: string;
  @Input() news_id: number;
  @Input() title?: string;
  @Input() detail?: string;
  @Input() type?: number;
  @Input() active?: string;
  @Input() img1: string;
  @Input() img2: string;
  @Input() img3: string;
  @Input() img4: string;
  @Input() img5: string;
  @Input() img6: string;
  @Input() img7: string;
  @Input() img8: string;
  @Input() img9: string;
  @Input() img10: string;
  InformationType: string;
  ActiveCheckbox: boolean;
  selectedValue = "0";
  inputValue: string;
  Img = [];
  showUploadList = {
    showPreviewIcon: true,
    showRemoveIcon: true,
    hidePreviewIconInNonImage: true
  };
  fileList = [];
  previewImage: string | undefined = '';
  previewVisible = false;
  ImgAll = [];

  handlePreview = (file: UploadFile) => {
    this.previewImage = file.url || file.thumbUrl;
    this.previewVisible = true;
  };

  // handleChange({ file, fileList }: { [key: string]: any }): void {
  //   const status = file.status;
  //   this.fileList = fileList
  //   if (status !== 'uploading') {
  //   }
  //   if (status === 'done') {
  //     this.msg.success(`${file.name} file uploaded successfully.`);
  //   } else if (status === 'error') {
  //     this.msg.error(`${file.name} file upload failed.`);
  //   }
  // }
  constructor(private msg: NzMessageService) { }

  ngOnInit() {
    this.title = this.typeComponent === "new" ? null : this.title
    this.detail = this.typeComponent === "new" ? null : this.detail
    this.InformationType = this.typeComponent === "new" ? "0" : this.type.toString()
    this.ActiveCheckbox = this.typeComponent === "new" ? true : this.active === "Y" ? true : false
    this.img1 = this.typeComponent === "new" ? null : this.img1
    this.img2 = this.typeComponent === "new" ? null : this.img2
    this.img3 = this.typeComponent === "new" ? null : this.img3
    this.img4 = this.typeComponent === "new" ? null : this.img4
    this.img5 = this.typeComponent === "new" ? null : this.img5
    this.img6 = this.typeComponent === "new" ? null : this.img6
    this.img7 = this.typeComponent === "new" ? null : this.img7
    this.img8 = this.typeComponent === "new" ? null : this.img8
    this.img9 = this.typeComponent === "new" ? null : this.img9
    this.img10 = this.typeComponent === "new" ? null : this.img10
    if (this.typeComponent === "edit") {
      const Img = [
        { uid: 1, thumbUrl: this.img1 },
        { uid: 2, thumbUrl: this.img2 },
        { uid: 3, thumbUrl: this.img3 },
        { uid: 4, thumbUrl: this.img4 },
        { uid: 5, thumbUrl: this.img5 },
        { uid: 6, thumbUrl: this.img6 },
        { uid: 7, thumbUrl: this.img7 },
        { uid: 8, thumbUrl: this.img8 },
        { uid: 9, thumbUrl: this.img9 },
        { uid: 10, thumbUrl: this.img10 },

      ]
      for (var item in Img) {
        if (Img[item].thumbUrl !== "undefined" && Img[item].thumbUrl != "") {
          this.ImgAll.push(Img[item])
        }
      }
      this.fileList=this.ImgAll
    }
  }

}
