import { Injectable } from '@angular/core';
import { urlServer } from '../../../URL';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class InformationService {

  constructor(private http: HttpClient) { }
  
  public GetInformation() {
    return this.http.get(urlServer.ipServer + `GetInformation`);
  }
  public GetInformationDetails(news_id:number) {
    return this.http.get(urlServer.ipServer + `GetInformationDetail/${news_id}`);
  }
  public SearchInformation(title:string,date,type:string){
    return this.http.get(urlServer.ipServer + `SearchInformation/?title=${title}&date=${date}&type=${type}`)
  }
  public DeleteInformation(news_id){
    return this.http.delete(urlServer.ipServer + `DeleteInformation/${news_id}`)
  }
  public AddInformation(data){
    return this.http.post(urlServer.ipServer + `AddInformation`,data)
  }
  public EditInformation(data){
    return this.http.put(urlServer.ipServer + `UpdateInformation`,data)
  }
  public GetLastId(){
    return this.http.get(urlServer.ipServer + `getlastid`)
  }
  public uploadfile(id,file){
    return this.http.patch(urlServer.ipServer + `uploadfile/${id}`,file)
  }
}
