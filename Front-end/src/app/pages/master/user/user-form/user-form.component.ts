import { Component, OnInit, Input } from '@angular/core';
import { BranchService } from '../../branch/branch.service';
import { fi_FI, UploadFile } from 'ng-zorro-antd';
import { log, debug, isNullOrUndefined } from 'util';
import { FromService } from './user-from.model';
import { UserService } from '../user.service';
import { differenceInCalendarDays } from 'date-fns';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {
  @Input() typeComponent: string;
  @Input() username: string;
  @Input() password: string;
  @Input() level_barberid: number;
  @Input() status: string;
  @Input() img_profile: string;///
  @Input() firstname: string;
  @Input() lastname: string;
  @Input() nickname: string;
  @Input() date_of_birth: any;////
  @Input() phone_number: string;
  @Input() gender: number;///
  @Input() Email: string;
  @Input() address: string;
  @Input() province_name: string;
  @Input() province_id: number;
  @Input() amphur_name: string;
  @Input() amphur_id: number;
  @Input() district_name: string;
  @Input() district_id: number;
  @Input() zipcode: string;
  @Input() zipcode_id: number;
  @Input() active: string;
  @Input() branch_id: number;
  @Input() name: string;
  @Input() cr_date: Date;///
  @Input() cr_by: string;
  @Input() up_date: Date;///
  @Input() up_by: string;
  @Input() status_soc
  UserType: string;
  StatusCheckbox: boolean;
  ActiveCheckbox: boolean;
  Branch: string;
  UserName_cr: string;
  SEX: string;
  Password;
  ////////////////
  Province: string;
  Amphur: string;
  District: string;
  Zipcode: string;
  Province_id: number;
  Amphur_id: number;
  District_id: number;
  Zipcode_id: number;
  DisabledAmphurData = true;
  DisabledDistrictData = true;
  DisabledZipcodeData = true;
  /////////////////
  ProvinceData: any = [];
  AmphurData: any = [];
  DistrictData: any = [];
  ZipcodeData: any = [];
  /////////////////
  BranchData: any = [];
  ServiceData: any = [];
  SkillArr: any = [];;
  /////////////////////////////////////////
  showUploadList = {
    showPreviewIcon: true,
    showRemoveIcon: true,
    hidePreviewIconInNonImage: true
  };
  fileList = [];
  previewImage: string | undefined = '';
  previewVisible = false;
  Img = [];
  ImgAll = [];

  handlePreview = (file: UploadFile) => {
    this.previewImage = file.url || file.thumbUrl || this.img_profile;
    this.previewVisible = true;
  };
  ///////////////////////////////////
  constructor(private service: BranchService, private serviceUser: UserService) { }
  ngOnInit() {
    this.UserType = this.typeComponent === "new" ? "4" : this.level_barberid.toString()
    this.GetServiceList(this.UserType)
    
    if (this.typeComponent === "new") {
      this.Amphur_id = null;
      this.District_id = null;
      this.Zipcode_id = null;
      this.date_of_birth = null; 
    }
    else {
      
      this.ShowAmphur(this.province_id)
      this.ShowDistrict(this.amphur_id)
      this.ShowZipcode(this.district_id)
      this.Amphur_id = this.amphur_id;
      this.District_id = this.district_id;
      this.Zipcode_id = this.zipcode_id;
      this.date_of_birth = this.date_of_birth === "0000-00-00" ? null : this.date_of_birth;
      this.SkillBarber(this.username);
      this.Password=this.password
    }
    this.username = this.typeComponent === "new" ? null : this.username
    this.password = this.typeComponent === "new" ? null : this.password
    this.StatusCheckbox = this.typeComponent === "new" ? true : this.status === "Y" ? true : false
    this.img_profile = this.typeComponent === "new" ? null : this.img_profile
    this.firstname = this.typeComponent === "new" ? null : this.firstname
    this.lastname = this.typeComponent === "new" ? null : this.lastname
    this.nickname = this.typeComponent === "new" ? null : this.nickname
    this.phone_number = this.typeComponent === "new" ? null : this.phone_number
    this.gender = this.typeComponent === "new" ? 0 : this.gender
    this.Email = this.typeComponent === "new" ? null : this.Email
    this.address = this.typeComponent === "new" ? null : this.address
    this.province_name = this.typeComponent === "new" ? null : this.province_name
    this.province_id = this.typeComponent === "new" ? null : this.province_id
    this.amphur_name = this.typeComponent === "new" ? null : this.amphur_name
    this.amphur_id = this.typeComponent === "new" ? null : this.amphur_id
    this.district_id = this.typeComponent === "new" ? null : this.district_id
    this.district_name = this.typeComponent === "new" ? null : this.district_name
    this.zipcode = this.typeComponent === "new" ? null : this.zipcode
    this.zipcode_id = this.typeComponent === "new" ? null : this.zipcode_id
    this.ActiveCheckbox = this.typeComponent === "new" ? true : this.active === "Y" ? true : false
    this.branch_id = this.typeComponent === "new" ? null : this.branch_id
    this.name = this.typeComponent === "new" ? null : this.name
    this.status_soc = this.typeComponent === "new" ? false : this.status_soc === "Y" ? true : false
    this.ShowProvince()
    this.GetBranch()

    if (this.typeComponent === "edit") {
      const Img = [
        { uid: 1, thumbUrl: this.img_profile },
      ]
      for (var item in Img) {
        if (Img[item].thumbUrl !== "undefined" && Img[item].thumbUrl != "") {
          this.ImgAll.push(Img[item])
        }
      }
      this.fileList = this.ImgAll
    }
  }
  disabledDate = (current: Date): boolean => {
    let today = new Date
    // Can not select days before today and today
    return differenceInCalendarDays(current, today) > 0;
  };
  SkillBarber(username) {
    this.service.Getskill(username).subscribe(
      (res: any[]) => {
        let SkillBarber=[];
        for (let i = 0; i < res.length; i++) {
          SkillBarber.push(res[i].service_name)
        }
        this.SkillArr = SkillBarber
      }
    )
  }

  ShowProvince() {
    this.service.GetProvince().subscribe(
      (res) => {
        this.ProvinceData = res as any[]
      }
    )
  }
  ShowAmphur(data) {
    this.DisabledAmphurData = false;
    this.service.GetAmphur(data).subscribe(
      (res) => {
        this.AmphurData = res as any[]
      }
    )
  }
  ShowDistrict(data) {
    this.DisabledDistrictData = false;
    this.service.GetDistrict(data).subscribe(
      (res) => {
        this.DistrictData = res as any[]
      }
    )
  }
  ShowZipcode(data) {
    this.DisabledZipcodeData = false;
    this.service.GetZipcode(data).subscribe(
      (res) => {
        this.ZipcodeData = res as any[]
      }
    )
  }
  GetBranch() {
    this.service.GetBranch().subscribe(
      (res) => {
        this.BranchData = res as any[]
      }
    )
  }
  GetServiceList(UserType) {
    this.SkillArr=[];
    this.service.GetShowListService().subscribe(
      (res: any[]) => {
        if (UserType === "2") {
          this.ServiceData = res.filter(x => x.level_barberid == 2)
        }
        else {
          this.ServiceData =res;
        }
      }
    )
  }
  // onChangeService(event, list: any, ) {
  //   // debugger
  //   if (event) {
  //     this.SkillArr = this.SkillArr || [];
  //     this.SkillArr.push(list)
  //   } else {
  //     for (let i = 0; i < this.SkillArr.length; i++) {
  //       if (this.SkillArr[i].name === list.name) {
  //         this.SkillArr.splice(i, 1);
  //       }
  //     }
  //   }
  // }
  ///////////////////////////////////////////////////////////
  // GetSkill(id: string) {
  //   this.GetServiceList(this.UserType);
  //   this.service.Getskill(id).subscribe(
  //      async (res: any[]) => {
  //       let temp;
  //       let temp1 = new Array();
  //       let flag:boolean = true;
  //       for (let index = 0; index < this.ServiceData.length; index++) {
  //         temp = new FromService();
  //         temp.baber_name = await this.username;
  //         temp.service_name = await this.ServiceData[index].name;
  //         await this.serviceUser.CheckSkill(res[index].service_name).subscribe(
  //           async (result) => {
  //             flag = await isNullOrUndefined(result[0].name) ? true : true;
  //           }
  //         )
  //         temp.flag = await flag;
  //         temp1.push(temp);
  //       }
  //       this.SkillArr = temp1
  //     }
  //   )
  // }
}