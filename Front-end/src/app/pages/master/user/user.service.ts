import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { urlServer } from '../../../URL'

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  GetShowUser() {
    return this.http.get(urlServer.ipServer + `ShowUsers`)
  }
  public GetUserDetail(username: string) {
    return this.http.get(urlServer.ipServer = `UsersDetail/${username}`)
  }
  public SearchUser(firstname: string, level_barberid: number) {
    return this.http.get(urlServer.ipServer + `SearchUsers/?firstname=${firstname}&level_barberid=${level_barberid}`)
  }
  public DeleteUser(username: string) {
    return this.http.delete(urlServer.ipServer + `DeleteUser/${username}`)
  }
  public AddUser(data) {
    return this.http.post(urlServer.ipServer + `AddUser`, data)
  }
  public EditUser(data) {
    return this.http.put(urlServer.ipServer + `UpdateUser`, data)
  }
  public GetBarberDetail(username: string) {
    return this.http.get(urlServer.ipServer + `DetailBarber/${username}`)
  }
  public Getskill(username: string) {
    return this.http.get(urlServer.ipServer + `Detailskills/${username}`)
  }
  public CheckSkill(skill: string) {
    return this.http.get(urlServer.ipServer + `CheckSkill/${skill}`)
  }
  public GetListCheckWork(username){
    return this.http.get(urlServer.ipServer + `GetListCheckWork/${username}`)
  }
}
