import { Component, OnInit } from '@angular/core';
import { SettingbarberService } from './settingbarber.service';
import { Time } from '@angular/common';
import { NzModalService, NzNotificationService } from 'ng-zorro-antd';

@Component({
  selector: 'app-settingbarber',
  templateUrl: './settingbarber.component.html',
  styleUrls: ['./settingbarber.component.css']
})
export class SettingbarberComponent implements OnInit {
  time
  time_s: number;
  Shop_insurance: number;
  Shop_insurance_month: number;
  Social_Security_mon: number;
  BarberLV2: number;
  ValueBarberLV2: number;
  AwolLv2: number;
  BarberLV3: number;
  ValueBarberLV3: number;
  AwolLv3: number;
  constructor(private service: SettingbarberService, private modalService: NzModalService, private notificationService: NzNotificationService, ) { }

  Getsettingbarber() {
    this.service.GetSettingbarber().subscribe(
      (res) => {
        let formatTime = `2019-10-15T`+res[0].chack_in;
        this.time=new Date(formatTime)
        this.time_s = res[0].time_mi;
        this.Shop_insurance = res[0].Shop_security;
        this.Shop_insurance_month = res[0].Shop_security_mon;
        this.Social_Security_mon = res[0].social_security_mon;
      }
    )
  }
  GetSettingSalary() {
    this.service.GetSettingSalary().subscribe(
      (res) => {
        this.BarberLV2 = res[0].salary;
        this.ValueBarberLV2 = res[0].percent;
        this.AwolLv2 = res[0].Awol
        this.BarberLV3 = res[1].salary;
        this.ValueBarberLV3 = res[1].percent;
        this.AwolLv3 = res[1].Awol
      }
    )
  }
  submitFormSetting() {
    this.modalService.success({
      nzTitle: '<h5 class = "modal-title">ยืนยันการ แก้ไข</h5>',
      nzContent: 'คุณต้องการ แก้ไข SettingBarber ใช่หรือไม่ ?',
      nzClosable: false,
      nzCancelText: "ยกเลิก",
      nzOkText: "ยืนยัน",
      nzOnOk: () => new Promise(resolev => setTimeout(resolev, 100))
        .then(() => {
          const value = {
            id: 1,
            chack_in: this.time.getHours()+':'+this.time.getMinutes()+':00',
            time_mi: this.time_s,
            social_security_mon: this.Social_Security_mon,
            shop_security: this.Shop_insurance,
            shop_security_mon: this.Shop_insurance_month,
          }
          this.service.EditSettingbarber(value).subscribe(
            (res) => {
              this.Getsettingbarber()
            }
          )

          this.notificationService.create("success", "<p>แก้ไขข้อมูลสำเร็จ</p>", null)
        })
    })

  }
  submitFormSalary() {

    this.modalService.success({
      nzTitle: '<h5 class = "modal-title">ยืนยันการ แก้ไข</h5>',
      nzContent: 'คุณต้องการ แก้ไขSettingSalary ใช่หรือไม่ ?',
      nzClosable: false,
      nzCancelText: "ยกเลิก",
      nzOkText: "ยืนยัน",
      nzOnOk: () => new Promise(resolev => setTimeout(resolev, 100))
        .then(() => {
          const Salary = [
            {
              level_barberid: 2,
              salary: this.BarberLV2,
              percent: this.ValueBarberLV2,
              Awol: this.AwolLv2
            },
            {
              level_barberid: 3,
              salary: this.BarberLV3,
              percent: this.ValueBarberLV3,
              Awol: this.AwolLv3
            }]
          const value = {
            FormSalary: Salary,
          }
          this.service.EditSettingSalary(value).subscribe(
            (res) => {
              this.GetSettingSalary()
            }

          )

          this.notificationService.create("success", "<p>แก้ไขข้อมูลสำเร็จ</p>", null)
        })
    })

  }

  resetFormSetting() {
    this.modalService.confirm({
      nzTitle: '<h5 class = "modal-title">ยืนยันการ ล้างข้อมูล</h5>',
      nzContent: 'คุณต้องการ ล้างข้อมูลการตั้งค่าช่างเสริมสวย ใช่หรือไม่ ?',
      nzClosable: false,
      nzCancelText: "ยกเลิก",
      nzOkText: "ยืนยัน",
      nzOnOk: () => new Promise(resolev => setTimeout(resolev, 100))
        .then(() => {
          this.Getsettingbarber()
          this.notificationService.create("success", "<p>รีเซ็ตข้อมูลสำเร็จ</p>", null)
        })
    })
  }
  resetFormSalary() {
    this.modalService.confirm({
      nzTitle: '<h5 class = "modal-title">ยืนยันการ ล้างข้อมูล</h5>',
      nzContent: 'คุณต้องการ ล้างข้อมูลการตั้งค่าเงินเดือน ใช่หรือไม่ ?',
      nzClosable: false,
      nzCancelText: "ยกเลิก",
      nzOkText: "ยืนยัน",
      nzOnOk: () => new Promise(resolev => setTimeout(resolev, 100))
        .then(() => {
          this.GetSettingSalary()
          this.notificationService.create("success", "<p>รีเซ็ตข้อมูลสำเร็จ</p>", null)
        })
    })

  }

  ngOnInit() {
    this.Getsettingbarber()
    this.GetSettingSalary()
    document.getElementById("content").classList.remove("p-0")
  }

}
