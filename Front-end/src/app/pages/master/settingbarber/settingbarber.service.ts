import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {urlServer} from '../../../URL'
@Injectable({
  providedIn: 'root'
})
export class SettingbarberService {

  constructor(private http: HttpClient) { }

  public GetSettingbarber(){
    return this.http.get(urlServer.ipServer +`GetSettingbarber`);
  }
  public GetSettingSalary(){
    return this.http.get(urlServer.ipServer +`GetSettingSalary`);
  }
  public EditSettingbarber(data){
    return this.http.put(urlServer.ipServer + `UpdateSettingbarber`,data)
  }
  public EditSettingSalary(data){
    return this.http.put(urlServer.ipServer + `UpdateSettingSaraly`,data)
  }
}
