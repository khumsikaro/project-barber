import { Component, OnInit } from '@angular/core';
import { HolidayService } from './holiday.service';
import { NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { HolidayFormComponent } from './holiday-form/holiday-form.component';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-holiday',
  templateUrl: './holiday.component.html',
  styleUrls: ['./holiday.component.css']
})
export class HolidayComponent implements OnInit {
  HolidayListData: any[];

  constructor(private service: HolidayService,
    private modalService: NzModalService,
    private notificationService: NzNotificationService, ) { }

  ngOnInit() {
    this.GetHoliday()
    document.getElementById("content").classList.remove("p-0")
  }

  GetHoliday() {
    this.service.GetHolidayList().subscribe(
      (res) => {
        this.HolidayListData = res as any[];
      }
    )
  }
  deleteHoliday(data) {
    this.modalService.confirm({
      nzTitle: '<h5 class = "model-title">ยืนยันการลบ<h5>',
      nzContent: 'คุณต้องการลบ ' + data.vac_type + ' ใช่หรือไม่ ?',
      nzClosable: false,
      nzCancelText: "ยกเลิก",
      nzOkText: "ยืนยัน",
      nzOnOk: () => new Promise(resolev => setTimeout(resolev, 100))
        .then(() => {
          this.service.DeleteHoliday(data.leave_type_id).subscribe(
            (res) => {
              this.notificationService.create("success", "ลบข้อมูลสำเร็จ", null)
              this.GetHoliday();
            }
          )
        }
        )
    })
  }
  openModal(typeComponent: string, data?: any) {
    const modal = this.modalService.create({
      nzTitle: typeComponent === 'new' ? '<h5 class="modal-title">เพิ่มประเภทของการหยุด</h5>' : '<h5 class="modal-title">เเก้ไขประเภทของการหยุด<h5>',
      nzContent: HolidayFormComponent,
      nzComponentParams: {
        typeComponent: typeComponent,
        leave_type_id: isNullOrUndefined(data) ? null : data.leave_type_id,
        vac_type: isNullOrUndefined(data) ? null : data.vac_type,
        MonthMax: isNullOrUndefined(data) ? null : data.MonthMax,
        yearMax: isNullOrUndefined(data) ? null : data.yearMax,
        
      },
      nzClosable: true,
      nzWidth:700,
      nzFooter: [
        {
          label: 'ยกเลิก',
          shape: 'default',
          onClick: () => modal.destroy()
        },
        {
          label: 'บันทึก',
          type: 'primary',
          onClick:()=> new Promise(resolve => setTimeout(resolve, 0)).then(
            ()=>{
              const instance = modal.getContentComponent();
              return instance 
            }
          ).then((instance)=>{
            if(instance.MonthMax>instance.yearMax)
            this.notificationService.create("error","<p>จำนวนวันลาต่อเดือนต้องน้อยกว่าจำนวนวันลาต่อปี</p>", null)
            if(isNullOrUndefined(instance.vac_type))
            this.notificationService.create("error","<p>กรุณาใส่ชื่อประเภทการลา</p>", null)
            if(isNullOrUndefined(instance.MonthMax))
            this.notificationService.create("error","<p>กรุณาใส่จำนวนวันสูงสุดในการลาต่อเดือน</p>", null)
            if(isNullOrUndefined(instance.yearMax))
            this.notificationService.create("error","<p>กรุณาใส่จำนวนวันสูงสุดในการลาต่อปี</p>", null)
            if(!isNullOrUndefined(instance.vac_type)&&!isNullOrUndefined(instance.MonthMax)&&!isNullOrUndefined(instance.yearMax) && instance.MonthMax<=instance.yearMax){
              this.AddOrEdit(instance).subscribe(
                (res) => {
                 if (JSON.stringify(res)[11] === 's') {
                   this.notificationService.create("success", typeComponent === "new" ? "<p>เพิ่มข้อมูลสำเร็จ</p>" : "<p>แก้ไขข้อมูลสำเร็จ</p>", null)
                   modal.destroy();
                   this.GetHoliday();
                 }
                 if (JSON.stringify(res)[11] === 'e') {
                   this.notificationService.create("error","<p>ชื่อประเภทการวันหยุดนี้มีอยู่แล้ว</p>", null)
                 }
                }
              )
            }
          })
        }
      ]
    })
  }
  AddOrEdit(data){
    if (data.typeComponent === "new") {
      const value ={
        leave_type_id: data.leave_type_id,
        vac_type:data.vac_type,
        MonthMax: data.MonthMax,
        yearMax: data.yearMax,
          
        }
      return this.service.AddHoliday(value)
      
    }
    else {
      const value = {
        leave_type_id: data.leave_type_id,
        vac_type:data.vac_type,
        MonthMax: data.MonthMax,
        yearMax: data.yearMax,
        }
      return this.service.EditHoliday(value)
    }
  }
}
