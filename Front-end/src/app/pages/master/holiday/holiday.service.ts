import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {urlServer} from '../../../URL'

@Injectable({
  providedIn: 'root'
})
export class HolidayService {

  constructor(private http:HttpClient) { }
  GetHolidayList(){
    return this.http.get(urlServer.ipServer+`Getleveltype`)
  }
  
  EditHoliday(data){
    return this.http.put(urlServer.ipServer+`Upleveltype`,data)
  }
  DeleteHoliday(id:number){
    return this.http.delete(urlServer.ipServer+`DeleteHoliday/${id}`)
  }
  AddHoliday(data){
    return this.http.post(urlServer.ipServer+`AddHoliday`,data)
  }
}
