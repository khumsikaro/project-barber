import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-holiday-form',
  templateUrl: './holiday-form.component.html',
  styleUrls: ['./holiday-form.component.css']
})
export class HolidayFormComponent implements OnInit {
  @Input() typeComponent: string;
  @Input() leave_type_id: number;
  @Input() vac_type: string;
  @Input() MonthMax: number;
  @Input() yearMax: number;
  constructor() { }

  ngOnInit() {
    this.vac_type = this.typeComponent === "new" ? null : this.vac_type
    this.MonthMax = this.typeComponent === "new" ? null : this.MonthMax
    this.yearMax = this.typeComponent === "new" ? null : this.yearMax
  }

}
