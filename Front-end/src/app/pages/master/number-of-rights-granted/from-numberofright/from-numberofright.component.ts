import { Component, OnInit, Input } from '@angular/core';
import { NumberOfRightsGrantedService } from '../number-of-rights-granted.service';
import { TypeCheckCompiler } from '@angular/compiler/src/view_compiler/type_check_compiler';

@Component({
  selector: 'app-from-numberofright',
  templateUrl: './from-numberofright.component.html',
  styleUrls: ['./from-numberofright.component.css']
})
export class FromNumberofrightComponent implements OnInit {
  @Input() typeComponent: string;
  @Input() service_name: string;
  @Input() id: number;
  @Input() Max_point:number;
  GetShowListService:any[];
  

  constructor(private service:NumberOfRightsGrantedService) { }
  
  ShowListService() {
    
    this.service.GetShowListService().subscribe(
      (res: any[]) => {
        this.GetShowListService = res as any[]
       
      }
    )
  }
  ngOnInit() {
    this.ShowListService()
    this.id=this.typeComponent==="new" ? null:this.id
    this.service_name=this.typeComponent==="new" ? null:this.service_name
    this.Max_point=this.typeComponent==="new " ? null:this.Max_point
  }

}
