import { Component, OnInit } from '@angular/core';
import { NumberOfRightsGrantedService } from './number-of-rights-granted.service';
import { NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { InformationFromComponent } from '../information/information-from/information-from.component';
import { isNullOrUndefined } from 'util';
import { FromNumberofrightComponent } from './from-numberofright/from-numberofright.component';
import { Type } from '@angular/compiler/src/core';

@Component({
  selector: 'app-number-of-rights-granted',
  templateUrl: './number-of-rights-granted.component.html',
  styleUrls: ['./number-of-rights-granted.component.css']
})
export class NumberOfRightsGrantedComponent implements OnInit {
  ServiceSettingPointData:any;
  isLoading:boolean;
  ServiceName:string;

  constructor(private service:NumberOfRightsGrantedService,    
    private modalService: NzModalService,
    private notificationService: NzNotificationService,) { }

    GetSettingPonit(){
    this.isLoading = true;
    this.service.GetSettingPonit().subscribe(
      (res: any[])=>{
        this.ServiceSettingPointData=res as any[]
        this.isLoading=false

      }
    )
  }

  
  ngOnInit() {
    this.GetSettingPonit();
    document.getElementById("content").classList.remove("p-0")
  }
  openModal(typeComponent: string, data?: any) {
    const modal = this.modalService.create({
      nzTitle: typeComponent === 'new' ? 'เพิ่มบริการเเละเเต้ม' : 'เเก้ไขบริการเเละเเต้ม',
      nzContent: FromNumberofrightComponent,
      nzComponentParams: {
        typeComponent: typeComponent,
        id: isNullOrUndefined(data) ? null : data.id,
        service_name: isNullOrUndefined(data) ? null : data.service_name,
        Max_point: isNullOrUndefined(data) ? null : data.Max_point,
        
      },
      nzClosable: true,
      nzWidth:700,
      nzFooter: [
        {
          label: 'ยกเลิก',
          shape: 'default',
          onClick: () => modal.destroy()
        },
        {
          label: 'บันทึก',
          type: 'primary',
          onClick: () => new Promise(resolve => setTimeout(resolve, 0)).then(
            () => {
              const instance = modal.getContentComponent();
              return instance
            }
          ).then((instance) => {
            if(isNullOrUndefined(instance.service_name))
            this.notificationService.create("error","<p>กรุณาเลือกชื่อบริการ</p>", null)
            if(isNullOrUndefined(instance.Max_point))
            this.notificationService.create("error","<p>กรุณาใส่จำนวนแต้ม</p>", null)
            if(!isNullOrUndefined(instance.service_name) && !isNullOrUndefined(instance.Max_point)){
              this.AddOrEdit(instance).subscribe(
                 (res) => { 
                  if (JSON.stringify(res)[11] === 's') {
                    this.notificationService.create("success", typeComponent === "new" ? "เพิ่มข้อมูลสำเร็จ" : "แก้ไขข้อมูลสำเร็จ", null)
                    modal.destroy();
                    this.GetSettingPonit();
                  }
                  else if(JSON.stringify(res)[11] === 'e'){
                    this.notificationService.create("error","<p>ชื่อบริการนี้มีข้อมูลโปรโมชั่นอยู่แล้ว</p>", null)
                  }
                }
              )
            }
          })
        }
      ]
    })
  }
  AddOrEdit(data){
    if(data.typeComponent ==="new"){
      const value = {
        service_name:data.service_name,
        Max_point:data.Max_point,
      }
      return this.service.addSettingPoint(value)
    }
    else{
      const value ={
        id:data.id,
        service_name:data.service_name,
        Max_point:data.Max_point,
      }
      return this.service.EditSettingPoint(value)
    }
  }
  deteleSericr(data){
    this.modalService.confirm({
      nzTitle:'<h5 class = "model-title">ยืนยันการลบ<h5>',
      nzContent:'คุณต้องการลบโปรโมชั่นของบริการ '+data.service_name+' ใช่หรือไม่',
      nzClosable: false,
      nzCancelText:"ยกเลิก",
      nzOkText: "ยืนยัน",
      nzOnOk: () => new Promise(resolev => setTimeout(resolev, 100)).then(() => {
          this.service.DeleteSettingPoint(data.id).subscribe(
            (res) => {
              this.notificationService.create("success", "ลบข้อมูลสำเร็จ", null)
              this.GetSettingPonit()
            }
          )
        }
        )
    })
  }
}
