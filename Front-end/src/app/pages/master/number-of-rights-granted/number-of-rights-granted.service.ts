import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { urlServer } from '../../../URL';
@Injectable({
  providedIn: 'root'
})
export class NumberOfRightsGrantedService {

  constructor(private http:HttpClient) { }
  
  public GetSettingPonit(){
    return this.http.get(urlServer.ipServer + `Getpoint`);
  }
  public EditSettingPoint(data){
    return this.http.put(urlServer.ipServer + `Uppoint`,data);
  }
  public addSettingPoint(data){
    return this.http.post(urlServer.ipServer + `Addpoint `,data);
  }
  public GetShowListService() {
    return this.http.get(urlServer.ipServer + `ShowListService`);
  }
  public DeleteSettingPoint(id:number ){
    return this.http.delete(urlServer.ipServer+`Deletepoint/${id}`);
  }
}

