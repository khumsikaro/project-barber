import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { urlServer } from '../../../../URL'

@Component({
  selector: 'app-feedback-form',
  templateUrl: './feedback-form.component.html',
  styleUrls: ['./feedback-form.component.css']
})
export class FeedbackFormComponent implements OnInit {
  @Input() dateStart
  @Input() dateEnd
  @Input() Branch_id
  @Input() SeleteType
  @Input() data
  BranchFeedbackReportDetaildata;
  ServiceFeedbackReportDetaildata;
  BarberFeedbackReportDetaildata;
  constructor(private http: HttpClient, ) { }

  ngOnInit() {
    this.TableFrom(this.data)
  }


  TableFrom(data) {
    switch (this.SeleteType) {
      case 1: {
        this.http.get(urlServer.ipServer + `BranchFeedbackReportDetail/${this.dateStart}/${this.dateEnd}/${data.branch_id}`).subscribe((res) => {
          this.BranchFeedbackReportDetaildata = res as any
        })
        break;
      }
      case 2: {
        this.http.get(urlServer.ipServer + `ServiceFeedbackReportDetail/${this.dateStart}/${this.dateEnd}/${data.name}/${this.Branch_id}`).subscribe((res) => {
          this.ServiceFeedbackReportDetaildata = res as any
        })
        break;
      }
      case 3: {
        this.http.get(urlServer.ipServer + `BarberFeedbackReportDetail/${this.dateStart}/${this.dateEnd}/${data.username}/${this.Branch_id}`).subscribe((res) => {
          this.BarberFeedbackReportDetaildata = res as any
        })
        break;
      }
      default:
        break;
    }
  }

}
