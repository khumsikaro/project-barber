import { Component, OnInit } from '@angular/core';
import { urlServer } from '../../../URL'
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2';
import { NzModalService } from 'ng-zorro-antd';
import { FeedbackFormComponent } from './feedback-form/feedback-form.component';
import { LoginService } from '../../login/login.service';
import { SerBarofBranchComponent } from './ser-barof-branch/ser-barof-branch.component';
import { ServiceandBranchComponent } from './serviceand-branch/serviceand-branch.component';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {
  constructor(private http: HttpClient, private modalService: NzModalService, private session: LoginService, ) { }
  SeleteType: number = 1;
  USER;
  ngOnInit() {
    this.USER = this.session.getActiveUser();
    this.GetBranch()
    document.getElementById("content").classList.remove("p-0")
  }
  date;
  day = new Date
  today = new Date(this.day.getFullYear(), (this.day.getMonth()), this.day.getDay());
  Branch_id;
  Branchdata;
  FeedbackBranchdata
  FeedbackServicedata
  FeedbackBarberdata
  GetBranch() {
    this.http.get(urlServer.ipServer + `ShowListBranch`).subscribe((res) => { this.Branchdata = res as any })
  }

  SeleteTypeReport() {
    switch (this.SeleteType) {
      case 1:
        {
          let Branch = this.USER.levelUsers == 1 ? this.USER.branch_id : this.Branch_id
          if (this.date != undefined && this.date.length != 0) {
            let dateStart = this.date[0]
            let dateEnd = this.date[1]
            if (this.USER.levelUsers == 1) {
              this.http.get(urlServer.ipServer + `BranchFeedbackReport/${dateStart}/${dateEnd}`).subscribe((res: any[]) => {
                
                if (res.length == 0) {
                  Swal.fire({
                    html: '<p>ไม่มีข้อมูล!</p>',
                  })
                }
                else {
                  this.FeedbackBranchdata = res.filter(x => x.branch_id == Branch)
                  if (this.FeedbackBranchdata.length == 0) {
                    Swal.fire({
                      html: '<p>ไม่มีข้อมูล!</p>',
                    })
                  }
                }
              })
            }
            else {
              this.http.get(urlServer.ipServer + `BranchFeedbackReport/${dateStart}/${dateEnd}`).subscribe((res: any) => {
                if (res.length == 0) {
                  Swal.fire({
                    html: '<p>ไม่มีข้อมูล!</p>',
                  })
                }
                else {
                  this.FeedbackBranchdata = res as any
                }
              })
            }
          }
          else { Swal.fire({ html: '<p>ใส่ข้อมูลให้ครบก่อนค้นหา!</p>', }) } break;
        }

      case 2: {
        let Branch = this.USER.levelUsers == 1 ? this.USER.branch_id : this.Branch_id
        if (this.date != undefined && Branch != undefined) {
          let dateStart = this.date[0]
          let dateEnd = this.date[1]
          if (this.USER.levelUsers == 1) {
            this.http.get(urlServer.ipServer + `ServiceFeedbackReport/${dateStart}/${dateEnd}/${Branch}`).subscribe((res: any) => {
              if (res.length == 0) {
                Swal.fire({
                  html: '<p>ไม่มีข้อมูล!</p>',
                })
              }
              else {

                this.FeedbackServicedata = res as any
              }
            })
          }
          else {
            this.http.get(urlServer.ipServer + `ServiceFeedbackReport/${dateStart}/${dateEnd}/${Branch}`).subscribe((res: any) => {
              if (res.length == 0) {
                Swal.fire({
                  html: '<p>ไม่มีข้อมูล!</p>',
                })
              }
              else {
                this.FeedbackServicedata = res as any
              }
            })
          }
        }
        else { Swal.fire({ html: '<p>ใส่ข้อมูลให้ครบก่อนค้นหา!</p>', }) } break;
      }
      case 3: {
        let Branch = this.USER.levelUsers == 1 ? this.USER.branch_id : this.Branch_id
        if (this.date != undefined && Branch != undefined) {
          let dateStart = this.date[0]
          let dateEnd = this.date[1]
          if (this.USER.levelUsers == 1) {
            this.http.get(urlServer.ipServer + `BarberFeedbackReport/${dateStart}/${dateEnd}/${Branch}`).subscribe((res: any) => {
              if (res.length == 0) {
                Swal.fire({
                  html: '<p>ไม่มีข้อมูล!</p>',
                })
              }
              else {
                this.FeedbackBarberdata = res as any
              }
            })
          }
          else {
            this.http.get(urlServer.ipServer + `BarberFeedbackReport/${dateStart}/${dateEnd}/${Branch}`).subscribe((res: any) => {
              if (res.length == 0) {
                Swal.fire({
                  html: '<p>ไม่มีข้อมูล!</p>',
                })
              }
              else {
                this.FeedbackBarberdata = res as any
              }
            })
          }
        }
        else { Swal.fire({ html: '<p>ใส่ข้อมูลให้ครบก่อนค้นหา!</p>', }) } break;
      }
      default:
        break;
    }
  }
  Reset() {
    this.FeedbackBranchdata = []
    this.FeedbackServicedata = []
    this.FeedbackBarberdata = []
  }

  DetailFromReport(data, type) {
    let Branch = this.USER.levelUsers == 1 ? this.USER.branch_id : this.Branch_id
    let dateStart = this.date[0].getFullYear() + '-' + (this.date[0].getMonth() + 1) + '-' + this.date[0].getDate();
    let dateEnd = this.date[1].getFullYear() + '-' + (this.date[1].getMonth() + 1) + '-' + this.date[1].getDate();
    if (this.SeleteType == 1) {
      const modal = this.modalService.create({
        nzTitle: type === 'branch' ? '<h5 class="modal-title">รายละเอียดสาขา</h5>' : type === 'service' ? '<h5 class="modal-title">รายละเอียดบริการ</h5>' : '<h5 class="modal-title">รายละเอียดช่าง</h5>',
        nzWidth: 700,
        nzContent: FeedbackFormComponent,
        nzComponentParams: {
          dateStart: dateStart,
          dateEnd: dateEnd,
          Branch_id: Branch,
          SeleteType: this.SeleteType,
          data: data
        },
        nzClosable: true,
        nzFooter: null,
      })
    }
    else {
      const modal = this.modalService.create({
        nzTitle: type === 'branch' ? '<h5 class="modal-title">รายละเอียดสาขา</h5>' : type === 'service' ? '<h5 class="modal-title">รายละเอียดบริการ</h5>' : '<h5 class="modal-title">รายละเอียดช่าง</h5>',
        nzWidth: 700,
        nzContent: FeedbackFormComponent,
        nzComponentParams: {
          dateStart: dateStart,
          dateEnd: dateEnd,
          Branch_id: Branch,
          SeleteType: this.SeleteType,
          data: data
        },
        nzClosable: true,
        nzFooter: null,
      })
    }
  }
  DetailFromBarberServiceOfBranch(data, type) {
    let dateStart = this.date[0].getFullYear() + '-' + (this.date[0].getMonth() + 1) + '-' + this.date[0].getDate();
    let dateEnd = this.date[1].getFullYear() + '-' + (this.date[1].getMonth() + 1) + '-' + this.date[1].getDate();
    const modal = this.modalService.create({
      nzTitle: type === 'barber' ? '<h5 class="modal-title">รายละเอียดช่างต่อสาขา</h5>' : '<h5 class="modal-title">รายละเอียดบริการต่อสาขา</h5>',
      nzWidth: 700,
      nzContent: SerBarofBranchComponent,
      nzComponentParams: {
        dateStart: dateStart,
        dateEnd: dateEnd,
        data: data,
        type: type,
      },
      nzClosable: true,
      nzFooter: null,
    })
  }
  DetailFromBarberService(data, type) {
    let Branch = this.USER.levelUsers == 1 ? this.USER.branch_id : this.Branch_id
    let dateStart = this.date[0].getFullYear() + '-' + (this.date[0].getMonth() + 1) + '-' + this.date[0].getDate();
    let dateEnd = this.date[1].getFullYear() + '-' + (this.date[1].getMonth() + 1) + '-' + this.date[1].getDate();
    const modal = this.modalService.create({
      nzTitle: type === 'barber' ? '<h5 class="modal-title">รายละเอียดช่างต่อบริการ</h5>' : '<h5 class="modal-title">รายละเอียดบริการต่อช่าง</h5>',
      nzWidth: 700,
      nzContent: ServiceandBranchComponent,
      nzComponentParams: {
        dateStart: dateStart,
        dateEnd: dateEnd,
        data: data,
        type: type,
        Branch: Branch
      },
      nzClosable: true,
      nzFooter: null,
    })
  }
}
