import { Component, OnInit, Input } from '@angular/core';
import { urlServer } from '../../../../URL'
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-serviceand-branch',
  templateUrl: './serviceand-branch.component.html',
  styleUrls: ['./serviceand-branch.component.css']
})
export class ServiceandBranchComponent implements OnInit {
  @Input() dateStart;
  @Input() dateEnd;
  @Input() data;
  @Input() type;
  @Input() Branch;
  Servicedata
  Barberdata
  constructor(private http: HttpClient, ) { }

  ngOnInit() {
    if(this.type==="barber"){
      this.http.get(urlServer.ipServer + `ServiceFeedbackReportDetail/${this.dateStart}/${this.dateEnd}/${this.data.name}/${this.Branch}`).subscribe((res) => {
        this.Servicedata = res as any 
      })
    }
    else{
      
      this.http.get(urlServer.ipServer + `BarberFeedbackReportDetail/${this.dateStart}/${this.dateEnd}/${this.data.username}/${this.Branch}`).subscribe((res) => {
        this.Barberdata = res as any
      })
    }
  }
}
