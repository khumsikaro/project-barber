import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { urlServer } from '../../../../URL'
@Component({
  selector: 'app-ser-barof-branch',
  templateUrl: './ser-barof-branch.component.html',
  styleUrls: ['./ser-barof-branch.component.css']
})
export class SerBarofBranchComponent implements OnInit {
  @Input() dateStart;
  @Input() dateEnd;
  @Input() data;
  @Input() type;
  FeedbackServicedata;
  FeedbackBarberdata;
  constructor(private http: HttpClient, ) { }

  ngOnInit() {
    if(this.type==="service"){
      this.http.get(urlServer.ipServer + `ServiceFeedbackReport/${this.dateStart}/${this.dateEnd}/${this.data.branch_id}`).subscribe((res) => {
        this.FeedbackServicedata = res as any
      })
    }
    else{
      this.http.get(urlServer.ipServer + `BarberFeedbackReport/${this.dateStart}/${this.dateEnd}/${this.data.branch_id}`).subscribe((res: any) => {
        this.FeedbackBarberdata = res as any
      })
    }
  }

}
