import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-question-from',
  templateUrl: './question-from.component.html',
  styleUrls: ['./question-from.component.css']
})
export class QuestionFromComponent implements OnInit {
  @Input() answer: string;
  @Input() answer_dt: Date;
  @Input() email: string;
  @Input() in_id: number;
  @Input() name: string;
  @Input() phone: string;
  @Input() question: string;
  @Input() question_dt:Date;
  @Input() res_id: string;
  @Input() subject: string;
  @Input() title: string;
  @Input() Type :string;

  constructor() { }

  ngOnInit() {
  }

}
