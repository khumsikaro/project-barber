import { Component, OnInit } from '@angular/core';
import { QuestionService } from './question.service';
import { LoginService } from '../../login/login.service';
import { NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { QuestionFromComponent } from './question-from/question-from.component';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {
  QuestionList: any[];
  USER;

  constructor(private service: QuestionService,
    private session: LoginService,
    private modalService: NzModalService,
    private notificationService: NzNotificationService,) { }
  
  ngOnInit() {
    this.USER = this.session.getActiveUser();
    this.GetQuestion()
    document.getElementById("content").classList.remove("p-0")
  }
  GetQuestion() {
    this.service.GetQuestionList().subscribe(
      (res) => {
        this.QuestionList = res as any[];
      }
    )
  }
  openModal(data,Type) {
    const modal = this.modalService.create({
      nzTitle: '<h5 class="modal-title" >ฟอร์มการตอบกลับ</h5>',
      nzContent: QuestionFromComponent,
      nzWidth:700,
      nzComponentParams: {
        Type:Type,
        answer: isNullOrUndefined(data) ? null : data.answer,
        answer_dt: isNullOrUndefined(data) ? null : data.answer_dt,
        in_id: isNullOrUndefined(data) ? null : data.in_id,
        name: isNullOrUndefined(data) ? null : data.name,
        phone: isNullOrUndefined(data) ? null : data.phone,
        email: isNullOrUndefined(data) ? null : data.email,
        question: isNullOrUndefined(data) ? null : data.question,
        question_dt: isNullOrUndefined(data) ? null : data.question_dt,
        res_id: isNullOrUndefined(data) ? null : data.res_id,
        subject: isNullOrUndefined(data) ? null : data.subject,
        title: isNullOrUndefined(data) ? null : data.title,
      },
      nzClosable: true,
      nzFooter: [
        {
          label: 'ยกเลิก',
          shape: 'default',
          onClick: () => modal.destroy()
        },
        {
          label: 'บันทึก',
          type: 'primary',
          onClick: () => new Promise(resolve => setTimeout(resolve, 0)).then(
            () => {
              const instance = modal.getContentComponent();
              return instance
            } 
          ).then((instance) => {
            if(isNullOrUndefined(instance.answer)|| instance.answer.trim()==="")
            this.notificationService.create("error","<p>กรุณาใส่คำตอบ</p>", null)
            else if(!isNullOrUndefined(instance.answer)&& instance.answer.trim()!==""){
              this.UpdateQuestion(instance).subscribe(
                 (res) => {
                  var mailOptions = {
                    from:this.USER.Email,
                    to:instance.email,
                    subject:`ตอบคำถามหัวข้อ${instance.title} ร้าน Hair 2 Hair`,
                    text: `<h5>เรื่อง:</h5>${instance.subject} <br> ${instance.answer}`
                  };
                  this.service.SendEmail(mailOptions).subscribe(
                    (res)=>{}
                  )
                  if (JSON.stringify(res)[11] === 's') {
                    this.notificationService.create("success", "ตอบกลับสำเร็จ", null)
                    modal.destroy();
                    this.GetQuestion()
                  }
                }
              )
            }
          })
        },
      ]
    })
  }
  UpdateQuestion(data){
    const value={
      in_id:data.in_id,
      answer:data.answer,
      res_id:this.USER.username
    }
    return this.service.AddAnswerQuestion(value)  
  }
}
