import { Injectable } from '@angular/core';
import { urlServer } from '../../../URL'
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  constructor(private http: HttpClient) { }

  public GetQuestionList() {
    return this.http.get(urlServer.ipServer + `GetQuestion`)
  }
  public AddAnswerQuestion(data) {
    return this.http.put(urlServer.ipServer + `EditInformation`, data)
  }
  public SendEmail(data) {
    return this.http.post(urlServer.ipServer + `Email`,data)
  }
}
