import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { urlServer } from '../../../URL'
import Swal from 'sweetalert2';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';

@Component({
  selector: 'app-revenue',
  templateUrl: './revenue.component.html',
  styleUrls: ['./revenue.component.css']
})
export class RevenueComponent implements OnInit {
  day = new Date
  today = new Date(this.day.getFullYear(), (this.day.getMonth()), this.day.getDay());
  Revenuedata;
  Checklength
  constructor(private http: HttpClient ) { 
    
  }
  
  ngOnInit() {
    this.GetRevenue()
    document.getElementById("content").classList.remove("p-0")
  }
  GetRevenue() {
    let Month = (this.today.getMonth() + 1) + '-' + this.today.getFullYear();
    this.http.get(urlServer.ipServer + `GetMoneyToMonth/${Month}`).subscribe((res: any) => {
      this.Checklength=res.length
      if(res.length==0){
        Swal.fire({
          html: '<p>ไม่มีข้อมูล!</p>',
        })
      }
      else{
        this.Revenuedata = res as any
        let Branch = []; let money = [];
        for (let i = 0; i < res.length; i++) {
          Branch.push(res[i].Branch)
          money.push(res[i].MoneytoMonth)
        }
        this.pieChartLabels = Branch;
        this.pieChartData = money;
      }
    })
  }
  public pieChartLabels=['0','1','2','3','4','5','6','7','8','9','10'];
  public pieChartData=[0,1,2,3,4,5,6,7,8,9,10];
  public pieChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [pluginDataLabels];
}
