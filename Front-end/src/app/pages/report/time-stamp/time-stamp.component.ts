import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {urlServer} from '../../../URL'
import Swal from 'sweetalert2';
import { NzModalService } from 'ng-zorro-antd';
import { DetailTimestempFormComponent } from './detail-timestemp-form/detail-timestemp-form.component';
@Component({
  selector: 'app-time-stamp',
  templateUrl: './time-stamp.component.html',
  styleUrls: ['./time-stamp.component.css']
})
export class TimeStampComponent implements OnInit {
  day = new Date
  today = new Date(this.day.getFullYear(), (this.day.getMonth()), this.day.getDate());
  Branch_id;
  Branchdata;
  TimeStampBarber
  constructor(private http:HttpClient,private modalService: NzModalService) { }
  ngOnInit() {
    this.GetBranch()
    document.getElementById("content").classList.remove("p-0")
  }
  GetBranch(){
    this.http.get(urlServer.ipServer + `ShowListBranch`).subscribe((res)=>{this.Branchdata =res as any})
  }
  GetTimeStampBarberByBranch(){
    if(this.Branch_id!=undefined){
      let Month = (this.today.getMonth() + 1) + '-' + this.today.getFullYear();
      this.http.get(urlServer.ipServer + `GetTimeStampBarberByBranchReport/${Month}/${this.Branch_id}`).subscribe((res:any)=>{
        
        
        if(res.length==0){
          Swal.fire({
            html: '<p>ไม่มีข้อมูล!</p>',
          })
        }
        else{
          this.TimeStampBarber =res[0] as any
        }
      })
    }
    else{ 
      Swal.fire({
        html: '<p>ใส่ข้อมูลให้ครบก่อนค้นหา!</p>',
      })
    }
  }
  DetailTimeStemp(data,Type){
    const modal = this.modalService.create({
      nzTitle: Type==="late" ? '<h5 class="modal-title">รายละเอียดการสาย</h5>':'<h5 class="modal-title">รายละเอียดการหยุด</h5>',
      nzWidth: 700,
      nzContent: DetailTimestempFormComponent,
      nzComponentParams: {
        Type: Type,
        Username:data.username,
        Month:this.today
      },
      nzClosable: true,
      nzFooter: null,
    })
  }
Reset(){
  this.TimeStampBarber=[]
}
}
