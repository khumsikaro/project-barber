import { Component, OnInit, Input } from '@angular/core';
import {urlServer} from '../../../../URL'
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-detail-timestemp-form',
  templateUrl: './detail-timestemp-form.component.html',
  styleUrls: ['./detail-timestemp-form.component.css']
})
export class DetailTimestempFormComponent implements OnInit {
 @Input () Type
 @Input ()  Username
 @Input ()  Month
 latedata
 Stopdata
  constructor(private http:HttpClient) { }

  ngOnInit() {
    if(this.Type==="late"){
      this.http.get(urlServer.ipServer+`GetLate/${this.Month}/${this.Username}`).subscribe((res)=>{
        this.latedata = res as any
      })
    }
    else{
      this.http.get(urlServer.ipServer+`GetLeavesDetail/${this.Month}/${this.Username}`).subscribe((res)=>{
        this.Stopdata = res as any
      })
    }
  }

}
