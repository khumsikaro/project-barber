import { Component, OnInit } from '@angular/core';
import { SalaryBarberService } from './salary-barber.service';
import Swal from 'sweetalert2';
import * as jsPDF from 'jspdf'
import html2canvas from 'html2canvas';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-salary-barber',
  templateUrl: './salary-barber.component.html',
  styleUrls: ['./salary-barber.component.css']
})
export class SalaryBarberComponent implements OnInit {
  today = new Date();
  MonthFrom = (this.today.getMonth() + 1) + " ปี " + this.today.getFullYear();
  Branch_id
  Branchdata;
  GetSalaryBarberdata;ArrName; SUBJECTdata;GetSumper;
  Getsalary;revenue;late;absence;chemical; Socialsecurity;
  rent;equipment; DeductInsurance; balance;JobInsurance;
  time = this.today.getHours() + ":" + this.today.getMinutes() + ":" + this.today.getSeconds();
  date = this.today.getDate() + '-' + (this.today.getMonth() + 1) + '-' + this.today.getFullYear();
  constructor(private service: SalaryBarberService) { }

  ngOnInit() {
    this.GetBranch()
    document.getElementById("content").classList.remove("p-0")
    // this.GetSalary()
  }
  GetBranch() {
    this.service.GetBranch().subscribe((res) => { this.Branchdata = res as any })
  }
  SeleterBranch(BranchName) {
  }
  SeleterMonth(month) {
    this.MonthFrom = (month.getMonth() + 1) + " ปี " + month.getFullYear()
  }
  calculate(res) {
    let Input = res; let cal = []; for (let i in Input) { cal.push(Input[i]) }
    let Ans1 = cal; let Ans2 = []; for (let i = 1; i < Ans1.length; i++) { Ans2.push(Ans1[i]) }
    let LastAns = Ans2
    return LastAns
  }
  Changenumber(input1, input2) {
    let A = input1; let B = input2;
    A.shift(); let ANS = A.map(value => parseInt(value).toFixed(0));
    let RES= ANS.map(value => parseInt(value))
    let RES2 = RES.map(value => value.toLocaleString('en-us', { minimumFractionDigits: 0 }))
    RES2.unshift(B); 
    return RES2

  }
  GetSalary() {
    if (isNullOrUndefined(this.Branch_id)) {
      Swal.fire({
        html: '<p>ใส่ข้อมูลให้ครบ!</p>',
      })
    }
    else {
      let date = (this.today.getMonth() + 1) + '-' + this.today.getFullYear();
      this.service.GetSalaryBarber(date, this.Branch_id).subscribe((res: any) => {
        if (res.length == 0) {
          Swal.fire({
            html: '<p>ไม่มีข้อมูล!</p>',
          })
        }
        else {
          this.ArrName = Object.keys(res[0][1]).splice(2,Object.keys(res[0][1]).length)
          let arrBarberdata = [];
          for (let i in res[0]) {
            if (res[0][i].SUBJECT !== "รวม%") {
              arrBarberdata.push(this.calculate(res[0][i]))
            }
            else break;
          }
          this.GetSalaryBarberdata = arrBarberdata
          ////////////รวม%////////////////////////// ไม่เอา.00
          let arrSumdata1 = [];
          for (let i in res[0]) {
            if (res[0][i].SUBJECT == "รวม%") {
              arrSumdata1.push(this.calculate(res[0][i]))
            }
            else continue;
          }
          this.GetSumper = arrSumdata1;
          // this.GetSumper = [this.Changenumber(arrSumdata1[0],arrSumdata1[0][0])];
          ////////////เงินเดือน/////////////////////
          let arrSumdata2 = [];
          for (let i in res[0]) {
            if (res[0][i].SUBJECT == "เงินเดือน") {
              arrSumdata2.push(this.calculate(res[0][i]))
            }
            else continue;
          }
          this.Getsalary = arrSumdata2;
          // this.Getsalary = [this.Changenumber(arrSumdata2[0],arrSumdata2[0][0])];
          ////////////รายรับ///////////////////////ไม่เอา.00
          let arrSumdata3 = [];
          for (let i in res[0]) {
            if (res[0][i].SUBJECT == "รายรับ") {
              arrSumdata3.push(this.calculate(res[0][i]))
            }
            else continue;
          }
          this.revenue = arrSumdata3;
          // this.revenue = [this.Changenumber(arrSumdata3[0],arrSumdata3[0][0])];
          ////////////สาย/////////////////////////
          let arrSumdata4 = [];
          for (let i in res[0]) {
            if (res[0][i].SUBJECT == "สาย") {
              arrSumdata4.push(this.calculate(res[0][i]))
            }
            else continue;
          }
          this.late = arrSumdata4;
          ////////////ขาด/////////////////////////
          let arrSumdata5 = [];
          for (let i in res[0]) {
            if (res[0][i].SUBJECT == "ขาด") {
              arrSumdata5.push(this.calculate(res[0][i]))
            }
            else continue;
          }
          this.absence = arrSumdata5;
          ////////////เคมี/////////////////////////
          let arrSumdata6 = [];
          for (let i in res[0]) {
            if (res[0][i].SUBJECT == "เคมี") {
              arrSumdata6.push(this.calculate(res[0][i]))
            }
            else continue;
          }
          this.chemical = arrSumdata6;
          ////////////ประกันสังคม///////////////////
          let arrSumdata7 = [];
          for (let i in res[0]) {
            if (res[0][i].SUBJECT == "ประกันสังคม") {
              arrSumdata7.push(this.calculate(res[0][i]))
            }
            else continue;
          }
          this.Socialsecurity = arrSumdata7;
          ////////////ค่าห้อง///////////////////////
          let arrSumdata8 = [];
          for (let i in res[0]) {
            if (res[0][i].SUBJECT == "ค่าห้อง") {
              arrSumdata8.push(this.calculate(res[0][i]))
            }
            else continue;
          }
          this.rent = arrSumdata8;
          ////////////อุปกรณ์///////////////////////
          let arrSumdata9 = [];
          for (let i in res[0]) {
            if (res[0][i].SUBJECT == "อุปกรณ์") {
              arrSumdata9.push(this.calculate(res[0][i]))
            }
            else continue;
          }
          this.equipment = arrSumdata9;
          ////////////หักประกัน/////////////////////
          let arrSumdata10 = [];
          for (let i in res[0]) {
            if (res[0][i].SUBJECT == "หักประกัน") {
              arrSumdata10.push(this.calculate(res[0][i]))
            }
            else continue;
          }
          this.DeductInsurance = arrSumdata10;
          ////////////เงินสุทธิ//////////////////////ไม่เอา.00
          let arrSumdata11 = [];
          for (let i in res[0]) {
            if (res[0][i].SUBJECT == "เงินสุทธิ") {
              arrSumdata11.push(this.calculate(res[0][i]))
            }
            else continue;
          }
          this.balance = arrSumdata11;
          // this.balance = [this.Changenumber(arrSumdata11[0],arrSumdata11[0][0])];
          ////////////ประกันงาน////////////////////
          let arrSumdata12 = [];
          for (let i in res[0]) {
            if (res[0][i].SUBJECT == "ประกันงาน") {
              arrSumdata12.push(this.calculate(res[0][i]))
            }
            else continue;
          }
          this.JobInsurance = arrSumdata12;
        }
      })
    }
  }

  public demoFromHTML() {
    var data = document.getElementById('TableSalary');
    html2canvas(data).then(canvas => {
      // Few necessary setting options  
      var imgWidth = 300;
      var pageHeight = 295;
      var imgHeight = canvas.height * imgWidth / canvas.width;
      var heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL('image/PNG', 0.1)
      let pdf = new jsPDF('p', 'mm', 'a2'); // A4 size page of PDF  
      var position =10;
      pdf.addImage(contentDataURL, 'PNG', 60, position, imgWidth, imgHeight, undefined, 'FAST')
      pdf.save("Test"); // Generated PDF   
    });
  }
}
