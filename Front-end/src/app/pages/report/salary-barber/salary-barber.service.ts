import { Injectable } from '@angular/core';
import {urlServer} from '../../../URL'
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SalaryBarberService {

  constructor(private http:HttpClient) { }
  public GetSalaryBarber(date,branch){
    return this.http.get(urlServer.ipServer+`GetSalaryBarber/${date}/${branch}`)
  }
  public GetBranch() {
    return this.http.get(urlServer.ipServer + `ShowListBranch`)
  }
}
