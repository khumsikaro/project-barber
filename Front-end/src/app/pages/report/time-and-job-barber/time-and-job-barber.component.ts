import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { urlServer } from '../../../URL'
import Swal from 'sweetalert2';

@Component({
  selector: 'app-time-and-job-barber',
  templateUrl: './time-and-job-barber.component.html',
  styleUrls: ['./time-and-job-barber.component.css']
})
export class TimeAndJobBarberComponent implements OnInit {
  today = new Date;
  JobBarberdata;
  Branch_id;
  Branchdata;
  totalmoney;
  Barberdata ;
  username;
  Barberdisabled = true;
  Check=false;
  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.GetBranch()
    document.getElementById("content").classList.remove("p-0")
    
  }
formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  }

  TimeAndJobBarber() {
    let date = this.today
    if(this.Branch_id==undefined){
      Swal.fire({
        html: '<p>กรุณาใส่ข้อมูลสาขาและช่าง!</p>',
      })
    }
    else if(this.username==undefined){
      Swal.fire({
        html: '<p>กรุณาใส่ข้อมูลช่าง!</p>',
      })
    }
    else if(this.Branch_id!=undefined && this.username!=undefined ){
      this.http.get(urlServer.ipServer + `TimeAndJobBarber/${date}/${this.username}`).subscribe((res:any) => {
        if(res.length==0){
          Swal.fire({
            html: '<p>ไม่มีข้อมูล!</p>',
          })
        }
        else{
          this.Check=true
            this.JobBarberdata = res as any;
            let Ans = 0; for (let i = 0; i < this.JobBarberdata.length; i++) { Ans += this.JobBarberdata[i].money }
            this.totalmoney = this.formatNumber(Ans) 
        }
      })
    }
  }
  GetBranch() {
    this.http.get(urlServer.ipServer + `ShowListBranch`).subscribe((res) => { this.Branchdata = res as any })
  }

  GetBarber(Branch_id) {
    this.http.get(urlServer.ipServer + `ShowListBarberbranch/${Branch_id}`).subscribe((res) => {
      this.username=null
      this.Barberdata = res
      this.Barberdisabled = false;
    })
  }

}
