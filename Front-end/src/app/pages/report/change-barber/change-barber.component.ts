import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { urlServer } from '../../../URL'
import Swal from 'sweetalert2';
@Component({
  selector: 'app-change-barber',
  templateUrl: './change-barber.component.html',
  styleUrls: ['./change-barber.component.css']
})
export class ChangeBarberComponent implements OnInit {
  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.GetBranch()
    document.getElementById("content").classList.remove("p-0")
  }
  day = new Date
  today = new Date(this.day.getFullYear(), (this.day.getMonth()), this.day.getDay());
  Branch_id;
  Branchdata;
  ChangeBarberReportdata

  GetBranch() {
    this.http.get(urlServer.ipServer + `ShowListBranch`).subscribe((res) => { this.Branchdata = res as any })
  }
  GetChangeBarberReport() {
    if (this.Branch_id != undefined) {
      let Month = (this.today.getMonth() + 1) + '-' + this.today.getFullYear();
      this.http.get(urlServer.ipServer + `GetChangeBarberReport/${Month}/${this.Branch_id}`).subscribe((res:any) => {
        if (res.length == 0) {
          Swal.fire({
            html: '<p>ไม่มีข้อมูล!</p>',
          })
        }
        else{
          this.ChangeBarberReportdata = res as any
        }
      })
    }
    else{
      Swal.fire({
        html: '<p>ใส่ข้อมูลก่อนค้นหา!</p>',
      })
    }
  }

}
