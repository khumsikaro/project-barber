import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportRoutingModule } from './report-routing.module';
import { CancelServiceComponent } from './cancel-service/cancel-service.component';
import { TimeAndJobBarberComponent } from './time-and-job-barber/time-and-job-barber.component';
import { RevenueComponent } from './revenue/revenue.component';
import { SalaryBarberComponent } from './salary-barber/salary-barber.component';
import { TimeStampComponent } from './time-stamp/time-stamp.component';
import { ChangeBarberComponent } from './change-barber/change-barber.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { QuestionComponent } from './question/question.component';
import { ReportComponent } from './report.component';
import { ZorroModule } from 'src/app/lib.ngZorro';
import { QuestionFromComponent } from './question/question-from/question-from.component';
import { FormsModule } from '@angular/forms';
import { GuardService } from '../login/guard.service';
import { ChartsModule } from 'ng2-charts';
import { FeedbackFormComponent } from './feedback/feedback-form/feedback-form.component';
import { QueueGuardsService } from '../queue/queue-guards.service';
import { MasterGuardsService } from '../master/master-guards.service';
import { ReportGuardsService } from './report-guards.service';
import { DetailCancelFormComponent } from './cancel-service/detail-cancel-form/detail-cancel-form.component';
import { DetailTimestempFormComponent } from './time-stamp/detail-timestemp-form/detail-timestemp-form.component';
import { SerBarofBranchComponent } from './feedback/ser-barof-branch/ser-barof-branch.component';
import { ServiceandBranchComponent } from './feedback/serviceand-branch/serviceand-branch.component';
import { QdetailFormComponent } from './cancel-service/detail-cancel-form/qdetail-form/qdetail-form.component';


@NgModule({
  declarations: [CancelServiceComponent, TimeAndJobBarberComponent, RevenueComponent, SalaryBarberComponent, TimeStampComponent, ChangeBarberComponent, FeedbackComponent, QuestionComponent, ReportComponent, QuestionFromComponent, FeedbackFormComponent, DetailCancelFormComponent, DetailTimestempFormComponent, SerBarofBranchComponent, ServiceandBranchComponent, QdetailFormComponent],
  imports: [
    CommonModule,
    ReportRoutingModule,
    ZorroModule,
    FormsModule,
    ChartsModule,
  ],
  entryComponents: [
    QuestionFromComponent,
    FeedbackFormComponent,
    DetailCancelFormComponent,
    DetailTimestempFormComponent,
    SerBarofBranchComponent,
    ServiceandBranchComponent,
    QdetailFormComponent
  ],
  providers: [
    GuardService,
    QueueGuardsService,
    MasterGuardsService,
    ReportGuardsService
  ]
})
export class ReportModule { }
