import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportComponent } from './report.component';
import { CancelServiceComponent } from './cancel-service/cancel-service.component';
import { ChangeBarberComponent } from './change-barber/change-barber.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { QuestionComponent } from './question/question.component';
import { RevenueComponent } from './revenue/revenue.component';
import { SalaryBarberComponent } from './salary-barber/salary-barber.component';
import { TimeAndJobBarberComponent } from './time-and-job-barber/time-and-job-barber.component';
import { TimeStampComponent } from './time-stamp/time-stamp.component';
import { GuardService } from '../login/guard.service';
import { MasterGuardsService } from '../master/master-guards.service';
import { ReportGuardsService } from './report-guards.service';


const routes: Routes = [
  {
    path:'Report',
    component:ReportComponent,
    children:[
    {
      path:'CancelService',
      component:CancelServiceComponent,
      canActivate:[MasterGuardsService],
    },
    {
      path:'ChangeBarber',
      component:ChangeBarberComponent,
      canActivate:[MasterGuardsService],
    },
    {
      path:'Feedback',
      component:FeedbackComponent,
      canActivate:[ReportGuardsService],
    },
    {
      path:'Question',
      component:QuestionComponent,
      canActivate:[MasterGuardsService],
    },
    {
      path:'Revenue',
      component:RevenueComponent,
      canActivate:[MasterGuardsService],
    },
    {
      path:'SalaryBarber',
      component:SalaryBarberComponent,
      canActivate:[MasterGuardsService],
    },
    {
      path:'TimeAndJobBarber',
      component:TimeAndJobBarberComponent,
      canActivate:[MasterGuardsService],
    },
    {
      path:'TimeStamp',
      component:TimeStampComponent,
      canActivate:[MasterGuardsService],
    },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportRoutingModule { }
