import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { urlServer } from '../../../../../URL'
@Component({
  selector: 'app-qdetail-form',
  templateUrl: './qdetail-form.component.html',
  styleUrls: ['./qdetail-form.component.css']
})
export class QdetailFormComponent implements OnInit {
@Input() id;
listdata:any
  constructor(private http:HttpClient) { }

  ngOnInit() {
    this.http.get(urlServer.ipServer+`GetQueueDetail/${this.id}`).subscribe((res)=>{
      this.listdata =res as any;
    })
  }

}
