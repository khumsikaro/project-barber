import { Component, OnInit, Input } from '@angular/core';
import { urlServer } from '../../../../URL'
import { HttpClient } from '@angular/common/http';
import { NzModalService } from 'ng-zorro-antd';
import { QdetailFormComponent } from './qdetail-form/qdetail-form.component';
@Component({
  selector: 'app-detail-cancel-form',
  templateUrl: './detail-cancel-form.component.html',
  styleUrls: ['./detail-cancel-form.component.css']
})
export class DetailCancelFormComponent implements OnInit {
  @Input() Year;
  @Input() Username;
  Detaildata
  constructor(private http: HttpClient,private modalService: NzModalService, ) { }

  ngOnInit() {
    this.http.get(urlServer.ipServer + `CancelServiceDetail/${this.Year}/${this.Username}`).subscribe((res) => {
      this.Detaildata = res as any;
    })
  }

  Detail(data) {
    const modal = this.modalService.create({
      nzTitle: 'รายละเอียดคิว',
      // nzWidth: 700,
      nzContent: QdetailFormComponent,
      nzComponentParams: {
        id: data.queue_id,
      },
      nzClosable: true,
      nzFooter: null,
    })
    
  }
}
