import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { urlServer } from '../../../URL'
import { NzModalService } from 'ng-zorro-antd';
import { LoginService } from '../../login/login.service';
import { DetailCancelFormComponent } from './detail-cancel-form/detail-cancel-form.component';
import Swal from 'sweetalert2';
import { isNullOrUndefined } from 'util';
@Component({
  selector: 'app-cancel-service',
  templateUrl: './cancel-service.component.html',
  styleUrls: ['./cancel-service.component.css']
})
export class CancelServiceComponent implements OnInit {
  today = new Date
  Branch_id=null;
  CancelServicedata: any=[];
  Branchdata;
  constructor(private http: HttpClient,private modalService: NzModalService, private session: LoginService, ) { }
  ngOnInit() {
    this.GetBranch()
    // this.CancelService()
    document.getElementById("content").classList.remove("p-0")
  }
Reset(){
  this.CancelServicedata=[]
}


  CancelService() {
    let Year = this.today.getFullYear()
    if(!isNullOrUndefined(this.Branch_id)){
      this.http.get(urlServer.ipServer + `CancelService/${Year}/${this.Branch_id}`).subscribe((res:any) => {
        if(res.length==0){
          Swal.fire({
            html: '<p>ไม่มีข้อมูล!</p>',
          })
        }
        else{
          this.CancelServicedata = res as any;
        }
      })
    }
    else{
      Swal.fire({
        html: '<p>กรุณาเลือกสาขาก่อนค้นหา!</p>',
      })
    }
  }
  GetBranch() {
    this.http.get(urlServer.ipServer + `ShowListBranch`).subscribe((res) => { this.Branchdata = res as any })
  }
DetailFrom(data){
let Year = this.today.getFullYear()
const modal = this.modalService.create({
  nzTitle: 'รายละเอียดจำนวนการยกเลิก',
  // nzWidth: 700,
  nzContent: DetailCancelFormComponent,
  nzComponentParams: {
    Year: Year,
    Username:data.username
  },
  nzClosable: true,
  nzFooter: null,
})

}

}
