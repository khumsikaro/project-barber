import { Injectable } from '@angular/core';
import { urlServer } from 'src/app/URL';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HistroryServiceService {

  constructor(private http:HttpClient) {}

  GetHistory(username){
    return this.http.get(urlServer.ipServer + `HistroryList/${username}`)
  }
  DeteailHistory(queue_id:number){
    return this.http.get(urlServer.ipServer+`HistroryListDetail/${queue_id}`)
  }
  public Point(username){
    return this.http.get(urlServer.ipServer + `PointDetailUser/${username}`)
  }
  public SearchHistoryCustomer(username,date){
    return this.http.get(urlServer.ipServer + `SearchHistoryCustomer/${username}/${date}`)
  }
  
}