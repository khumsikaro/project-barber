import { Component, OnInit } from '@angular/core';
import { HistroryServiceService } from './histrory-service.service';
import { LoginService } from '../../login/login.service';
import { months } from 'moment';
import { ThunderboltFill } from '@ant-design/icons-angular/icons/public_api';
import { NzModalService } from 'ng-zorro-antd';
import { DetailHistoryComponent } from './detail-history/detail-history.component';
import { isNullOrUndefined } from 'util';


@Component({
  selector: 'app-history-service',
  templateUrl: './history-service.component.html',
  styleUrls: ['./history-service.component.css']
})
export class HistoryServiceComponent implements OnInit {
  HistroryList: any[];
  today = new Date();
  date = (this.today.getMonth() + 1) + '-' + this.today.getFullYear();
  USER;


  constructor(private service: HistroryServiceService,
    private session: LoginService,
    private modalService: NzModalService
  ) { }

  ngOnInit() {
    this.USER = this.session.getActiveUser();
    this.GetHistoryList();
    this.GetPointCus()
    document.getElementById("content").classList.remove("p-0")
  }
  Shdate;
  GetHistoryList() {
    this.service.GetHistory(this.USER.username).subscribe(
      (res) => {
        this.HistroryList = res as any[];
      }
      )
      this.Shdate = null
  }
  // numberWithCommas(x) {
  //     return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  // }
  openModal(data?: any) {
    const modal = this.modalService.create({
      nzTitle: 'รายละเอียดบริการ',
      nzWidth: 1200,
      nzContent: DetailHistoryComponent,
      nzComponentParams: {
        total_money: isNullOrUndefined(data) ? null : data.total_money,
        queue_id: isNullOrUndefined(data) ? null : data.queue_id,

      },
      nzClosable: true,
      nzFooter: null,
    })
  }
  Pointdata
  GetPointCus() {
    this.service.Point(this.USER.username).subscribe((res) => {
      this.Pointdata = res as any
    })
  }
  onChange(data) {
    let date = data.getFullYear() + "-" + (data.getMonth() + 1) + '-' +  data.getDate() 
    this.service.SearchHistoryCustomer(this.USER.username, date).subscribe(
      (res: any[]) => {
        this.HistroryList = res as any;
      }
    )
  }
}
