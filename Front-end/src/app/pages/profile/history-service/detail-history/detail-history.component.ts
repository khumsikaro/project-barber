import { Component, OnInit, Input } from '@angular/core';
import { HistroryServiceService } from '../histrory-service.service';

@Component({
  selector: 'app-detail-history',
  templateUrl: './detail-history.component.html',
  styleUrls: ['./detail-history.component.css']
})
export class DetailHistoryComponent implements OnInit {
  DetailHistory;
  @Input() queue_id:number;
  @Input() total_money:number;   

  constructor(private service:HistroryServiceService,) { }

  ngOnInit() {
    this.GetHistoryDetail()
  }

  GetHistoryDetail(){
    this.service.DeteailHistory(this.queue_id).subscribe(
      (res) =>{
        this.DetailHistory = res as any;
      }
    )
  }
}
