import { Component, OnInit, Input } from '@angular/core';
import { StatusQueueService } from '../status-queue.service';
import { LoginService } from 'src/app/pages/login/login.service';

@Component({
  selector: 'app-detail-status-queue-form',
  templateUrl: './detail-status-queue-form.component.html',
  styleUrls: ['./detail-status-queue-form.component.css']
})
export class DetailStatusQueueFormComponent implements OnInit {
  @Input() queue_id:number;

  QueueDetaildata;
  constructor(private service : StatusQueueService,
    private session: LoginService) { }

  ngOnInit() {
    this.GetQueueuDetail()
  }
  GetQueueuDetail() {
    this.service.GetQueueDetail(this.queue_id).subscribe(
      (res) => {
        this.QueueDetaildata = res as any;
      }
    )
  }

}
