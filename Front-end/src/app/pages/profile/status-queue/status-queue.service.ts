import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {urlServer} from '../../../URL'

@Injectable({
  providedIn: 'root'
})
export class StatusQueueService {

  constructor(private http:HttpClient) { }

  public GetQueueDetail(id){
    return this.http.get(urlServer.ipServer+`GetQueueDetail/${id}`)
  }
  public GetStatusQueueUser(username,date){
    return this.http.get(urlServer.ipServer+`GetStatusQueueUser/${username}/${date}`)
  }
  public  searchStatusQueueUser(username,date){
    return this.http.get(urlServer.ipServer + `searchStatusQueueUser/?username=${username}&date=${date}`)
  }
  public CancelQueue(data){
    return this.http.put(urlServer.ipServer+`CancelQueue`,data)
  }

}
