import { Component, OnInit } from '@angular/core';
import { StatusQueueService } from './status-queue.service';
import { LoginService } from '../../login/login.service';
import { NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { isNullOrUndefined } from 'util';
import { DetailStatusQueueFormComponent } from './detail-status-queue-form/detail-status-queue-form.component';
import Swal from 'sweetalert2'
import { differenceInCalendarDays } from 'date-fns';
import { PagesComponent } from '../../pages.component';
@Component({
  selector: 'app-status-queue',
  templateUrl: './status-queue.component.html',
  styleUrls: ['./status-queue.component.css']
})
export class StatusQueueComponent implements OnInit {
  USER;
  StatusQueueUserdata;
  today = new Date();
  constructor(
    private service: StatusQueueService,
    private session: LoginService,
    private modalService: NzModalService,
    private notificationService: NzNotificationService,
    private PageBadge: PagesComponent, ) { }

  ngOnInit() {
    this.USER = this.session.getActiveUser();
    this.GetStatusQueueUser()
    document.getElementById("content").classList.remove("p-0")
  }
  disabledDate = (current: Date): boolean => {
    let today = new Date
    // Can not select days before today and today
    return differenceInCalendarDays(current, today) < 0;
  };
  Length
  GetStatusQueueUser() {
    this.PageBadge.ngOnInit()
    this.service.GetStatusQueueUser(this.USER.username,this.today).subscribe(
      (res: any) => {
        // this.Length= res.Length
          this.StatusQueueUserdata = res as any;
        if (res.length == 0) {
          Swal.fire({
            title: '<strong>ยังไม่มีการจองในวันนี้</strong>',
            html:
              'ท่านต้องการจองคิวหรือไม่?',
            showCloseButton: true,
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText:
              '<a style="color: white" href="Queue/TableQueue" >จองคิว</a>',
            confirmButtonAriaLabel: 'Thumbs up, great!',
            cancelButtonText:
              '<a >ไม่จอง</a>',
          }).then((result) => {
            if (result.value==false) {
              this.GetStatusQueueUser()
            }
          })
        }
      }
    )
  }
  searchQueueUser(date: Date): void {
    this.GetStatusQueueUser()
    this.service.searchStatusQueueUser(this.USER.username,date).subscribe(
      (res: any) => {
        if (res.length != 0) {
          this.StatusQueueUserdata = res as any;
        }
        else {
          Swal.fire({
            title: 'ยังไม่มีการจองในวันดังกล่าว',
            html: '<p>กรุณาทำการจองคิวก่อน!</p>',
          })
        }
      }
    )
  }
  CancelQueue(data) {
    Swal.fire({
      title: 'คุณต้องการยกเลิกการจองหรือไม่?',
      html: `<p>หมายเหตุ</p><p style="color: tomato">การยกเลิกการจองคิวบ่อยเกินไปอาจทำให้คุณถูกยกเลิกการเป็นสมาชิกได้!!!</p>
      `,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: '<p>ต้องการ!</p>',
      cancelButtonText:'<p>ไม่ต้องการ</p>'
    }).then((result) => {
      if (result.value) {
        let Q = { queue_id: data.queue_id, cancel_queue:1,};
        this.service.CancelQueue(Q).subscribe((res) => {
          Swal.fire({
            html: '<p>ทำการยกเลิกการจองคิวสำเร็จ!</p>',
          }).then((result) => {
            this.GetStatusQueueUser()
          })
        })
      }
    })
  }
  OpenDetail(data?: any) {
    const modal = this.modalService.create({
      nzTitle: '<h5 class="modal-title">รายละเอียด</h5>',
      nzWidth: 700,
      nzContent: DetailStatusQueueFormComponent,
      nzComponentParams: {
        queue_id: isNullOrUndefined(data) ? null : data.queue_id,
      },
      nzClosable: true,
      nzFooter: null,
    })
  }


}
