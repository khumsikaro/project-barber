import { Injectable } from '@angular/core';
import {urlServer} from '../../../URL'
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class JobQueueBarberService {

  constructor(private http:HttpClient) { }
  public CheckQueuePrivateBarber(username,date){
    return this.http.get(urlServer.ipServer+`CheckQueuePrivateBarber/${username}/${date}`)
  }
  public searchQueuePrivateBarber(username,date){
    return this.http.get(urlServer.ipServer+`searchQueuePrivateBarber/?username=${username}&date=${date}`)
  }
}
