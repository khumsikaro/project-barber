import { Component, OnInit } from '@angular/core';
import { JobQueueBarberService } from './job-queue-barber.service';
import { LoginService } from '../../login/login.service';
import Swal from 'sweetalert2'
import { PagesComponent } from '../../pages.component';
@Component({
  selector: 'app-job-queue-barber',
  templateUrl: './job-queue-barber.component.html',
  styleUrls: ['./job-queue-barber.component.css']
})
export class JobQueueBarberComponent implements OnInit {
  USER;
  today = new Date();
  QueuePrivateBarberdata;
  constructor(private service: JobQueueBarberService, private session: LoginService, private PageBadge: PagesComponent, ) { }

  ngOnInit() {
    this.USER = this.session.getActiveUser();
    this.GetQueuePrivateBarber()
    document.getElementById("content").classList.remove("p-0")
  }
  GetQueuePrivateBarber() {
    this.PageBadge.ngOnInit()
    let date = this.today
    this.service.CheckQueuePrivateBarber(this.USER.username, date).subscribe(
      (res: any) => {
        if (res.length != 0) {
          this.QueuePrivateBarberdata = res as any;
        }
        else {
          Swal.fire({
            html: '<p>ยังไม่มีคิวงานในวันนี้</p>',
          })
        }
      }
    )
  }
}
