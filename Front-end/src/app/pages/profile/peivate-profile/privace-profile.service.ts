import { Injectable } from '@angular/core';
import {urlServer} from '../../../URL'
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PrivaceProfileService {

  constructor( private http:HttpClient) { }

  public GetPrivaceProfile(Username:string){
    return this.http.get(urlServer.ipServer+`UsersDetail/${Username}`)
  }
  public Getskill(username: string) {
    return this.http.get(urlServer.ipServer + `Detailskills/${username}`)
  }
  public EditProfile(data:any){
return this.http.put(urlServer.ipServer+`EditProfile`,data)
  }
  ////////////////////////////////////////////////////////////////////
  public GetProvince(){
    return this.http.get(urlServer.ipServer+`GetProvince`)
  }
  public GetAmphur(Province_id:number){
    return this.http.get(urlServer.ipServer+`GetAmphur/${Province_id}`)
  }
  public GetDistrict(Amphur_id:number){
    return this.http.get(urlServer.ipServer+`GetDistrict/${Amphur_id}`)
  }
  public GetZipcode(District_id:number){
    return this.http.get(urlServer.ipServer+`GetZipcode/${District_id}`)
  }
  ////////////////////////////////////////////////////////////////////
  ////////////////////////การลา///////////////////////////////
  public CheckHolidayPrivateBarber(username,date){
    return this.http.get(urlServer.ipServer+`CheckHolidayPrivateBarber/${username}/${date}`)
  }
 ///////////////////////////เงินเดือน//////////////////////////////////////
 public  CheckSalary(date,Branch,username){
  return this.http.get(urlServer.ipServer+`PriatveSalaryBarber/${date}/${Branch}/${username}`)
 }

 
}
