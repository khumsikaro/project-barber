import { Component, OnInit, Input } from '@angular/core';
import { PrivaceProfileService } from '../privace-profile.service';
import { UploadFile, NzModalService } from 'ng-zorro-antd';
import { LoginService } from 'src/app/pages/login/login.service';
import Swal from 'sweetalert2'
import { differenceInCalendarDays } from 'date-fns';

@Component({
  selector: 'app-edit-private-profile-form',
  templateUrl: './edit-private-profile-form.component.html',
  styleUrls: ['./edit-private-profile-form.component.css']
})
export class EditPrivateProfileFormComponent implements OnInit {
  ProvinceData: any = [];
  AmphurData: any = [];
  DistrictData: any = [];
  ZipcodeData: any = [];
  USER;
/////////////////////////////////////////////////
@Input() img_profile
@Input() firstname
@Input() lastname
@Input() nickname
@Input() date_of_birth
@Input() phone_number
@Input() Email
@Input() gender
@Input() province_id
@Input() amphur_id
@Input() district_id
@Input() zipcode_id
@Input() address
@Input() level_barberid

  constructor(private service:PrivaceProfileService, private session: LoginService,) { }
  ngOnInit() {
    this.USER = this.session.getActiveUser();
    this.ShowProvince()
    this.ShowAmphur(this.province_id)
    this.ShowDistrict(this.amphur_id)
    this.ShowZipcode(this.district_id)
  }
      disabledDate = (current: Date): boolean => {
        let today = new Date
        // Can not select days before today and today
        return differenceInCalendarDays(current, today) > 0;
      };
    ///////////////////////////////////////////////
    showUploadList = {
      showPreviewIcon: true,
      showRemoveIcon: true,
      hidePreviewIconInNonImage: true
    };
    fileList = [];
    previewImage: string | undefined = '';
    previewVisible = false;
    handlePreview = (file: UploadFile) => {
      this.previewImage = file.url || file.thumbUrl;
      this.previewVisible = true;
    };

  ShowProvince() {
    this.service.GetProvince().subscribe(
      (res) => {
        this.ProvinceData = res as any[]
      }
    )
  }
  ShowAmphur(data) {
    this.service.GetAmphur(data).subscribe(
      (res) => {
        this.AmphurData = res as any[]
      }
    )
  }
  ShowDistrict(data) {
    this.service.GetDistrict(data).subscribe(
      (res) => {
        this.DistrictData = res as any[]
      }
    )
  }
  ShowZipcode(data) {
    this.service.GetZipcode(data).subscribe(
      (res) => {
        this.ZipcodeData = res as any[]
      }
    )
  }
}
