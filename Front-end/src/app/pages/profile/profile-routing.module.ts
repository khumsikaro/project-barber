import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileComponent } from './profile.component';
import { HistoryServiceComponent } from './history-service/history-service.component';
import { JobQueueBarberComponent } from './job-queue-barber/job-queue-barber.component';
import { PeivateProfileComponent } from './peivate-profile/peivate-profile.component';
import { StatusQueueComponent } from './status-queue/status-queue.component';
import { GuardService } from '../login/guard.service';
import { BarberGuardsService } from './barber-guards.service';
import { CustomerGuardsService } from './customer-guards.service';



const routes: Routes = [
  {
    path: 'Profile',
    component: ProfileComponent,
    children: [
      {
        path: 'HistoryService',
        canActivate:[CustomerGuardsService],
        component: HistoryServiceComponent
      },
      {
        path: 'JobQueueBarber',
        canActivate:[BarberGuardsService],
        component: JobQueueBarberComponent
      },
      {
        path:'PeivateProfile',
        canActivate:[GuardService],
        component:PeivateProfileComponent
      },
      {
        path:'StatusQueue',
        canActivate:[CustomerGuardsService],
        component:StatusQueueComponent
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
