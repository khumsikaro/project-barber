import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileRoutingModule } from './profile-routing.module';
import { StatusQueueComponent } from './status-queue/status-queue.component';
import { HistoryServiceComponent } from './history-service/history-service.component';
import { JobQueueBarberComponent } from './job-queue-barber/job-queue-barber.component';
import { PeivateProfileComponent } from './peivate-profile/peivate-profile.component';
import { ProfileComponent } from './profile.component';
import { FormsModule } from '@angular/forms';
import { ZorroModule } from 'src/app/lib.ngZorro';
import { DetailStatusQueueFormComponent } from './status-queue/detail-status-queue-form/detail-status-queue-form.component';
import { GuardService } from '../login/guard.service';
import { DetailHistoryComponent } from './history-service/detail-history/detail-history.component';
import { EditPrivateProfileFormComponent } from './peivate-profile/edit-private-profile-form/edit-private-profile-form.component';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { BarberGuardsService } from './barber-guards.service';
import { CustomerGuardsService } from './customer-guards.service';


@NgModule({
  declarations: [ StatusQueueComponent, HistoryServiceComponent, JobQueueBarberComponent, PeivateProfileComponent, ProfileComponent, DetailStatusQueueFormComponent,DetailHistoryComponent, EditPrivateProfileFormComponent,],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    FormsModule,
    ZorroModule,
    NzPopoverModule,
  ],
  entryComponents:[
    DetailStatusQueueFormComponent,
    DetailHistoryComponent,
    EditPrivateProfileFormComponent,
  ],
  providers: [
    BarberGuardsService,
    CustomerGuardsService,
    GuardService
  ]
})
export class ProfileModule { }
