import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { LoginService } from '../login/login.service';
import { PagesService } from '../pages.service';
import { NzNotificationService, NzModalService, NzDrawerService, NzDrawerRef, } from 'ng-zorro-antd';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-ask',
  templateUrl: './ask.component.html',
  styleUrls: ['./ask.component.css']
})
export class AskComponent implements OnInit {
  Askform: FormGroup;
  constructor(private fb: FormBuilder,
    private session: LoginService,
    private service: PagesService,
    private notification: NzNotificationService,
    private modalService: NzModalService,
    private drawerRef: NzDrawerRef<string>
    ) { }

  ngOnInit() {
    this.Askform = this.fb.group({
      Name: [null, [Validators.required]],
      Email: [null, [Validators.email, Validators.required]],
      Title: [null, [Validators.required]],
      Subject: [null, [Validators.required]],
      Detail: [null, [Validators.required]],
      Phone: [],
    });
  }

  SendInformation() {
    for (const i in this.Askform.controls) {
      this.Askform.controls[i].markAsDirty();
      this.Askform.controls[i].updateValueAndValidity();
    }

    if (this.Askform.value.Name!=null
      &&this.Askform.value.Email!=null
      &&this.Askform.value.Title!=null
      &&this.Askform.value.Subject!=null
      &&this.Askform.value.Detail!=null) {

      let data = {
        name: this.Askform.value.Name,
        email: this.Askform.value.Email,
        phone: this.Askform.value.Phone ? this.Askform.value.Phone : "ไม่มี",
        title: this.Askform.value.Title,
        subject: this.Askform.value.Subject,
        question: this.Askform.value.Detail,
      }
      this.service.SendInformation(data).subscribe(
        (res) => {
          this.drawerRef.close();
          Swal.fire({
            title: 'ส่งข้อมูลสำเร็จ',
            html: `คุณ ${this.Askform.value.Name}
            รอการตอบกลับไปยังอีเมลล์ <h6 style="color:Tomato;"><U>${this.Askform.value.Email}</U></h6><br>
            `,
          }).then((result) => {
            if (result.value) {
              // window.history.go(0)
              // this.modalService.closeAll;
            }
          })
        }
      )
    }
  }
}
