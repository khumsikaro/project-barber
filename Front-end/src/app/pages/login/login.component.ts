import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { LoginService } from './login.service';
import { NzModalService, NzNotificationService } from 'ng-zorro-antd';
import Swal from 'sweetalert2'
import { HttpClient } from '@angular/common/http';
import {urlServer} from '../../URL'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  validateForm: FormGroup;
  constructor(private fb: FormBuilder, private service: LoginService,
    private modalService: NzModalService,
    private notificationService: NzNotificationService, private http:HttpClient) { }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      userName: [null, [Validators.required]],
      password: [null, [Validators.required]],
      remember: [true]
    });


  }

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
    const data = {
      username: this.validateForm.value.userName,
      password: btoa(this.validateForm.value.password),

    }
    this.service.Login(data).subscribe(
      (res: any) => {
        if (res.status === "Y" && res.status_login==0) {
          let Upstatuslogin={username:data.username,status_login:1,}
          this.http.put(urlServer.ipServer + `UpdateStatusLogin`, Upstatuslogin).subscribe((res)=>{
            this.service.Login(data).subscribe((USER:any)=>{
              this.service.setActiveUser(USER);
              this.modalService.closeAll();
              Swal.fire({
                imageUrl: USER.img_profile,
                imageWidth: 290,
                imageHeight: 290,
                title: 'ยินดีต้อนรับ!',
                showConfirmButton: false,
                text: `คุณ ${USER.firstname} ${USER.lastname}`,
                timer: 1500
              })
              setTimeout(() => window.history.go(0), 1500);
            })
          })
        }
        else if(res.status_login==1){
          Swal.fire({
            title: 'ไอดีนี้มีการล็อคอินอยู่แล้ว?',
            text: "คุณต้องการล็อคอินหรือไม่!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ใช่,ฉันต้องการล็อคอิน',
            cancelButtonText: 'ไม่'
          }).then((result) => {
            if (result.value) {
              let Upstatuslogin={username:data.username,status_login:1,}
              this.http.put(urlServer.ipServer + `UpdateStatusLogin`, Upstatuslogin).subscribe((res)=>{
                this.service.Login(data).subscribe((USER:any)=>{
                  this.service.setActiveUser(USER);
                  this.modalService.closeAll();
                  Swal.fire({
                    imageUrl: USER.img_profile,
                    imageWidth: 290,
                    imageHeight: 290,
                    title: 'ยินดีต้อนรับ!',
                    showConfirmButton: false,
                    text: `คุณ ${USER.firstname} ${USER.lastname}`,
                    timer: 1500
                  })
                  setTimeout(() => window.history.go(0), 1500);
                })
              })
            }
            else{
              this.modalService.closeAll();
            }
          })
        }
        else {
          this.modalService.closeAll();
          Swal.fire({
            type: 'warning',
            title: 'ไม่สามารถเข้าสู่ระบบได้!',
            showConfirmButton: false,
            text: `ท่านไม่ได้รับอนุญาติให้เข้าสู่ระบบ`,
            timer: 1500
          })
        }
      }
    )
  }
}
