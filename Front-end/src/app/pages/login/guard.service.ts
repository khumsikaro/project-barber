import { Injectable } from '@angular/core';
import {
  CanActivate, Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivateChild
} from '@angular/router';
import { LoginService } from './login.service';

@Injectable()
export class GuardService implements CanActivate {

  constructor(private _router: Router, private AuthenService: LoginService) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> | boolean {
    let currentUser = this.AuthenService.getActiveUser();
    return new Promise<boolean>(resolve => {
      if (currentUser !== null) {
        resolve(true);
      } else {
        resolve(false);
        this._router.navigate(['']);
      }

    });
  }
}