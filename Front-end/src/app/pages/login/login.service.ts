import { Injectable } from '@angular/core';
import {urlServer} from '../../URL'
import { HttpClient } from '@angular/common/http';
import { SessionStorageService } from 'ngx-webstorage';
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http:HttpClient,private sessionService:SessionStorageService) { }
  
  public setActiveUser(data) {
    this.sessionService.store('ACTIVE_USER', data);
  }

  public getActiveUser() {
    return this.sessionService.retrieve('ACTIVE_USER');
  }

  public clearActiveUser() {
    this.sessionService.clear('ACTIVE_USER');
  }
  public Login(data){
    return this.http.post(urlServer.ipServer + `Login`,data)
  }
}
