import { Injectable } from '@angular/core';
import {urlServer} from '../../../URL'
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PayService {

  constructor(private http:HttpClient) { }

  public GetQueuePay(branchid){
    return this.http.get(urlServer.ipServer+`GetQueuePay/${branchid}`)
  }

  public UpdateQueueDetail(data){
    return this.http.put(urlServer.ipServer+`UpdateQueueDetail`,data)
  }

  public PayQueue(data){
    return this.http.put(urlServer.ipServer+`UpdateStatusPay`,data)
  }

  /////////////////////Detail-pay-from////////////////
  public GetQueueDetail(id){
    return this.http.get(urlServer.ipServer+`GetQueueDetail/${id}`)
  }

  public SendFeedback(data){
    return this.http.put(urlServer.ipServer+`SendFeedback`,data)
  }

  // public GetPoint(){
  //   return this.http.get(urlServer.ipServer+`GetPointCustomer`)
  // }
  
  // public UsePoint(serviceid){
  //   return this.http.get(urlServer.ipServer+`GetServiceCharge/${serviceid}`)
  // }
}
