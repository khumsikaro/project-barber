import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-edit-queue-detail',
  templateUrl: './edit-queue-detail.component.html',
  styleUrls: ['./edit-queue-detail.component.css']
})
export class EditQueueDetailComponent implements OnInit {
@Input() branch_id:number;
@Input() queue_id:number;
@Input() service_id:string;
@Input() que_det_id:number;
@Input() service_charge:number;
@Input() take_time:number;
@Input() add_money:number;
@Input() add_time:number;
@Input() detail:string;
@Input() Nickname1:string;
  constructor() { }

  ngOnInit() {
  }

}
