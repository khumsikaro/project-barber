import { Component, OnInit, Input } from '@angular/core';
import { PayService } from '../pay.service';
import { NzNotificationService, NzModalService } from 'ng-zorro-antd';
import { LoginService } from 'src/app/pages/login/login.service';

@Component({
  selector: 'app-feedback-customer',
  templateUrl: './feedback-customer.component.html',
  styleUrls: ['./feedback-customer.component.css']
})
export class FeedbackCustomerComponent implements OnInit {
  @Input() branch_id: number;
  @Input() queue_id: number;
  @Input() customer_id: string;
  @Input() queue_dt: Date;
  @Input() firstname: string;
  @Input() lastname: string;
  @Input() nickname: string;
  @Input() feedback_branch: number;
  QueueDetailData;
  Feedback_service_1; Feedback_barber_1; Feedback_service_2;
  Feedback_barber_2; Feedback_service_3; Feedback_barber_3;
  Service1; Service2; Service3; Barber1; Barber2; Barber3;
  Q_id1; Q_id2; Q_id3;
  tooltips = ['แย่มาก', 'แย่', 'ธรรมดา', 'ดี', 'ดีมาก'];
  Length:number
  constructor(
    private session: LoginService,
    private modalService: NzModalService,
    private notificationService: NzNotificationService,
    private service: PayService,
  ) { }

  ngOnInit() {
    this.GetQueueDetail(this.queue_id)
  }
  GetQueueDetail(queue_id) {
    this.service.GetQueueDetail(queue_id).subscribe(
      (res: any) => {
        this.Length = res.length
        switch (res.length) {
          case 1: {
            this.Barber1 = res[0].Nickname1;this.Service1 = res[0].service_id;this.Q_id1 = res[0].que_det_id;
            this.Feedback_service_1=res[0].feedback_service;this.Feedback_barber_1=res[0].feedback_barber
            break;
          };
          case 2: {
            this.Barber1 = res[0].Nickname1;this.Service1 = res[0].service_id;this.Q_id1 = res[0].que_det_id;
            this.Barber2 = res[1].Nickname1;this.Service2 = res[1].service_id;this.Q_id2 = res[1].que_det_id;
            this.Feedback_service_1=res[0].feedback_service;this.Feedback_barber_1=res[0].feedback_barber;
            this.Feedback_service_2=res[1].feedback_service;this.Feedback_barber_2=res[1].feedback_barber;
            break;
          };
          case 3: {
            this.Barber1 = res[0].Nickname1;this.Service1 = res[0].service_id;this.Q_id1 = res[0].que_det_id;
            this.Barber2 = res[1].Nickname1;this.Service2 = res[1].service_id;this.Q_id2 = res[1].que_det_id;
            this.Barber3 = res[2].Nickname1;this.Service3 = res[2].service_id;this.Q_id3 = res[2].que_det_id;
            this.Feedback_service_1=res[0].feedback_service;this.Feedback_barber_1=res[0].feedback_barber;
            this.Feedback_service_2=res[1].feedback_service;this.Feedback_barber_2=res[1].feedback_barber;
            this.Feedback_service_3=res[2].feedback_service;this.Feedback_barber_3=res[2].feedback_barber;
            break;
          };
        }
      }
    )
  }
}
