import { Component, OnInit, Input } from '@angular/core';
import { PayService } from '../pay.service';
import { NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { isNullOrUndefined } from 'util';
import { EditQueueDetailComponent } from '../edit-queue-detail/edit-queue-detail.component';

@Component({
  selector: 'app-detai-befor-edit-queue',
  templateUrl: './detai-befor-edit-queue.component.html',
  styleUrls: ['./detai-befor-edit-queue.component.css']
})
export class DetaiBeforEditQueueComponent implements OnInit {
  @Input() branch_id: number
  @Input() queue_id: number
  constructor(private service: PayService,
    private modalService: NzModalService,
    private notificationService: NzNotificationService, ) { }
  QueueDetaildata;

  ngOnInit() {
    this.GetQueueuDetail()
  }

  GetQueueuDetail() {
    this.service.GetQueueDetail(this.queue_id).subscribe(
      (res) => {
        this.QueueDetaildata = res as any;
      }
    )
  }
  EditDetailQueue(data) {
    const modal = this.modalService.create({
      nzTitle: '<h5 class="modal-title">แก้ไขรายละเอียดคิว</h5>',
      nzWidth: 700,
      nzContent: EditQueueDetailComponent,
      nzComponentParams: {
        branch_id: isNullOrUndefined(data) ? null : data.branch_id,
        queue_id: isNullOrUndefined(data) ? null : data.queue_id,
        service_id: isNullOrUndefined(data) ? null : data.service_id,
        que_det_id: isNullOrUndefined(data) ? null : data.que_det_id,
        service_charge: isNullOrUndefined(data) ? null : data.service_charge,
        take_time: isNullOrUndefined(data) ? null : data.take_time,
        add_money: isNullOrUndefined(data) ? null : data.add_money,
        add_time: isNullOrUndefined(data) ? null : data.add_time,
        detail: isNullOrUndefined(data) ? null : data.detail,
        Nickname1: isNullOrUndefined(data) ? null : data.Nickname1,

      },
      nzClosable: true,
      nzFooter: [
        {
          label: 'ยกเลิก',
          shape: 'default',
          onClick: () => modal.destroy()
        },
        {
          label: 'ยืนยัน',
          type: 'primary',
          onClick: () => new Promise(resolve => setTimeout(resolve, 0)).then(
            () => {
              const instance = modal.getContentComponent();
              return instance
            }
          ).then((instance) => {
            this.AddDetail(instance).subscribe(
              (res) => {
                if (JSON.stringify(res)[11] === 's') {
                  this.notificationService.create("success", "เพิ่มรายละเอียดสำเร็จ", null)
                  modal.destroy();
                  this.GetQueueuDetail();
                }
              }
            )
          })
        }
      ],
    })
  }
  AddDetail(data) {
    const value = {
      que_det_id:data.que_det_id,
      add_money:data.add_money,
      add_time:data.add_time,
      detail:data.detail
    }
    return this.service.UpdateQueueDetail(value)
  }
}
