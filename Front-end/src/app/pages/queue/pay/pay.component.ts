import { Component, OnInit } from '@angular/core';
import { PayService } from './pay.service';
import { LoginService } from '../../login/login.service';
import { NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { isNullOrUndefined } from 'util';
import { DetailPayFromComponent } from './detail-pay-from/detail-pay-from.component';
import { FeedbackCustomerComponent } from './feedback-customer/feedback-customer.component';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-pay',
  templateUrl: './pay.component.html',
  styleUrls: ['./pay.component.css']
})
export class PayComponent implements OnInit {
  USER;
  PayListdata;
  constructor(private service: PayService,
    private session: LoginService,
    private modalService: NzModalService,
    private notificationService: NzNotificationService, ) { }

  ngOnInit() {
    this.USER = this.session.getActiveUser();
    this.GetPayList()
    document.getElementById("content").classList.remove("p-0")
  }

  GetPayList() {
    this.service.GetQueuePay(this.USER.branch_id).subscribe((res) => {
      this.PayListdata = res as any;
    })
  }

  Pay(data) {
    const modal = this.modalService.create({
      nzTitle: '<h5 class="modal-title">รายละเอียดการจ่ายเงิน</h5>',
      nzContent: DetailPayFromComponent,
      nzWidth: 1000,
      nzComponentParams: {
        branch_id: isNullOrUndefined(data) ? null : data.branch_id,
        queue_id: isNullOrUndefined(data) ? null : data.queue_id,
        customer_id: isNullOrUndefined(data) ? null : data.customer_id,
        total_time: isNullOrUndefined(data) ? null : data.total_time,
        total_money: isNullOrUndefined(data) ? null : data.total_money,
        queue_dt: isNullOrUndefined(data) ? null : data.queue_dt,
        status_confirm: isNullOrUndefined(data) ? null : data.status_confirm,
        status_come: isNullOrUndefined(data) ? null : data.status_come,
        cancel_queue: isNullOrUndefined(data) ? null : data.cancel_queue,
        status_pay: isNullOrUndefined(data) ? null : data.status_pay,
        firstname: isNullOrUndefined(data) ? null : data.firstname,
        lastname: isNullOrUndefined(data) ? null : data.lastname,
        nickname: isNullOrUndefined(data) ? null : data.nickname,
        phone_number: isNullOrUndefined(data) ? null : data.phone_number,
        img_profile: isNullOrUndefined(data) ? null : data.img_profile,
      },
      nzClosable: true,
      nzFooter: [
        {
          label: 'ยกเลิก',
          shape: 'default',
          onClick: () => modal.destroy()
        },
        {
          label: 'คิดเงิน',
          type: 'primary',
          onClick: () => new Promise(resolve => setTimeout(resolve, 0)).then(
            () => {
              const instance = modal.getContentComponent();
              return instance
            }
          ).then((instance) => {
            this.TakeYourMoney(instance).subscribe(
              (res) => {
                if (JSON.stringify(res)[11] === 's') {
                  this.notificationService.create("success", "จ่ายเงินสำเร็จ", null)
                  modal.destroy();
                  this.GetPayList();
                  this.FeedBack(instance);
                }
              }
            )
          })
        }
      ]
    })
  }
  TakeYourMoney(data: any) {
    let UsePoint = [];
    let Service_use_point = data.ServiceUsePoint;
    let PointAdd = [];
    let Add_point = data.DetailQueuedata;
    for (let i = 0; i < Service_use_point.length; i++) {
      UsePoint.push([Service_use_point[i].service_id, Service_use_point[i].Maxpoint])
    }
    for (let i = 0; i < Add_point.length; i++) {
      PointAdd.push(Add_point[i].service_id)
    }
    var value = {
      status_pay: 1,
      queue_id: data.queue_id,
      username: data.customer_id,
      total_time: data.SumTime,
      total_money: data.TotalMoney,
      ServiceUsePoint: UsePoint,
      AddPoint: PointAdd,
    }
    return this.service.PayQueue(value)
  }
  FeedBack(data) {
    const modal = this.modalService.create({
      nzTitle: '<h5 class="modal-title">ฟอร์มการให้คะแนน</h5>',
      nzContent: FeedbackCustomerComponent,
      nzWidth: 1000,
      nzComponentParams: {
        branch_id: isNullOrUndefined(data) ? null : data.branch_id,
        queue_id: isNullOrUndefined(data) ? null : data.queue_id,
        customer_id: isNullOrUndefined(data) ? null : data.customer_id,
        queue_dt: isNullOrUndefined(data) ? null : data.queue_dt,
        firstname: isNullOrUndefined(data) ? null : data.firstname,
        lastname: isNullOrUndefined(data) ? null : data.lastname,
        nickname: isNullOrUndefined(data) ? null : data.nickname,
        feedback_branch: isNullOrUndefined(data) ? null : data.feedback_branch,
      },
      nzClosable: true,
      nzFooter: [
        {
          label: 'ยกเลิก',
          shape: 'default',
          onClick: () => modal.destroy()
        },
        {
          label: 'ส่งคะแนน',
          type: 'primary',
          onClick: () => new Promise(resolve => setTimeout(resolve, 0)).then(
            () => {
              const instance = modal.getContentComponent();
              return instance
            }
          ).then((instance) => {
            this.SendFeedBack(instance).subscribe(
              (res) => {
                if (JSON.stringify(res)[11] === 's') {
                  Swal.fire({
                    // title: `คุณ ${data.firstname} ${data.lastname}`,
                    title: '<p>ขอบคุณที่แสดงความคิดเห็นนะค่ะ</p>',
                  })
                  modal.destroy();
                  this.GetPayList();
                }
              }
            )
          })
        }
      ]
    })
  }
  SendFeedBack(data) {
    switch (data.Length) {
      case 1: {
        var FeedbackDeatil = [{ que_det_id: data.Q_id1,
           feedback_barber: data.Feedback_barber_1, 
           feedback_service: data.Feedback_service_1,},];
        break;
      };
      case 2: {
        var FeedbackDeatil = [{
          que_det_id: data.Q_id1,
          feedback_barber: data.Feedback_barber_1,
          feedback_service: data.Feedback_service_1
        },
        {
          que_det_id: data.Q_id2,
          feedback_barber: data.Feedback_barber_2,
          feedback_service: data.Feedback_service_2
        },];
        break;
      }
      case 3: {
        var FeedbackDeatil = [{
          que_det_id: data.Q_id1,
          feedback_barber: data.Feedback_barber_1,
          feedback_service: data.Feedback_service_1
        },
        {
          que_det_id: data.Q_id2,
          feedback_barber: data.Feedback_barber_2,
          feedback_service: data.Feedback_service_2
        },
        {
          que_det_id: data.Q_id3,
          feedback_barber: data.Feedback_barber_3,
          feedback_service: data.Feedback_service_3
        },];
        break;
      }
    }
    var value = {
      queue_id: data.queue_id,
      feedback_branch: data.feedback_branch,
      feedback_detail: FeedbackDeatil,
      stempfeedback:1,
    }
    return this.service.SendFeedback(value)
  }
}
