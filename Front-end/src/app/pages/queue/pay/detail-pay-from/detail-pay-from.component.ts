import { Component, OnInit, Input } from '@angular/core';
import { PayService } from '../pay.service';
import { LoginService } from 'src/app/pages/login/login.service';
import { NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { EditQueueDetailComponent } from '../edit-queue-detail/edit-queue-detail.component';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-detail-pay-from',
  templateUrl: './detail-pay-from.component.html',
  styleUrls: ['./detail-pay-from.component.css']
})
export class DetailPayFromComponent implements OnInit {
  @Input() branch_id: number;
  @Input() queue_id: number;
  @Input() customer_id: string;
  @Input() total_time: number;
  @Input() total_money: number;
  @Input() queue_dt;
  @Input() status_confirm: number;
  @Input() status_come: number;
  @Input() cancel_queue: number;
  @Input() status_pay: number;
  @Input() firstname: string
  @Input() lastname: string
  @Input() nickname: string
  @Input() phone_number: string
  @Input() img_profile: string
  constructor(private service: PayService,
    private session: LoginService,
    private modalService: NzModalService,
    private notificationService: NzNotificationService, ) { }
  DetailQueuedata;
  Pointdata;
  SumMoneyAll;
  TotalMoney
  SumTime;
  LengthDetail;
  ServiceUsePoint = [];
  CheckServicePoint = [];
  UsePointMoney = []
  ngOnInit() {
    this.GetDetailQueue(this.queue_id)
  }

  GetDetailQueue(queue_id) {
    this.service.GetQueueDetail(queue_id).subscribe(
      (res: any[]) => {
        this.LengthDetail = res.length
        this.DetailQueuedata = res as any[];
        this.Pointdata = res as any[];
        switch (res.length) {
          case 1: {
            this.SumMoneyAll = ((this.DetailQueuedata[0].service_charge ? this.DetailQueuedata[0].service_charge : 0) + (this.DetailQueuedata[0].add_money ? this.DetailQueuedata[0].add_money : 0)).toLocaleString('en-us', { minimumFractionDigits: 0 })
            this.SumTime = ((this.DetailQueuedata[0].take_time ? this.DetailQueuedata[0].take_time : 0) + (this.DetailQueuedata[0].add_time ? this.DetailQueuedata[0].add_time : 0) - (this.UsePointMoney[0] ? this.UsePointMoney[0] : 0))
            this.TotalMoney =((this.DetailQueuedata[0].service_charge ? this.DetailQueuedata[0].service_charge : 0) + (this.DetailQueuedata[0].add_money ? this.DetailQueuedata[0].add_money : 0))
            break;
          };
          case 2: {
            this.SumMoneyAll = (((this.DetailQueuedata[0].service_charge ? this.DetailQueuedata[0].service_charge : 0) + (this.DetailQueuedata[0].add_money ? this.DetailQueuedata[0].add_money : 0)) +
              ((this.DetailQueuedata[1].service_charge ? this.DetailQueuedata[1].service_charge : 0) + (this.DetailQueuedata[1].add_money ? this.DetailQueuedata[1].add_money : 0) - (this.UsePointMoney[0] ? this.UsePointMoney[0] : 0) - (this.UsePointMoney[1] ? this.UsePointMoney[1] : 0))).toLocaleString('en-us', { minimumFractionDigits: 0 })

            this.SumTime = ((this.DetailQueuedata[0].take_time ? this.DetailQueuedata[0].take_time : 0) + (this.DetailQueuedata[0].add_time ? this.DetailQueuedata[0].add_time : 0)) +
              ((this.DetailQueuedata[1].take_time ? this.DetailQueuedata[1].take_time : 0) + (this.DetailQueuedata[1].add_time ? this.DetailQueuedata[1].add_time : 0))

              this.TotalMoney = (((this.DetailQueuedata[0].service_charge ? this.DetailQueuedata[0].service_charge : 0) + (this.DetailQueuedata[0].add_money ? this.DetailQueuedata[0].add_money : 0)) +
              ((this.DetailQueuedata[1].service_charge ? this.DetailQueuedata[1].service_charge : 0) + (this.DetailQueuedata[1].add_money ? this.DetailQueuedata[1].add_money : 0) - (this.UsePointMoney[0] ? this.UsePointMoney[0] : 0) - (this.UsePointMoney[1] ? this.UsePointMoney[1] : 0)))
            break;
          };
          case 3: {
            this.SumMoneyAll = (((this.DetailQueuedata[0].service_charge ? this.DetailQueuedata[0].service_charge : 0) + (this.DetailQueuedata[0].add_money ? this.DetailQueuedata[0].add_money : 0)) +
              ((this.DetailQueuedata[1].service_charge ? this.DetailQueuedata[1].service_charge : 0) + (this.DetailQueuedata[1].add_money ? this.DetailQueuedata[1].add_money : 0)) +
              ((this.DetailQueuedata[2].service_charge ? this.DetailQueuedata[2].service_charge : 0) + (this.DetailQueuedata[2].add_money ? this.DetailQueuedata[2].add_money : 0) - (this.UsePointMoney[0] ? this.UsePointMoney[0] : 0) - (this.UsePointMoney[1] ? this.UsePointMoney[1] : 0) - (this.UsePointMoney[2] ? this.UsePointMoney[2] : 0))).toLocaleString('en-us', { minimumFractionDigits: 0 })

            this.SumTime = ((this.DetailQueuedata[0].take_time ? this.DetailQueuedata[0].take_time : 0) + (this.DetailQueuedata[0].add_time ? this.DetailQueuedata[0].add_time : 0)) +
              ((this.DetailQueuedata[1].take_time ? this.DetailQueuedata[1].take_time : 0) + (this.DetailQueuedata[1].add_time ? this.DetailQueuedata[1].add_time : 0)) +
              ((this.DetailQueuedata[2].take_time ? this.DetailQueuedata[2].take_time : 0) + (this.DetailQueuedata[2].add_time ? this.DetailQueuedata[2].add_time : 0))

              this.TotalMoney = (((this.DetailQueuedata[0].service_charge ? this.DetailQueuedata[0].service_charge : 0) + (this.DetailQueuedata[0].add_money ? this.DetailQueuedata[0].add_money : 0)) +
              ((this.DetailQueuedata[1].service_charge ? this.DetailQueuedata[1].service_charge : 0) + (this.DetailQueuedata[1].add_money ? this.DetailQueuedata[1].add_money : 0)) +
              ((this.DetailQueuedata[2].service_charge ? this.DetailQueuedata[2].service_charge : 0) + (this.DetailQueuedata[2].add_money ? this.DetailQueuedata[2].add_money : 0) - (this.UsePointMoney[0] ? this.UsePointMoney[0] : 0) - (this.UsePointMoney[1] ? this.UsePointMoney[1] : 0) - (this.UsePointMoney[2] ? this.UsePointMoney[2] : 0)))
            break;
          }
        }
      }
    )
  }
  UsePoint(data, i) {
    this.modalService.confirm({
      nzTitle: '<i>ยืนยันการใช้แต้ม?</i>',
      nzContent: `บริการ:${data.service_id}`,
      nzOnOk: () => {
        if (this.ServiceUsePoint.length < this.LengthDetail) {
          const result = this.ServiceUsePoint.filter((res) => {
            return res == data
          })
          if (result.length == 0) {
            this.ServiceUsePoint.push(data)
            // this.SumMoneyAll = this.SumMoneyAll - data.service_charge;
            this.UsePointMoney[i] = data.service_charge;
            this.CheckServicePoint[i] = false;
            this.GetDetailQueue(this.queue_id)
          }
        }
      }
    });
  }
  OpenAddDetailFrom(data) {
    const modal = this.modalService.create({
      nzTitle: '<h5 class="modal-title">แก้ไขรายละเอียดเพิ่มเติม</h5>',
      nzContent: EditQueueDetailComponent,
      nzWidth: 700,
      nzComponentParams: {
        branch_id: isNullOrUndefined(data) ? null : data.branch_id,
        queue_id: isNullOrUndefined(data) ? null : data.queue_id,
        service_id: isNullOrUndefined(data) ? null : data.service_id,
        que_det_id: isNullOrUndefined(data) ? null : data.que_det_id,
        service_charge: isNullOrUndefined(data) ? null : data.service_charge,
        take_time: isNullOrUndefined(data) ? null : data.take_time,
        add_money: isNullOrUndefined(data) ? null : data.add_money,
        add_time: isNullOrUndefined(data) ? null : data.add_time,
        detail: isNullOrUndefined(data) ? null : data.detail,
        Nickname1: isNullOrUndefined(data) ? null : data.Nickname1,
      },
      nzClosable: true,
      nzFooter: [
        {
          label: 'ยกเลิก',
          shape: 'default',
          onClick: () => modal.destroy()
        },
        {
          label: 'ยืนยัน',
          type: 'primary',
          onClick: () => new Promise(resolve => setTimeout(resolve, 0)).then(
            () => {
              const instance = modal.getContentComponent();
              return instance
            }
          ).then((instance) => {
            if (isNullOrUndefined(instance.detail) || instance.detail.trim() === '')
              this.notificationService.create("error", "กรุณาใส่รายละเอียดการเพิ่ม", null)
            else if (!isNullOrUndefined(instance.detail) && instance.detail.trim() !== '') {
              this.AddDetail(instance).subscribe(
                (res) => {
                  if (JSON.stringify(res)[11] === 's') {
                    this.notificationService.create("success", "เพิ่มรายละเอียดสำเร็จ", null)
                    modal.destroy();
                    this.GetDetailQueue(this.queue_id)
                  }
                }
              )
            }
          })
        }
      ],
    })
  }
  AddDetail(data) {
    const value = {
      que_det_id: data.que_det_id,
      add_money: data.add_money,
      add_time: data.add_time,
      detail: data.detail
    }
    return this.service.UpdateQueueDetail(value)
  }
}
