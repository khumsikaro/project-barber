import { Component, OnInit } from '@angular/core';
import { NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { ReservationsFromComponent } from './reservations-from/reservations-from.component';
import { TableQueueService } from './table-queue.service';
import { LoginService } from '../../login/login.service';
import Swal from 'sweetalert2'
import { isNullOrUndefined } from 'util';
import { HttpClient } from '@angular/common/http';
import { urlServer } from '../../../URL'
import { from } from 'rxjs';
import { ConfirmQueueComponent } from '../confirm-queue/confirm-queue.component';
import { async } from '@angular/core/testing';
@Component({
  selector: 'app-table-queue',
  templateUrl: './table-queue.component.html',
  styleUrls: ['./table-queue.component.css']
})
export class TableQueueComponent implements OnInit {
  ShowTableQ: any[];
  BranchList: any[];
  BranchId = 13;
  StatusBranch;
  today = new Date();
  time = this.today.getHours() + ":" + this.today.getMinutes() + ":" + this.today.getSeconds();
  date = this.today.getFullYear() + '-' + (this.today.getMonth() + 1) + '-' + this.today.getDate();
  USER;
  constructor(private modalService: NzModalService,
    private notificationService: NzNotificationService,
    private service: TableQueueService,
    private session: LoginService,
    private http: HttpClient
  ) { }
  GetNameBranch() {
    this.service.GetBranch().subscribe(
      (res: any[]) => {
        this.BranchList = res.filter(x => x.active === "Y")
        if (this.USER.levelUsers != 1) {
        }
      }
    )
  }

  SelectTable() {
    if (this.USER == null) {
      this.service.GetTableQueue(this.BranchId, this.date).subscribe(
        (res) => {
          this.ShowTableQ = res[0] as any[];
        }
      )
      this.service.GetBranchDetail(this.BranchId).subscribe((res) => {
        this.StatusBranch = res[0].status
      })
    }
    else if (this.USER.levelUsers == 1) {
      this.BranchId = this.USER.branch_id
      this.service.GetTableQueue(this.USER.branch_id, this.date).subscribe(
        (res) => {
          this.ShowTableQ = res[0] as any[];
        }
      )
      this.service.GetBranchDetail(this.USER.branch_id).subscribe((res) => {
        this.StatusBranch = res[0].status
      })
    }
    else {

      this.service.GetTableQueue(this.BranchId, this.date).subscribe(
        (res) => {
          this.ShowTableQ = res[0] as any[];
        }
      )
      this.service.GetBranchDetail(this.BranchId).subscribe((res) => {
        this.StatusBranch = res[0].status
      })
    }

  }

  ngOnInit() {
    this.GetNameBranch();
    this.USER = this.session.getActiveUser();
    this.SelectTable()
    document.getElementById("content").classList.remove("p-0")

  }
  NoLogin() {
    Swal.fire({
      html: '<p>กรุณาทำการสมัครสมาชิกที่สาขา หรือ เข้าสู่ระบบ!</p>',
    })
  }
  reservationsOpenFrom() {
    const modal = this.modalService.create({
      nzTitle: '<h5 class="modal-title">แบบฟอร์มการจองคิว</h5>',
      nzContent: ReservationsFromComponent,
      nzComponentParams: {
        BranchId: this.BranchId,
        BranchIdManager: this.USER.branch_id,
        levelUsers: this.USER.levelUsers,
      },
      nzClosable: true,
      nzWidth: 600,
      nzFooter: [
        {
          label: 'ยกเลิก',
          shape: 'default',
          onClick: () => modal.destroy()
        },
        {
          label: 'ยืนยันการจอง',
          type: 'primary',
          onClick: () => new Promise(resolve => setTimeout(resolve, 0)).then(
            async () => {
              const instance = modal.getContentComponent();
              
              
              let CountFrom = instance.listOfControl.length
              let CountLock = instance.serviceIsDisable.filter(x => x == true).length
              if (CountFrom != instance.serviceid.length)
              this.notificationService.create("error", `กรุณาเลือกบริการ`, null)
              if (CountFrom != instance.barberid.length)
              this.notificationService.create("error", "กรุณาเลือกช่างหรือเลือกไม่ระบุช่าง", null)
              if (CountFrom != instance.TimeStart.length)
              this.notificationService.create("error", "กรุณาเลือกเวลาเริ่มทำบริการ", null)
              else if (CountFrom == instance.serviceid.length && CountFrom == instance.barberid.length && CountFrom == instance.TimeStart.length) {
                if (CountFrom == CountLock) {
                  this.showConfirm(instance);
                  modal.destroy()
                }
                else
                  this.notificationService.create("error", "กรุณากดปุ่มล็อกคิว เพื่อเป็นการยืนยันก่อน!", null)
              }
            }
          )
        }
      ]
    })
  }

  showConfirm(instance) {
    var day = instance.datatimes.getDate(); var month = (instance.datatimes.getMonth() + 1); var year = instance.datatimes.getFullYear()
    var CountFrom = instance.listOfControl.length;
    var HtmlFrom;
    var TotalMoney;
    var TotalTime;
    switch (CountFrom) {
      case 1: {
        TotalMoney = (instance.service_charge[0])
        TotalTime = (instance.TakeTime[0])
        HtmlFrom = `<div nz-col nzSpan="12" style="margin: 25px"><span>คุณ</span> ${this.USER.firstname} ${this.USER.lastname}</div>
          <div style="border-style: solid;">
              <div nz-col nzSpan="12" >รายละเอียด</div>
              <div nz-col nzSpan="12" style="border-style: dashed;margin: 25px"><span>บริการ:</span> ${instance.serviceid[0].name}  <span>ช่าง:</span> ${instance.Nickname[0] == '' ? "ไม่ระบุช่าง" : instance.Nickname[0]} <br>
              <span>เวลา:</span> ${instance.TimeStart[0]}:00 - ${instance.TimeEnd[0].substring(0, 5)} <span>น.</span></div>
              <div nz-col nzSpan="12"><span>จำนวนเงินทั้งหมด:</span> ${TotalMoney.toLocaleString('en-us', {minimumFractionDigits: 0})} <span>บาท</span>     <span>ใช้เวลา:</span> ${TotalTime} <span>ชม.</span></div>
              <div nz-col nzSpan="12"><span>วันที่</span> ${day} <span>เดือน</span> ${month} <span>ปี</span> ${year}</div>
          </div>`
        break;
      }
      case 2: {
        TotalMoney = (instance.service_charge[0]) +
          (instance.service_charge[1])
        TotalTime = (instance.TakeTime[0]) +
          (instance.TakeTime[1])
        HtmlFrom = `<div nz-col nzSpan="12" style="margin: 25px"><span>คุณ</span> ${this.USER.firstname} ${this.USER.lastname}</div>
          <div style="border-style: solid;">
              <div nz-col nzSpan="12" >รายละเอียด</div>
              <div nz-col nzSpan="12" style="border-style: dashed;margin: 25px"><span>บริการ:</span> ${instance.serviceid[0].name}  <span>ช่าง:</span> ${instance.Nickname[0] == '' ? "ไม่ระบุช่าง" : instance.Nickname[0]} <br>
              <span>เวลา:</span> ${instance.TimeStart[0]}:00 - ${instance.TimeEnd[0].substring(0, 5)} <span>น.</span></div>
              <div nz-col nzSpan="12" style="border-style: dashed;margin: 25px"><span>บริการ:</span> ${instance.serviceid[1].name}  <span>ช่าง:</span> ${instance.Nickname[1] == '' ? "ไม่ระบุช่าง" : instance.Nickname[1]} <br>
              <span>เวลา:</span> ${instance.TimeStart[1]}:00 - ${instance.TimeEnd[1].substring(0, 5)} <span>น.</span></div>
              <div nz-col nzSpan="12"><span>จำนวนเงินทั้งหมด:</span> ${TotalMoney.toLocaleString('en-us', {minimumFractionDigits: 0})} <span>บาท</span>     <span>ใช้เวลา:</span> ${TotalTime} <span>ชม.</span></div>
              <div nz-col nzSpan="12"><span>วันที่</span> ${day} <span>เดือน</span> ${month} <span>ปี</span> ${year}</div>
          </div>`
        break;
      }
      case 3: {
        TotalMoney = (instance.service_charge[0]) +
          (instance.service_charge[1]) +
          (instance.service_charge[2]);
        TotalTime = (instance.TakeTime[0]) +
          (instance.TakeTime[1]) +
          (instance.TakeTime[2]);
        HtmlFrom = `<div nz-col nzSpan="12" style="margin: 25px"><span>คุณ</span> ${this.USER.firstname} ${this.USER.lastname}</div>
          <div style="border-style: solid;">
              <div nz-col nzSpan="12" >รายละเอียด</div>
              <div nz-col nzSpan="12" style="border-style: dashed;margin: 25px"><span>บริการ:</span> ${instance.serviceid[0].name}  <span>ช่าง:</span> ${instance.Nickname[0] == '' ? "ไม่ระบุช่าง" : instance.Nickname[0]} <br>
              <span>เวลา:</span> ${instance.TimeStart[0]}:00 - ${instance.TimeEnd[0].substring(0, 5)} <span>น.</span></div>
              <div nz-col nzSpan="12" style="border-style: dashed;margin: 25px"><span>บริการ:</span> ${instance.serviceid[1].name}  <span>ช่าง:</span> ${instance.Nickname[1] == '' ? "ไม่ระบุช่าง" : instance.Nickname[1]} <br>
              <span>เวลา:</span> ${instance.TimeStart[1]}:00 - ${instance.TimeEnd[1].substring(0, 5)} <span>น.</span></div>
              <div nz-col nzSpan="12" style="border-style: dashed;margin: 25px"><span>บริการ:</span> ${instance.serviceid[2].name}  <span>ช่าง:</span> ${instance.Nickname[2] == '' ? "ไม่ระบุช่าง" : instance.Nickname[2]} <br>
              <span>เวลา:</span> ${instance.TimeStart[2]}:00 - ${instance.TimeEnd[2].substring(0, 5)} <span>น.</span></div>
              <div nz-col nzSpan="12"><span>จำนวนเงินทั้งหมด:</span> ${TotalMoney.toLocaleString('en-us', {minimumFractionDigits: 0})} <span>บาท</span>     <span>ใช้เวลา:</span> ${TotalTime} <span>ชม.</span></div>
              <div nz-col nzSpan="12"><span>วันที่</span> ${day} <span>เดือน</span> ${month} <span>ปี</span> ${year}</div>
          </div>`
        break;
      }
    }
    Swal.fire({
      title: 'ยืนยันการจอง?',
      html: HtmlFrom,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'ยืนยัน',
      cancelButtonText: 'ยกเลิก'
    }).then((result) => {
      if (result.value) {
        this.submitForm(instance).subscribe(
          (res) => {
            Swal.fire({
              title: 'การจอง!',
              html: '<p>ส่งข้อมูลการจองสำเร็จ!</p>'
            })
            this.SelectTable()
          }
        )
      }
    })
  }
  submitForm(instance) {
    var date = instance.datatimes.getFullYear() + '-' + (instance.datatimes.getMonth() + 1) + '-' + instance.datatimes.getDate();
    var CountFrom = instance.TakeTime.length;
      var TotalMoney;
      var TotalTime;
      var QueueDetail1;
      switch (CountFrom) {
        case 1: {
          TotalMoney = (instance.service_charge[0])
          TotalTime = (instance.TakeTime[0])
          QueueDetail1 = [{
            id: 1,
            service_id: instance.serviceid[0].name,
            barber_id: instance.barberid[0],
            start_dt: `${instance.TimeStart[0]}:00:00`,
            finish_dt: instance.TimeEnd[0],
          },]
          break;
        }
        case 2: {
          TotalMoney = (instance.service_charge[0]) +
            (instance.service_charge[1] )
          TotalTime = (instance.TakeTime[0] ) +
            (instance.TakeTime[1])
          QueueDetail1 = [{
            id: 1,
            service_id: instance.serviceid[0].name,
            barber_id:instance.barberid[0],
            start_dt: `${instance.TimeStart[0]}:00:00`,
            finish_dt: instance.TimeEnd[0],
          },
          {
            id: 2,
            service_id: instance.serviceid[1].name,
            barber_id: instance.barberid[1],
            start_dt: `${instance.TimeStart[1]}:00:00`,
            finish_dt: instance.TimeEnd[1],
          },]
          break;
        }
        case 3: {
          TotalMoney = (instance.service_charge[0]) +
            (instance.service_charge[1]) +
            (instance.service_charge[2]);
          TotalTime = (instance.TakeTime[0]) +
            (instance.TakeTime[1]) +
            (instance.TakeTime[2]);
          QueueDetail1 = [{
            id: 1,
            service_id: instance.serviceid[0].name,
            barber_id: instance.barberid[0],
            start_dt: `${instance.TimeStart[0]}:00:00`,
            finish_dt: instance.TimeEnd[0],
          },
          {
            id: 2,
            service_id: instance.serviceid[1].name,
            barber_id: instance.barberid[1],
            start_dt: `${instance.TimeStart[1]}:00:00`,
            finish_dt: instance.TimeEnd[1],
          },
          {
            id: 3,
            service_id: instance.serviceid[2].name,
            barber_id: instance.barberid[2],
            start_dt: `${instance.TimeStart[2]}:00:00`,
            finish_dt: instance.TimeEnd[2],
          },]
          break;
        }
      }
      if (instance.levelUsers == 1) {
        let Queue: object = {
          branch_id: instance.BranchIdManager,
          customer_id: this.USER.username,
          total_time: TotalTime,
          total_money: TotalMoney,
          queue_dt: date,
          status_confirm: 1,
          status_come: instance.Express == true ? 1 : 0, /////การจองด่วน
          status_pay: 0,
          queue_detail: QueueDetail1,
        }
        return this.service.AddQueue(Queue)
      }
      else {
        let Queue: object = {
          branch_id: instance.BranchId,
          customer_id: this.USER.username,
          total_time: TotalTime,
          total_money: TotalMoney,
          queue_dt: date,
          status_confirm: 0,
          status_come: 0,
          status_pay: 0,
          queue_detail: QueueDetail1,
        }
        return this.service.AddQueue(Queue)
      }
  }

}
