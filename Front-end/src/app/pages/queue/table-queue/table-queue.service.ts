import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {urlServer} from '../../../URL'

@Injectable({
  providedIn: 'root'
})
export class TableQueueService {

  constructor(private http:HttpClient) { }

  public GetBranch(){
    return this.http.get(urlServer.ipServer + `ShowListBranch`)
  }
  public GetBranchDetail(Branch_id:number){
    return this.http.get(urlServer.ipServer+`BranchDetail/${Branch_id}`)
  }
  public GetTableQueue(Branch_id:number,date){
    return this.http.get(urlServer.ipServer+`GetQueue/${Branch_id}/${date}`)
  }
  public GetShowListService() {
    return this.http.get(urlServer.ipServer + `ShowListService`)
  }
  public GetListBarber(Branch_id){
    return this.http.get(urlServer.ipServer+`ShowListBarberbranch/${Branch_id}`)
  }
  public AddQueue(data){
    return this.http.post(urlServer.ipServer+`AddQueue`,data)
  }
  public ServiceDetail(name: string) {
    return this.http.get(urlServer.ipServer + `ServiceDetail/${name}`)
  }
  public SelectBarberBefor(barberid){
    return this.http.get(urlServer.ipServer+`SelectBarberBefor/${barberid}`)
  }
  public SelectServiceBefor(Branch_id,serviceid){
    return this.http.get(urlServer.ipServer+`SelectServiceBefor/${Branch_id}/${serviceid}`)
  }
  public chageService(serviceid,barberid,branch_id,date){
    return this.http.get(urlServer.ipServer + `ChageService/?service_id=${serviceid}&barber_id=${barberid}&branch_id=${branch_id}&date=${date}`)
  }
  public changeBarber(barberid,serviceid,branch_id,date){
    return this.http.get(urlServer.ipServer + `ChangeBarber/?barber_id=${barberid}&service_id=${serviceid}&branch_id=${branch_id}&date=${date}`)
  }
  public changeTimes(barber_id,date,timestart1,timestop1,timestart2,timestop2){
    return this.http.get(urlServer.ipServer + `ChangeTimes/?barber_id=${barber_id}&date=${date}&timestart1=${timestart1}&timestop1=${timestop1}&timestart2=${timestart2}&timestop2=${timestop2}`)
  }
  public GetbarberDetail(Barberid){
    return this.http.get(urlServer.ipServer + `DetailBarber/${Barberid}`)
  }
}
