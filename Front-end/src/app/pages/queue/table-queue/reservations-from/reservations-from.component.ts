import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, Validators, AbstractControl, FormBuilder } from '@angular/forms';
import { TableQueueService } from '../table-queue.service';
import { LoginService } from 'src/app/pages/login/login.service';
import Swal from 'sweetalert2';
import { differenceInCalendarDays } from 'date-fns'
import { DisabledTimeFn, DisabledTimePartial } from 'ng-zorro-antd/date-picker/standard-types';
import { BranchService } from 'src/app/pages/master/branch/branch.service';
import { isNullOrUndefined, debug } from 'util';
import { timeUnits } from 'ng-zorro-antd';


@Component({
  selector: 'app-reservations-from',
  templateUrl: './reservations-from.component.html',
  styleUrls: ['./reservations-from.component.css']
})
export class ReservationsFromComponent implements OnInit {
  @Input() BranchId: number;
  @Input() BranchIdManager: number;
  @Input() levelUsers: number;
  date = new Date;
  disabledDate = (current: Date): boolean => {
    let today = new Date
    // Can not select days before today and today
    return differenceInCalendarDays(current, today) < 0;
  };

  ShowListBarber: any[];
  ShowListBarber2 = [];
  HairList = [];
  NailList = [];
  FaceList = [];
  username: any;
  dateStemp;

  TypeDisabled = true;
  TimeStartDisabled = true;
  ServiceDisabled = true;
  DateDisabled = false;
  selectedTypeQueue;
  Express = false;

  branchName: string = "";
  serviceid: Array<any> = [];
  serviceName: string = null;
  barberid: Array<any> = [];
  barberName: any = null;
  TakeTime: Array<any> = [];
  service_charge: Array<any> = [];
  listBarber: any[];
  Nickname: Array<any> = [];

  timeIsDisable: Array<boolean> = [true, true, true];
  barberIsDisable: Array<boolean> = [false, false, false];
  serviceIsDisable: Array<boolean> = [false, false, false];
  lockIsbutton: Array<boolean> = [true, true, true];
  deleteIsbutton: Array<boolean> = [false, false, false];

  timeBarber: string = null;
  datatimes: Date = new Date;
  TimeStart: Array<any> = [];
  TimeEnd: Array<any> = [];
  TimeStratModel: string = null;
  disabledEndDate = (endValue: Date): boolean => {
    if (!endValue || !this.date) {
      return false;
    }
    return endValue.getTime() <= this.date.getTime();
  };
  constructor(private fb: FormBuilder,
    private service: TableQueueService,
    private session: LoginService,
    private serviceBranch: BranchService
  ) { }

  ngOnInit() {
    this.validateForm = this.fb.group({});
    this.addField();
    this.username = this.session.getActiveUser();
    this.serviceBranch.GetBranchDetail(this.BranchId).subscribe(res => this.branchName = res[0].name)
  }
  DateChange(event, flag) {
    this.datatimes = event;
    if (flag === 'clear') {
      let Event = event;
      let Length = this.listOfControl.length;
      for (let index = 0; index < Length; index++) {
        let id = { id: index, usernameBarber: `usernameBarber${index}`, Service_id: `Service_id${index}`, TimeStart: `TimeStart${index}`, TimeEnd: `TimeEnd${index}` }
        this.removeField(id, Event, index, flag)
      }
      this.timeIsDisable = [true, true, true];
      this.barberIsDisable = [false, false, false];
      this.serviceIsDisable = [false, false, false];
      this.lockIsbutton = [true, true, true];
      this.deleteIsbutton = [false, false, false];
      this.barberid = []; this.serviceid = []; this.TimeStart = []; this.TimeEnd = []; this.TakeTime = []; this.service_charge = [];; this.Nickname = [];
      this.addField()
    }
  }


  /////////////////////////////////validateForm1//////////////////////////////////////\
  validateForm: FormGroup;
  listOfControl: Array<{ id: number; usernameBarber: string; Service_id: string; TimeStart: string, TimeEnd: string }> = [];
  addField(e?: MouseEvent): void {
    if (e) {
      // e.preventDefault();
    }
    if (this.listOfControl.length != 3) {
      const id = this.listOfControl.length > 0 ? this.listOfControl[this.listOfControl.length - 1].id + 1 : 0;
      const control = {
        id,
        usernameBarber: `usernameBarber${id}`,
        Service_id: `Service_id${id}`,
        TimeStart: `TimeStart${id}`,
        TimeEnd: `TimeEnd${id}`
      };

      const index = this.listOfControl.push(control);
      this.validateForm.addControl(
        this.listOfControl[index - 1].usernameBarber,
        new FormControl(null),
      );
      this.validateForm.addControl(
        this.listOfControl[index - 1].Service_id,
        new FormControl(null),
      );
      this.validateForm.addControl(
        this.listOfControl[index - 1].TimeStart,
        new FormControl(null),
      );
      this.validateForm.addControl(
        this.listOfControl[index - 1].TimeEnd,
        new FormControl(null),
      );
      this.ControlDisable(index)
    }

  }

  removeField(i: { id: number; usernameBarber: string; Service_id: string; TimeStart: string, TimeEnd: string }, e: MouseEvent, index, flag): void {

    // e.preventDefault();
    if (this.listOfControl.length > 1) {
      const index = this.listOfControl.indexOf(i);
      this.listOfControl.splice(index, 1);
      this.validateForm.removeControl(i.usernameBarber);
      this.validateForm.removeControl(i.Service_id);
      this.validateForm.removeControl(i.TimeStart);
      this.serviceid.splice(index, 1); this.barberid.splice(index, 1); this.TimeStart.splice(index, 1);
      this.TimeEnd.splice(index, 1); this.TakeTime.splice(index, 1); this.service_charge.splice(index, 1);; this.Nickname.splice(index, 1);
      switch (index) {
        case 0: {
          this.timeIsDisable[0] = this.timeIsDisable[1] == true ? true : false; this.barberIsDisable[0] = this.barberIsDisable[1] == true ? true : false; this.serviceIsDisable[0] = this.serviceIsDisable[1] == true ? true : false;
          this.lockIsbutton[0] = this.lockIsbutton[1] == true ? true : false; this.deleteIsbutton[0] = this.deleteIsbutton[1] == true ? true : false; this.timeIsDisable[1] = this.timeIsDisable[2] == true ? true : false;
          this.barberIsDisable[1] = this.barberIsDisable[2] == true ? true : false; this.serviceIsDisable[1] = this.serviceIsDisable[2] == true ? true : false; this.lockIsbutton[1] = this.lockIsbutton[2] == true ? true : false;
          this.deleteIsbutton[1] = this.deleteIsbutton[2] == true ? true : false; this.timeIsDisable[2] = false; this.barberIsDisable[2] = false; this.serviceIsDisable[2] = false; this.lockIsbutton[2] = false; this.deleteIsbutton[2] = false; break;
        } case 1: {
          this.timeIsDisable[1] = this.timeIsDisable[2] == true ? true : false; this.barberIsDisable[1] = this.barberIsDisable[2] == true ? true : false; this.serviceIsDisable[1] = this.serviceIsDisable[2] == true ? true : false;
          this.lockIsbutton[1] = this.lockIsbutton[2] == true ? true : false; this.deleteIsbutton[1] = this.deleteIsbutton[2] == true ? true : false; this.timeIsDisable[2] = false; this.barberIsDisable[2] = false;
          this.serviceIsDisable[2] = false; this.lockIsbutton[2] = false; this.deleteIsbutton[2] = false; break;
        }
      }
    }
    else {
      if (flag === 'clear') {
        const index = this.listOfControl.indexOf(i);
        this.listOfControl.splice(index, 1);
        this.validateForm.removeControl(i.usernameBarber);
        this.validateForm.removeControl(i.Service_id);
        this.validateForm.removeControl(i.TimeStart);
        this.serviceid.splice(index, 1); this.barberid.splice(index, 1); this.TimeStart.splice(index, 1);
        this.TimeEnd.splice(index, 1); this.TakeTime.splice(index, 1);; this.service_charge.splice(index, 1); this.Nickname.splice(index, 1);
      }

    }

  }
  getFormControl(name: string): AbstractControl {
    return this.validateForm.controls[name];
  }

  GetObjectData(event, i, flag) {
    if (flag === "service") {
      if (event) {
        this.GetDataFromService(this.setDataString(this.serviceid, flag), i, flag, this.barberid.length == 0 ? "" : isNullOrUndefined(this.barberid[i]) ? "x" : this.barberid[i]);
      }
      else {
        if (!isNullOrUndefined(this.serviceName)) {
          this.serviceid[i] = this.serviceName;
          this.service.ServiceDetail(this.serviceid[i].name).subscribe(
            (res) => {
              this.TakeTime[i] = res[0].take_time
              this.service_charge[i] = res[0].service_charge
            }
          )
        }
        this.serviceName = null;
      }
    }
    else if (flag === "barber") {
      if (event) {
        this.GetDataFromService(this.setDataString(this.barberid, flag), i, flag, isNullOrUndefined(this.serviceid[i]) ? "" : this.serviceid[i].name);
      }
      else {
        if (!isNullOrUndefined(this.barberName)) {
          this.Nickname[i] = isNullOrUndefined(this.barberName.nickname) ? "" : this.barberName.nickname;
          this.barberid[i] = isNullOrUndefined(this.barberName.username) ? "" : this.barberName.username;
        }
        this.barberName = null;
      }
    }
    else if (flag = "time") {
      if (event) {
        this.GetDataFromService(this.barberid[i], i, flag, "");
      }
      else {
        this.TimeStart[i] = this.TimeStratModel;
        this.TimeStratModel = null;

        this.setTime(this.TimeStart[i], i);
      }
    }

    this.ControlDisable(i);
  }
  GetDataFromService(data, i, flag, dataFlag) {
    if (flag === "service") {
      this.service.chageService(data, dataFlag, this.BranchId, this.formatDate(this.datatimes)).subscribe(
        (res: any) => {
          this.HairList[i] = res.filter(x => x.type == 0);
          this.NailList[i] = res.filter(x => x.type == 1);
          this.FaceList[i] = res.filter(x => x.type == 2);
        })
    }
    else if (flag === "barber") {
      this.service.changeBarber(data, dataFlag, this.BranchId, this.formatDate(this.datatimes)).subscribe(
        (res: any[]) => {
          this.listBarber = res;
        }
      )
    }
    else if (flag === "time") {
      this.service.changeTimes(data, this.formatDate(this.datatimes), this.ConvertNumberToTime(this.TimeStart[0]), isNullOrUndefined(this.TimeEnd[0]) ? "" : this.TimeEnd[0], this.ConvertNumberToTime(i == 1 || i == 0 ? "" : this.TimeStart[1]), isNullOrUndefined(this.TimeEnd[1]) ? "" : this.TimeEnd[1]).subscribe(
        (res) => {
         
          this.timeBarber = res[0] as any;
        }
      )
    }

  }
  setTime(TimeStart, i) {
    this.TimeEnd[i] = null;
    switch (+TimeStart + this.TakeTime[i]) {
      case 9: this.TimeEnd[i] = "9:00:00"
        break;
      case 10: this.TimeEnd[i] = "10:00:00"
        break;
      case 11: this.TimeEnd[i] = "11:00:00"
        break;
      case 12: this.TimeEnd[i] = "12:00:00"
        break;
      case 13: this.TimeEnd[i] = "13:00:00"
        break;
      case 14: this.TimeEnd[i] = "14:00:00"
        break;
      case 15: this.TimeEnd[i] = "15:00:00"
        break;
      case 16: this.TimeEnd[i] = "16:00:00"
        break;
      case 17: this.TimeEnd[i] = "17:00:00"
        break;
      case 18: this.TimeEnd[i] = "18:00:00"
        break;
      case 19: this.TimeEnd[i] = "19:00:00"
        break;
      case 20: this.TimeEnd[i] = "20:00:00"
        break;
    }
  }
  setDataString(dataArray, flag) {
    let data = "''";
    if (flag === "service") {
      for (let index = 0; index < dataArray.length; index++) {
        try {
          data += ",'" + dataArray[index].name + "'";
        }
        catch (err) {
          data = "''";
        }
      }
    }
    else {
      for (let index = 0; index < dataArray.length; index++) {
        data += ",'" + dataArray[index] + "'";
      }
    }
    return data;
  }
  ControlDisable(i) {
    if ((!isNullOrUndefined(this.barberid[i]) || this.barberid[i] === "") && !isNullOrUndefined(this.serviceid[i])) {
      this.timeIsDisable[i] = false;
      if (!isNullOrUndefined(this.TimeEnd[i])) {
        this.lockIsbutton[i] = false;
      }
    }
    else
      this.timeIsDisable[i] = true;
  }
  LockButton(i) {
    if (!isNullOrUndefined(this.barberid[i]) && !isNullOrUndefined(this.serviceid[i]) && !isNullOrUndefined(this.TimeEnd[i])) {
      this.timeIsDisable[i] = true;
      this.barberIsDisable[i] = true;
      this.serviceIsDisable[i] = true;
      this.lockIsbutton[i] = true;
      this.deleteIsbutton[i] = true;
    }

  }
  formatDate(date) {
    let d = "";
    if (!isNullOrUndefined(date)) {
      let day = date.getDate().toString();
      let month = (date.getMonth() + 1).toString();
      let year = date.getFullYear().toString();
      d = year + "-" + month + "-" + day
    }
    return d;
  }
  ConvertNumberToTime(data) {
    let time = "";
    switch (data) {
      case "8": time = "08:00:00"
        break;
      case "9": time = "09:00:00"
        break;
      case "10": time = "10:00:00"
        break;
      case "11": time = "11:00:00"
        break;
      case "12": time = "12:00:00"
        break;
      case "13": time = "13:00:00"
        break;
      case "14": time = "14:00:00"
        break;
      case "15": time = "15:00:00"
        break;
      case "16": time = "16:00:00"
        break;
      case "17": time = "17:00:00"
        break;
      case "18": time = "18:00:00"
        break;
      case "19": time = "19:00:00"
        break;
      case "20": time = "20:00:00"
        break;
    }
    return time;
  }
}
