import { Injectable } from '@angular/core';
import {
  CanActivate, Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivateChild
} from '@angular/router';
import { LoginService } from '../login/login.service';
@Injectable({
  providedIn: 'root'
})
export class QueueGuardsService implements CanActivate {
  constructor(private _router: Router, private AuthenService: LoginService) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> | boolean {
    let QueueUser = this.AuthenService.getActiveUser();
    return new Promise<boolean>(resolve => {
      if (QueueUser != null) {
        if (QueueUser.levelUsers == 1) {
          resolve(true);
        } else {
          resolve(false);
          this._router.navigate(['']);
        }
      }
      else {
        resolve(false);
        this._router.navigate(['']);
      }
    });
  }

}
