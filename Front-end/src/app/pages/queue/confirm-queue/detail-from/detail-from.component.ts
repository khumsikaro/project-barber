import { Component, OnInit, Input } from '@angular/core';
import { ConfirmQueueService } from '../confirm-queue.service';
import { NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { ChangeBarberFromComponent } from '../change-barber-from/change-barber-from.component';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-detail-from',
  templateUrl: './detail-from.component.html',
  styleUrls: ['./detail-from.component.css']
})
export class DetailFromComponent implements OnInit {
  @Input() branch_id: number;
  @Input() queue_id: number;
  QueueDetaildata;
  constructor(private service: ConfirmQueueService,
    private modalService: NzModalService,
    private notificationService: NzNotificationService, ) { }

  ngOnInit() {
    this.GetQueueuDetail()
  }

  GetQueueuDetail() {
    this.service.GetQueueDetail(this.queue_id).subscribe(
      (res) => {
        this.QueueDetaildata = res as any;
      }
    )
  }


  openModal(data?: any) {
    const modal = this.modalService.create({
      nzTitle: '<h5 class="modal-title">เปลี่ยนช่าง</h5>',
      nzWidth: 700,
      nzContent: ChangeBarberFromComponent,
      nzComponentParams: {
        service_id: isNullOrUndefined(data) ? null : data.service_id,
        status_come: isNullOrUndefined(data) ? null : data.status_coome,
        barber_id: isNullOrUndefined(data) ? null : data.barber_id,
        customer_id: isNullOrUndefined(data) ? null : data.customer_id,
        detail_change_ber: isNullOrUndefined(data) ? null : data.detail_hange_ber,
        change_ber: isNullOrUndefined(data) ? null : data.change_ber,
        que_det_id: isNullOrUndefined(data) ? null : data.que_det_id,
        Nickname1: isNullOrUndefined(data) ? null : data.Nickname1,
        queue_dt:  isNullOrUndefined(data) ? null : data.queue_dt,
        start_dt: isNullOrUndefined(data) ? null : data.start_dt,
        finish_dt: isNullOrUndefined(data) ? null : data.finish_dt,


      },
      nzClosable: true,
      nzFooter: [
        {
          label: 'ยกเลิก',
          shape: 'default',
          onClick: () => modal.destroy()
        },
        {
          label: 'ยืนยัน',
          type: 'primary',
          onClick: () => new Promise(resolve => setTimeout(resolve, 0)).then(
            () => {
              const instance = modal.getContentComponent();
              return instance
            }
          ).then((instance) => {
            if(isNullOrUndefined(instance.change_ber))
            this.notificationService.create("error", "กรุณาเลือกช่างที่จะเปลี่ยน", null)
            if(isNullOrUndefined(instance.detail_change_ber)|| instance.detail_change_ber.trim()==='')
            this.notificationService.create("error", "กรุณาใส่เหตุผลการเปลี่ยน", null)
            else if(!isNullOrUndefined(instance.change_ber)&&!isNullOrUndefined(instance.detail_change_ber)&&instance.detail_change_ber.trim()!==''){
              this.EditChangeBarber(instance).subscribe(
                (res) => {
                  if (JSON.stringify(res)[11] === 's') {
                    this.notificationService.create("success", "เปลี่ยนช่างสำเร็จ", null)
                    modal.destroy();
                    this.GetQueueuDetail();
                  }
                }
              )
            }
          })
        }
      ]
    })
  }
  EditChangeBarber(data) {
    const value = {
      que_det_id: data.que_det_id,
      barber_id: data.barber_id,
      change_ber: data.change_ber,
      detail_change_ber: data.detail_change_ber
    }
    return this.service.ChangeBarber(value)
  }
}
