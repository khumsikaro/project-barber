import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import{urlServer} from '../../../URL'

@Injectable({
  providedIn: 'root'
})
export class ConfirmQueueService {

  constructor(private http:HttpClient) { }
 public GetQueue(Branchid){
   return this.http.get(urlServer.ipServer+`GetQueueCome/${Branchid}`)
 }
 public GetQueueDetail(id){
  return this.http.get(urlServer.ipServer+`GetQueueDetail/${id}`)
}
 public ComeQueue(data){
   return this.http.put(urlServer.ipServer+`ComeQueue`,data)
 }
 public ChangeBarber(data){
return this.http.put(urlServer.ipServer+`ChangeBarber`,data)
 }
 ///////////////////////ChangeBarber//////////////////////////////

 public GetBarberService(serviceid, branch_id,date,timeStart,timeStop) {
  return this.http.get(urlServer.ipServer + `ServiceBarberChange/${serviceid}/${branch_id}/${date}/${timeStart}/${timeStop}`)
}
 public GetcountBarber(branchid,serviceid,month){
  return this.http.get(urlServer.ipServer+`GetcountBarber/${branchid}/${serviceid}/${month}`)
 }
}
