import { Component, OnInit, Input } from '@angular/core';
import { ConfirmQueueService } from '../confirm-queue.service';
import { LoginService } from 'src/app/pages/login/login.service';

@Component({
  selector: 'app-change-barber-from',
  templateUrl: './change-barber-from.component.html',
  styleUrls: ['./change-barber-from.component.css']
})
export class ChangeBarberFromComponent implements OnInit {

  constructor(private service: ConfirmQueueService, private session: LoginService) { }
  @Input() service_id: string;
  @Input() status_come: number;
  @Input() barber_id: string;
  @Input() customer_id: string;
  @Input() detail_change_ber: string;
  @Input() change_ber: string;
  @Input() que_det_id: number;
  @Input() Nickname1: string
  @Input()  queue_dt:any;
  @Input() start_dt:any;
  @Input() finish_dt:any;
  berServicedata: any[];
  USER;
  ngOnInit() {
    this.USER = this.session.getActiveUser();
    this.ShowBarberService()
    this.GetcountBarber()
  }
  today = new Date();
  month = (this.today.getMonth() + 1) + '-' + this.today.getFullYear();
  GetcountBarberdata;
  GetcountBarber() {
    this.service.GetcountBarber(this.USER.branch_id, this.service_id,this.month).subscribe(
      (res) => {
        this.GetcountBarberdata = res as any;
      }
    )
  }
  ShowBarberService() {
    let ToDate=new Date(this.queue_dt)
    let datetoday= ToDate.getFullYear() + '-' + (ToDate.getMonth() + 1) + '-' + ToDate.getDate();
    this.service.GetBarberService(this.service_id,this.USER.branch_id,datetoday,this.start_dt,this.finish_dt).subscribe(
      (res)=>{

        this.berServicedata = res as any;

      }
    )
  }
}
