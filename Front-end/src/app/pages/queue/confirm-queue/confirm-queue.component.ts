import { Component, OnInit } from '@angular/core';
import { ConfirmQueueService } from './confirm-queue.service';
import { NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { ChangeBarberFromComponent } from './change-barber-from/change-barber-from.component';
import { isNullOrUndefined } from 'util';
import { LoginService } from '../../login/login.service';
import { DetailFromComponent } from './detail-from/detail-from.component';

@Component({
  selector: 'app-confirm-queue',
  templateUrl: './confirm-queue.component.html',
  styleUrls: ['./confirm-queue.component.css']
})
export class ConfirmQueueComponent implements OnInit {
  QueueConfrimdata;
  USER;
  constructor(private service: ConfirmQueueService,
    private modalService: NzModalService,
    private notificationService: NzNotificationService,
    private session: LoginService, ) { }

  ngOnInit() {
    this.USER = this.session.getActiveUser();
    this.GetQueueCome()
    document.getElementById("content").classList.remove("p-0")
  }

  GetQueueCome() {
    this.service.GetQueue(this.USER.branch_id).subscribe(
      (res) => {
        this.QueueConfrimdata = res as any;
      }
    )
  }
  StatusCome(data) {
    const value = {
      queue_id: data.queue_id,
      status_come: 1,
    }
    this.modalService.confirm({
      nzTitle: '<h5>คุณต้องการยืนยันการมาใช่หรือไม่?</h5>',
      nzContent: `คุณ${data.firstname} ${data.lastname}`,
      nzOnOk: () => this.service.ComeQueue(value).subscribe((res) => {
        this.GetQueueCome();
        this.notificationService.create("success","ยืนยันการมาสำเร็จ", null)
      })

    });


  }
  StatusNotCome(data) {
    const value = {
      queue_id: data.queue_id,
      status_come:2,
    }
    this.modalService.confirm({
      nzTitle:  `คุณ${data.firstname} ${data.lastname}`,
      nzContent: '<h5>ไม่ได้มาตามเวลา?</h5>',
      nzOnOk: () => this.service.ComeQueue(value).subscribe((res) => {
        this.GetQueueCome();
        this.notificationService.create("success","อัพเดทสถานะไม่มา", null)
      })

    });


  }

  OpenDetail(data?: any) {
    const modal = this.modalService.create({
      nzTitle: '<h5 class="modal-title">รายละเอียดการจอง</h5>',
      nzWidth: 700,
      nzContent: DetailFromComponent,
      nzComponentParams: {
        branch_id: isNullOrUndefined(data) ? null : data.branch_id,
        queue_id: isNullOrUndefined(data) ? null : data.queue_id,
      },
      nzClosable: true,
      nzFooter: null,
    })
  }
}
