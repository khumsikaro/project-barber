import { Component, OnInit, Injectable } from '@angular/core';
import { ApproveQueueService } from './approve-queue.service';
import { NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { LoginService } from '../../login/login.service';
import { isNullOrUndefined } from 'util';
import { DetailApproveQComponent } from './detail-approve-q/detail-approve-q.component';
import Swal from 'sweetalert2'
import { PagesComponent } from '../../pages.component';


@Component({
  selector: 'app-approve-queue',
  templateUrl: './approve-queue.component.html',
  styleUrls: ['./approve-queue.component.css']
})
export class ApproveQueueComponent implements OnInit {
  // [x: string]: any;
  QueueList: any[];
  USER;
  constructor(private service: ApproveQueueService,
    private session: LoginService,
    private modalService: NzModalService,
    private notificatioonService: NzNotificationService,
    private PageBadge: PagesComponent,
  ) { }

  ngOnInit() {
    this.USER = this.session.getActiveUser();
    this.GetQueue(this.USER.branch_id);
    document.getElementById("content").classList.remove("p-0")
  }
  GetQueue(branchid) {
    this.service.GetQueueList(branchid).subscribe(
      (res) => {
        this.QueueList = res as any[];
        this.PageBadge.ngOnInit()
      }
    )
  }

  openModal(data?: any) {
    const modal = this.modalService.create({
      nzContent: DetailApproveQComponent,
      nzWidth: 1800,
      nzComponentParams: {
        queue_id: isNullOrUndefined(data) ? null : data.queue_id,
        branch_id: isNullOrUndefined(data) ? null : data.branch_id,
        Email: isNullOrUndefined(data) ? null : data.Email,
        firstname: isNullOrUndefined(data) ? null : data.firstname,
        lastname: isNullOrUndefined(data) ? null : data.lastname,
        customer_id: isNullOrUndefined(data) ? null : data.customer_id,
        queue_dt: isNullOrUndefined(data) ? null : data.queue_dt,
        instance: data,

      },
      nzClosable: true,
      nzFooter: [
        {
          label: 'ไม่อนุญาติ',
          shape: 'dashed',
          onClick: () => {
            const instance = modal.getContentComponent();
            this.ApproveOrNot('No', instance).subscribe(
              (res) => {
                var mailOptions = {
                  from: this.USER.Email,
                  to: instance.Email,
                  subject: `ร้าน Hair 2 Hair ไม่อนุมัติการจองคิว `,
                  text: `<B>เรื่อง:</B>การจองคิว <br>คิวของคุณ <u>${instance.firstname}&nbsp;${instance.lastname}</u> ไม่ได้รับการอนุมัติ`
                };
                this.service.SendEmail(mailOptions).subscribe(
                  (res) => { })
                if (JSON.stringify(res)[11] === 's') {
                  this.notificatioonService.create("error", "ไม่อนุญาติ", null)
                  modal.destroy();
                  this.GetQueue(this.USER.branch_id)
                }

              }
            )
          }
        },
        {
          label: 'อนุญาติ',
          type: 'primary',
          onClick: () => new Promise(resolve => setTimeout(resolve, 0)).then(
            () => {
              const instance = modal.getContentComponent();
              return instance;
            }
          ).then((instance) => {
          
            
            let LengthQueueDetail = instance.DetailApproveQ.length;
            switch (LengthQueueDetail) {
              case 1: {
                if (instance.DetailApproveQ[0].barber_id !== '') {
                  this.ApproveOrNot('Approve', instance).subscribe(
                    (res) => {
                      let Fulldate = new Date(instance.DetailApproveQ[0].queue_dt)
                      var date = Fulldate.getDate() + '-' + (Fulldate.getMonth() + 1) + '-' + Fulldate.getFullYear();
                      let taxt = `<B>ร้าน Hair 2 Hair  </B> <br>
                      คิวของคุณ : <u>${instance.firstname}&nbsp;${instance.lastname}</u><b> ได้รับการอนุมัติแล้ว </b><br>
                      <b>รายละเอียด : </b> <br> 
                      <b>บริการ : </b>${instance.DetailApproveQ[0].service_id} <b>ทำโดยช่าง</b> ${instance.DetailApproveQ[0].Nickname1} <br>
                      <b>เวลาเริ่ม : </b> ${instance.DetailApproveQ[0].start_dt} ณ วันที่ : ${date} <br>
                      <b style="color:red;">*กรุณามาก่อนเวลา 15 นาที ถ้ามาช้ากว่าเวลาถือว่าสละสิทธิ <br> `
                      var mailOptions = {
                        from: this.USER.Email,
                        to: instance.Email,
                        subject: `ร้าน Hair 2 Hair ยืนยันการจองคิว`,
                        text: taxt
                      };
                      this.service.SendEmail(mailOptions).subscribe(
                        (res) => { })
                      if (JSON.stringify(res)[11] === 's') {
                        this.notificatioonService.create("success", "การจองคิวได้รับการอนุมัติ", null)
                        modal.destroy();
                        this.GetQueue(this.USER.branch_id)
                      }
                    }
                  )
                }
                else {
                  this.notificatioonService.create("error", "กรุณาเลือกช่างให้ครบก่อน", null)
                }
                break;
              }
              case 2: {
                if (instance.DetailApproveQ[0].barber_id !== '' && instance.DetailApproveQ[1].barber_id !== '') {
                  this.ApproveOrNot('Approve', instance).subscribe(
                    (res) => {
                      let Fulldate = new Date(instance.DetailApproveQ[0].queue_dt)
                      var date = Fulldate.getDate() + '-' + (Fulldate.getMonth() + 1) + '-' + Fulldate.getFullYear();
                      let taxt = `<B>ร้าน Hair 2 Hair  </B> <br>
                      คิวของคุณ : <u>${instance.firstname}&nbsp;${instance.lastname}</u><b> ได้รับการอนุมัติแล้ว </b><br>
                      <b>รายละเอียด : </b> <br> 
                      <b>บริการ : </b>${instance.DetailApproveQ[0].service_id} <b>ทำโดยช่าง</b> ${instance.DetailApproveQ[0].Nickname1} <br>
                      <b>บริการ : </b>${instance.DetailApproveQ[1].service_id} <b>ทำโดยช่าง</b> ${instance.DetailApproveQ[1].Nickname1} <br>
                      <b>เวลาเริ่ม : </b> ${instance.DetailApproveQ[0].start_dt} ณ วันที่ : ${date} <br>
                      <b style="color:red;">*กรุณามาก่อนเวลา 15 นาที ถ้ามาช้ากว่าเวลาถือว่าสละสิทธิ <br> `
                      var mailOptions = {
                        from: this.USER.Email,
                        to: instance.Email,
                        subject: `ร้าน Hair 2 Hair ยืนยันการจองคิว`,
                        text: taxt
                      };
                      this.service.SendEmail(mailOptions).subscribe(
                        (res) => { })
                      if (JSON.stringify(res)[11] === 's') {
                        this.notificatioonService.create("success", "การจองคิวได้รับการอนุมัติ", null)
                        modal.destroy();
                        this.GetQueue(this.USER.branch_id)
                      }
                    }
                  )
                }
                else {
                  this.notificatioonService.create("error", "กรุณาเลือกช่างให้ครบก่อน", null)
                }
                break;
              }
              case 3: {
                if (instance.DetailApproveQ[0].barber_id !== '' && instance.DetailApproveQ[1].barber_id !== '' && instance.DetailApproveQ[2].barber_id !== '') {
                  this.ApproveOrNot('Approve', instance).subscribe(
                    (res) => {
                      let Fulldate = new Date(instance.DetailApproveQ[0].queue_dt)
                      var date = Fulldate.getDate() + '-' + (Fulldate.getMonth() + 1) + '-' + Fulldate.getFullYear();
                      let taxt = `<B>ร้าน Hair 2 Hair  </B> <br>
                      คิวของคุณ : <u>${instance.firstname}&nbsp;${instance.lastname}</u><b> ได้รับการอนุมัติแล้ว </b><br>
                      <b>รายละเอียด : </b> <br> 
                      <b>บริการ : </b>${instance.DetailApproveQ[0].service_id} <b>ทำโดยช่าง</b> ${instance.DetailApproveQ[0].Nickname1} <br>
                      <b>บริการ : </b>${instance.DetailApproveQ[1].service_id} <b>ทำโดยช่าง</b> ${instance.DetailApproveQ[1].Nickname1} <br>
                      <b>บริการ : </b>${instance.DetailApproveQ[2].service_id} <b>ทำโดยช่าง</b> ${instance.DetailApproveQ[2].Nickname1} <br>
                      <b>เวลาเริ่ม : </b> ${instance.DetailApproveQ[0].start_dt} ณ วันที่ : ${date} <br>
                      <b style="color:red;">*กรุณามาก่อนเวลา 15 นาที ถ้ามาช้ากว่าเวลาถือว่าสละสิทธิ <br> `
                      var mailOptions = {
                        from: this.USER.Email,
                        to: instance.Email,
                        subject: `ร้าน Hair 2 Hair ยืนยันการจองคิว`,
                        text: taxt
                      };
                      this.service.SendEmail(mailOptions).subscribe(
                        (res) => { })
                      if (JSON.stringify(res)[11] === 's') {
                        this.notificatioonService.create("success", "การจองคิวได้รับการอนุมัติ", null)
                        modal.destroy();
                        this.GetQueue(this.USER.branch_id)
                      }
                    }
                  )
                }
                else {
                  this.notificatioonService.create("error", "กรุณาเลือกช่างให้ครบก่อน", null)
                }
                break;
              }
            }
          })
        },
      ]
    })
  }
  ApproveOrNot(type, data) {
    if (type === "Approve") {
      const value = {
        queue_id: data.queue_id,
        status_confirm: 1,
      }
      return this.service.EditeQueueConfrim(value)
    }
    else if (type === "No") {
      const value = {
        queue_id: data.queue_id,
        status_confirm: 2,
      }
      return this.service.EditeQueueConfrim(value)
    }
  }
}
