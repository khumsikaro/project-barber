import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { urlServer } from 'src/app/URL';

@Injectable({
  providedIn: 'root'
})
export class ApproveQueueService {

  constructor(private http: HttpClient) { }

  GetQueueList(Branch_id: number, ) {
    return this.http.get(urlServer.ipServer + `GetQueueConFirm/${Branch_id}`)
  }
  EditeQueueConfrim(data) {
    return this.http.put(urlServer.ipServer + `EditConfirmQueue`, data)
  }
  SendEmail(data) {
    return this.http.post(urlServer.ipServer + `Email`, data)
  }
  DetailApproveQ(queue_id: number) {
    return this.http.get(urlServer.ipServer + `GetQueueDetail/${queue_id}`)
  }


  public GetBarberService(serviceid, branch_id,date,timeStart,timeStop) {
    return this.http.get(urlServer.ipServer + `ServiceBarberChange/${serviceid}/${branch_id}/${date}/${timeStart}/${timeStop}`)
  }
  public GetcountBarber(branch_id, serviceid, month) {
    return this.http.get(urlServer.ipServer + `GetcountBarber/${branch_id}/${serviceid}/${month}`)
  }

  public ChangeBarber(data) {
    return this.http.put(urlServer.ipServer + `AddBarberInQueue`, data)
  }
  public GetTableQueue(Branch_id: number, date) {
    return this.http.get(urlServer.ipServer + `GetQueue/${Branch_id}/${date}`)
  }
  public DeleteQueueDetail(queuedetailid){
    return this.http.delete(urlServer.ipServer+`DeleteQueueDetail/${queuedetailid}`)
  }
}
