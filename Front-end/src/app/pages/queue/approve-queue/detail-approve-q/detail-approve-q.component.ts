import { Component, OnInit, Input, Injectable } from '@angular/core';
import { ApproveQueueService } from '../approve-queue.service';
import { NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { FromChangeDetailApperoveComponent } from '../from-change-detail-apperove/from-change-detail-apperove.component';
import { isNullOrUndefined } from 'util';
import Swal from 'sweetalert2';
import { LoginService } from 'src/app/pages/login/login.service';
import { HttpClient } from '@angular/common/http';
import { urlServer } from '../../../../URL'
import { ApproveQueueComponent } from '../approve-queue.component';
@Component({
  selector: 'app-detail-approve-q',
  templateUrl: './detail-approve-q.component.html',
  styleUrls: ['./detail-approve-q.component.css']
})
export class DetailApproveQComponent implements OnInit {
  @Input() customer_id: string;
  @Input() Email: string;
  @Input() firstname: string;
  @Input() lastname: string;
  @Input() queue_id: number;
  @Input() branch_id: number;
  @Input() start_dt: number;
  @Input() queue_dt;
  @Input() instance;
  UserDetail: any
  DetailApproveQ;
  ShowTableQ
  USER
  constructor(private service: ApproveQueueService,
    private modealService: NzModalService,
    private notificationService: NzNotificationService,
    private session: LoginService,
    private http: HttpClient,
    private ApproveQueueComponent: ApproveQueueComponent
  ) { }
  ShowListDeailQ() {
    this.service.DetailApproveQ(this.queue_id).subscribe(
      (res: any) => {
        this.DetailApproveQ = res as any[]
        if (res.length == 0) {
          let element: HTMLElement = document.getElementsByClassName('ant-btn-dashed')[0] as HTMLElement;
          element.click();
        }
      }
    )
  }

  ngOnInit() {
    this.USER = this.session.getActiveUser();
    this.ShowListDeailQ()
    this.SelectTable(this.branch_id, this.queue_dt)

  }
  SelectTable(BranchId, date) {
    let dates = new Date(date)
    let datetoday = dates.getFullYear() + '-' + (dates.getMonth() + 1) + '-' + dates.getDate();
    this.service.GetTableQueue(BranchId, datetoday).subscribe(
      (res) => {
        this.ShowTableQ = res[0] as any[];
      }
    )
  }
  openModal(data?: any) {
    const modal = this.modealService.create({
      nzTitle: '<h5 class="modal-title">เพิ่มช่างให้</h5>',
      nzContent: FromChangeDetailApperoveComponent,
      nzComponentParams: {
        barber_id: isNullOrUndefined(data) ? null : data.barber_id,
        service_id: isNullOrUndefined(data) ? null : data.service_id,
        customer_id: this.customer_id,
        branch_id: this.branch_id,
        que_det_id: isNullOrUndefined(data) ? null : data.que_det_id,
        Nickname1: isNullOrUndefined(data) ? null : data.Nickname1,
        queue_dt: this.queue_dt,
        start_dt: isNullOrUndefined(data) ? null : data.start_dt,
        finish_dt: isNullOrUndefined(data) ? null : data.finish_dt,

      },
      nzClosable: true,
      nzFooter: [
        {
          label: 'ยกเลิก',
          shape: 'default',
          onClick: () => modal.destroy()
        },
        {
          label: 'ยืนยัน',
          type: 'primary',
          onClick: () => new Promise(resolve => setTimeout(resolve, 0)).then(
            () => {
              const instance = modal.getContentComponent();
              return instance
            }
          ).then((instance) => {
            this.EditChangeBarber(instance).subscribe(
              (res) => {
                if (JSON.stringify(res)[11] === 's') {
                  this.notificationService.create("success", "เพิ่มช่างสำเร็จ", null)
                  modal.destroy();
                  this.GetQueueuDetail();
                }
              }
            )
          })
        }
      ]
    })
  }

  GetQueueuDetail() {
    this.service.DetailApproveQ(this.queue_id).subscribe(
      (res) => {
        this.DetailApproveQ = res as any;
      }
    )
  }

  EditChangeBarber(data) {
    const value = {
      que_det_id: data.que_det_id,
      barber_id: data.barber_id,
    }
    return this.service.ChangeBarber(value)
  }
  DeleteQueueDetail(data) {
    this.modealService.confirm({
      nzTitle: '<h5 class = "model-title">ลบคิวบริการ<h5>',
      nzContent: 'ยืนยันการลบคิวบริการ' + data.service_id + 'ใช่หรือไม่ ?',
      nzClosable: true,
      nzCancelText: "ยกเลิก",
      nzOkText: "ยืนยัน",
      nzOnOk: () => new Promise(resolev => setTimeout(resolev, 100))
        .then(() => {
          this.service.DeleteQueueDetail(data.que_det_id).subscribe(
            (res) => {
              this.notificationService.create("success", "ลบข้อมูลสำเร็จ", null)
              this.ShowListDeailQ()
            }
          )
        }
        )
    })
  }
}
