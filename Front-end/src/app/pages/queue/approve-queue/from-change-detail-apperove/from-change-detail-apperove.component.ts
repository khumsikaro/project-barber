import { Component, OnInit, Input, Injectable } from '@angular/core';
import { ApproveQueueService } from '../approve-queue.service';
import { LoginService } from 'src/app/pages/login/login.service';

@Component({
  selector: 'app-from-change-detail-apperove',
  templateUrl: './from-change-detail-apperove.component.html',
  styleUrls: ['./from-change-detail-apperove.component.css']
})
export class FromChangeDetailApperoveComponent implements OnInit {

  constructor(private service: ApproveQueueService, private session: LoginService, ) { }
  @Input() service_id: string;
  @Input() branch_id: number;
  @Input() status_come: number;
  @Input() barber_id: string;
  @Input() customer_id: string;
  @Input() que_det_id: number;
  @Input() Nickname1: string;
  @Input() barber_name: string;
  @Input() queue_dt: any;
  @Input() start_dt: any
  @Input() finish_dt: any;

  berServicedata: any[];
  GetcountBarberdata;
  USER;
  today = new Date();
 

  ngOnInit() {
    this.USER = this.session.getActiveUser();
    this.ShowBarberService()
    this.GetcontBarber()
  }
  ShowBarberService() {
    let ToDate=new Date(this.queue_dt)
    let datetoday= ToDate.getFullYear() + '-' + (ToDate.getMonth() + 1) + '-' + ToDate.getDate();
    this.service.GetBarberService(this.service_id,this.USER.branch_id,datetoday,this.start_dt,this.finish_dt).subscribe(
      (res)=>{

        this.berServicedata = res as any;

      }
    )
  }

  GetcontBarber() {
   let month = (this.today.getMonth() + 1) + '-' + this.today.getFullYear();

    this.service.GetcountBarber(this.USER.branch_id, this.service_id,month).subscribe(
      (res) => {
        this.GetcountBarberdata = res as any;
      }

    )
  }

}
