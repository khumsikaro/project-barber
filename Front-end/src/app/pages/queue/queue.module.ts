import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { QueueRoutingModule } from './queue-routing.module';
import { QueueComponent } from './queue.component';
import { TableQueueComponent } from './table-queue/table-queue.component';
import { ApproveQueueComponent } from './approve-queue/approve-queue.component';
import { ConfirmQueueComponent } from './confirm-queue/confirm-queue.component';
import { PayComponent } from './pay/pay.component';
import { FeedbackCustomerComponent } from './pay/feedback-customer/feedback-customer.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ZorroModule } from 'src/app/lib.ngZorro';
import { ReservationsFromComponent } from './table-queue/reservations-from/reservations-from.component';
import { ChangeBarberFromComponent } from './confirm-queue/change-barber-from/change-barber-from.component';
import { DetailFromComponent } from './confirm-queue/detail-from/detail-from.component';
import { DetailPayFromComponent } from './pay/detail-pay-from/detail-pay-from.component';
import { EditQueueDetailComponent } from './pay/edit-queue-detail/edit-queue-detail.component';
import { DetailApproveQComponent } from './approve-queue/detail-approve-q/detail-approve-q.component';
import { FromChangeDetailApperoveComponent } from './approve-queue/from-change-detail-apperove/from-change-detail-apperove.component';
import { GuardService } from '../login/guard.service';
import { QueueGuardsService } from './queue-guards.service';
import { ApproveQueueService } from './approve-queue/approve-queue.service';
import { PagesComponent } from '../pages.component';



@NgModule({
  declarations: [
    QueueComponent,
     TableQueueComponent, 
     ApproveQueueComponent, 
     ConfirmQueueComponent, 
     PayComponent, 
     FeedbackCustomerComponent, 
     ReservationsFromComponent, 
     DetailApproveQComponent,      
     ChangeBarberFromComponent, 
     DetailFromComponent, 
     DetailPayFromComponent,  
     FromChangeDetailApperoveComponent, 
     EditQueueDetailComponent,],
  imports: [
    CommonModule,
    QueueRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ZorroModule,  
    NzToolTipModule,

  ],
  entryComponents:[
    ReservationsFromComponent,
    DetailApproveQComponent,
    ChangeBarberFromComponent,
    DetailFromComponent,
    DetailPayFromComponent,
    FeedbackCustomerComponent,
    EditQueueDetailComponent,
    FromChangeDetailApperoveComponent,
    ApproveQueueComponent,
  ],
  providers: [
    QueueGuardsService,ApproveQueueComponent,PagesComponent
  ]
})
export class QueueModule { }
