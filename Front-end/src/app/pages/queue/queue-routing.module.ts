import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QueueComponent } from './queue.component';
import { ApproveQueueComponent } from './approve-queue/approve-queue.component';
import { ConfirmQueueComponent } from './confirm-queue/confirm-queue.component';
import { PayComponent } from './pay/pay.component';
import { TableQueueComponent } from './table-queue/table-queue.component';
import { FeedbackCustomerComponent } from './pay/feedback-customer/feedback-customer.component';
import { GuardService } from '../login/guard.service';
import { QueueGuardsService } from './queue-guards.service';


const routes: Routes = [
  {
    path:'Queue',
    component:QueueComponent,
    children:[
     {
      path:'ApproveQueue',
      component:ApproveQueueComponent,
      canActivate:[QueueGuardsService],
     },
     {
      path:'ConfirmQueue',
      component:ConfirmQueueComponent,
      canActivate:[QueueGuardsService],
     },
     {
      path:'Pay',
      component:PayComponent,
      canActivate:[QueueGuardsService],
     },
     {
      path:'Pay/FeedbackCustomer',
      component:FeedbackCustomerComponent,
      canActivate:[QueueGuardsService],
     },
     {
      path:'TableQueue',
      component:TableQueueComponent
     },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QueueRoutingModule { }
