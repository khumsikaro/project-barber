import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BarberdetailRoutingModule } from './barberdetail-routing.module';
import { BarberdetailComponent } from './barberdetail.component';
import { ZorroModule } from 'src/app/lib.ngZorro';


@NgModule({
  declarations: [BarberdetailComponent],
  imports: [
    CommonModule,
    BarberdetailRoutingModule,
    ZorroModule,
  ]
})
export class BarberdetailModule { }
