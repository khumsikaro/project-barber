import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BarberdetailComponent } from './barberdetail.component';


const routes: Routes = [
  {
    path:'BarberDetail/:username',
    component:BarberdetailComponent,
  },
  {
    path:'',
    redirectTo:'BarberDetail',
    pathMatch:'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BarberdetailRoutingModule { }
