import { Component, OnInit } from '@angular/core';
import { UserService } from '../../master/user/user.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-barberdetail',
  templateUrl: './barberdetail.component.html',
  styleUrls: ['./barberdetail.component.css']
})
export class BarberdetailComponent implements OnInit {
  DetailBarber: any[];
  Skillhairdata;
  Skillnaildata;
  Skillfacedata;
  UsernameBarber: string;

  constructor(private service: UserService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.UsernameBarber = this.route.snapshot.params.username
    this.ShowDetailBarber();
    this.GetSkill()
    this.CheckWork()
    document.getElementById("content").classList.remove("p-0")
  }

  ShowDetailBarber() {
    this.service.GetBarberDetail(this.UsernameBarber).subscribe(
      (res) => {
        this.DetailBarber = res[0] as any[];
      }
    )
  }
  GetSkill() {
    this.service.Getskill(this.UsernameBarber).subscribe((res: any[]) => {
      this.Skillhairdata = res.filter(x => x.type == 0)
      this.Skillnaildata = res.filter(x => x.type == 1)
      this.Skillfacedata = res.filter(x => x.type == 2)
    })
  }
  StatusCheckWork;
  StatusCheckWorkcome;
  StatusCheckWorkAwol;
  CheckWork(){
    this.service.GetListCheckWork(this.UsernameBarber).subscribe((res:any[])=>{
      this.StatusCheckWork =res.length
      if(res.length!=0){
        this.StatusCheckWorkcome=res[0].start_work
        this.StatusCheckWorkAwol=res[0].status_awol
      }
    })
  }


}
