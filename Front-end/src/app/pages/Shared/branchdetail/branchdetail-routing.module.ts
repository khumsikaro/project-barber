import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BranchdetailComponent } from './branchdetail.component';


const routes: Routes = [
  {
    path:'BranchDetail/:branch_id',
    component:BranchdetailComponent,
  },
  {
    path:'',
    redirectTo:'BranchDetail',
    pathMatch:'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BranchdetailRoutingModule { }
