import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BranchService } from '../../master/branch/branch.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-branchdetail',
  templateUrl: './branchdetail.component.html',
  styleUrls: ['./branchdetail.component.css']
})
export class BranchdetailComponent implements OnInit {
  BranchData: any;
  Branch_id: number;
 
  array: any = [];
  MAP: string;
  constructor(private route: ActivatedRoute, private service: BranchService, private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.Branch_id = this.route.snapshot.params.branch_id
    this.ShowDetailBranch(this.Branch_id)
    document.getElementById("content").classList.remove("p-0")
  }
  ShowDetailBranch(Branchid) {
    this.service.GetBranchDetail(Branchid).subscribe(
      (res) => {
        this.BranchData = res as any[]
        const img = [
          this.BranchData[0].img1,
          this.BranchData[0].img2,
          this.BranchData[0].img3,
          this.BranchData[0].img4,
          this.BranchData[0].img5,
          this.BranchData[0].img6,
          this.BranchData[0].img7,
          this.BranchData[0].img8,
          this.BranchData[0].img9,
          this.BranchData[0].img10,
        ]
        for(var item in img){
          if(img[item] !=="undefined" && img[item] != ""){
            this.array.push(img[item])
          }
        }

      }
    )
  }
}
