import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BranchdetailRoutingModule } from './branchdetail-routing.module';
import { BranchdetailComponent } from './branchdetail.component';
import { ZorroModule } from 'src/app/lib.ngZorro';


@NgModule({
  declarations: [BranchdetailComponent],
  imports: [
    CommonModule,
    BranchdetailRoutingModule,
    ZorroModule,
    
  ]
})
export class BranchdetailModule { }
