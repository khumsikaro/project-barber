import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServicedetailRoutingModule } from './servicedetail-routing.module';
import { ServicedetailComponent } from './servicedetail.component';
import { ZorroModule } from 'src/app/lib.ngZorro';


@NgModule({
  declarations: [ServicedetailComponent],
  imports: [
    CommonModule,
    ServicedetailRoutingModule,
    ZorroModule,
  ]
})
export class ServicedetailModule { }
