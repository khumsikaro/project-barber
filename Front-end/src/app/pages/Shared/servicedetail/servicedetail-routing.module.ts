import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ServicedetailComponent } from './servicedetail.component';


const routes: Routes = [
  {
    path:'ServiceDetail/:name',
    component:ServicedetailComponent,
  },
  {
    path:'',
    redirectTo:'ServiceDetail',
    pathMatch:'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServicedetailRoutingModule { }
