import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServiceService } from '../../master/service/service.service';

@Component({
  selector: 'app-servicedetail',
  templateUrl: './servicedetail.component.html',
  styleUrls: ['./servicedetail.component.css']
})
export class ServicedetailComponent implements OnInit {
  ServiceDatadetail: any;
  ServiceName: string;
  array: any = [];

  constructor(private route: ActivatedRoute, private service: ServiceService) { }

  ngOnInit() {
    this.ServiceName = this.route.snapshot.params.name
    this.Showdetailservice(this.ServiceName)
    document.getElementById("content").classList.remove("p-0")
  }
  Showdetailservice(ServiceName) {
    this.service.GetServiceDetail(ServiceName).subscribe(
      (res) => {
        this.ServiceDatadetail = res as any[]
        const img = [
          this.ServiceDatadetail[0].img1,
          this.ServiceDatadetail[0].img2,
          this.ServiceDatadetail[0].img3,
          this.ServiceDatadetail[0].img4,
          this.ServiceDatadetail[0].img5,
          this.ServiceDatadetail[0].img6,
          this.ServiceDatadetail[0].img7,
          this.ServiceDatadetail[0].img8,
          this.ServiceDatadetail[0].img9,
          this.ServiceDatadetail[0].img10,
        ]
        for(var item in img){
          if(img[item] !=="undefined" && img[item] != ""){
            this.array.push(img[item])
          }
        }
      }
    )
  }
}
