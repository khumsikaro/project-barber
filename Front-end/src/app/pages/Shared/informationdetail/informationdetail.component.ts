import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { InformationService } from '../../master/information/information.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-informationdetail',
  templateUrl: './informationdetail.component.html',
  styleUrls: ['./informationdetail.component.css']
})
export class InformationdetailComponent implements OnInit {
  NewsDatadetail: any;
  News_ID: number;
  array: any = [];
  Type;
  title;
  create_dt;
  detail;

  constructor(private route: ActivatedRoute, private service: InformationService, private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.News_ID = this.route.snapshot.params.news_id
    this.Showdetailnews()
    document.getElementById("content").classList.remove("p-0")
  }

  Showdetailnews() {
    this.service.GetInformationDetails(this.News_ID).subscribe(
      (result) => {
        this.NewsDatadetail = result[0] as any
        this.Type = this.NewsDatadetail.type
        this.title=this.NewsDatadetail.title
        this.create_dt=this.NewsDatadetail.create_dt
        this.detail =this.NewsDatadetail.detail
        const img = [
          this.NewsDatadetail.img1,
          this.NewsDatadetail.img2,
          this.NewsDatadetail.img3,
          this.NewsDatadetail.img4,
          this.NewsDatadetail.img5,
          this.NewsDatadetail.img6,
          this.NewsDatadetail.img7,
          this.NewsDatadetail.img8,
          this.NewsDatadetail.img9,
          this.NewsDatadetail.img10,
        ]
        for (var item in img) {
          if (img[item] !== "undefined" && img[item] != "") {
            this.array.push(img[item])
          }
        }
      }
    )
  };
  sanitize(url: string) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

}
