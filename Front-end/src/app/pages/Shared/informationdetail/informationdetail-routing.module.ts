import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InformationdetailComponent } from './informationdetail.component';


const routes: Routes = [
  {
    path:'InformationDetail/:news_id',
    component:InformationdetailComponent,
  },
  {
    path:'',
    redirectTo:'InformationDetail',
    pathMatch:'full'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InformationdetailRoutingModule { }
