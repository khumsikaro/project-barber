import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InformationdetailRoutingModule } from './informationdetail-routing.module';
import { InformationdetailComponent } from './informationdetail.component';
import { ZorroModule } from 'src/app/lib.ngZorro';


@NgModule({
  declarations: [InformationdetailComponent],
  imports: [
    CommonModule,
    InformationdetailRoutingModule,
    ZorroModule,
  ]
})
export class InformationdetailModule { }
