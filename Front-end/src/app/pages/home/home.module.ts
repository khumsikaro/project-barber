import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home-routing.module';
import { ShowHomeComponent } from './show-home/show-home.component';
import { ZorroModule } from 'src/app/lib.ngZorro';
import { FormsModule } from '@angular/forms';
import { GuardService } from '../login/guard.service';
import { NgbAlertModule, NgbPaginationModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [ShowHomeComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    ZorroModule,
    FormsModule,
    NgbPaginationModule, 
    NgbAlertModule,
    NgbModule,
  ],
  providers: [
    GuardService
  ]
})
export class HomeModule { }
