import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShowHomeComponent } from './show-home/show-home.component';


const routes: Routes = [
  {
    path:'',
    component:ShowHomeComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
