import { Component, OnInit } from '@angular/core';
import { HomeService } from '../../master/home/home.service';
import { timer } from 'rxjs/internal/observable/timer';
import { urlServer } from '../../../URL'
import { HttpClient } from '@angular/common/http';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-show-home',
  templateUrl: './show-home.component.html',
  styleUrls: ['./show-home.component.css'],
  providers: [NgbCarouselConfig]
})
export class ShowHomeComponent implements OnInit {
  images = ["../../../../assets/image/S_queue.jpg", "../../../../assets/image/S_service.jpg", "../../../../assets/image/S_barber.jpg"];
  today = new Date
  AboutUsTital;
  AboutUsImg;
  AboutUsDetail;
  constructor(private service: HomeService, private http: HttpClient, config: NgbCarouselConfig) {
    config.interval = 10000;
    config.wrap = true;
    config.keyboard = false;
    config.pauseOnHover = false;
    config.showNavigationArrows = false;
  }
  ngOnInit() {
    this.http.get(urlServer.ipServer + `ShowListBranch`).subscribe((res: any[]) => {
      let arrBranch = res.filter(x => x.active === "Y");
      for (let i in arrBranch) {  this.GetListBarber(arrBranch[i].branch_id, this.today) }
    })
    this.GetAboutUs()
    this.GetInformation()
    this.GetListHome()
    document.getElementById("content").classList.add("p-0")
  }

  GetListBarber(Branch_id, Date) {
    this.http.get(urlServer.ipServer + `GetCheck/${Branch_id}/${Date}`).subscribe((res) => { })
  }
  GetAboutUs() {
    this.http.get(urlServer.ipServer + `GetAbout`).subscribe((res) => {
      this.AboutUsTital = res[0].detail
      this.AboutUsImg = res[1].detail
      this.AboutUsDetail = res[2].detail
    })
  }
  Informationdata
  GetInformation(){
    this.http.get(urlServer.ipServer + `GetInformation`).subscribe((res:any[])=>{
     let info=res.filter(x=>x.active==="Y")
     let Sum=[]
      for(let i=0;i<4;i++){
        Sum.push(info[i])
      }
      this.Informationdata=Sum
    })
  }
  HomeListData
  GetListHome() {
    this.service.GetHomeList().subscribe(
      (res:any[]) => {
        this.HomeListData = res.filter(x=>x.active ==="Y")
        
      }
    )
  }
}
