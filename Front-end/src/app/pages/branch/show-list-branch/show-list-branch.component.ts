import { Component, OnInit } from '@angular/core';
import { BranchService } from '../../master/branch/branch.service';

@Component({
  selector: 'app-show-list-branch',
  templateUrl: './show-list-branch.component.html',
  styleUrls: ['./show-list-branch.component.css']
})
export class ShowListBranchComponent implements OnInit {
  selectedValue = '';

  gridStyle = {
    width: '25%',
    textAlign: 'center'
  };

  constructor(private service:BranchService) { }
  ShowListBranchData:any;
  

  ngOnInit() {
    this.ShowListBranch();
    document.getElementById("content").classList.remove("p-0")
  }
  ShowListBranch(){
    this.service.GetBranch().subscribe(
      (res : any[])=> {
        this.ShowListBranchData=res.filter(x=>x.active==="Y")    
      }
    )
  }

}
