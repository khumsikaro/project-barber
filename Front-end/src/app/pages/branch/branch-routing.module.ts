import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BranchComponent } from './branch.component';
import { ShowListBranchComponent } from './show-list-branch/show-list-branch.component';
import { GuardService } from '../login/guard.service';


const routes: Routes = [
  {
    path:'Branch',
    component:BranchComponent,
    children:[
      {
        path:'ShowListBranch',
        component:ShowListBranchComponent
      },

    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BranchRoutingModule { }
