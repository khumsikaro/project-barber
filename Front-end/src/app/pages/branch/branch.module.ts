import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BranchRoutingModule } from './branch-routing.module';
import { ShowListBranchComponent } from './show-list-branch/show-list-branch.component';
import { ZorroModule } from 'src/app/lib.ngZorro';
import { BranchComponent } from './branch.component';
import { GuardService } from '../login/guard.service';


@NgModule({
  declarations: [ShowListBranchComponent, BranchComponent],
  imports: [
    CommonModule,
    BranchRoutingModule,
    ZorroModule
  ],
  providers: [
    GuardService
  ]
})
export class BranchModule { }
